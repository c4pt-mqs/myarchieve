const body = document.body;
const modeToggle = document.querySelector("#mode-toggle");
const postsContainer = document.querySelector('.posts-container');
const clickNav = document.getElementById('click-nav');
const breadcrumb = document.getElementById('breadcrumb');
const cardDeck = document.querySelector('.card-deck');
const mobileNav = document.querySelector('.mobile-nav');

// Add system preference detection
function getPreferredTheme() {
    const savedMode = localStorage.getItem("light-mode");
    if (savedMode !== null) {
        return savedMode === "true";
    }
    return window.matchMedia('(prefers-color-scheme: light)').matches;
}

// Update theme toggle initialization
const preferredTheme = getPreferredTheme();
if (preferredTheme) {
    body.classList.add("light-mode");
    updateModeIcon("bx-toggle-left");
} else {
    updateModeIcon("bxs-toggle-right");
}

if (modeToggle) {
    modeToggle.addEventListener("click", function () {
        body.classList.toggle("light-mode");
        localStorage.setItem("light-mode", body.classList.contains("light-mode"));
        const modeIcon = body.classList.contains("light-mode") ? "bx-toggle-left" : "bxs-toggle-right";
        updateModeIcon(modeIcon);
    });
}

function updateModeIcon(icon) {
    modeToggle.innerHTML = `<i class="bx ${icon}"></i>`;
}

function toggleNav() {
    const navButton = document.getElementById('click-nav');
    const sidenav = document.querySelector('#sidenav');
    const overlay = document.querySelector('.nav-overlay');
    const menuIcon = navButton.querySelector('i');

    if (!navButton || !sidenav || !overlay || !menuIcon) {
        console.error('Navigation elements not found');
        return;
    }

    const isExpanded = navButton.getAttribute('aria-expanded') === 'true';
    
    // Toggle aria-expanded state
    navButton.setAttribute('aria-expanded', !isExpanded);
    
    if (!isExpanded) {
        sidenav.classList.add('show');
        overlay.classList.add('show');
        document.body.style.overflow = 'hidden'; // Prevent background scrolling
        menuIcon.className = 'bx bx-x'; // Change to close icon
        
        // Add touch event listeners when menu is open
        sidenav.addEventListener('touchstart', handleNavTouchStart);
        sidenav.addEventListener('touchmove', handleNavTouchMove);
        sidenav.addEventListener('touchend', handleNavTouchEnd);
        overlay.addEventListener('click', closeNav);
    } else {
        closeNav();
    }
}

function closeNav() {
    const navButton = document.getElementById('click-nav');
    const sidenav = document.querySelector('#sidenav');
    const overlay = document.querySelector('.nav-overlay');
    const menuIcon = navButton.querySelector('i');
    
    if (sidenav && overlay) {
        navButton.setAttribute('aria-expanded', 'false');
        sidenav.classList.remove('show');
        overlay.classList.remove('show');
        document.body.style.overflow = ''; // Restore scrolling
        menuIcon.className = 'bx bx-menu'; // Change back to menu icon
        
        // Remove touch event listeners when menu is closed
        sidenav.removeEventListener('touchstart', handleNavTouchStart);
        sidenav.removeEventListener('touchmove', handleNavTouchMove);
        sidenav.removeEventListener('touchend', handleNavTouchEnd);
    }
}

// Navigation touch handling
const navTouchState = {
    startX: 0,
    startY: 0,
    currentX: 0,
    currentY: 0,
    moving: false
};

function handleNavTouchStart(e) {
    navTouchState.startX = e.touches[0].clientX;
    navTouchState.startY = e.touches[0].clientY;
    navTouchState.moving = true;
}

function handleNavTouchMove(e) {
    if (!navTouchState.moving) return;
    
    navTouchState.currentX = e.touches[0].clientX;
    navTouchState.currentY = e.touches[0].clientY;
    
    const deltaX = navTouchState.currentX - navTouchState.startX;
    const deltaY = Math.abs(navTouchState.currentY - navTouchState.startY);
    
    // If horizontal swipe is greater than vertical movement
    if (deltaX < 0 && Math.abs(deltaX) > deltaY) {
        e.preventDefault(); // Prevent scrolling while swiping
    }
}

function handleNavTouchEnd() {
    if (!navTouchState.moving) return;
    
    const deltaX = navTouchState.currentX - navTouchState.startX;
    const deltaY = Math.abs(navTouchState.currentY - navTouchState.startY);
    const swipeThreshold = 50;
    
    // If it's a left swipe (deltaX < 0) and horizontal movement is greater than vertical
    if (deltaX < -swipeThreshold && Math.abs(deltaX) > deltaY) {
        closeNav();
    }
    
    // Reset touch state
    navTouchState.moving = false;
    navTouchState.startX = 0;
    navTouchState.startY = 0;
    navTouchState.currentX = 0;
    navTouchState.currentY = 0;
}

// Initialize navigation
document.addEventListener('DOMContentLoaded', function () {
    // Initialize all required elements
    const searchButton = document.querySelector('.post-search-button');
    const searchModal = document.querySelector('.search-modal');
    const searchModalContent = document.querySelector('.search-modal-content');
    const searchInput = document.querySelector('.post-search-input');
    const searchResults = document.querySelector('.search-results');
    const cancelButton = document.querySelector('.search-cancel-button');
    const navButton = document.getElementById('click-nav');
    const sidenav = document.querySelector('#sidenav');
    const overlay = document.querySelector('.nav-overlay');

    // Track modal states
    let isSearchModalActive = false;
    let isNavModalActive = false;

    // Initialize navigation
    if (navButton && sidenav && overlay) {
        // Create a new nav button with fresh event listeners
        const newNavButton = navButton.cloneNode(true);
        navButton.parentNode.replaceChild(newNavButton, navButton);
        
        // Nav button click handler
        newNavButton.addEventListener('click', (e) => {
            e.preventDefault();
            e.stopPropagation();
            if (isSearchModalActive) {
                closeSearchModal(); // Close search if open
            }
            isNavModalActive = !isNavModalActive;
            toggleNav();
        });

        // Overlay click handler
        overlay.addEventListener('click', (e) => {
            e.preventDefault();
            e.stopPropagation();
            if (isNavModalActive) {
                isNavModalActive = false;
                toggleNav();
            }
        });

        // Click outside nav handler
        document.addEventListener('click', (e) => {
            if (isNavModalActive && sidenav && !sidenav.contains(e.target) && 
                !newNavButton.contains(e.target) && !overlay.contains(e.target)) {
                isNavModalActive = false;
                toggleNav();
            }
        });

        // Touch events for nav
        document.addEventListener('touchstart', (e) => {
            if (isSearchModalActive) return; // Don't handle touch if search is active
            handleTouchStart(e);
        }, { passive: true });

        document.addEventListener('touchmove', (e) => {
            if (isSearchModalActive) return;
            handleTouchMove(e);
        }, { passive: false });

        document.addEventListener('touchend', (e) => {
            if (isSearchModalActive) return;
            handleTouchEnd(e);
        }, { passive: true });
    }

    // Initialize search functionality
    if (searchButton && searchModal && searchInput && cancelButton) {
        // Search button click
        searchButton.addEventListener('click', async (e) => {
            e.preventDefault();
            e.stopPropagation();
            if (isNavModalActive) {
                toggleNav(); // Close nav if open
            }
            try {
                searchButton.classList.add('loading');
                isSearchModalActive = true;
                await toggleSearchModal();
            } catch (error) {
                handleError(error, 'search');
            } finally {
                searchButton.classList.remove('loading');
            }
        });

        // Cancel button click
        cancelButton.addEventListener('click', (e) => {
            e.preventDefault();
            e.stopPropagation();
            isSearchModalActive = false;
            closeSearchModal();
        });

        // Search input
        searchInput.addEventListener('input', debounce((e) => {
            if (!isSearchModalActive) return;
            filterPosts(e.target.value);
            selectedResultIndex = -1;
        }, 150));

        // Search modal click outside
        searchModal.addEventListener('mousedown', (e) => {
            if (e.target === searchModal) {
                e.preventDefault();
                e.stopPropagation();
                isSearchModalActive = false;
                closeSearchModal();
            }
        });

        // Prevent search modal content clicks from bubbling
        searchModalContent.addEventListener('mousedown', (e) => {
            e.stopPropagation();
        });
    }

    // Global keyboard shortcuts
    document.addEventListener('keydown', (e) => {
        // Handle Escape key
        if (e.key === 'Escape') {
            e.preventDefault();
            if (isSearchModalActive) {
                isSearchModalActive = false;
                closeSearchModal();
            } else if (isNavModalActive) {
                isNavModalActive = false;
                toggleNav();
            }
            return;
        }

        // Handle Ctrl/Cmd + K for search
        if ((e.ctrlKey || e.metaKey) && e.key.toLowerCase() === 'k') {
            e.preventDefault();
            e.stopPropagation();
            if (isNavModalActive) {
                isNavModalActive = false;
                toggleNav();
            }
            isSearchModalActive = !isSearchModalActive;
            toggleSearchModal();
        }
    });

    // Handle window resize
    let resizeTimeout;
    window.addEventListener('resize', () => {
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(() => {
            if (window.innerWidth > 1023) {
                if (isNavModalActive) {
                    isNavModalActive = false;
                    sidenav?.classList.remove('show');
                    overlay?.classList.remove('show');
                    document.body.classList.remove('nav-open');
                    newNavButton?.setAttribute('aria-expanded', 'false');
                }
            }
        }, 250);
    });

    // Handle navigation active states
    function updateActiveNavigation() {
        const currentPath = window.location.pathname;
        const navLinks = document.querySelectorAll('.nav-pages a');
        
        navLinks.forEach(link => {
            // Remove any existing aria-current
            link.removeAttribute('aria-current');
            
            // Get the link's path
            const linkPath = link.getAttribute('href');
            
            // Check if the current path matches the link path
            // Handle home page and other pages properly
            if (currentPath === linkPath || 
                (currentPath === '/' && (linkPath === '/' || linkPath === '' || linkPath === '#')) ||
                (currentPath !== '/' && currentPath !== '' && linkPath.includes(currentPath))) {
                link.setAttribute('aria-current', 'page');
            }
        });
    }

    // Update active state on page load
    updateActiveNavigation();

    // Update active state when navigating using history API
    window.addEventListener('popstate', updateActiveNavigation);

    // Handle navigation clicks
    document.querySelectorAll('.nav-pages a').forEach(link => {
        link.addEventListener('click', function() {
            // Remove current from all links
            document.querySelectorAll('.nav-pages a').forEach(l => {
                l.removeAttribute('aria-current');
            });
            // Set current on clicked link
            this.setAttribute('aria-current', 'page');
        });
    });
});

// Create search modal structure
const searchModal = document.createElement('div');
searchModal.className = 'search-modal';

const searchModalContent = document.createElement('div');
searchModalContent.className = 'search-modal-content';

const searchInputContainer = document.createElement('div');
searchInputContainer.className = 'search-input-container';

const searchInputWrapper = document.createElement('div');
searchInputWrapper.className = 'search-input-wrapper';

// Create search icon
const searchIcon = document.createElement('i');
searchIcon.className = 'bx bx-search-alt-2 search-icon';

const searchInput = document.createElement('input');
searchInput.className = 'post-search-input';
searchInput.type = 'text';
searchInput.placeholder = 'Search posts...';

const cancelButton = document.createElement('button');
cancelButton.className = 'search-cancel-button';
cancelButton.innerHTML = '<i class="bx bx-x"></i><span>Cancel</span>';

const searchResults = document.createElement('div');
searchResults.className = 'search-results';

// Assemble modal structure
searchInputWrapper.appendChild(searchIcon);
searchInputWrapper.appendChild(searchInput);
searchInputContainer.appendChild(searchInputWrapper);
searchInputContainer.appendChild(cancelButton);
searchModalContent.appendChild(searchInputContainer);
searchModalContent.appendChild(searchResults);
searchModal.appendChild(searchModalContent);
document.body.appendChild(searchModal);

// Search functionality
const searchButton = document.querySelector('.post-search-button');
let searchIndex = null;
let searchIndexPromise = null;

// HTML entity decoder
const decodeHTML = (html) => {
    if (!html) return '';
    const doc = new DOMParser().parseFromString(html, 'text/html');
    return doc.documentElement.textContent;
};

// Add debounce utility at the top
const debounce = (func, wait) => {
    let timeout;
    return function executedFunction(...args) {
        const later = () => {
            clearTimeout(timeout);
            func(...args);
        };
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
    };
};

// Optimize loadSearchIndex function
const loadSearchIndex = async () => {
    try {
        // Return cached index if available
        if (searchIndex) {
            return searchIndex;
        }

        // Return existing promise if loading
        if (searchIndexPromise) {
            return searchIndexPromise;
        }

        const indexScript = document.getElementById('search-index');
        if (!indexScript) {
            console.error('Search index script not found');
            return null;
        }

        // Create promise and cache it
        searchIndexPromise = new Promise((resolve) => {
            try {
                const rawIndex = JSON.parse(indexScript.textContent);
                // Process index once and cache it
                searchIndex = {
                    posts: rawIndex.posts.map(post => ({
                        ...post,
                        // Pre-process searchable content
                        searchContent: `${post.title} ${post.content} ${post.summary} ${post.author} ${post.category} ${post.subcategory}`.toLowerCase(),
                        title: decodeHTML(post.title),
                        content: decodeHTML(post.content),
                        summary: decodeHTML(post.summary),
                        author: decodeHTML(post.author)
                    }))
                };
                resolve(searchIndex);
            } catch (error) {
                console.error('Error processing search index:', error);
                resolve(null);
            }
        });

        return searchIndexPromise;
    } catch (error) {
        console.error('Error loading search index:', error);
        return null;
    }
};

// Add selected result tracking
let selectedResultIndex = -1;

const updateSelectedResult = (newIndex) => {
    const results = searchResults.querySelectorAll('.post-result-item');
    const totalResults = results.length;
    
    if (totalResults === 0) return;

    // Remove previous selection
    if (selectedResultIndex >= 0) {
        results[selectedResultIndex]?.classList.remove('selected');
    }

    // Update index with wrapping
    selectedResultIndex = (newIndex + totalResults) % totalResults;

    // Add selection to new item
    const selectedItem = results[selectedResultIndex];
    selectedItem.classList.add('selected');
    selectedItem.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
};

const resetSearch = () => {
    searchInput.value = '';
    searchResults.innerHTML = '';
    selectedResultIndex = -1;
};

const toggleSearchModal = async () => {
    try {
        console.log('Toggling search modal...');
        if (!searchModal) {
            console.error('Search modal not found!');
            return;
        }
        
        const wasActive = searchModal.classList.contains('active');
        console.log('Was active:', wasActive);
        
        if (!wasActive) {
            searchModal.classList.add('active');
            searchInput.value = '';
            searchResults.innerHTML = '';
            selectedResultIndex = -1;
            if (!searchInput) {
                console.error('Search input not found!');
                return;
            }
            searchInput.focus();
            document.body.style.overflow = 'hidden';
        } else {
            closeSearchModal();
        }
        
        console.log('Modal active state after toggle:', searchModal.classList.contains('active'));
    } catch (error) {
        handleError(error, 'toggleSearchModal');
    }
};

function closeSearchModal() {
    searchModal.classList.remove('active');
    searchInput.value = '';
    searchResults.innerHTML = '';
    selectedResultIndex = -1;
    document.body.style.overflow = '';
}

// Optimize filterPosts function
const filterPosts = async (searchValue) => {
    const normalizedSearch = searchValue.trim().toLowerCase();
    searchResults.innerHTML = '';

    const index = await loadSearchIndex();
    if (!index) return;

    // Early return for empty search
    if (!normalizedSearch) {
        searchResults.innerHTML = '';
        return;
    }

    // Use more efficient filtering
    const scoredPosts = index.posts
        .reduce((acc, post) => {
            // Use pre-processed searchContent
            const score = getSearchScore(post, normalizedSearch);
            if (score > 0) {
                acc.push({ post, score });
            }
            return acc;
        }, [])
        .sort((a, b) => b.score - a.score);

    // Use DocumentFragment for better performance
    const fragment = document.createDocumentFragment();
    
    if (scoredPosts.length > 0) {
        scoredPosts.forEach(({ post }) => {
            const resultItem = createResultItem(post, normalizedSearch);
            fragment.appendChild(resultItem);
        });
    } else {
        const noResults = document.createElement('div');
        noResults.className = 'no-results-message';
        noResults.textContent = 'No matching posts found';
        fragment.appendChild(noResults);
    }

    searchResults.appendChild(fragment);
};

// Separate scoring logic
const searchScoring = {
    title: 100,
    permalink: 90,
    summary: 75,
    content: 50,
    category: 60,
    subcategory: 55
};

function getSearchScore(post, searchTerm) {
    return Object.entries(searchScoring).reduce((score, [field, weight]) => {
        if (post[field]?.toLowerCase().includes(searchTerm)) {
            score += weight;
        }
        return score;
    }, 0);
}

// Separate result item creation
const createResultItem = (post, searchTerm) => {
    const resultItem = document.createElement('div');
    resultItem.className = 'post-result-item';
    
    const title = highlightText(post.title || '', searchTerm);
    const summary = post.summary ? highlightText(post.summary, searchTerm) : '';
    
    resultItem.innerHTML = `
        <a href="${post.permalink}" class="post-result-link">
            <div class="post-result-header">
                <h3 class="post-result-title">${title}</h3>
                <span class="post-result-type ${post.type}">${post.type}</span>
            </div>
            ${summary ? `<p class="post-result-summary">${summary.substring(0, 150)}${summary.length > 150 ? '...' : ''}</p>` : ''}
            <div class="post-result-meta">
                ${post.date ? `
                    <span class="post-result-date">
                        <i class="bx bx-calendar"></i>
                        ${post.date}
                    </span>
                ` : ''}
                ${post.author ? `
                    <span class="post-result-author">
                        <i class="bx bx-user"></i>
                        ${post.author}
                    </span>
                ` : ''}
                ${post.type === 'note' && post.category ? `
                    <span class="post-result-category">
                        <i class="bx bx-folder"></i>
                        ${post.category}
                    </span>
                ` : ''}
                ${post.type === 'note' && post.subcategory ? `
                    <span class="post-result-subcategory">
                        <i class='bx bxs-folder-open'></i>
                        ${post.subcategory}
                    </span>
                ` : ''}
            </div>
        </a>
    `;

    return resultItem;
};

// Optimize highlight function
const highlightText = (text = '', searchTerm) => {
    if (!searchTerm) return text;
    const regex = new RegExp(`(${searchTerm})`, 'gi');
    return text.replace(regex, '<mark>$1</mark>');
};

// Add touch gesture handling
let touchStartX = 0;
let touchMoveX = 0;
let isDragging = false;
const SWIPE_THRESHOLD = 50; // Minimum distance for swipe
const DRAG_THRESHOLD = 10; // Minimum distance to start dragging

function handleTouchStart(e) {
    // Only handle touch events near the edge when menu is closed
    // or anywhere on the menu when it's open
    const sidenav = document.querySelector('#sidenav');
    const isExpanded = document.getElementById('click-nav')?.getAttribute('aria-expanded') === 'true';
    
    if (!isExpanded && e.touches[0].clientX > 40) {
        return;
    }
    
    if (!sidenav || (!isExpanded && !sidenav.contains(e.target) && e.touches[0].clientX > 40)) {
        return;
    }

    const touch = e.touches[0];
    touchStartX = touch.clientX;
    touchMoveX = touch.clientX;
    isDragging = false;

    if (sidenav) {
        sidenav.style.transition = 'none';
    }
}

function handleTouchMove(e) {
    if (!isDragging && !touchStartX) return; // Exit if touch didn't start properly
    if (e.touches.length > 1) return; // Ignore multi-touch

    const touch = e.touches[0];
    const currentX = touch.clientX;
    const deltaX = currentX - touchStartX;
    
    // Start dragging only after exceeding threshold
    if (!isDragging && Math.abs(deltaX) > DRAG_THRESHOLD) {
        isDragging = true;
        const sidenav = document.querySelector('#sidenav');
        if (sidenav) {
            sidenav.style.transition = 'none';
            sidenav.classList.add('dragging');
        }
    }
    
    if (isDragging) {
        e.preventDefault();
        touchMoveX = currentX;
        
        const sidenav = document.querySelector('#sidenav');
        const navButton = document.getElementById('click-nav');
        const isExpanded = navButton?.getAttribute('aria-expanded') === 'true';
        const overlay = document.querySelector('.nav-overlay');
        
        if (isExpanded) {
            // When menu is open, only allow dragging left (to close)
            if (deltaX < 0) {
                const translateX = Math.max(deltaX, -280);
                const progress = 1 - Math.abs(translateX) / 280;
                
                if (sidenav) {
                    sidenav.style.transform = `translateX(${translateX}px)`;
                }
                
                // Update overlay opacity
                if (overlay) {
                    overlay.style.transition = 'none';
                    overlay.style.opacity = progress.toFixed(2);
                    overlay.style.visibility = progress > 0 ? 'visible' : 'hidden';
                }
            }
        } else {
            // When menu is closed, only allow dragging right (to open)
            // and only if touch started near the left edge (40px)
            if (touchStartX < 40 && deltaX > 0) {
                const translateX = Math.min(deltaX - 280, 0);
                const progress = (deltaX) / 280;
                
                if (sidenav) {
                    sidenav.style.transform = `translateX(${translateX}px)`;
                }
                
                // Show and update overlay opacity
                if (overlay) {
                    overlay.style.transition = 'none';
                    overlay.style.visibility = 'visible';
                    overlay.style.opacity = progress.toFixed(2);
                }
            }
        }
    }
}

function handleTouchEnd(e) {
    if (!isDragging) return;
    
    const deltaX = touchMoveX - touchStartX;
    const sidenav = document.querySelector('#sidenav');
    const navButton = document.getElementById('click-nav');
    const isExpanded = navButton?.getAttribute('aria-expanded') === 'true';
    const overlay = document.querySelector('.nav-overlay');
    
    // Reset styles
    if (sidenav) {
        sidenav.classList.remove('dragging');
        sidenav.style.transform = '';
        sidenav.style.transition = '';
    }
    
    // Reset overlay
    if (overlay) {
        overlay.style.transition = '';
        overlay.style.opacity = '';
        
        if (!isExpanded) {
            setTimeout(() => {
                if (!navButton?.getAttribute('aria-expanded') === 'true') {
                    overlay.style.visibility = 'hidden';
                }
            }, 300); // Match the transition duration
        }
    }
    
    if (isExpanded) {
        // If dragged left far enough, close the menu
        if (deltaX < -SWIPE_THRESHOLD) {
            toggleNav();
        }
    } else {
        // If dragged right far enough and started from edge, open the menu
        if (touchStartX < 40 && deltaX > SWIPE_THRESHOLD) {
            toggleNav();
        }
    }
    
    isDragging = false;
    touchStartX = 0;
    touchMoveX = 0;
}

// Add keyboard shortcuts for search results navigation
searchInput.addEventListener('keydown', (e) => {
    if (e.key === 'ArrowDown') {
        e.preventDefault();
        updateSelectedResult(selectedResultIndex + 1);
    } else if (e.key === 'ArrowUp') {
        e.preventDefault();
        updateSelectedResult(selectedResultIndex - 1);
    } else if (e.key === 'Enter' && selectedResultIndex >= 0) {
        e.preventDefault();
        const selectedLink = searchResults.querySelectorAll('.post-result-item')[selectedResultIndex]
            ?.querySelector('a');
        if (selectedLink) {
            selectedLink.click();
        }
    }
});

// Add better error handling for search and navigation
const handleError = (error, context) => {
    console.error(`Error in ${context}:`, error);
    // Show user-friendly error message
    const errorMessage = document.createElement('div');
    errorMessage.className = 'error-message';
    errorMessage.textContent = `Something went wrong. Please try again later.`;
    document.body.appendChild(errorMessage);
    setTimeout(() => errorMessage.remove(), 3000);
};

// Use in try-catch blocks
try {
    // existing code
} catch (error) {
    handleError(error, 'search');
}

// Add performance monitoring
const reportWebVitals = () => {
    const vitals = {};
    
    new PerformanceObserver((entries) => {
        entries.getEntries().forEach((entry) => {
            vitals[entry.name] = entry.value;
        });
    }).observe({ entryTypes: ['largest-contentful-paint'] });
    
    window.addEventListener('load', () => {
        setTimeout(() => {
            console.log('Performance metrics:', vitals);
        }, 0);
    });
};

reportWebVitals();