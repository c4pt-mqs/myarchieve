const noteWidgetSearchInput = document.querySelector('.note-widget-search-input');
const noteContainers = Array.from(document.querySelectorAll('.notes-container'));
const navLinks = Array.from(document.querySelectorAll('.nav-link'));
let isNavOpen;

// Check if it's the first visit
if (localStorage.getItem('isNavOpen') === null) {
    // Default to open on desktop, closed on mobile
    isNavOpen = window.innerWidth > 1024;
} else {
    isNavOpen = localStorage.getItem('isNavOpen') === 'true';
}

document.addEventListener('DOMContentLoaded', function () {
    const sidenavMain = document.querySelector('.sidenav-main');
    const menuButton = document.querySelector('.menu-bar');
    const modeToggle = document.querySelector('#mode-toggle');

    // Initialize navigation state
    updateNavState();

    function updateNavState() {
        if (isNavOpen) {
            sidenavMain.style.display = 'flex';
            menuButton.querySelector('i').classList.remove('bx-menu-alt-left');
            menuButton.querySelector('i').classList.add('bx-menu');
        } else {
            sidenavMain.style.display = 'none';
            menuButton.querySelector('i').classList.remove('bx-menu');
            menuButton.querySelector('i').classList.add('bx-menu-alt-left');
        }
        localStorage.setItem('isNavOpen', isNavOpen);
    }

    function toggleNav() {
        isNavOpen = !isNavOpen;
        updateNavState();
    }

    menuButton.addEventListener('click', toggleNav);
});

function showNoteContent(title, author, date, content) {
    const welcomeContent = document.querySelector('.welcome-content');
    if (welcomeContent) {
        welcomeContent.style.display = 'none';
    }

    document.querySelector('.post-title').innerText = title;
    document.querySelector('.post-meta').innerHTML = `
        <li>
            <i class='bx bx-user'></i>
            <a class="post-meta-author" href="/about#${author.toLowerCase()}">${author}</a>
        </li>
        <li>
            <i class='bx bx-calendar'></i>
            <time class="post-meta-date" aria-label="Created date">${date}</time>
        </li>
    `;
    document.querySelector('.main-content').innerHTML = content;

    // Close sidenav on mobile when a note is selected
    const isMobile = window.innerWidth <= 1024;
    if (isMobile && isNavOpen) {
        const sidenavMain = document.querySelector('.sidenav-main');
        const menuButton = document.querySelector('.menu-bar');
        
        isNavOpen = false;
        sidenavMain.style.display = 'none';
        menuButton.querySelector('i').classList.remove('bx-menu-alt-left');
        menuButton.querySelector('i').classList.add('bx-menu-alt-right');
        localStorage.setItem('isNavOpen', isNavOpen);
    }
}

var postsContents = document.getElementsByClassName('posts-content');

// Use modern clipboard API instead of execCommand
const copyToClipboard = async (text) => {
    try {
        await navigator.clipboard.writeText(text);
        return true;
    } catch (err) {
        console.error('Failed to copy:', err);
        return false;
    }
};

// Simplified copy button handler
document.querySelector('.posts-content').addEventListener('mouseover', () => {
    document.querySelectorAll('.highlight > pre').forEach(pre => {
        if (pre.querySelector('.copy-button')) return;
        
        const btn = document.createElement('button');
        btn.className = 'copy-button';
        btn.textContent = 'Copy';
        
        btn.addEventListener('click', async () => {
            const text = pre.textContent.replace('Copy', '').trim();
            const success = await copyToClipboard(text);
            
            btn.textContent = success ? 'Copied!' : 'Failed!';
            btn.disabled = true;
            
            setTimeout(() => {
                btn.textContent = 'Copy';
                btn.disabled = false;
            }, 1000);
        });
        
        pre.appendChild(btn);
    });
});

function toggleNotes(subcategoryTitle) {
    const notesContainers = document.querySelectorAll(`.notes-container[data-subcategory="${subcategoryTitle}"]`);
    const navLink = document.querySelector(`.nav-link[onclick*="${subcategoryTitle}"]`);
    const chevronIcon = navLink.querySelector('i.bx-chevron-right');

    // Toggle the chevron icon rotation
    chevronIcon.classList.toggle('rotated');

    // Get the new state based on the first container
    const firstContainer = notesContainers[0];
    const wasCollapsed = firstContainer.classList.contains('collapsed');

    // Toggle each note container
    notesContainers.forEach(container => {
        container.classList.toggle('collapsed');
        // Explicitly set display property
        container.style.display = wasCollapsed ? 'flex' : 'none';
    });

    // Store the state
    localStorage.setItem(`note-state-${subcategoryTitle}`, JSON.stringify({ 
        isOpen: wasCollapsed // if it was collapsed, it's now open
    }));
}

function toggleSubcategories(categoryTitle) {
    const sublinksContainer = document.getElementById(`${categoryTitle}-sub-links`);
    const isVisible = window.getComputedStyle(sublinksContainer).display !== 'none';
    sublinksContainer.style.display = isVisible ? 'none' : 'block';
    
    // Store the state
    localStorage.setItem(`category-state-${categoryTitle}`, JSON.stringify({
        isOpen: !isVisible
    }));
}

// Initialize states on page load
document.addEventListener('DOMContentLoaded', function () {
    // First restore category states
    document.querySelectorAll('[id$="-sub-links"]').forEach(sublinks => {
        const categoryTitle = sublinks.id.replace('-sub-links', '');
        const savedState = localStorage.getItem(`category-state-${categoryTitle}`);
        try {
            if (savedState) {
                const state = JSON.parse(savedState);
                sublinks.style.display = state.isOpen ? 'block' : 'none';
            } else {
                // Default to closed state
                sublinks.style.display = 'none';
            }
        } catch (e) {
            console.error('Error restoring category state:', e);
            sublinks.style.display = 'none';
        }
    });

    // Then restore note states
    document.querySelectorAll('.nav-link[onclick*="toggleNotes"]').forEach(link => {
        try {
            const onclickAttr = link.getAttribute('onclick') || '';
            const matches = onclickAttr.match(/toggleNotes\(['"]([^'"]+)['"]\)/);
            if (!matches) return;
            
            const subcategoryTitle = matches[1];
            const savedState = localStorage.getItem(`note-state-${subcategoryTitle}`);
            
            const containers = document.querySelectorAll(`.notes-container[data-subcategory="${subcategoryTitle}"]`);
            const chevronIcon = link.querySelector('i.bx-chevron-right');
            
            // Default to collapsed state if no saved state
            let isOpen = false;
            
            if (savedState) {
                try {
                    const state = JSON.parse(savedState);
                    isOpen = state.isOpen;
                } catch (e) {
                    console.error('Error parsing saved state:', e);
                }
            }

            containers.forEach(container => {
                if (!isOpen) {
                    container.classList.add('collapsed');
                    container.style.display = 'none'; // Ensure it's hidden
                } else {
                    container.classList.remove('collapsed');
                    container.style.display = 'flex'; // Ensure it's visible
                }
            });

            if (chevronIcon) {
                if (isOpen) {
                    chevronIcon.classList.add('rotated');
                } else {
                    chevronIcon.classList.remove('rotated');
                }
            }
        } catch (e) {
            console.error('Error restoring note state:', e);
        }
    });
});

function toggleElementsByTitle(title, className) {
    const elements = $(`.${className}`);

    elements.each(function () {
        var elementTitle = $(this).attr('id').split("-")[0];
        $(this).toggle(elementTitle === title);
    });
}

function showSubcategories(sublinkTitle) {
    toggleElementsByTitle(sublinkTitle, "subcategories");
}

function setActiveNoteByHash() {
    const hash = window.location.hash;
    if (!hash) return;
    
    const targetNoteLink = document.querySelector(`a[href="${hash}"]`);
    if (!targetNoteLink) return;
    
    targetNoteLink.click();

    const subcategoryLink = targetNoteLink.closest(".subcategories");
    if (subcategoryLink) {
        const subcategoryTitle = subcategoryLink.id.split("-")[0];
        showSubcategories(subcategoryTitle);
    }
}

window.addEventListener("DOMContentLoaded", setActiveNoteByHash);
window.addEventListener("hashchange", setActiveNoteByHash);

const body = document.body;
const modeToggle = document.querySelector("#mode-toggle");
const postsContainer = document.querySelector('.posts-container');

const savedMode = localStorage.getItem("light-mode");
if (savedMode === "true") {
    body.classList.add("light-mode");
    updateModeIcon("bx-toggle-left");
} else {
    updateModeIcon("bxs-toggle-right");
}

if (modeToggle) {
    modeToggle.addEventListener("click", function () {
        body.classList.toggle("light-mode");
        localStorage.setItem("light-mode", body.classList.contains("light-mode"));
        const modeIcon = body.classList.contains("light-mode") ? "bx-toggle-left" : "bxs-toggle-right";
        updateModeIcon(modeIcon);
    });
}

function updateModeIcon(icon) {
    modeToggle.innerHTML = `<i class="bx ${icon}"></i>`;
}

// Add this new function to handle initial state
document.addEventListener('DOMContentLoaded', function() {
    const hash = window.location.hash;
    const welcomeContent = document.querySelector('.welcome-content');
    
    if (!hash && welcomeContent) {
        welcomeContent.style.display = 'block';
    }
});

// Initialize notes container click handlers
document.addEventListener('DOMContentLoaded', function() {
    document.querySelectorAll('.notes-container').forEach(container => {
        container.addEventListener('click', function(event) {
            // Prevent click if user clicked on share buttons or their children
            if (event.target.closest('.share-buttons')) {
                return;
            }
            
            const link = container.querySelector('a.note-widget-title');
            if (link) {
                event.preventDefault();
                // Update URL with the hash
                window.location.hash = link.getAttribute('href');
                // Trigger the click on the link
                link.click();
            }
        });
    });
});

// Search functionality
const searchButton = document.querySelector('.note-widget-search');

// Create modal structure
const searchModal = document.createElement('div');
searchModal.className = 'search-modal';

const searchModalContent = document.createElement('div');
searchModalContent.className = 'search-modal-content';

const searchInputContainer = document.createElement('div');
searchInputContainer.className = 'search-input-container';

const searchInputWrapper = document.createElement('div');
searchInputWrapper.className = 'search-input-wrapper';

const searchInput = document.createElement('input');
searchInput.className = 'note-widget-search-input';
searchInput.type = 'text';
searchInput.placeholder = 'Search notes...';

const cancelButton = document.createElement('button');
cancelButton.className = 'search-cancel-button';
cancelButton.textContent = 'Cancel';

const searchResults = document.createElement('div');
searchResults.className = 'search-results';

// Assemble modal structure
searchInputWrapper.appendChild(searchInput);
searchInputContainer.appendChild(searchInputWrapper);
searchInputContainer.appendChild(cancelButton);
searchModalContent.appendChild(searchInputContainer);
searchModalContent.appendChild(searchResults);
searchModal.appendChild(searchModalContent);
document.body.appendChild(searchModal);

const toggleSearchModal = () => {
    searchModal.classList.toggle('active');
    if (searchModal.classList.contains('active')) {
        searchInput.value = '';
        searchInput.focus();
        filterNotes('');
        document.body.style.overflow = 'hidden'; // Prevent background scrolling
    } else {
        document.body.style.overflow = ''; // Restore scrolling
    }
};

const filterNotes = (searchValue) => {
    const normalizedSearch = searchValue.trim().toLowerCase();
    let hasVisibleNotes = false;
    searchResults.innerHTML = '';

    // Process each note container
    document.querySelectorAll('.notes-container').forEach(noteContainer => {
        const noteTitle = noteContainer.querySelector('.note-widget-title').textContent.trim();
        const noteLink = noteContainer.querySelector('.note-widget-title').getAttribute('href');
        
        if (noteTitle.toLowerCase().includes(normalizedSearch)) {
            hasVisibleNotes = true;
            const resultItem = document.createElement('div');
            resultItem.className = 'notes-container';
            resultItem.innerHTML = `
                <a href="${noteLink}" class="note-widget-title">
                    <i class='bx bx-file'></i>
                    <span>${noteTitle}</span>
                </a>
            `;
            
            // Add click handler to result item
            resultItem.addEventListener('click', (e) => {
                e.preventDefault();
                window.location.hash = noteLink;
                const originalLink = document.querySelector(`a[href="${noteLink}"]`);
                if (originalLink) {
                    originalLink.click();
                }
                toggleSearchModal();
            });
            
            searchResults.appendChild(resultItem);
        }
    });

    // Show no results message if needed
    if (!hasVisibleNotes) {
        const noResults = document.createElement('div');
        noResults.className = 'no-results-message';
        noResults.textContent = 'No matching notes found';
        searchResults.appendChild(noResults);
    }
};

// Event listeners for search
searchButton.addEventListener('click', toggleSearchModal);
cancelButton.addEventListener('click', toggleSearchModal);
searchInput.addEventListener('input', (e) => filterNotes(e.target.value));

// Close modal on escape key or clicking outside
document.addEventListener('keydown', (e) => {
    if (e.key === 'Escape' && searchModal.classList.contains('active')) {
        toggleSearchModal();
    }
});

searchModal.addEventListener('click', (e) => {
    if (e.target === searchModal) {
        toggleSearchModal();
    }
});

// Add line numbers and language detection to code blocks
document.querySelectorAll('pre code').forEach((block) => {
    // Add line numbers
    const lines = block.textContent.split('\n').length;
    const lineNumbers = Array(lines).fill().map((_, i) => `<span class="line-number">${i + 1}</span>`).join('');
    block.innerHTML = `<span class="line-numbers">${lineNumbers}</span>${block.innerHTML}`;
    
    // Add language badge
    const language = block.className.split('-')[1];
    if (language) {
        const badge = document.createElement('div');
        badge.className = 'code-language';
        badge.textContent = language;
        block.parentNode.insertBefore(badge, block);
    }
});

