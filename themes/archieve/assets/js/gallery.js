document.addEventListener('DOMContentLoaded', function() {
  const galleries = document.querySelectorAll('.gallery-grid');
  if (!galleries.length) return;

  // Create modal structure if it doesn't exist
  let modal = document.querySelector('.gallery-modal');
  if (!modal) {
    modal = document.createElement('div');
    modal.className = 'gallery-modal';
    modal.innerHTML = `
      <div class="gallery-modal-content">
        <img class="gallery-modal-image" src="" alt="" loading="lazy">
        <div class="gallery-caption"></div>
        <button class="gallery-close" aria-label="Close">&times;</button>
        <button class="gallery-prev" aria-label="Previous">&#10094;</button>
        <button class="gallery-next" aria-label="Next">&#10095;</button>
        <div class="gallery-progress">
          <div class="gallery-progress-bar"></div>
        </div>
      </div>
    `;
    document.body.appendChild(modal);
  }

  // Cache DOM elements
  const modalImg = modal.querySelector('.gallery-modal-image');
  const modalCaption = modal.querySelector('.gallery-caption');
  const progressBar = modal.querySelector('.gallery-progress-bar');
  
  // State management
  const state = {
    currentGallery: null,
    currentIndex: 0,
    images: [],
    imageCache: new Map(),
    isAnimating: false,
    isDragging: false,
    startX: 0,
    currentX: 0,
    dragThreshold: 50
  };

  // Preload images
  const preloadImage = (src) => {
    if (state.imageCache.has(src)) return Promise.resolve();
    
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.onload = () => {
        state.imageCache.set(src, true);
        resolve();
      };
      img.onerror = reject;
      img.src = src;
    });
  };

  const preloadAdjacentImages = async () => {
    const preloadPromises = [];
    [-2, -1, 1, 2].forEach(offset => {
      const index = state.currentIndex + offset;
      if (index >= 0 && index < state.images.length) {
        const src = state.images[index].src || state.images[index].dataset.src;
        preloadPromises.push(preloadImage(src));
      }
    });
    await Promise.all(preloadPromises);
  };

  // Update progress bar
  const updateProgress = () => {
    if (progressBar && state.images.length > 0) {
      progressBar.style.width = `${((state.currentIndex + 1) / state.images.length) * 100}%`;
    }
  };

  // Modal management
  const openModal = async (gallery, index) => {
    if (state.isAnimating) return;
    state.isAnimating = true;

    state.currentGallery = gallery;
    state.images = Array.from(gallery.querySelectorAll('.gallery-item img'));
    state.currentIndex = index;

    const currentImage = state.images[state.currentIndex];
    const src = currentImage.src || currentImage.dataset.src;
    const caption = currentImage.getAttribute('alt') || '';

    modal.style.display = 'block';
    modalImg.classList.add('loading');
    
    try {
      await preloadImage(src);
      modalImg.src = src;
      modalCaption.textContent = caption;
      modalCaption.style.display = caption ? 'block' : 'none';

      requestAnimationFrame(() => {
        modal.classList.add('active');
        modalImg.classList.remove('loading');
        document.body.style.overflow = 'hidden';
        updateProgress();
        preloadAdjacentImages();
        state.isAnimating = false;
      });
    } catch (error) {
      console.error('Failed to load image:', error);
      closeModal();
    }
  };

  const closeModal = () => {
    if (state.isAnimating) return;
    modal.classList.remove('active');
    document.body.style.overflow = '';
    
    setTimeout(() => {
      modal.style.display = 'none';
      modalImg.src = '';
      modalImg.style.transform = '';
      state.currentGallery = null;
      state.images = [];
      state.currentIndex = 0;
      state.isDragging = false;
    }, 150);
  };

  // Navigation with loading indicator
  const navigate = async (direction) => {
    if (state.isAnimating) return;
    state.isAnimating = true;

    const nextIndex = direction === 'next'
      ? (state.currentIndex + 1) % state.images.length
      : (state.currentIndex - 1 + state.images.length) % state.images.length;

    const nextImage = state.images[nextIndex];
    const src = nextImage.src || nextImage.dataset.src;
    const caption = nextImage.getAttribute('alt') || '';

    modalImg.classList.add('loading');
    
    try {
      await preloadImage(src);
      state.currentIndex = nextIndex;
      modalImg.src = src;
      modalCaption.textContent = caption;
      modalCaption.style.display = caption ? 'block' : 'none';
      updateProgress();
      
      requestAnimationFrame(() => {
        modalImg.classList.remove('loading');
        preloadAdjacentImages();
        state.isAnimating = false;
      });
    } catch (error) {
      console.error('Failed to load image:', error);
      modalImg.classList.remove('loading');
      state.isAnimating = false;
    }
  };

  // Mouse drag handling
  const handleDragStart = (e) => {
    if (e.button !== 0) return; // Only handle left click
    state.isDragging = true;
    state.startX = e.clientX;
    modalImg.style.transition = 'none';
    modalImg.style.cursor = 'grabbing';
  };

  const handleDragMove = (e) => {
    if (!state.isDragging) return;
    state.currentX = e.clientX;
    const deltaX = state.currentX - state.startX;
    modalImg.style.transform = `translateX(${deltaX}px)`;
  };

  const handleDragEnd = () => {
    if (!state.isDragging) return;
    
    const deltaX = state.currentX - state.startX;
    modalImg.style.transition = 'transform 0.2s ease';
    modalImg.style.transform = '';
    modalImg.style.cursor = 'grab';
    
    if (Math.abs(deltaX) > state.dragThreshold) {
      navigate(deltaX < 0 ? 'next' : 'prev');
    }
    
    state.isDragging = false;
  };

  // Event listeners
  modal.querySelector('.gallery-close').addEventListener('click', closeModal);
  modal.querySelector('.gallery-prev').addEventListener('click', () => navigate('prev'));
  modal.querySelector('.gallery-next').addEventListener('click', () => navigate('next'));
  modal.addEventListener('click', (e) => {
    if (e.target === modal) closeModal();
  });

  // Mouse drag events
  modalImg.addEventListener('mousedown', handleDragStart);
  window.addEventListener('mousemove', handleDragMove);
  window.addEventListener('mouseup', handleDragEnd);

  // Keyboard navigation
  document.addEventListener('keydown', (e) => {
    if (!state.currentGallery) return;
    
    switch(e.key) {
      case 'Escape': closeModal(); break;
      case 'ArrowLeft': navigate('prev'); break;
      case 'ArrowRight': navigate('next'); break;
    }
  });

  // Touch handling
  let touchStartX = 0;
  let touchStartY = 0;
  let touchStartTime = 0;
  const DOUBLE_TAP_DELAY = 300;
  let lastTapTime = 0;

  modalImg.addEventListener('touchstart', (e) => {
    touchStartX = e.touches[0].clientX;
    touchStartY = e.touches[0].clientY;
    touchStartTime = Date.now();
    modalImg.style.transition = 'none';

    // Handle double tap to close
    const currentTime = Date.now();
    const tapLength = currentTime - lastTapTime;
    if (tapLength < DOUBLE_TAP_DELAY && tapLength > 0) {
      closeModal();
    }
    lastTapTime = currentTime;
  }, { passive: true });

  modalImg.addEventListener('touchmove', (e) => {
    if (!touchStartX || !touchStartY) return;

    const deltaX = touchStartX - e.touches[0].clientX;
    const deltaY = touchStartY - e.touches[0].clientY;

    // If vertical swipe is greater than horizontal, allow scrolling
    if (Math.abs(deltaY) > Math.abs(deltaX)) {
      return;
    }

    // For horizontal swipe, prevent scrolling
    e.preventDefault();
    modalImg.style.transform = `translateX(${-deltaX}px)`;
  }, { passive: false });

  modalImg.addEventListener('touchend', (e) => {
    const deltaX = touchStartX - e.changedTouches[0].clientX;
    const deltaY = touchStartY - e.changedTouches[0].clientY;
    const touchDuration = Date.now() - touchStartTime;

    modalImg.style.transition = 'transform 0.2s ease';
    modalImg.style.transform = '';

    // Handle swipe up to close
    if (deltaY > 100 && Math.abs(deltaY) > Math.abs(deltaX)) {
      closeModal();
      return;
    }

    // Handle horizontal swipe for navigation
    if (Math.abs(deltaX) > 50 && Math.abs(deltaX) > Math.abs(deltaY)) {
      navigate(deltaX > 0 ? 'next' : 'prev');
    }

    // Handle quick tap for showing/hiding controls
    if (touchDuration < 200 && Math.abs(deltaX) < 5 && Math.abs(deltaY) < 5) {
      modal.querySelector('.gallery-close').style.opacity = 
        modal.querySelector('.gallery-close').style.opacity === '0' ? '1' : '0';
    }

    touchStartX = 0;
    touchStartY = 0;
  }, { passive: true });

  // Trackpad navigation
  let lastWheelTime = 0;
  const WHEEL_TIMEOUT = 50;

  modalImg.addEventListener('wheel', (e) => {
    if (e.ctrlKey) {
      e.preventDefault();
      const scale = Math.min(Math.max(1, 1 + (e.deltaY > 0 ? -0.1 : 0.1)), 3);
      modalImg.style.transform = `scale(${scale})`;
      return;
    }

    if (Math.abs(e.deltaX) > Math.abs(e.deltaY)) {
      e.preventDefault();
      
      const now = Date.now();
      if (now - lastWheelTime < WHEEL_TIMEOUT) return;
      lastWheelTime = now;

      if (Math.abs(e.deltaX) > 5) {
        navigate(e.deltaX > 0 ? 'next' : 'prev');
      }
    }
  }, { passive: false });

  // Initialize galleries
  galleries.forEach(gallery => {
    gallery.style.setProperty('--spacing', `${gallery.dataset.spacing || '10'}px`);
    
    gallery.querySelectorAll('.gallery-item').forEach((item, index) => {
      item.addEventListener('click', () => openModal(gallery, index));
    });
  });
}); 