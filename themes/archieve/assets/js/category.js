document.addEventListener('DOMContentLoaded', function() {
    const categoryItems = document.querySelectorAll('.category-item');
    let activeItem = null;

    // Handle direct category URLs and redirect to hash-based URL
    function handleDirectCategoryURL() {
        const path = window.location.pathname;
        if (path.match(/\/(categories|tags)\/[^/]+\/?$/)) {
            const termType = path.split('/')[1]; // 'categories' or 'tags'
            const termName = path.split('/')[2]; // the category or tag name
            const cleanTermName = termName.replace(/\/$/, ''); // remove trailing slash if present
            
            // Redirect to the hash-based URL
            window.location.href = `/${termType}/#${cleanTermName}`;
            return true;
        }
        return false;
    }

    // Function to handle tag/category navigation
    function handleTermNavigation(termId, shouldPushState = true) {
        const targetHeader = document.getElementById(termId + '-header');
        
        if (targetHeader) {
            const isClosingCurrentItem = activeItem === targetHeader && targetHeader.classList.contains('active');

            // First close all items
            categoryItems.forEach(function(item) {
                const list = item.nextElementSibling;
                const chevron = item.querySelector('.toggle-icon');
                item.classList.remove('active');
                chevron.style.transform = 'rotate(0deg)';
                if (list) {
                    list.classList.remove('visible');
                    list.style.display = 'none';
                }
            });

            // If we're closing the current item, just clear active state and return
            if (isClosingCurrentItem) {
                activeItem = null;
                if (shouldPushState) {
                    history.pushState(null, '', window.location.pathname);
                }
                return;
            }

            // Open the target item
            const targetList = document.getElementById(termId);
            const targetChevron = targetHeader.querySelector('.toggle-icon');
            
            targetHeader.classList.add('active');
            targetChevron.style.transform = 'rotate(90deg)';
            
            if (targetList) {
                targetList.style.display = 'block';
                // Trigger reflow
                targetList.offsetHeight;
                targetList.classList.add('visible');
            }

            // Smooth scroll to the header
            const offset = 100; // Adjust based on your header height
            const targetPosition = targetHeader.getBoundingClientRect().top + window.pageYOffset - offset;
            window.scrollTo({
                top: targetPosition,
                behavior: 'smooth'
            });

            // Update active item reference
            activeItem = targetHeader;

            // Update URL without refresh if needed
            if (shouldPushState) {
                const newUrl = new URL(window.location);
                newUrl.hash = termId;
                history.pushState({termId: termId}, '', newUrl);
            }
        }
    }

    // Handle clicks on category items
    categoryItems.forEach(function(item) {
        item.addEventListener('click', function(e) {
            e.preventDefault();
            const termId = item.getAttribute('data-term');
            handleTermNavigation(termId);
        });
    });

    // Handle initial load and browser back/forward
    function handleInitialState() {
        // First check for direct category URLs
        if (handleDirectCategoryURL()) {
            return; // Stop here as we're redirecting
        }

        // Handle hash-based navigation
        if (window.location.hash) {
            const termId = window.location.hash.substring(1);
            handleTermNavigation(termId, false);
        }
    }

    // Handle browser back/forward
    window.addEventListener('popstate', function(e) {
        if (e.state && e.state.termId) {
            handleTermNavigation(e.state.termId, false);
        } else if (!window.location.hash) {
            // Close all if no hash
            categoryItems.forEach(function(item) {
                const list = item.nextElementSibling;
                const chevron = item.querySelector('.toggle-icon');
                item.classList.remove('active');
                chevron.style.transform = 'rotate(0deg)';
                if (list) {
                    list.classList.remove('visible');
                    list.style.display = 'none';
                }
            });
            activeItem = null;
        }
    });

    // Intercept tag/category clicks from posts
    document.addEventListener('click', function(e) {
        const termLink = e.target.closest('a[href^="/tags/"], a[href^="/categories/"]');
        if (termLink) {
            e.preventDefault();
            const href = termLink.getAttribute('href');
            
            // Handle both direct and hash-based URLs
            if (href.includes('#')) {
                const termId = href.split('#')[1];
                handleTermNavigation(termId);
            } else {
                const termType = href.split('/')[1]; // 'tags' or 'categories'
                const termName = href.split('/')[2].replace(/\/$/, ''); // remove trailing slash
                window.location.href = `/${termType}/#${termName}`;
            }
        }
    });

    // Handle initial page load
    handleInitialState();
});