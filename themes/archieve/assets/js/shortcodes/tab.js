document.addEventListener("DOMContentLoaded", function () {
    const tabGroups = document.querySelectorAll(".tab");

    tabGroups.forEach(function (tabGroup) {
        const tabNavItems = tabGroup.querySelectorAll("[data-tab-nav] li");
        const tabContentPanels = tabGroup.querySelectorAll("[data-tab-content] [data-tab-panel]");

        tabNavItems.forEach(function (tabNavItem) {
            tabNavItem.addEventListener("click", function () {
                const tabName = this.getAttribute("data-tab");

                // Remove active class from all tab nav items
                tabNavItems.forEach(function (item) {
                    item.classList.remove("active");
                    item.setAttribute("tabindex", "-1");
                });

                // Add active class to the clicked tab nav item
                this.classList.add("active");
                this.setAttribute("tabindex", "0");

                // Hide all tab content panels
                tabContentPanels.forEach(function (panel) {
                    panel.classList.remove("active");
                });

                // Show the corresponding tab content panel
                const tabContentPanel = tabGroup.querySelector(`[data-tab-panel="${tabName}"]`);
                if (tabContentPanel) {
                    tabContentPanel.classList.add("active");
                }
            });
        });
    });
});