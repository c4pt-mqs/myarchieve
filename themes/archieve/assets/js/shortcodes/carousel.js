// Self-contained carousel module
// Carousel module with safe namespacing
(function (window) {
    function initCarousels() {
        const carousels = document.querySelectorAll('.carousel');
        if (!carousels.length) return;

        carousels.forEach(function (carousel) {
            const ele = carousel.querySelector('ul');
            const amountvisible = Math.round(ele.offsetWidth / ele.querySelector('li:nth-child(1)').offsetWidth);
            const bullets = carousel.querySelectorAll('ol li');
            const slides = carousel.querySelectorAll('ul li');
            const nextarrow = carousel.querySelector('.next');
            const prevarrow = carousel.querySelector('.prev');

            // Initialize the carousel
            nextarrow.style.display = 'block';
            prevarrow.style.display = 'block';
            ele.scrollLeft = 0;
            bullets[0].classList.add('selected');
            slides[0].classList.add('selected');
            
            if (amountvisible > 1) {
                const removeels = carousel.querySelectorAll('ol li:nth-last-child(-n + ' + (amountvisible - 1) + ')');
                removeels.forEach(function (removeel) {
                    removeel.remove();
                });
            }

            // Update the selected slide without debounce
            const setSelected = function () {
                bullets.forEach(function (bullet) {
                    bullet.classList.remove('selected');
                });
                slides.forEach(function (slide) {
                    slide.classList.remove('selected');
                });
                
                const scrolllength = carousel.querySelector('ul li:nth-child(2)').offsetLeft - carousel.querySelector('ul li:nth-child(1)').offsetLeft;
                const nthchild = (Math.round((ele.scrollLeft / scrolllength) + 1));
                
                try {
                    carousel.querySelector('ol li:nth-child(' + nthchild + ')').classList.add('selected');
                    carousel.querySelector('ul li:nth-child(' + nthchild + ')').classList.add('selected');
                    
                    if (carousel.parentElement.parentElement.querySelector('.dynamictitle')) {
                        const title = carousel.querySelector('ul li:nth-child(' + nthchild + ') img').getAttribute('title');
                        if (title) carousel.parentElement.parentElement.querySelector('.dynamictitle').innerHTML = title;
                    }
                } catch (e) {
                    console.log('Carousel update error:', e);
                }
            };

            const scrollTo = function (event) {
                event.preventDefault();
                ele.scrollLeft = ele.querySelector(this.getAttribute('href')).offsetLeft;
            };

            const nextSlide = function () {
                if (!carousel.querySelector('ol li:last-child').classList.contains('selected')) {
                    carousel.querySelector('ol li.selected').nextElementSibling.querySelector('a').click();
                } else {
                    carousel.querySelector('ol li:first-child a').click();
                }
            };

            const prevSlide = function () {
                if (!carousel.querySelector('ol li:first-child').classList.contains('selected')) {
                    carousel.querySelector('ol li.selected').previousElementSibling.querySelector('a').click();
                } else {
                    carousel.querySelector('ol li:last-child a').click();
                }
            };

            const setInteracted = function () {
                ele.classList.add('interacted');
            };

            // Attach the handlers (without debounce)
            ele.addEventListener("scroll", setSelected);
            ele.addEventListener("touchstart", setInteracted);
            ele.addEventListener('keydown', function (e) {
                if (e.key == 'ArrowLeft') ele.classList.add('interacted');
                if (e.key == 'ArrowRight') ele.classList.add('interacted');
            });

            nextarrow.addEventListener("click", nextSlide);
            nextarrow.addEventListener("mousedown", setInteracted);
            nextarrow.addEventListener("touchstart", setInteracted);

            prevarrow.addEventListener("click", prevSlide);
            prevarrow.addEventListener("mousedown", setInteracted);
            prevarrow.addEventListener("touchstart", setInteracted);

            bullets.forEach(function (bullet) {
                bullet.querySelector('a').addEventListener('click', scrollTo);
                bullet.addEventListener("mousedown", setInteracted);
                bullet.addEventListener("touchstart", setInteracted);
            });

            //setInterval for autoplay
            if (carousel.getAttribute('duration')) {
                setInterval(function () {
                    if (ele != document.querySelector(".carousel:hover ul") && !ele.classList.contains('interacted')) {
                        nextarrow.click();
                    }
                }, carousel.getAttribute('duration'));
            }
        });
    }

    // Alternative way to attach the DOMContentLoaded event
    if (document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded', initCarousels);
    } else {
        initCarousels();
    }
})(window);