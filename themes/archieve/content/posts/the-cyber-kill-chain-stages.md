---
title: "Bir Siber Saldırının Aşamaları: Cyber Kill Chain Nedir ve Nasıl Çalışır?"
date: 2022-05-03T09:00:00Z
draft: false
toc: true
type: post
author: Anonim
categories:
  - Siber Güvenlik
tags:
  - siber-güvenlik
  - tehdit-tespiti
  - saldırı-aşamaları
---

Siber güvenlik, günümüzde hayati bir öneme sahiptir. Bilgi sistemlerini korumak, gelişmiş saldırılara karşı mücadele etmek için Lockheed Martin tarafından geliştirilen Cyber Kill Chain, bir saldırının aşamalarını tanımlayan bir çerçevedir. Bu yazıda, Kill Chain'in aşamalarını inceleyerek, her bir adımın nasıl çalıştığını anlayacağız.

{{< figure src="/img/content/cyber-security/the-cyber-kill-chain-chema.png" alt="the-cyber-kill-chain-chema" loading="lazy" width="70%" height="auto" >}}


## Keşif Aşaması

İlk aşama olan "Keşif", saldırganın hedef hakkında bilgi toplama sürecidir. Saldırgan, potansiyel zafiyetleri, hedefin güvenlik önlemlerini ve diğer kritik bilgileri belirlemek için aktif veya pasif yöntemlerle hedefi analiz eder.

**İnteraktif Soru:** Saldırganın keşif aşamasında hangi tür bilgileri toplaması beklenir?

- Saldırgan, hedefin ağ yapısı, güvenlik zafiyetleri, çalışanlar ve altyapı hakkında bilgi toplar. Bu, gelecekteki saldırılar için hedefin zayıf noktalarını belirlemesine yardımcı olur.

## Silahlanma Aşaması
"Silahlanma" aşamasında saldırgan, hedefe karşı kullanmak üzere kötü amaçlı yazılımları geliştirir ve istismarlar oluşturur. Bu aşama, saldırganın teknik yeteneklerini kullanarak etkili bir saldırı aracı oluşturduğu kritik bir noktadır.

**İnteraktif Soru:** Saldırganın silahlanma aşamasında kullanabileceği kötü amaçlı yazılımlar nelerdir?

- Saldırganlar genellikle virüsler, truva atları, solucanlar veya sıfır gün saldırıları gibi çeşitli kötü amaçlı yazılım türlerini kullanabilirler. Bu yazılımlar, hedef sistemde zararlı faaliyetlerde bulunmak için tasarlanmıştır.

## Teslimat Aşaması
Saldırgan, istismar ve kötü amaçlı yazılımı hedefe iletmek için çeşitli yöntemleri kullanır. Bu, e-posta, güvenlik açıklarını sömürme veya başka bir teslimat mekanizması olabilir.

**İnteraktif Soru:** Saldırganın teslimat aşamasında kullanabileceği yöntemler nelerdir?

- Teslimat aşamasında saldırganlar genellikle phishing e-postaları, zararlı bağlantılar, USB cihazları veya başka güvenlik zafiyetlerini hedefleyen yöntemler kullanabilirler.

## Sömürü Aşaması
"Sömürü" aşamasında, saldırgan kötü amaçlı yazılımı çalıştırarak hedefin zafiyetlerini istismar eder. Bu, sisteme giriş noktasını sağlar.

**İnteraktif Soru:** Sistemdeki zafiyetlerin nasıl istismar edilebileceğini bilmek neden önemlidir?

- Zafiyetler, saldırganların hedef sisteme giriş yapmasını sağlayan kapılardır. Bu nedenle, bu zafiyetleri bilmek ve kapatmak, savunma stratejilerini güçlendirmek açısından hayati önem taşır.

## Kurulum Aşaması
Bu aşamada, saldırgan kötü amaçlı yazılımı ve arka kapıları hedef sistemde kurar. Bu, saldırganın erişimini sürdürmesine ve istediği bilgileri toplamasına olanak tanır.

**İnteraktif Soru:** Kötü amaçlı yazılımın kurulum aşamasında dikkat edilmesi gereken ana noktalar nelerdir?

- Kötü amaçlı yazılımın kurulumunda saldırganlar, algılanmamak için gizli kalma ve iz bırakmama konularına dikkat ederler. Ayrıca, yetkisiz erişim sağlamak ve bağlantılarını korumak için çeşitli teknikleri kullanabilirler.

## Komut ve Kontrol Aşaması
Saldırgan, hedef sistem üzerinde kontrol sağlamak için bir komut ve kontrol altyapısı kurar. Bu, saldırganın uzaktan erişim ve yönetim yeteneği kazanmasını sağlar.

**İnteraktif Soru:** Komut ve kontrol altyapısının tespiti için kullanılan güvenlik önlemleri nelerdir?

- Güvenlik uzmanları, anomali tespiti, ağ izleme, davranış analizi ve trafiği izleme gibi yöntemlerle komut ve kontrol trafiğini tespit etmeye çalışırlar. Bu, saldırganların uzaktan kontrol yeteneklerini saptamak için kullanılır.

## Eylem Aşaması
Son aşama olan "Eylem", saldırganın amacına ulaştığı ve kötü niyetli eylemleri gerçekleştirdiği aşamadır. Bu, bilgi hırsızlığı, veri manipülasyonu veya başka kötü niyetli eylemleri içerebilir.

**İnteraktif Soru:** Saldırganın eylem aşamasında gerçekleştirebileceği potansiyel zararlar nelerdir?

- Eylem aşamasında saldırganlar, bilgi hırsızlığı, veri manipülasyonu, servis kesintileri ve hatta ağ içindeki diğer cihazlara yönelik ek saldırılar gibi potansiyel zararlar gerçekleştirebilirler.

{{< line >}}

{{< quote author="Lockheed Martin" >}}
"Lockheed Martin'e göre, Cyber Kill Chain'in aşamalarını anlamak, saldırıyı yavaşlatmak veya engellemek ve sonuçta veri kaybını önlemek için kritik bir adımdır. Bilgi sistemlerimizi koruma konusundaki bu sürekli çabada güncel kalmak önemlidir. Saldırılara karşı hazırlıklı olmak için bu aşamaları anlamak ve buna göre hareket etmek, siber güvenlik açısından hayati önem taşır."
{{< /quote >}}