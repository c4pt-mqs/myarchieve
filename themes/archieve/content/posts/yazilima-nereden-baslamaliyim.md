---
title: "Yazılım'a Nereden Başlamalıyım?"
date: 2021-03-21T09:00:00Z
draft: false
type: post
author: Kaptan
categories:
  - Yazılım
tags:
  - programlama
  - yazılım
  - rehber
---


Programlama yapmaya karar verdiniz ve nereden başlayacağınız konusunda kararsızsınız; işte burada sizlere kendinizi nasıl geliştirebileceğinize dair kaynaklar ve bilgiler vereceğiz. Aklınızı sürekli nereden başlamalıyım sorusundan kurtarıp, harekete geçmenizin gerekliliğini kavramanız gerekiyor. Çünkü siz istiyorsunuz ve gerekeni de siz yapmalısınız.

## Türkçe Anlatım
You can find the English guide of this article [here]({{< ref "where-to-start-programming.md" >}}).

Bir çok sayıda yazıda **İngilizce** öğrenmenin gerekliliğinden bahsediliyor ve bir noktadan sonra Türkçe kaynaklar yetersiz kalacaktır. Bu konuda rehberimize [buradan]({{< ref "ingilizce-ogrenmek-icin-yardimci-kaynaklar.md" >}}) ulaşabilirsiniz. Şimdi başlayalım:

### Bilgisayarlar Nasıl Çalışır?

a. [Bilgisayarda 0 ve 1'ler ne iş yapar? [Video]](https://www.youtube.com/watch?v=ai2HX77tpEs)
  
b. [Programlama Dili Nedir: 0'lar ve 1'ler [Metin]](https://tabella.org/2019/07/01/programlama-dili-nedir-0lar-ve-1ler/)
   
### Programlamaya Nereden Başlamalıyım?

a. [Yazılıma Nereden Başlanır? 15 Yazılımcının Cevabı: [Video]](https://www.youtube.com/watch?v=6xayBNPZMaE)
   
b. [Yazılıma Nereden Başlamalıyım? [Metin]](https://www.sadikturan.com/yazilima-nereden-baslamaliyim)

### Hangi Programlama Dilini Öğrenmeliyim ?

a. [Programlamanın kullanıldığı alanlar – Nereye Yönelebiliriz? [Metin]](https://startupvadisi.com/programlamanin-kullanildigi-alanlar-nereye-yonelebiliriz/)

{{< figure src="/img/content/software/which-programming-language-should-i-learn-first.png" alt="Programlamayı neden öğrenmek istiyorsun?" loading="lazy" width="100%" height="auto">}}

Eğer karar verdiyseniz, bir yazılım dilini nasıl ve nereden öğrenebileceğiniz ile ilgili kaynakları paylaşacağız;
Bundan önce bir programlama dili öğrenirken dikkat etmeniz gereken bazı hususlar var:

1. Eğitimleri izleyerek 1 aydan fazla harcamayın.
2. Gerektiği zaman ve gerektiği kadar öğrenin.
3. Projeler Oluşturun.
4. Kodlama görevleri yapın.
5. Basit, çalışma projeleri yapın.

b. [Türkçe Kaynaklar – Hepsi Bir Arada [Metin]](https://turkcekaynaklar.com/)



