---
title: "Dil Bariyerini Aşmak: Farklı Dilde Yazışmalara Dair Deneyimlerim"
date: 2023-06-21T18:58:06+03:00
draft: false
toc: false
type: post
author: Anonim
categories:
  - Kişisel Gelişim
tags:
  - dil-öğrenimi
  - iletişim
  - yabancı-dil
---

Uluslararası alanda iletişim kurarken, Türkçe dilini sıkça tercih ediyorum. Ancak, karşımdaki kişilerin çoğu Türkçe bilmiyor. Bu nedenle, yazılarımı Türkçe yazdıktan sonra Google Çeviri'yi kullanarak İngilizce'ye çeviriyorum. Peki neden böyle yapıyorum:

**İngilizce Bilmiyormuş Gibi Algı:**

Bazı insanlar, benim İngilizce bilmeyen biri olduğumu düşünüyorlar. Bu algıyı ortadan kaldırmak için, yazılarımı Türkçe olarak yazdıktan sonra İngilizce çeviri ile paylaşıyorum.

**İngilizce Bilmeyenler İçin Kolaylık:**

İngilizce bilmeyenler, kendi dilleriyle iletişim kurabilme imkanına sahip oluyorlar. Bu, farklı dil becerilerine sahip kişiler arasında etkili bir iletişimi teşvik etmeyi amaçlıyor.

**Dandik Çeviri Algısı:**

Bazı kişiler, Google Çeviri'nin kalitesiz çeviriler yaptığı düşüncesine kapılarak, yazdığım Türkçe metni kendi çabalarıyla incelemeyi tercih ediyorlar. Bu da bireyin işine geliyor.

**İngilizceyi Türkçeye Çevirmekle Uğraşmak:**

Beden ben İngilizce'yi Türkçe'ye çevirmeye uğraşıyım ki.Temel amacım, karşılıklı anlayışı artırmak ve dil bariyerini aşmaktır.

Sonuç olarak dil bariyeri, uluslararası iletişimde önemli bir engel oluşturabilir. Ancak benim tercih ettiğim yöntemle, farklı dil becerilerine sahip kişilere erişebilmek ve anlaşılabilir bir iletişim kurabilmek mümkün olmaktadır. Her ne kadar bazıları Google Çeviri'yi eleştirse de, benim için bu araç, dilin gücünü ve iletişimdeki engelleri aşma çabalarımı destekleyen önemli bir araç olmuştur. Dil bariyerini aşmak için çaba harcamak, kültürler arası anlayışı artırmanın bir yolu olarak benim için vazgeçilmezdir.
