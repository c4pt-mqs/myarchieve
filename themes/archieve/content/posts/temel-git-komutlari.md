---
title: Temel Git Komutları
date: 2023-09-07T09:00:00Z
image: "/img/content/software/git_sticker.jpeg"
draft: false
toc: true
type: post
author: Kaptan
categories:
  - Yazılım
tags:
  - git
  - programming
  - tutorial
---

**[Git](https://git-scm.com/)**, modern yazılım geliştirme süreçlerinde, projeleri verimli bir şekilde yönetmek için yaygın olarak kullanılan güçlü bir sürüm kontrol sistemidir.

{{< figure src="/img/content/software/git_sticker.jpeg" alt="Development Stages Of A Traditional SDLC" loading="lazy" width="70%" height="auto" >}}

İster yeni başlayan ister deneyimli bir geliştirici olun, Git komutlarında uzmanlaşmak projeler üzerinde işbirliği yapmak, değişiklikleri izlemek ve kod sürümlerini yönetmek için çok önemlidir. Ancak, başlangıçta Git'in karmaşık yapısı ve komutları, yeni başlayanlar için kafa karıştırıcı olabilir. Bu blog yazısında en yaygın Git komutlarından bahsedeceğim.


## Git Deposu Oluşturmak ve Yapılandırmak

**Repo projesini oluşturun:**

```zsh
git init test
```

**Yapılandırma ayarlarını görüntüleme:**

```zsh
git config --list
```

**Repo için varsayılan dalı ayarlayın (main veya master olarak düşünün):**

```zsh
git config --global init.defaultBranch main
```

**Git için kullanılacak bilgileri ayarlayın:**

```zsh
git config --global user.name "[username]"
git config --global user.email [mail_address]
```

**Config bilgilerini lokalde kaydet:**

```zsh
git config --global credential.helper store
```

## Değişiklikleri İzleme ve Yönetme

**Tüm dosyaları işleme ekleyin:**

```zsh
git add .
```

**Belirli dosyaları işleme ekleyin:**

```zsh
git add file1 file2
```

**Değişiklikleri kontrol edin:**

```zsh
git status
```

**Yüklenecek dosyanın açıklamasını yapın:**

```zsh
git commit -m "commit mesajı"
```

**İlgili bilgilerdeki değişikliklerin geçmişini izleyin:**

```zsh
git log
```

**Tüm dalların tek satır formatında commit geçmişini gösterin:**

```zsh
git log --all --oneline
```


## Uzak Depo İşlemleri

**Uzak depoyu klonla:**

```zsh
git clone [repo_url]
```

**Uzak depo bilgilerini görüntüle:**

```zsh
git remote -v
```

**Uzak depodan güncellemeleri al:**

```zsh
git pull
```

**Yerel değişiklikleri uzak depoya gönder:**

```zsh
git push
```


## Dal Yönetimi

**Dalları görüntülemek:**

```zsh
git branch
```

**Yeni dal oluşturmak:**

```zsh
git branch [new_branch]
```

**Yeni bir dal oluşturuken aynı zamanda o dala geçiş yap:**

```zsh
git checkout -b [new_branch]
```

**Farklı bir dala geçiş yapmak:**

```zsh
git checkout [other_branch]
```

**Yeni dalı mevcut dala birleştirin:**

```zsh
git merge [new_branch]
```

**Dalı silmek:**

```zsh
git branch -d [new_branch]
```

**Dalı yeniden adlandır:**

```zsh
git branch -m [eski_dal_adı] [yeni_dal_adı]
```

**Uzak depoda yeni bir dal oluştur:**

```zsh
git push origin [yeni_dal_adı]
```

**Uzak daldaki değişiklikleri yerel dala çek:**

```zsh
git fetch origin [uzak_dal_adı]:[yerel_dal_adı]
```


## Değişiklikleri İnceleme

**Geçici olarak mevcut değişiklikleri saklayın:**

```zsh
git stash push
```

**Saklanan değişiklikleri görüntüleyin:**

```zsh
git stash list
```

**Saklanan değişiklikleri kaldırmadan yeniden uygulayın:**

```zsh
git stash apply
```

**Saklanan değişiklikleri kaldırarak yeniden uygulayın:**

```zsh
git stash pop
```

**Tüm saklanan değişiklikleri temizleyin:**

```zsh
git stash clear
```

**Bir dosya üzerindeki değişiklikleri görüntülemek:**

```zsh
git blame [dosya_adı]
```

**Bir dosya üzerindeki değişiklikleri karşılaştırmak:**

```zsh
git diff [dosya_adı]
```

**Hatalı bir kod değişikliğinin hangi noktada gerçekleştiğini bulmak:**

```zsh
git bisect start
git bisect bad
git bisect good [commit_id]
```

## Değişiklikleri Geri Alma

**İstenilen sayıda commit'i sil:**

```zsh
git reset --soft HEAD~3
# veya
git rebase -i HEAD~3

git push origin main --force
git reset --hard
```

**Değişiklikleri belirli bir commit'e geri al:**

```zsh
git revert [commit_id]
```

**Değişiklikleri tamamen geri al (dikkatli kullanın, tüm geçmişi sıfırlar)**
```zsh
git reset --hard [commit_id]
```

### Tipik Bir Git Akışı

```zsh
git init
git remote add origin https://github.com/username/project-name
git add .
git commit -m "Initial commit: Set up basic project structure and functionalities"
git branch -M main
git push -uf origin main
```

```zsh
git clone https://github.com/username/project-name.git
cp -R local-repo/* downloaded-repo/
cd downloaded-repo/
git add .
git commit -m "Initial commit: Set up basic project structure and functionalities"
git push origin main
```

## Geleneksel Commit Mesajları

Proje ortamında tutarlı ve açık commit mesajları sağlamak için [Conventional Commits Specification](https://www.conventionalcommits.org/tr/v1.0.0/)'i takip etmelisiniz. Bu, commit mesajlarınızın bir düzen içinde olmasını sağlar ve takımınızın iş akışını kolaylaştırır. Örneğin:

### Commit Mesajları Formatı

```r
type: Subject

body

footer
```

**type (tür):**
```css
feat: Yeni bir özellik eklenirken kullanılır.
fix: Bir hata düzeltildiğinde kullanılır.
docs: Belgelendirmeye ilişkin değişiklikler için kullanılır.
style: Kod düzenlemeleri (beyaz boşluk, biçimlendirme) için kullanılır.
refactor: Kodun yeniden düzenlenmesi, yapısal değişiklikler için kullanılır.
test: Testlerle ilgili değişiklikler için kullanılır.
chore: Yardımcı araçlar ve işlemlerle ilgili değişiklikler için kullanılır.
```

**subject (konu):**

Subject; büyük harfle başlamalı, 50 karakteri geçmemeli ve nokta ile bitmemelidir. Açıklamada yapılan işi belirtmek için emir tonu kullanın ve neyi değiştirdiğinizi veya eklediğinizi açıkça ifade edin.

**body (gövde):**

Body; commit'inizin daha ayrıntılı bir açıklamasını içerir. İhtiyaca bağlı olarak kullanılır ve değişikliklerin neden yapıldığını, nasıl yapıldığını veya diğer ilgili ayrıntıları içerebilir. Genellikle daha büyük ve karmaşık değişiklikler için kullanışlıdır.

**footer (altbilgi):**

Footer; commit ile ilgili referanslar veya kapatılması gereken sorun numaraları gibi ek bilgileri içerebilir.

**Example:**

```md
feat: Add user authentication feature

Implemented user authentication functionality to allow users to securely log in and manage their accounts.

- Added user login and registration forms
- Implemented password hashing for security
- Created user session management
- Added "Forgot Password" feature

Closes #27
```

### .gitignore Dosyası

Projenizin kök dizininde bir `.gitignore` dosyası oluşturarak Git'in izlememesini istediğiniz dosyaları ve klasörleri tanımlayabilirsiniz. Örneğin:

```r
# Derleme dosyaları
*.o
*.out

# IDE ve editör özel dosyaları
.vscode/
.idea/

# Modüller
node_modules/
```

## Sık Karşılaşılan Hatalar ve Çözümleri

**Error:**

{{< notice error >}}
! [remote rejected] main -> main (pre-receive hook declined)<br>
! [rejected] main -> main (fetch first)<br>
error: failed to push some refs to 'https[:]//gitlab.com/username/myproject.git'
{{< /notice >}}

**Çözümü:**

```zsh
git pull origin main --rebase
git fetch origin main
git push -f origin main
```

**Error:**

{{< notice error >}}
From https[:]//gitlab.com/username/myproject<br>
\* branch            main       -> FETCH_HEAD<br>
fatal: refusing to merge unrelated histories
{{< /notice >}}

**Çözümü:**

```zsh
git pull origin main --no-rebase --allow-unrelated-histories
```

### **Daha Fazla Kaynak ve Pratik İçin:**

- [Awesome Git](https://github.com/dictcp/awesome-git)
- [W3Schools Git Kılavuzu](https://www.w3schools.com/git/git_getstarted.asp)
- [Conventional Commits Specification](https://www.conventionalcommits.org/)
- [Oh My Git!](https://ohmygit.org/)
