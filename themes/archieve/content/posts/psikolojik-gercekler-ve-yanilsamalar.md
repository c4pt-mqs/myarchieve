---
title: "Psikolojik Gerçekler ve Yanılsamalar"
date: 2023-09-30T18:58:06+03:00
draft: false
toc: false
type: post
author: Kaptan
categories:
  - Fikirler
tags:
  - psikoloji
  - kişisel-gelişim
  - motivasyon
---

İnsan tarihi boyunca, gerçekliğin soğuk ve acımasız gerçekleriyle yüzleşmek yerine, insanlar sıklıkla hayal gücünün kucaklayıcı kollarına sığınma eğiliminde olmuşlardır. Altmış yılı aşkın bir süredir varlığını sürdüren ve Gustave Le Bon tarafından dile getirilen ünlü söz, "Kitleler hiçbir zaman gerçeği arzulamamıştır. Onlara yanılsamalar sunabilen kolayca onların efendisi olabilir; yanılsamalarını yok etmeye çalışan ise her zaman kurbanları olmuştur."

Bu söz, toplumun genel eğilimini ve insan psikolojisinin derinliklerindeki ilginç bir gerçeği dile getiriyor. İnsanlar, çoğu zaman rahatsız edici olan gerçeklikten kaçmak için hayal gücünün sunduğu illüzyonlara çekiliyor gibi görünüyor. Belki de bu, insan doğasının bir savunma mekanizmasıdır; çünkü yaşamın karmaşıklığı ve belirsizliğiyle başa çıkabilmek için insan zihninin sıklıkla sığınak aradığı düşünülebilir.

Bu eğilim, günümüzde medya, siyaset ve toplumsal dinamikler aracılığıyla belirgin bir şekilde ortaya çıkıyor. Gerçeklerin karmaşıklığı ve belirsizliği, birçok insanı kolayca yönlendirilebilen, hoşa giden yanılsamalara yönlendiriyor gibi görünüyor. Politik liderler, manipülatif reklamlar ve bilgi akışını kontrol etmeye çalışan güç odakları, insanların gerçeklerden uzaklaşmasını sağlayacak illüzyonlar sunarak etkileme çabasındalar.

Ancak, gerçeği sorgulayan ve yanılsamaları reddeden bireyler her zaman bu oyunun mağduru olmuştur. Toplumsal normlara karşı duran, doğruları savunan bireyler genellikle toplumun dışına itilir ve çoğunluğun kabul ettiği yanılsamalara karşı çıktıkları için marjinalleştirilirler. Bu durum, gerçekle yüzleşmeye cesaret edenlerin, toplumun yarattığı illüzyonların kurbanı olma potansiyelini beraberinde getiriyor.

Sonuç olarak, insan doğası gerçeklerle yüzleşmek yerine yanılsamaları tercih etme eğilimindedir. Bu eğilim, toplumun genel dinamiklerinde ve bireyler arasındaki ilişkilerde belirgin bir şekilde görülebilir. Ancak, belki de gerçeklerle barışık yaşamanın ve yanılsamalardan arınmanın, insanlığın evrimine katkı sağlayacak bir adım olduğunu düşünmek gerekiyor.
