---
title: Python Uygulama Örnekleri ve Notları
date: 2022-02-12T09:00:00Z
summary: "Bu yazımda, python öğrenirken kullandığınız birçok komutu derledim. Unuttuğunuz ve takıldığınız yerlerde size yardımcı olması temennim olur. Python programlama dili, geniş bir komut yelpazesine sahip ve çeşitli alanlarda kullanabilirsiniz."
draft: false
toc: true
type: post
author: Kaptan
categories:
  - Yazılım
tags:
  - python
  - programming
  - coding
  - tutorial
---


Bu yazımda, python öğrenirken kullandığınız birçok komutu derledim. Unuttuğunuz ve takıldığınız yerlerde size yardımcı olması temennim olur. Python programlama dili, geniş bir komut yelpazesine sahip ve çeşitli alanlarda kullanabilirsiniz. Sıkça kullanılan komutları derledim:

## Temeller
Bir sayı yazdır:

```py
print(123)
```


Bir dizi yazdır:

```py
print("test")
```


Sayıları toplamak:

```py
print(1+2)
```


Değişken atamak:

```py
number = 123
```


Değişkeni yazdır:

```py
print(number)
```

     

Fonksiyonu çağır:

```py
x = min(1, 2)
```


Yorum satırı:

```py
#Bu bir yorumdur
```


## Türler
- Integer => 42
- String => "a string"
- List => [1, 2, 3]
- Tuple => (1, 2, 3)
- Boolean => True

## Faydalı fonksiyonlar
Ekrana yazdır:

```py
print("hi")
```


Uzunluğu hesapla:

```py
len("test")
```


Sayıların minimum değeri:

```py
min(1, 2)
```


Sayıların maximum değeri:

```py
max(1, 2)
```


Tamsayıya dönüştür:

```py
int("123")
```


Dizeye dönüştür:

```py
str(123)
```


Boolean'a dönüştür:

```py
bool(1)
```


Sayıların aralığını al:

```py
range(5, 10)
```


Bir değer yazdırmak:

```py
return 123
```


İndeksleme:

```py
"test"[0]
```



Dilimleme:

```py
"test"[1:3]
```


Sonraki döngü ile devam ettir:

```py
continue
```


Döngüden çık:

```py
break
```


Liste ekleme:

```py
numbers = numbers + [4]
```


Liste ekleme (metoduyla):

```py
numbers.append(4)
```


Liste öğesini ayıkla:

```py
value = numbers[0]
```


Liste öğesini atama:

```py
numbers[0] = 123
```


## Terimler
- **syntax:**                    harf ve sembollerin kodda düzenlenmesi
- **program:**                   bilgisayar için talimatların bütünü
- **print:**                     ekrana metin yazdır
- **string:**                    tırnak içine alınmış bir dizi kelime
- **variable:**                  atanabilir değer
- **value examples:**            a string, an integer, a boolean
- **assignment:**                bir değişkene bir değer vermek için '=' kullanmak
- **function:**                  içine değerler koyup - çağrıldığı bir fonksiyon
- **call (a function):**        fonksiyonu çalıştırmak için çağırmak
- **argument:**                  bir fonksiyona girdi vermek
- **parameter:**                 bir fonksiyon tanımı belirtmek
- **return value:**              bir fonksiyondan döndürülen değer
- **conditional:**               yalnızca bir koşul geçerliyse çalıştırılan bir talimat
- **loop:**                      talimatları tekrar tekrar çalıştırmanın bir yolu/döngü
- **list:**                      verilen değerleri tutan bir değer türü
- **tuple:**                     bir liste gibi, ancak değiştirilemez
- **indexing:**                  belirli bir durumdan, bir elementin çıkarılması
- **slicing:**                   bazı öğeleri arka arkaya çıkarmak
- **dictionary:**                anahtarlardan değerlere eşleme

## Hatırlatmalar
- Strings(dizeler) ve lists(listeler) 1'den değil 0'dan başlayarak dizine alınır.
- Print ve return aynı kavram değildir.
- Return anahtar sözcüğü yalnızca fonksiyonların içinde geçerlidir.
- Strings(dizeler) tırnak içine alınmalıdır.
- Variable(değer) veya function(fonksiyon) adlarına boşluk koyamazsınız.
- Typecasting(türünü belirtme) yapmadan strings ve integers ekleyemezsiniz
- Koşullar için, fonksiyon tanımları ve loops(döngüler) yazarken iki nokta üst üste kullanın.
- Tanımlayıcı değişken adları, kodunuzu daha iyi anlamanıza yardımcı olur.

Koşullar:

```py
if x == 1:
    print("x bir'dir.")
else:
    print("x bir değildir.")
```


Listeler:

```py
sayilar = [7, 8, 9]
ilk_sayi = sayilar[0]
sayilar[2] = 11
if 11 in sayilar:
    print("11 liste de değil!")
for n in sayilar:
    print(n)
```


Fonksiyonları Tanımlama:

```py
def my_func(parametre1, parametre2):
    result = parametre1 + parametre2
    return result
```


Döngüler:

```py
for sayi in [1, 2, 3]:
    print(sayi)
x = 0
while x < 10:
    print(x)
    x = x + 1
```


Sözlükler:

```py
sayilar = {
    1: "bir",
    2: "iki"
}
print(sayilar[1])
```


Karşılaştırmalar:
- Eşittir          ==
- Eşit değildir	  !=
- Küçüktür     	  <
- Küçük eşittir	  <=
- Büyüktür	      >

## Faydalı Yöntemler
Küçük harfe dönüştür:


```py
"XX".lower()
```


Büyük harfe dönüştür:

```py
"xx".upper()
```


Boşluklar koyarak böl:

```py
"a b c".split(" ")
```


Boşlukları kaldır:

```py
" a string ".strip()
```


Bir çok dizeyi tek bir dizede birleştir:

```py
" ".join(["a", "b"])
```


String … ile başlamalı:

```py
"xx".startswith("x")
```


String … ile bitmeli:

```py
"xx".endswith("x")
```


Listeleri say:

```py
[1, 2].count(2)
```


Listeyi sil:

```py
[1, 2].remove(2)
```


Dictionary keys:

```py
{1: 2}.keys()
```


Dictionary values:

```py
{1: 2}.values()
```


Dictionary key/value pairs:

```py
{1: 2}.items()
```


## Code Snippets


### Kullanıcıdan girdi alma:

```py
name = input("Enter your name: ")
print("Hello, " + name + "!")
```


### For döngüsü kullanma:

```py
numbers = [1, 2, 3, 4, 5]
for number in numbers:
    print(number)
```


### While döngüsü kullanma:

```py
count = 0
while count < 5:
    print(count)
    count += 1
```


### Listeyi filtreleme:

```py
numbers = [1, 2, 3, 4, 5]
even_numbers = [num for num in numbers if num % 2 == 0]
print(even_numbers)

```


### Fonksiyon tanımlama ve çağırma:

```py
def greet(name):
    print("Hi, " + name + "!")

greet("Jack")
```


### Sınıf tanımlama ve nesne oluşturma:

```py
class Person:
    def __init__(self, name):
        self.name = name

    def greet(self):
        print("Merhaba, benim adım " + self.name)

person = Person("Ali")
person.greet()
```


### Dosya okuma ve yazma:

```py
# Dosya yazma
with open("file.txt", "w") as file:
    file.write("This is a test.")

# Dosya okuma
with open("file.txt", "r") as file:
    content = file.read()
    print(content)
```


### Dosya işlemleri (CSV):

```py
import csv

# CSV dosyası yazma
data = [["Name", "Age"], ["Jake", 30], ["Alan", 25]]
with open("data.csv", "w", newline="") as file:
    writer = csv.writer(file)
    writer.writerows(data)

# CSV dosyası okuma
with open("data.csv", "r") as file:
    reader = csv.reader(file)
    for row in reader:
        print(row)
```


### Bir Dizeyi Ters Çevir:

```py
def reverse_string(string):
    return string[::-1]

text = "Hello, World!"
reversed_text = reverse_string(text)
print("Reversed Text:", reversed_text)
```


### Listeyi Tersine Çevir:

```py
def reverse_list(lst):
    reversed_lst = lst[::-1]
    return reversed_lst

my_list = [1, 2, 3, 4, 5]
reversed_list = reverse_list(my_list)
print("Reversed List:", reversed_list)
```


### Listedeki Sayıların Ortalamasını Hesaplama:

```py
def calculate_average(numbers):
    total = sum(numbers)
    average = total / len(numbers)
    return average

nums = [5, 2, 9, 1, 7]
average = calculate_average(nums)
print("Average:", average)
```


### Dictionary kullanma:

```py
person = {"name": "Jake", "age": 21, "city": "New York"}
print(person["name"])
print(person.get("age"))
```


### Try-Except bloğu kullanma:

```py
try:
    num = int(input("Bir sayı girin: "))
    result = 10 / num
    print("Sonuç:", result)
except ZeroDivisionError:
    print("Hata: Sıfıra bölme hatası!")
except ValueError:
    print("Hata: Geçersiz sayı girişi!")
```


### Modül içe aktarma:

```py
import math

radius = 5
area = math.pi * math.pow(radius, 2)
print("Area of the circle:", area)
```


### String işlemleri:

```py
text = "Hello, world!"
print(text.upper())
print(text.lower())
print(text.split(", "))

```


### Lambda fonksiyonu kullanma:

```py
double = lambda x: x * 2
print(double(5))
```


### Liste sıralama:

```py
numbers = [5, 2, 8, 1, 9]
numbers.sort()
print(numbers)
```


### Veri yapıları:

```py
# Tuple
point = (3, 5)
print(point[0])

# Set
fruits = {"apple", "banana", "orange"}
fruits.add("pear")
print(fruits)

# Dictionary
person = {"name": "John", "age": 30}
person["city"] = "New York"
print(person)
```


### API istekleri (requests kütüphanesi kullanarak):

```py
import requests

response = requests.get("https://api.example.com/data")
data = response.json()

# Verileri kullanma
for item in data:
    print(item["name"], item["value"])
```


### Web Scraping (Beautiful Soup kütüphanesi kullanarak):

```py
import requests
from bs4 import BeautifulSoup

# Web sayfasını çekme
url = "https://www.example.com"
response = requests.get(url)

# HTML içeriğini ayrıştırma
soup = BeautifulSoup(response.text, "html.parser")

# Etiketleri seçme
links = soup.find_all("a")
for link in links:
    print(link.get("href"))

```


### Binary'i Decimal'e Çevirme:

```py
binary = "101010"
decimal = int(binary, 2)
print("Decimal:", decimal)
```


Decimal'i Binary'e Çevirme:

```py
decimal = 42
binary = bin(decimal)[2:]
print("Binary:", binary)
```


### Faktöriyel Hesaplama:

```py
def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)

num = 5
result = factorial(num)
print("Factorial:", result)
```


### Fibonacci Serisi:

```py
def fibonacci(n):
    fib_sequence = [0, 1]
    for i in range(2, n):
        fib_sequence.append(fib_sequence[i - 1] + fib_sequence[i - 2])
    return fib_sequence

num = 8
fib_seq = fibonacci(num)
print("Fibonacci Sequence:", fib_seq)
```


### Linear Arama:

```py
def linear_search(arr, target):
    for i in range(len(arr)):
        if arr[i] == target:
            return i
    return -1

numbers = [3, 5, 1, 9, 2, 7]
target_num = 9
index = linear_search(numbers, target_num)
print("Target found at index:", index)
```


### Bubble Sıralama:

```py
def bubble_sort(arr):
    n = len(arr)
    for i in range(n - 1):
        for j in range(n - i - 1):
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]

numbers = [9, 2, 5, 1, 7, 3]
bubble_sort(numbers)
print("Sorted Numbers:", numbers)
```


### Asal Sayı mı?:

```py
def is_prime(num):
    if num <= 1:
        return False
    for i in range(2, int(num ** 0.5) + 1):
        if num % i == 0:
            return False
    return True

number = 17
is_prime_number = is_prime(number)
print("Is Prime Number:", is_prime_number)
```

### Bir Metindeki Kelimeleri Saymak:

```py
def count_words(text):
    words = text.split()
    word_count = len(words)
    return word_count

sentence = "This is a sample sentence."
word_count = count_words(sentence)
print("Word Count:", word_count)
```


### Bir Listedeki Maksimum Sayıyı Bulma:

```py
def find_max(numbers):
    max_num = float('-inf')
    for num in numbers:
        if num > max_num:
            max_num = num
    return max_num

nums = [5, 2, 9, 1, 7]
max_number = find_max(nums)
print("Maximum Number:", max_number)
```


### Rastgele Parola Oluşturma:

```py
import random
import string

def generate_password(length):
    characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(characters) for _ in range(length))
    return password

password_length = 10
random_password = generate_password(password_length)
print("Random Password:", random_password)
```


### Bir Sayının Üssünü Almak:

```py
def power(base, exponent):
    result = 1
    for _ in range(exponent):
        result *= base
    return result

base_num = 2
exponent_num = 3
power_result = power(base_num, exponent_num)
print("Power Result:", power_result)
```


### Bir Sayıdaki Rakamların Toplamı:

```py
def sum_of_digits(number):
    total = 0
    while number > 0:
        digit = number % 10
        total += digit
        number //= 10
    return total

num = 12345
digit_sum = sum_of_digits(num)
print("Sum of Digits:", digit_sum)
```


### Bir Dizideki Ünlüleri Saymak:

```py
def count_vowels(text):
    vowels = "aeıioöuü"
    count = 0
    for char in text.lower():
        if char in vowels:
            count += 1
    return count

sentence = "Hello, how are you?"
vowel_count = count_vowels(sentence)
print("Vowel Count:", vowel_count)
```


### Sıcaklık Dönüştürme:

```py
def celsius_to_fahrenheit(celsius):
    fahrenheit = (celsius * 9/5) + 32
    return fahrenheit

def fahrenheit_to_celsius(fahrenheit):
    celsius = (fahrenheit - 32) * 5/9
    return celsius

temp_celsius = 25
temp_fahrenheit = celsius_to_fahrenheit(temp_celsius)
print("Temperature in Fahrenheit:", temp_fahrenheit)

temp_fahrenheit = 77
temp_celsius = fahrenheit_to_celsius(temp_fahrenheit)
print("Temperature in Celsius:", temp_celsius)
```


### Caesar Cipher:

```py
def caesar_encrypt(text, shift):
    encrypted_text = ""
    for char in text:
        if char.isalpha():
            ascii_val = ord(char)
            shifted_val = (ascii_val - ord('a') + shift) % 26 + ord('a')
            encrypted_text += chr(shifted_val)
        else:
            encrypted_text += char
    return encrypted_text

def caesar_decrypt(text, shift):
    decrypted_text = ""
    for char in text:
        if char.isalpha():
            ascii_val = ord(char)
            shifted_val = (ascii_val - ord('a') - shift) % 26 + ord('a')
            decrypted_text += chr(shifted_val)
        else:
            decrypted_text += char
    return decrypted_text

message = "hello world"
encrypted_message = caesar_encrypt(message, 3)
print("Encrypted Message:", encrypted_message)

decrypted_message = caesar_decrypt(encrypted_message, 3)
print("Decrypted Message:", decrypted_message)
```


## Bonus
+ Zip lists:
zip([1, 2], ["one", "two"])

- Set:
my_set = {1, 2, 3}

+ Set intersection:
{1, 2} & {2, 3}

- Set union:
{1, 2} | {2, 3}

+ Index of list element:
[1, 2, 3].index(2)

- Sort a list:
numbers.sort()

+ Reverse a list:
numbers.reverse()

- Sum of list:
sum([1, 2, 3])

+ Numbering of list elements:
for i, item in enumerate(items):

- Read a file line by line:
for line in open("file.txt"):

+ Read file contents:
contents = open("file.txt").read()

- Random number between 1 and 10:
import random; x = random.randint(1, 10)

+ List comprehensions:
[x+1 for x in numbers]

- Check if any condition holds:
any([True, False])

+ Check if all conditions hold:
all([True, False])
