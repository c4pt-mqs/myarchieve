---
title: "Topluluklarda Soru Sorma ve Yardım Alma Rehberi"
date: 2022-12-03T09:00:00Z
lastmod: 2024-02-27T13:00:00Z
image: "/img/content/self-improvement/topluluklarda-yardim-istemek.jpeg"
draft: false
toc: true
type: post
author: Kaptan
categories:
  - Kişisel Gelişim
tags:
  - kişisel-gelişim
  - yardımlaşma
  - eğitim
---

## Üstad Nedir?

Üstat felsefesi, problemleri çözme, yaratıcılığı teşvik etme, özgürlüğe ve yardımlaşmaya değer verme gibi temel prensiplere dayanan bir felsefedir. Bu felsefeye göre, üstatlar kendilerini sürekli geliştirir, diğerleriyle bilgi ve deneyim paylaşırlar. Üstat olmak için sadece yetkinlik yeterli değil; aynı zamanda zeka, deneyim, kendini adama ve ciddi çalışma gerektirir.

{{< quote author="Üstad" >}}
To follow the path:<br>
look to the master,<br>
follow the master,<br>
walk with the master,<br>
see through the master,<br>
become the master.
{{< /quote >}}

### Üstatlar İçin Soru Sorma İlkeleri

Üstatlar, zor problemleri ve güzel hazırlanmış kışkırtıcı soruları severler. İyi sorular, anlama yetinizi güçlendirir ve gözden kaçmış sorunları ortaya çıkarır. Üstatlar için "iyi soru", önemli ve içten gelen bir hediyedir.

#### Soru Sormadan Önce Bunları Yapın

`Araştırma Yapın:` Sorunuzu sormadan önce internette araştırma yapın, dokümantasyonu okuyun ve bir arkadaşınıza danışın. Bu, tembel olmadığınızı ve insanların zamanını boşa harcamadığınızı gösterir.

- **İyi örnek:** Bir yazılım geliştiricisi, bir programlama dilinde karşılaştığı hata nedeniyle çevrimiçi topluluklara soru sormadan önce ilgili belgeleri inceleyip, benzer sorunları araştırır. Ardından, yalnızca çözümü bulamadığında topluluktan yardım ister.

- **Kötü örnek:** Bir öğrenci, matematik ödevini tamamlamak için problemi hakkında hiç araştırma yapmadan hemen arkadaşına mesaj atar.

`Google'ı Kullanın:` Almış olduğunuz hata iletilerini kullanarak Google'da arama yapın. Anahtar kelimeleri kullanın ve gereksiz detaylardan kaçının. Bu size doğru cevaba ulaştıracak veya sorunuzu tartışan bir topluluk bulmanıza yardımcı olabilir.

- **İyi örnek:** Bir kullanıcı, bir programı açarken sorunla karşılaştığında, önce hatayı Google'da arar ve çözümü bulmaya çalışır.

- **Kötü örnek:** Bir kullanıcı, bir programı nasıl kullanacağını öğrenmek için hemen birine mesaj atar.

`Soruyu Hazırlayın:` Sorunuzu hazırlarken, önce yaptığınız araştırmaları belirtin ve detaylı bir şekilde açıklayın. Acele hazırlanmış sorular, genellikle acele cevaplar alır veya hiç cevap almazlar. Sorunuzu göndermeden önce çözüm için çaba gösterdiğinizi insanlar da görmeli.

- **İyi örnek:** Bir kullanıcı, bilgisayarında karşılaştığı bir sorununu ekran görüntüleri ile birlikte detaylı bir şekilde açıklar ve çözüm denemelerini belirtir.

- **Kötü örnek:** Bir kullanıcı, sadece "Bilgisayarım açılmıyor, yardım edin!" şeklinde bir mesaj gönderir ve hiçbir ek bilgi vermez.

`Kesin Cevap Alma Hakkınız Yoktur:` Soru sormanız bir haktır ancak cevap alma hakkınız olduğunu düşünmeyin.

- **İyi örnek:** Bir kullanıcı, soru sorduğu bir forumda, cevap almak için sorusunu net bir şekilde formüle eder ve gerekirse ek bilgi sağlar.

- **Kötü örnek:** Bir kullanıcı, sorusuna cevap almadan hemen sinirlenip agresif bir dil kullanarak moderatörlere şikayette bulunur.


## Yardım İstemek

Sanal ortamda soracağınız teknik soruların zorluğu ve sizin soruyu nasıl sorduğunuz, alacağınız cevapları belirler. Sanal alemde artık tecrübeli kullanıcılardan da, üstatlar kadar iyi cevaplar alabilirsiniz. Ancak etkili cevaplar almak için üstatlar gibi davranmanız önemlidir.

{{< figure src="/img/content/self-improvement/topluluklarda-yardim-istemek.jpeg" alt="topluluklarda-yardım-istemek" loading="lazy" width="60%" height="auto" >}}

Sorularınıza hızlı cevap almak istiyorsanız diğerlerinin size yardım etmesini kolaylaştırmanız gerekiyor. Bu yüzden yardım isterken şu adımlar ile ilgili bilgi verin:

1. `Problemin ne olduğunu düşünüyorsun?`
2. `Tam olarak ne olmasını istiyorsun?`
3. `Aslında ne oluyor?`
4. `Nasıl buraya geldin?`
5. `Şimdiye kadar neler denedin?`

#### İyi bir başlık nasıl yazılır?

İyi bir başlık, sorununuzu net ve özlü bir şekilde ifade etmelidir. Başlığınızı mümkün olduğunca spesifik yaparak, sorununuzu anlatın.

**Kötü Örnek:** "Yardım Gerekiyor: Metin Dosyası Sorunu"

**Doğru:** "Java'da Metin Dosyasından Veri Okuma Sorununu Nasıl Çözebilirim?"

#### Cevaplara hızlı bir şekilde nasıl ulaşılır?

Cevaplara hızlı erişim için sorunuzu net ve anlaşılır bir şekilde yazın. Ayrıca, mevcut cevapları dikkatlice okuyun ve gereksiz tekrarları önlemek için önceki cevapları kontrol edin.

**Kötü Örnek:** "Uygulama hata veriyor, bir türlü çözemedim. Ne yapabilirim?"

**Doğru:** "'XXX' Uygulamamda beklenmedik bir hata alıyorum ve hata resimdeki gibidir. 'XXX'i denedim, ancak işe yaramadı. Başka neler yapabilirim?"

#### Genel konularda nasıl tavsiye alınır?

Geniş bir kullanıcı kitlesine sahip platformlar, çeşitli konularda uzmanlık ve deneyim paylaşımı için kategorileri bulunur. Sorunuzun doğru kategoride olduğundan emin olarak gereksinimlerinizi net bir şekilde açıklayın.

**Kötü Örnek:** "Bir konuda tavsiye almak istiyorum, ne yapmalıyım?"

**Doğru:** "Bir şirket kurmak istiyorum ve hangi belgeleri hazırlamam gerektiğini bilmiyorum. Yardımcı olabilecek birini nasıl bulabilirim?"

#### Soruların başlıklarında "etiket" bulunmalı mı?

Evet, sorularınıza uygun etiketler eklemek, doğru kişilerin sorununuzu bulmasına yardımcı olabilir. Ancak, gereksiz etiket kullanmaktan kaçının.

**Kötü Örnek:** "[Sorun] [Yardım] [Acil]"

**Doğru:** [Java] [Dosya İşlemleri] [Hata Çözümü]

#### Bir soru birden fazla kez gönderilebilir mi?

Genellikle evet, ancak aynı platforma veya siteye aynı soruyu tekrar tekrar göndermekten kaçının. İlginç cevaplar almak için farklı topluluklara başvurabilirsiniz.

**Kötü Örnek:** "Aynı soruyu 10 farklı kategoride gönderdim ama hala cevap alamadım. Lütfen cevap verin, acil!!!"

**Doğru:** "... Aynı soruyu Stack Overflow ve Reddit'de paylaştım ancak henüz cevap alamadım. Yardımcı olur musunuz?"


## Soruları Biçimlendirme

Sorularınızı okunabilir bir formatta yazmak, daha hızlı bir şekilde çözüme ulaşmanıza yardımcı olur. Ekran görüntüleri, sorunu anlamak için son derece değerli araçlardır. Yardım alacağınız toplulukta, ekran görüntünüzü sürükleyip bırakarak veya basitçe PrtScn ve yapıştır klavye kısayollarını kullanarak paylaşabilirsiniz. 

Eğer kod parçaları veya hata mesajları varsa, bunları vurgulayarak ve code formatında paylaşarak daha etkili bir destek alabilirsiniz. Normal cümlelerden farklı olarak, kodunuzu backtick {{< highlight md "hl_inline=true" >}}(`){{< /highlight >}} kullanarak gösterebilirsiniz. İngilizce klavyelerde Tab tuşunun üzerinde bulunurken; Türkçe klavyelerde `AltGr + ,` yaparak kullanabilirsiniz. Diğer klavyeler için [bakınız](https://superuser.com/questions/254076/how-do-i-type-the-tick-and-backtick-characters-on-windows).

- `Tek satırlık bir kod`

- {{< highlight go >}}
Çoklu satırlar için kodlar
Çoklu satırlar için kodlar
{{< / highlight >}}

Ayrıca, topluluğun arama işlevini kullanarak benzer sorunları veya hata mesajlarını aratabilirsiniz.


## Nasıl Yardımcı Olabilirsiniz?

Soru sorma konusunda etkili olmak kadar yardım ederken de önemli prensipleri göz ardı etmemek gerekir. Aşağıdaki kuralları gözden geçirin ve başkalarına yardım ederken de bunları dikkate alın.

**Gerçekten yardım etmek istiyor iseniz uğraş verin yoksa polimik bir ortam oluşturarak ortamı germeyin.**

{{< figure src="/img/content/self-improvement/nasil-yardim-edebilirsin.jpeg" alt="topluluklarda-yardım-istemek" loading="lazy" width="50%" height="auto" >}}

### Google'da Bulunabilecek Sorular İçin Bilinçli Cevaplar Verin

Soruyu soran kişinin bilgi seviyesini anlamaya çalışın ve anlaşılır bir dil kullanarak karşılık verin. Güvenilir kaynakları paylaşarak daha fazla bilgi edinmelerine yardımcı olun. Ayrıca tarafsız olun ve kişisel görüşlerinizi belirtmeyin.

{{< quote >}}
Unutmayın ki, Google bilgiye hızlı erişmenizi sağlasada topluluklar size deneyim ve derinlik katar. Ayrıca, Google'da bulunan bilgiler de genellikle toplulukların katkılarıyla oluşmuştur. Bu yüzden topluluklar olmasaydı, Google'da bulunan cevaplar da olmazdı.
{{< /quote >}}

**Kötü Örnek:** "Bu kadar basit soruyu Google'da aramak zor olamaz!"

**Doğru Örnek:** "Sorunuzu anladım. Örneğin, "Java'da bir diziyi nasıl sıralarım?" gibi bir soruyu araştırırken, 'Arrays.sort()' gibi bir yöntem kullanabilirsiniz. Ancak detaylı bilgi için Java'nın resmi belgelerine başvurmanızı öneririm."

### Soruyu Cevaplamak Yerine, Yanıta Ulaşmalarına Rehberlik Edin

Sorulara daha yönlendirici ve rehberlik edici cevaplar vermek, kullanıcıların doğru bilgiye ulaşmalarını sağlayabilir. Kullanıcıların sorunlarını çözmek yerine, onlara yanıtı bulabilecekleri yolları gösterin. Sorunları kendi başlarına çözmelerine yardımcı olacak ipuçları ve kaynakları sunarak, kendi problem çözme becerilerini geliştirmelerine destek olun.

**Kötü Örnek:** "reverse() yöntemi, bir listedeki öğeleri yerinde tersine çevirir. [::-1] dilimleme yöntemi ise bir dizinin tersine çevirilmiş bir kopyasını oluşturur."

**Doğru Örnek:** "Sorunuzla ilgili yardımcı olabilecek kaynaklar arasında Python belgeleri ve çevrimiçi forumlar var. 'reverse()' ve '[::-1]' yöntemlerini inceleyerek sorununuzu çözebilirsiniz."

### Nazik ve Yeterli Cevaplar Önemlidir

Kullanıcılara kaba veya yetersiz cevaplar vermek, topluluk içinde olumsuz bir atmosfer oluşturabilir. Her zaman nazik ve anlayışlı olun, sorunları çözmek için çaba gösterin. Sinirlendiğinizde veya gergin hissettiğinizde, tartışmalardan ve sohbetten uzaklaşın.

**Kötü Örnek:** "Bu soruyu defalarca cevapladım. Aynı şeyi sürekli tekrar ediyorsunuz!!"

**Doğru Örnek:** "Bu soruyu daha önce cevapladık, hatta 'XXX' bağlantısından ulaşabilirsiniz. aklına takılan başka bir şey olursa sormaktan çekinmeyin."

### Yanlış ve Mantıksız Cevaplardan Kaçının

Kullanıcılara yanlış veya mantıksız cevaplar vermek, sorunlarını daha da kötüleştirebilir. Kesin olmadığınız konularda sessiz kalın veya o konuda bilgili olduğunu düşündüğünüz bir kişiyi yardım etmeye teşvik edin.

**Kötü Örnek:** "Kesinlikle yanılıyorsunuz, benim cevabım doğru. Sizin bilginize ihtiyacım yok."

**Doğru Örnek:** "Farklı bir görüşe sahip olabiliriz, ancak doğru cevabı bulmak için uğraşabiliriz. Diğer insanlardan da yardım alabiliriz."


## Doğru Soru Sorma ve Cevaplama Örnekleri

{{< spoiler name="Örnek 1" >}}

#### Soru

Merhaba, Ben Python öğrenmeye yeni başlayan bir öğrenciyim ve projemde bir hata alıyorum. Bir dosyadan veri okumam gerekiyor ancak dosya okuma işlemiyle ilgili bir hata alıyorum. İşte kodum:

```py
with open('veriler.txt', 'w') as dosya:
    veriler = dosya.read()
```

Kodu çalıştırdığımda şu hatayı alıyorum:

`FileNotFoundError: [Errno 2] No such file or directory: 'veriler.txt'`

#### Cevap

Merhaba, Dosya okuma hatası almış olmanızın birkaç nedeni olabilir. İlk olarak, Python betiğinizin çalıştığı dizinde veriler.txt adında bir dosya olup olmadığını kontrol edin. Eğer dosya yoksa, FileNotFoundError hatasını alırsınız.

Eğer dosya mevcutsa, dosyanın tam yolunu belirtmelisiniz. Örneğin, `C:\Users\KullaniciAdi\Documents\veriler.txt` gibi.

Ayrıca, dosyayı açarken 'w' modunu kullanıyorsunuz, bu dosyanın üzerine yazmak ya da varsa içeriğini silmek için kullanılır. Eğer dosyanın sadece okuma izni varsa, 'r' modunu kullanmanız yeterlidir.

#### Yorum

Çok teşekkür ederim! Dosyanın bulunduğu dizini değiştirdiğimde kodum düzgün çalıştı. Bu hata nedeniyle saatlerce uğraştım, yardımınız için teşekkürler!

{{< /spoiler >}}

{{< spoiler name="Örnek 2" >}}

#### Soru

Merhaba, HTML ve CSS kullanarak bir web sayfası tasarlıyorum ve navigasyon menümü oluştururken bir sorunla karşılaştım. Menümü oluştururken yatay düzende başlıkları yan yana hizalıyorum ancak bir başlığın boyutu diğerlerinden farklı olduğunda, menüde düzensizlikler oluşuyor. Nasıl yapabilirim ki tüm başlıklar eşit boyutta ve hizalanmış bir şekilde kalsın?

#### Cevap

Merhaba, bu gibi durumlarda genellikle flexbox veya grid gibi CSS özelliklerini kullanarak istediğiniz düzeni sağlayabilirsiniz. Eşit boyutlu ve hizalı bir navigasyon menüsü oluşturmak için flexbox en iyi seçenek olabilir. Örneğin, tüm menü öğelerine aynı genişliği vermek için `flex-grow` veya `flex-basis` özelliklerini kullanabilirsiniz. Ayrıca, `justify-content` özelliğini `space-between` veya `space-around` gibi değerlerle kullanarak öğeleri eşit aralıklarla hizalayabilirsiniz.

**Örnek:**

```css
.nav-menu {
  display: flex;
  justify-content: space-between;
}

.nav-item {
  flex-grow: 1; /* Tüm öğelerin eşit genişlikte olmasını sağlar */
  /* veya */
  /* flex-basis: calc(100% / n); */
}
```

#### Yorum

Eline sağlık, çözüm için teşekkürler! Flexbox'un bu kadar güçlü bir araç olduğunu bilmiyordum, şimdi bu teknikleri deneyeceğim. Umarım sorunumu çözebilirim.

{{< /spoiler >}}

***

Bu ipuçlar ve örnekler, sanal ortamda soru sorma ve yardım alma süreçlerinizi daha verimli hale getirebilir. Sorunlarınızı net bir şekilde ifade edin, ilgili bağlamı sağlayın ve uygun topluluklardan yardım alın. Unutmayın, doğru sorularla daha iyi cevaplar alırsınız!


### Kaynaklar

Bu bölüm, içerik oluşturulurken yararlanılan faydalı bağlantıları içerir:

- [Sormak için sorma, sadece sor](https://dontasktoask.com/tr)
- [No Hello](https://nohello.net/tr/)
- [XY Problemi](https://fazlamesai.net/posts/xy-problemi)
- [Stack Overflow'ta Nasıl Sorulur](https://stackoverflow.com/help/how-to-ask)
- [Akıllı Yoldan Nasıl Soru Sorulur](https://edu.anarcho-copy.org/T%C3%BCrk%C3%A7e%20-%20Turkish/GNU%20Linux%20-%20Unix-Like/Linux%20Belgeleri%20-%20belgeler.org/belgeler.org/howto/smart-questions.html)
- [Odin Projesi - Yardım Nasıl İstenir](https://www.theodinproject.com/lessons/foundations-join-the-odin-community#before-asking-for-help)
