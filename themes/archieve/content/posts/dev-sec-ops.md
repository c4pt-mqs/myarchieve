---
title: Dev | Sec | Ops
date: 2023-09-08T09:00:00Z
draft: false
type: post
author: Kaptan
categories:
  - Yazılım
tags:
  - devops
  - sürekli-entegrasyon
  - yazılım-geliştirme
---

CI/CD (Sürekli Entegrasyon & Dağıtım), yazılım projelerini geliştirmek için Yazılım Geliştirme Yaşam Döngüsü (SDLC) olarak bilinen bir süreci/modeli takip eden geliştiriciler tarafından kullanılır.

### Geleneksel bir SDLC'nin geliştirme aşamaları

- **Planlama:** Proje yapısını ve zamanlamasını belirlemek ve belgelemek.
- **Analiz:** Proje gereksinimlerini analiz etmek ve kaynakları toplamak.
- **Tasarım:** Mimari ve arayüz tasarımı için yazılım modelini tasarlamak.
- **Uygulama:** Tasarım ve gereksinimleri göz önünde bulundurarak gerçek bir ürün geliştirmek.
- **Test:** Oluşturulan yazılımı test etmek, hataları düzeltmek ve kodu iyileştirmek.
- **Dağıtma ve Bakım:** Yazılım dağıtılır ve daha fazla geliştirme için izlenir.

{{< figure src="/img/content/software/development-stages-of-a-traditional-sdlc.png" alt="Development Stages Of A Traditional SDLC" loading="lazy" width="100%" height="auto" >}}

Geleneksel Yazılım Geliştirme Süreci, farklı geliştirme ekiplerinin farklı kod depolarını sürdürdüğü, kodun entegre edildiği, entegre kodun bir paket olarak derlendiği ve bu paketin talimatlarla Operasyon ekibine gönderildiği, ardından Operasyon ekibinin paketi bir test ortamına yerleştirdiği ve Test ekibinin yazılımı test ettiği bir süreçle çalışır.

Ancak, bu hızla değişen dünyada, bir iş döngüsünün sonunda genellikle teslim edilen bir işlevsel ürün alana kadar beklemek gibi bir yaklaşım ve modellere sahip olmak artık değerli değil.

### Yazılım Geliştirme Yaşam Döngüsü (SDLC)

İşte burada CI/CD devreye giriyor. Bu, daha çok bir kültür ve uygulama olmasına rağmen, CI/CD nedir?

"CI/CD, Sürekli Entegrasyon ve Sürekli Dağıtım/Teslimat anlamına gelir. Bu, paylaşılan depoda entegre edilen bir kodun otomasyon yardımıyla yazılımın üretimine birden fazla kez nasıl sürdürüldüğü ile ilgilidir."

Sürekli Entegrasyon, geliştiricilerin otomasyonun yardımıyla paylaşılan bir depoda kodu birden fazla kez günlük olarak nasıl entegre ettikleri ile ilgilidir. Sürekli Dağıtım ise yazılımın otomatik olarak test veya üretim ortamına nasıl sürüldüğü ile ilgilidir.

### Sürekli Entegrasyon (CI) ve Sürekli Teslimat/Dağıtım (CD)

CI/CD, geleneksel SDLC'den farklı olarak geliştiriciler aşağıdaki resimde olduğu gibi depoda derleme sonrası otomatik Unit ve UI testleri yapılır. Operasyon ekibi de, test ortamına giden otomatik betiklerle ilgilenirler. Test yapılıp onaylandıktan sonra yazılım üretime gönderilir. Burada Sürekli Entegrasyon, Pipeline; Sürekli Dağıtım, Release Pipeline olarak adlandırılabilir.

{{< figure src="/img/content/software/continuous-delivery.png" alt="Continuous Delivery" loading="lazy" width="100%" height="auto" >}}

Sürekli Dağıtım, yazılımın Otomatik Kabul Testi'nden (Automated Acceptance Testing) sonra otomatik olarak üretimin sürülmesini ifade eder. 

{{< figure src="/img/content/software/continuous-deployment.png" alt="Continuous Deployment" loading="lazy" width="100%" height="auto" >}}

CI/CD, yazılım geliştirmeye hız ve güvenilirlik kazandırır. Otomasyonun yardımıyla kodun hızlı entegrasyonu, kısa döngü ve tekrarlamalar sayesinde hızlı hata tespiti ve yanıtı sağlar. İnsan hatalarına daha az eğilimli olup otomatik betikleri kullanmak ve manuel talimatları takip etmekte daha hızlıdır. Ayrıca, DevOps'taki CI/CD, süreçleri ve otomasyon birleştirilerek Geliştirme ve Operasyon ekipleri arasında daha güvenilir ve uyumlu bir iş akışı oluşturulabilir.

**Kaynaklar:**

- https://www.geeksforgeeks.org/ci-cd-continuous-integration-and-continuous-delivery/
- https://www.udemy.com/course/ci-cd-devops/
