---
title: GNU/Linux mu? Linux mu?
date: 2023-11-06T09:00:00Z
summary: "Richard Stallman, GNU/Linux ifadesini kullanırken; Linus Torvalds ise sadece Linux olarak adlandırma yapıyor. Her ikisi de özgür yazılımı ve GNU felsefesini destekliyor iken sizce bu fikir ayrılığının sebebi ne olabilir?"
image: "/img/content/ideas/gnu_linux_concept.png"
draft: false
type: post
author: Kaptan
categories:
  - Özgür Yazılım
tags:
  - gnu
  - linux
  - açık-kaynak
  - özgür-yazılım
---

Revolution OS'u izlediğimde aklıma takılan bir şey oldu:

Richard Stallman, GNU/Linux ifadesini kullanırken; Linus Torvalds ise sadece Linux olarak adlandırma yapıyor. Her ikisi de özgür yazılımı ve GNU felsefesini destekliyor iken sizce bu fikir ayrılığının sebebi ne olabilir?

{{< figure src="/img/content/ideas/gnu_linux_concept.png" alt="GNU-Linux Concept" loading="lazy" width="100%" height="auto" >}}

Fikir ayrılığının temelinde, GNU ve Linux'a bakış açılarındaki farklılıklar da yatıyor. Kendimce bir araştırma yaparak bu fikir ayrılığın temelinde neler olabileceğine dair bazı sonuçlar elde ettim. Bakalım bunlar neymiş:

Stallman, GNU'nun katkılarını ve felsefesini vurgulamak istiyor. Çünkü bu projeyi o başlattı ve özgür yazılım hareketinin öncüsü olarak da biliniyor. Ayrıca GNU'yu bir işletim sistemi ve Linux'u GNU'nun üzerine inşa edilmiş bir çekirdek (kernel) olarak görüyor. Bu nedenle GNU özgür yazılımın temel bileşenlerini sağlarken Linux bu bileşenleri bir araya getirerek işletim sistemini oluşturduğunu savunur. Böylece, GNU/Linux'un bir bütün olarak özgür yazılım olduğunu savunuyor.

Torvalds, Linux işletim sistemi çekirdeğinin geliştiricisi/başlatıcısı olarak biliniyor. Torvalds, Linux'u teknik açıdan kendi başına bir işletim sistemi olarak görüyor ve GNU olmadan da çalışabileceğini söylüyor. GNU, Linux'a bazı ek özellikler ve araçlar sağlasa da Linux'un temel işlevleri için gerekli olmadığını ve bu nedenle Linux'un sadece Linux olarak adlandırılması gerektiğini savunuyor.

GNU, 1984 yılında başlatılan bir projedir. Linux ise 1991 yılında başlatılmıştır. Bu nedenle, GNU'nun gelişimi daha uzun sürmüş ve Linux'tan daha köklü bir geçmişe sahiptir. Stallman, özgür yazılımın sadece kaynak kodunun erişilebilir olması değil aynı zamanda yazılımın değiştirilebilir ve paylaşılabilir olması gerektiğine de inanıyor. Torvalds ise özgür yazılımın, kullanıcıların yazılımı istedikleri gibi kullanmalarına olanak sağlaması gerektiğine inanıyor.

Bu fikir ayrılığına rağmen Stallman ve Torvalds, özgür yazılımın yayılması için birlikte çalıştılar ve bu alanda önemli katkılarda bulundular.

Günümüzde ise basitlik ve alışkanlıklardan kaynaklı olarak daha çok Linux söylemini kullanmaktayız.

Revolution OS'u izlemek için:
https://youtu.be/PRt399_wP1I