---
title: "Tüm Adımlarıyla Bir Web Sitesi Nasıl Kurulur?"
date: 2024-04-05T09:00:00+03:00
draft: false
toc: true
type: post
author: Kaptan
categories:
  - Yazılım
tags:
  - programming
  - website
  - rehber
---

Merhaba, bugün sizlere adım adım bir web sitesi kurmanın temel adımlarını anlatacağım. Bu hızlı rehberi takip ederek istediğiniz alan adında tamamen işlevsel bir web sayfasına sahip olabilirsiniz.


## Alan Adı Belirlemek

- **Alan Adı (Domain):** Arama bölümüne yazdığınız bir web sitesinin adıdır. Bu sitenin alan adı myarchieve.net'tir.
- **Üst Düzey Alan Adı (TLD):** Bir alan adının uzantısıdır, örneğin .com, .net, .xyz, vb.
- **Kayıt Eden (Registrar):** Size almak istediğiniz alan adını satan ve kaydeden kuruluştur.

Bir alan adı en az 1 yıl, en fazla 10 yıl süre ile kayıt edilebilir. İlk defa satın alındığında ucuz olsada bazı özel TLD'ler daha pahalı olabilir. Ayrıca birisi bir alan adını satın aldığında, o kişiye o alan adının hakları verilir ve sonrasında daha yüksek bir fiyatla satılabilir. Bu nedenle, büyük bir proje düşünmüyor olsanız bile bir alan adını ayırtmak iyi bir fikir olabilir.

Seçtiğiniz alan adı aldığınız firmada kalması zorunlu değildir. Daha sonra daha iyi bir anlaşma bulduğunuzda farklı bir alan adı tescil eden bir firmaya transfer edebilirsiniz.

Alan adları web sitenizin verilerini saklamaz. Bir alan adının amacı, insanların web sitenizi bulmak için IP adresi yerine alan adları ile kolayca bulmasını sağlamaktır. Ayrıca insanların web sitenize bağlanmalarını sağlayan "DNS ayarları"nı belirlemek için kullanılır. (Buna daha sonra da değineceğim.)


### Alan Adını Kayıt Edeceğiniz Kuruluşu Belirlemek

Aşağıda paylaştığım firmalar hakkında bilgi edinerek istediğiniz alan adınızı sorgulayarak uygunluğunu kontrol edebilir ve kayıt işlemini tamamlayabilirsiniz.

#### Ücretsiz Alan Adları

- [AtakDomain](https://www.atakdomain.com/ucretsiz-hosting)
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Netlify](https://www.netlify.com/)
- [GitHub Pages](https://pages.github.com/)
- [ParsData](https://www.parsdata.com/en-us/free-domain)
- [Register.it](https://offerte.register.it/index.php?promo=Gratis)
- [TurkTicaret.Net](https://www.turkticaret.net/ogrenci-kampanyasi/)
- [Vercel](https://vercel.com/)

#### Ücretli Alan Adları

- [AtakDomain](https://www.atakdomain.com/)
- [Cenuta](https://www.cenuta.com/)
- [Gandi](https://www.gandi.net)
- [Squarespace](https://domains.squarespace.com/)
- [Hosting.com.tr](https://www.hosting.com.tr/)
- [Hostinger](https://www.hostinger.com.tr/)
- [GoDaddy](https://www.godaddy.com/)
- [Guzel.Net.tr](https://www.guzel.net.tr/)
- [NameSilo](https://www.namesilo.com/)
- [Natro](https://www.natro.com/)
- [Realtime Register](https://realtimeregister.com/)
- [Turhost](https://www.turhost.com/)

#### Gizliliğe Önem Veren Alan Adları Satan Firmalar

Normalde tüm web sitelerinin ICANN'de gerçek bir ad ve adresle kaydedilmesi gerekir ancak bu siteler bu gereksinimi atlayarak gerçek isim ve adres bilgileriniz gizli tutar:

- [Njalla](https://njal.la/)
- [Cheap Privacy](https://cheapprivacy.ru/)

### Alan Adı Satın Almak

Şimdi, seçtiğiniz alan adı tescil sitesine gidelim ve istediğiniz alan adını arayın. Eğer başkası tarafından satın alınmış ise ön sipariş seçeneği olabilir, lakin bu satın almak istediğiniz alan adı için yüksek fiyatlar istenebilir. Satın alınmamış bir alan adı tercih etmek daha iyidir.

Unutmayın ki alan adınız, dijital varlığınızı temsil eder. İyi bir alan adı seçimi, hem arama motorlarındaki optimizasyonu sağlamak hem de gelecekteki projeleriniz için önemlidir.

Bir alan adı sahibi olmak için yapmanız gerekenler bu kadar. Şimdi web sitenizi barındırmak için bir sunucu alacağız.


## Bir Sunucu Edinmek


