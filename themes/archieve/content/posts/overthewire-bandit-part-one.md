---
title: "OverTheWire: Wargames — Bandit Writeup [1. Kısım]"
date: 2023-06-03T09:00:00Z
summary: Bandit wargame tamamen yeni başlayanlara yöneliktir. Diğer wargame'leri oynayabilmek için gereken temel bilgileri öğretecek şekilde hazırlanmıştır. Bu yazıda oyundaki alternatif çözüm yollarını keşfedebilirsiniz.
draft: false
toc: true
type: post
author: Kaptan
categories:
  - OverTheWire
tags:
  - ctf
  - linux
  - siber-güvenlik
---


Bu oyuna başlamak için bir SSH istemcisi kullanmalısınız. Ayrıca her seviyeye kendi şifrsei ile giriş yapmalısınız. İhtiyacınız olan tek şey bir terminal. Eğer windows kullanıyorsanız powershell ya da PuTTY ile bağlanabilirsiniz.

**Sunucu:** _bandit.labs.overthewire.org_

**Bağlantı Noktası:** _2200_

**Kullanıcı:** _bandit0_

## Bandit Level 0

http://overthewire.org/wargames/bandit/bandit0.html

Web sitelerinde bize bandit0 için kullanıcı adı ve şifre veriyorlar ve biz de bandit1 şifresini bulmamız gerekiyor. İlk ssh bağlantımız için sitede belirtildiği üzere "bandit0" kullanıcı adı ve "bandit0" şifresiyle giriş yapılacak.

```r
ssh bandit0@bandit.labs.overthewire.org -p 2220
```

**Kullanıcı adı:** `bandit0`

**Şifre:** `bandit0`

## Bandit Level 0 → Level 1

http://overthewire.org/wargames/bandit/bandit1.html

Bandit0 kullanıcısıyla oturum açtıktan sonra "ls" komutunu çalıştırdım ve "readme" adında bir dosya olduğunu gördüm. "cat readme" komutunu kullanarak dosyayı okudum. Bu bandit1'e giriş yapmak için verilen şifreyi içeriyor.

**Kullanıcı adı:** `bandit1`

**Şifre:** `NH2SXQwcBdpmTEzi3bvBHMM9H66vVXjL`

## Bandit Level 1 → Level 2

http://overthewire.org/wargames/bandit/bandit2.html

Bandit2 kullanıcı şifresinin "-" adlı bir dosyada kayıtlı olduğu belirtiliyor. Hesaba giriş yaptığımda "ls" komutunu çalıştırdım ve dosyayı gördüm. Dosyayı okumak için birden fazla alternatif yol var: 

1. cat ./-
2. cat <-
3. more -
4. cat '-'

**Kullanıcı adı:** `bandit2`

**Şifre:** `rRGizSaX8Mk1RTb1CNQoXTcYZWU6lgzi`

## Bandit Level 2 → Level 3

http://overthewire.org/wargames/bandit/bandit3.html

Bandit3 kullanıcı şifresinin "spaces in this filename" adlı bir dosyada kayıtlı olduğu belirtiliyor. Hesaba giriş yaptığımda "ls" komutunu çalıştırdım ve dosyayı gördüm. Dosyayı okumak için birden fazla alternatif yol var: 

1. cat 'spaces in this filename'
2. cat spaces\ in\ this\ filename

**Kullanıcı adı:** `bandit3`

**Şifre:** `aBZ0W5EmUfAf7kHTQeOwd8bauFJ2lAiG`

## Bandit Level 3 → Level 4
http://overthewire.org/wargames/bandit/bandit4.html

Bandit4 kullanıcı şifresinin 'inhere' adlı dizininde gizli bir dosyada saklanıyor. Dosya ve dizinleri görmek için "ls" komutunu çalıştırdım, sonra inhere klasörüne gitmek için "cd" komutunu, tüm dosyaları (gizli olanları da) görmek için "ls -a" komutunu ve gizli dosyanın içeriğini görmek için "cat .hidden" komutunu kullandım.

**Kullanıcı adı:** `bandit4`

**Şifre:** `2EW7BBsr6aMMoJ2HjW067dm8EgX26xNe`

## Bandit Level 4 → Level 5

http://overthewire.org/wargames/bandit/bandit5.html

Bandit5 kullanıcı şifresinin 'inhere' klasörünün içinde bir dosyada saklanmaktadır. İçinde birden fazla dosya olduğu için tek tek okumak yerine, dosyalar hakkında bilgi almak istedim ve önce okunabilir metin olanları buldum. Şifre 'ASCII text' olan dosya da saklanmış.

```r
bandit4@bandit:~$ ls
inhere
bandit4@bandit:~$ cd inhere/
bandit4@bandit:~/inhere$ ls
-file00  -file01  -file02  -file03  -file04  -file05  -file06  -file07  -file08  -file09
bandit4@bandit:~/inhere$ file ./-file0*
./-file00: data
./-file01: data
./-file02: data
./-file03: data
./-file04: data
./-file05: data
./-file06: data
./-file07: ASCII text
./-file08: data
./-file09: Non-ISO extended-ASCII text, with no line terminators
bandit4@bandit:~/inhere$ cat ./-file07
lrIWWI6bB37kxfiCQZqUdOIYfr6eEeqR
```

**Kullanıcı adı:** `bandit5`

**Şifre:** `lrIWWI6bB37kxfiCQZqUdOIYfr6eEeqR`

## Bandit Level 5 → Level 6

http://overthewire.org/wargames/bandit/bandit6.html

Önce "inhere" klasörüne girdim ve ardından klasörleri görmek için "ls" komutunu çalıştırdım. İçinde bir sürü klasör ve bu klasörlerin içinde birden fazla dosya olduğunu fark ettim.

Her dosyayı okumak zorunda kalmamak için, bana verilen özellikleri kullanıp dosyayı aramanın bir yolunu bulmaya çalıştım. 'find' komuta baktığımda -size, -type ve -executable seçenekleri kullanabileceğimi düşündüm. Tüm bu parametreleri de find ile birleştirince istenilen dosyayı elde ettim.

```r
bandit5@bandit:~/inhere$ find -type f -size 1033c ! -executable
./maybehere07/.file2
bandit5@bandit:~/inhere$ cat ./maybehere07/.file2
P4L4vucdmLnm8I7Vl7jG1ApGSfjYKqJU
```

**Kullanıcı adı:** `bandit6`

**Şifre:** `P4L4vucdmLnm8I7Vl7jG1ApGSfjYKqJU`

## Bandit Level 6 → Level 7

http://overthewire.org/wargames/bandit/bandit7.html

Bu level için find ile -user, -group, -size parametrelerini kullanarak bandit7 kullanıcısına ve bandit6 grubuna ait; 33 byte boyutunda bir dosyayı aradım. Parametreleri istenildiği gibi kullanarak dosyayı okudum.

```r
bandit6@bandit:~$ find / -user bandit7 -group bandit6 -size 33c 2>/dev/null
/var/lib/dpkg/info/bandit7.password
bandit6@bandit:~$ cat /var/lib/dpkg/info/bandit7.password
z7WtoNQU2XfjmMtWA8u5rN4vzqu4v99S
```

**Kullanıcı adı:** `bandit7`

**Şifre:** `z7WtoNQU2XfjmMtWA8u5rN4vzqu4v99S`

## Bandit Level 7 → Level 8

http://overthewire.org/wargames/bandit/bandit8.html

Bir sonraki seviyenin şifresi, 'data.txt' dosyasında "millionth" kelimesinin yanında saklanıyor.

data.txt dosyasınu okuduğumda bir sürü kelime ve kelimelerin yanında şifre bulnuyordu. Tüm satırlar aynı yapıya sahipmiş gibi görünüyordu. Bize verilen ipucu aslında kolayca bulmamı sağladı. 'grep' ile "millionth" kelimesini aradım.

```r
bandit7@bandit:~$ cat data.txt | grep millionth
millionth	TESKZC0XvTetK0S9xNwm25STk5iWrBvP
```

**Kullanıcı adı:** `bandit8`

**Şifre:** `TESKZC0XvTetK0S9xNwm25STk5iWrBvP`

## Bandit Level 8 → Level 9

http://overthewire.org/wargames/bandit/bandit9.html

'data.txt' dosyasındaki verileri sıralayıp, benzersiz olanları bulmak için 'sort' ve 'uniq' komutlarını kullandım. Tekrar etmeyenleri satırları yazdırmak için de uniq ile '-u' parametresini kullandım.

```r
bandit8@bandit:~$ sort data.txt | uniq -u
EN632PlfYiZbn3PhVK3XOGSlNInNE00t
```

**Kullanıcı adı:** `bandit9`

**Şifre:** `EN632PlfYiZbn3PhVK3XOGSlNInNE00t`

## Bandit Level 9 → Level 10

http://overthewire.org/wargames/bandit/bandit10.html

Dosyayı okumaya çalıştığımda düz bir metin olmadığını ve karışık karakterler içerdiğini gördüm. Metinden okunabilir kelimeleri seçmek için 'strings' komutunu çalıştırdım. Ayrıca bize verilen bilgi ile '=' işareti ile başlayan satırları yazdırmak için grep kullanabiliriz.

```r
bandit9@bandit:~$ cat data.txt | strings | grep ^=
========== password
========== is
=TU%
=^,T,?
========== G7w8LIi6J3kTb8A7j9LgrywtEUlyyp6s
```

**Kullanıcı adı:** `bandit10`

**Şifre:** `G7w8LIi6J3kTb8A7j9LgrywtEUlyyp6s`

## Bandit Level 10 → Level 11

http://overthewire.org/wargames/bandit/bandit11.html

'data.txt' base64 ile kodlanmış bir satır içeriyor. Deşifre etmek için base64 ile '-d' parametresini kullandım.

```r
bandit10@bandit:~$ cat data.txt 
VGhlIHBhc3N3b3JkIGlzIDZ6UGV6aUxkUjJSS05kTllGTmI2blZDS3pwaGxYSEJNCg==
bandit10@bandit:~$ cat data.txt | base64 -d
The password is 6zPeziLdR2RKNdNYFNb6nVCKzphlXHBM
```

**Kullanıcı adı:** `bandit11`

**Şifre:** `6zPeziLdR2RKNdNYFNb6nVCKzphlXHBM`

Referanslar:
- https://www.dcode.fr/cipher-identifier
- https://cyberchef.io/#recipe=Magic(3,false,false,'')&input=Cg

## Bandit Level 11 → Level 12

http://overthewire.org/wargames/bandit/bandit12.html

'data.txt' dosyası ROT13 algoritmasıyla şifrelenmiş olduğunu farkettim. ROT13 şifresini çözmek için her harfi 13 karakter ilerideki harfle değiştirmek gerekiyor. Tek tek uğraşmak yerine bir siteden yardım alarak bunu çözebiliriz(ref).

```r
bandit11@bandit:~$ cat data.txt 
Gur cnffjbeq vf WIAOOSFzMjXXBC0KoSKBbJ8puQm5lIEi
```

**Kullanıcı adı:** `bandit12`

**Şifre:** `JVNBBFSmZwKKOP0XbFXOoW8chDz5yVRv`

Referanslar:
- https://rot13.com/

## Bandit Level 12 → Level 13

http://overthewire.org/wargames/bandit/bandit13.html

Bu bölüm baştan beri canımı sıkmış olsada sonunda şifreyi buldum. Dosyayı tekrar tekrar sıkıştırılmış hexdump olan data.txt dosyasında saklanmaktadır. 

Bu seviyede '/tmp' altında çalışabileceğimiz bir dizin oluşturmamız gerekiyor. Örneğin: `mkdir /tmp/myname123`. Ardından, cp dosyasını bu dizine kopyalayın ve mv kullanarak yeniden adlandırın.

```r
bandit12@bandit:~$ mkdir /tmp/myname123
bandit12@bandit:~$ cp data.txt /tmp/myname123
bandit12@bandit:~$ cd /tmp/myname123
bandit12@bandit:/tmp/myname123$ ls
data.txt
```

Dosyanın hexdump'ını çözebilmek için 'xxd' uygulaması ile '-r' parametresini çalıştırdım. Bu komut, hexdump'ı tersine çevirir.

```r
bandit12@bandit:/tmp/myname123$ xxd -r data.txt > dump.txt
```

İpucuda bize dosyanın birden çok kez sıkıştırıldığı söyleniyor. Bu nedenle hangi sıkıştırma yönteminin kullanıldığını bulmak için 'file' komutunu kullandım.

```r
bandit12@bandit:/tmp/myname123$ file dump.txt 
dump.txt: gzip compressed data, was "data2.bin", last modified: Sun Apr 23 18:04:23 2023, 
max compression, from Unix, original size modulo 2^32 581
```

gzip ile sıkıştırıldığını görüyoruz. İçeriğini görmek için dosyanın uzantısına '.gz' ekledim. Ardından onu açmak için 'gunzip' komutunu çalıştırdım. Tekrar tekrar sıkıştırıldığını bildiğim için 'file' ile kontrol ettim.

```r
bandit12@bandit:/tmp/myname123$ mv dump.txt dump.txt.gz
bandit12@bandit:/tmp/myname123$ gunzip dump.txt.gz
bandit12@bandit:/tmp/myname123$ ls
data.txt  dump.txt
bandit12@bandit:/tmp/myname123$ file *
data.txt: ASCII text
dump.txt: bzip2 compressed data, block size = 900k
```

bzip2 ile sıkıştırılmış olduğunu görüyorum. Bu nedenle uzantıya '.bz2' ekleyerek dosyayı açmak için 'bzip2'yi '-d' parametresi ile çalıştırdım. Başka bir dosya oluştu ve yine 'file' ile kontrol ettim.

```r
bandit12@bandit:/tmp/myname123$ mv dump.txt dump.bz2
bandit12@bandit:/tmp/myname123$ bzip2 -d dump.bz2
bandit12@bandit:/tmp/myname123$ file *
data.txt: ASCII text
dump:     gzip compressed data, was "data4.bin", last modified: Sun Apr 23 18:04:23 2023, 
max compression, from Unix, original size modulo 2^32 20480
```

Görünüşe göre tekrar gzip kullanıldı. Bu şekilde defalarca sıkıştırılan dosyayı aynı işlemleri yukarıda yaptığım şekilde devam ettirdim. En sonunda 'data8' ASCII text dosyası olarak okunabilir bir hâle geldi.

```r
bandit12@bandit:/tmp/myname123$ mv dump dump.gz
bandit12@bandit:/tmp/myname123$ gunzip dump.gz
bandit12@bandit:/tmp/myname123$ file *
data.txt: ASCII text
dump:     POSIX tar archive (GNU)
bandit12@bandit:/tmp/myname123$ mv dump dump.tar
bandit12@bandit:/tmp/myname123$ tar -xf dump.tar
bandit12@bandit:/tmp/myname123$ file *
data5.bin: POSIX tar archive (GNU)
data.txt:  ASCII text
dump.tar:  POSIX tar archive (GNU)
bandit12@bandit:/tmp/myname123$ mv data5.bin data5.tar
bandit12@bandit:/tmp/myname123$ tar -xf data5.tar 
bandit12@bandit:/tmp/myname123$ file *
data5.tar: POSIX tar archive (GNU)
data6.bin: bzip2 compressed data, block size = 900k
data.txt:  ASCII text
dump.tar:  POSIX tar archive (GNU)
bandit12@bandit:/tmp/myname123$ mv data6.bin data6.bz2
bandit12@bandit:/tmp/myname123$ bzip2 -d data6.bz2
bandit12@bandit:/tmp/myname123$ file *
data5.tar: POSIX tar archive (GNU)
data6:     POSIX tar archive (GNU)
data.txt:  ASCII text
dump.tar:  POSIX tar archive (GNU)
bandit12@bandit:/tmp/myname123$ mv data6 data6.tar
bandit12@bandit:/tmp/myname123$ tar -xf data6.tar
bandit12@bandit:/tmp/myname123$ file *
data5.tar: POSIX tar archive (GNU)
data6.tar: POSIX tar archive (GNU)
data8.bin: gzip compressed data, was "data9.bin", last modified: Sun Apr 23 18:04:23 2023, 
max compression, from Unix, original size modulo 2^32 49
data.txt:  ASCII text
dump.tar:  POSIX tar archive (GNU)
bandit12@bandit:/tmp/myname123$ mv data8.bin data8.gz
bandit12@bandit:/tmp/myname123$ gunzip data8.gz
bandit12@bandit:/tmp/myname123$ file *
data5.tar: POSIX tar archive (GNU)
data6.tar: POSIX tar archive (GNU)
data8:     ASCII text
data.txt:  ASCII text
dump.tar:  POSIX tar archive (GNU)
bandit12@bandit:/tmp/myname123$ cat data8
The password is wbWdlBxEir4CaE8LaPhauuOo6pwRmrDw
```

**Kullanıcı adı:** `bandit13`

**Şifre:** `wbWdlBxEir4CaE8LaPhauuOo6pwRmrDw`

## Bandit Level 13 → Level 14

http://overthewire.org/wargames/bandit/bandit14.html

İpucuda bahsettiği üzere SSH anahtarını sshkey.private adlı bir dosyada gördüm. 'bandit14' kullanıcısının profiline girmek için bu anahtarı kullanmalıyız. Bunun için 'ssh' komutunu '-i' parametresini kullanarak çalıştırdım. Ardından söylenildiği üzere '/etc/bandit_pass/bandit14' dosyasını okuyarak şifreyi yazdırdım.

```r
bandit13@bandit:~$ ssh -i sshkey.private bandit14@bandit.labs.overthewire.org -p 2220
bandit14@bandit:~$ whoami
bandit14
bandit14@bandit:~$ cat /etc/bandit_pass/bandit14
fGrHPx402xGC7U7rXKDaxiWFTOiF0ENq
```

**Kullanıcı adı:** `bandit14`

**Şifre:** `fGrHPx402xGC7U7rXKDaxiWFTOiF0ENq`

## Bandit Level 14 → Level 15

http://overthewire.org/wargames/bandit/bandit15.html

İpucuya göre localhost'ta 30000 numaralı port'tan bağlantı kurmamız ve elimizdeki şifreyi göndermemiz gerekiyor. Bunu yapmak için 'nc' çalıştırdım. Bağlandığımı görünce 'bandit14' şifresini yapıştırdım ve ardından şifreyi aldım.

```r
bandit14@bandit:~$ nc localhost 30000
fGrHPx402xGC7U7rXKDaxiWFTOiF0ENq
Correct!
jN2kgmIXJ6fShzhT2avhotn4Zcka6tnt
```

**Kullanıcı adı:** `bandit15`

**Şifre:** `jN2kgmIXJ6fShzhT2avhotn4Zcka6tnt`

## Bandit Level 15 → Level 16

http://overthewire.org/wargames/bandit/bandit16.html

Soruyla ilgili bilgileri çevirdiğimde --> 

Faydalı not: "KALP ATIYOR" ve "R BLOKUNU Oku" alıyor musunuz? -ign_eof kullanın ve kılavuz sayfasındaki "BAĞLANTILI KOMUTLAR" bölümünü okuyun. "R" ve "Q" ile birlikte, "B" komutu da o komutun bu versiyonunda çalışıyor...

'openssl' ile 30001 bağlantı noktasına bağlanmayı denedim ve bandit15'in şifresini yazdım ve bandit16'ın şifresini buldum. 'ign_eof' kullanmamız gerektiği söylense de, onu kullanmadan da şifreye ulaşabildik. Ancak genel anlamda 'ign_eof' veri girişini sonlandırmadan devam etmemizi sağlıyor. man page: "Girişin sonuna ulaşıldığında bağlantının kapatılmasını engeller."

```r
bandit15@bandit:~$ openssl s_client -connect localhost:30001
...

---
read R BLOCK
jN2kgmIXJ6fShzhT2avhotn4Zcka6tnt
Correct!
JQttfApK4SeyHwDlI9SXGR50qclOAil1

closed
```

**Kullanıcı adı:** `bandit16`

**Şifre:** `JQttfApK4SeyHwDlI9SXGR50qclOAil1`

