---
title: Typography
date: 2024-01-14T09:03:00Z
summary: This guide provides information on the representation of Markdown Syntax used while writing within the presented theme.
draft: false
toc: true
type: post
author: Archieve
categories:
  - Documentation
tags:
  - hugo
  - guide
  - documentation
---

## Headings

```md
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading
```

### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading


## Emphasis

```md
This is **bold** text.
This is _italic_ text.
This is ***bold and italic*** text.
This is ~~strikethrough~~ text.
This is <mark>highlighted</mark> text.
This is `marked` text. || This is <code>marked</code> text. || This is {{</* highlight md "hl_inline=true" */>}}marked{{</* /highlight */>}} text.
```

This is **bold** text.

This is _italic_ text.

This is ***bold and italic*** text.

This is ~~strikethrough~~ text.

This is <mark>highlighted</mark> text.

This is `marked` text.


### Horizontal Lines

`___`, `---`, `***` three of them create the `<hr>` tag

___
---
***


## Abbr

```md
{{</* abbr "HTML" "HyperText Markup Language" */>}}
```

{{< abbr "HTML" "HyperText Markup Language" >}}


## Color Text

```md
This is a {{</* color color="blue" text="blue text" */>}}
This is a <span style="color:blue">blue text</span>

This is a {{</* color color="green" text="green text" */>}}
This is a <span style="color:green">green text</span>

This is a {{</* color color="red" text="red text" */>}}
This is a <span style="color:red">red text</span>

This is a {{</* color color="orange" text="orange text" */>}}
This is a <span style="color:orange">orange text</span>

This is a {{</* color color="theme" text="theme text" */>}}
This is a <span>theme text</span>
```

This is a {{< color color="blue" text="blue text" >}}

This is a {{< color color="green" text="green text" >}}

This is a {{< color color="red" text="red text" >}}

This is a {{< color color="orange" text="orange text" >}}

This is a {{< color text="theme text" >}}


## Buttons

```md
{{</* button content="Small" size="small" link="#" */>}}
{{</* button content="Normal" size="normal" link="#" */>}}
{{</* button content="Large" size="large" link="#" */>}}

{{</* button color="red" icon="bx bxs-home" content="Red Button" link="#" */>}}
{{</* button color="green" icon="bx bxs-car" content="Green Button" link="#" */>}}
{{</* button color="blue" icon="bx bxs-book" content="Blue Button" link="#" */>}}
{{</* button color="orange" icon="bx bxs-search" content="Orange Button" link="#" */>}}

{{</* button-outline color="red" icon="bx bxs-home" content="Red Button" link="#" */>}}
{{</* button-outline color="green" icon="bx bxs-car" content="Green Button" link="#" */>}}
{{</* button-outline color="blue" icon="bx bxs-book" content="Blue Button" link="#" */>}}
{{</* button-outline color="orange" icon="bx bxs-search" content="Orange Button" link="#" */>}}
```

{{< button content="Small" size="small" link="#" >}}
{{< button content="Normal" size="normal" link="#" >}}
{{< button content="Large" size="large" link="#" >}}

{{< button color="red" icon="bx bxs-home" content="Red Button" link="#" >}}
{{< button color="green" icon="bx bxs-car" content="Green Button" link="#" >}}
{{< button color="blue" icon="bx bxs-book" content="Blue Button" link="#" >}}
{{< button color="orange" icon="bx bxs-search" content="Orange Button" link="#" >}}

{{< button-outline color="red" icon="bx bxs-home" content="Red Button" link="#" >}}
{{< button-outline color="green" icon="bx bxs-car" content="Green Button" link="#" >}}
{{< button-outline color="blue" icon="bx bxs-book" content="Blue Button" link="#" >}}
{{< button-outline color="orange" icon="bx bxs-search" content="Orange Button" link="#" >}}


## Notice

```md
{{</* notice info */>}}
This is a notice info text.
{{</* /notice */>}}

{{</* notice success */>}}
This is a notice success text.
{{</* /notice */>}}

{{</* notice warning */>}}
This is a notice warning text.
{{</* /notice */>}}

{{</* notice error */>}}
This is a notice error text.
{{</* /notice */>}}
```

{{< notice info >}}
This is a notice info text.
{{< /notice >}}

{{< notice success >}}
This is a notice success text.
{{< /notice >}}

{{< notice warning >}}
This is a notice warning text.
{{< /notice >}}

{{< notice error >}}
This is a notice error text.
{{< /notice >}}


## Text Layout

{{< text-layout "left" "Text left" >}}
{{< text-layout "center" "Text center" >}}
{{< text-layout "right" "Text right" >}}


## Lists

### Unordered Lists

```md
- Item 1
- Item 2
- Item 3
```

- Item 1
- Item 2
- Item 3

### Ordered Lists

```md
1. First item
2. Second item
3. Third item
```

1. First item
2. Second item
3. Third item

### Nesting Lists

```md
- Main item
    - Sub-item 1
    - Sub-item 2
        - Sub-sub-item 1
        - Sub-sub-item 2
    - Sub-item 3
- Another main item
```

- Main item
    - Sub-item 1
    - Sub-item 2
        - Sub-sub-item 1
        - Sub-sub-item 2
    - Sub-item 3
- Another main item

### Task Lists

```md
- [ ] Task 1
  - [x] Task 1.1 (completed)
  - [ ] Task 1.2
- [ ] Task 2
- [x] Task 3
```

- [ ] Task 1
  - [x] Task 1.1 (completed)
  - [ ] Task 1.2
- [ ] Task 2
- [x] Task 3


### Definition Lists

```md
**Term 1:**
: Definition of Term 1

**Term 2:**
: Definition of Term 2
```

**Term 1:**
: Definition of Term 1

**Term 2:**
: Definition of Term 2


## Quote

```md
{{</* quote author="Albert Einstein" */>}}
There are only two ways to live your life. One is as though nothing is a miracle. The other is as though everything is a miracle.
{{</* /quote */>}}
```

{{< quote author="Albert Einstein" >}}
There are only two ways to live your life. One is as though nothing is a miracle. The other is as though everything is a miracle.
{{< /quote >}}

### Blockquotes

```md
> Blockquotes can also be nested...
> > ...by using additional greater-than signs right next to each other or with spaces between arrows.

<!-- or -->

{{</* quote author="Someone" */>}}
Blockquotes can also be nested...
> ...by using additional greater-than signs right next to each other or with spaces between arrows.
{{</* /quote */>}}
```

{{< quote author="Someone" >}}
Blockquotes can also be nested...
> ...by using additional greater-than signs right next to each other or with spaces between arrows.
{{< /quote >}}


## Icons & Emojies

```md
<i class='bx bx-rotate-left'></i>
<i class='bx bx-check'></i>
<i class='bx bx-download'></i>
<i class='bx bx-star'></i>
<i class='bx bx-file'></i>
<i class='bx bxs-camera-movie'></i>
<i class='bx bxs-lock-alt'></i>
<i class='bx bxl-windows'></i>
<i class='bx bxl-firefox'></i>
<i class='bx bxl-docker'></i>
<i class='bx bx-code'></i>
<i class='bx bx-code-curly'></i>
😀 😃 😄 😁 😆 😅 😂 🤣 🙂 🙃 😌 😍 🥰 😋 😛 😝 😜 🤪 🤨 🧐 🤓 😎 
```

<i class='bx bx-rotate-left'></i>
<i class='bx bx-check'></i>
<i class='bx bx-download'></i>
<i class='bx bx-star'></i>
<i class='bx bx-file'></i>
<i class='bx bxs-camera-movie'></i>
<i class='bx bxs-lock-alt'></i>
<i class='bx bxl-windows'></i>
<i class='bx bxl-firefox'></i>
<i class='bx bxl-docker'></i>
<i class='bx bx-code'></i>
<i class='bx bx-code-curly'></i>
😀 😃 😄 😁 😆 😅 😂 🤣 🙂 🙃 😌 😍 🥰 😋 😛 😝 😜 🤪 🤨 🧐 🤓 😎 


## Table

```md
Syntax     | Description | Test Text   
-----------|-------------|-------------------------------
Header     | Title       | Here's this
Paragraph  | Text        | And more
```

Syntax     | Description | Test Text   
-----------|-------------|-------------------------------
Header     | Title       | Here's this
Paragraph  | Text        | And more


## Links

```md
[Link Text](https://gitlab.com/c4pt-mqs/myarchieve "Title of text")
Autoconverted link: https://gitlab.com/c4pt-mqs/myarchieve
<a href="http://www.youtube.com/watch?v=dQw4w9WgXcQ">
{{</* img src="https://img.youtube.com/vi/dQw4w9WgXcQ/0.jpg" alt="Description of image 3" loading="lazy" width="100%" height="auto" */>}}
</a>
```

[Link Text](https://gitlab.com/c4pt-mqs/myarchieve "Title of text")

Autoconverted link: https://gitlab.com/c4pt-mqs/myarchieve

Image Link:

<a href="http://www.youtube.com/watch?v=dQw4w9WgXcQ">
{{< figure src="https://img.youtube.com/vi/dQw4w9WgXcQ/0.jpg" alt="Description of image 3" loading="lazy" width="60%" height="auto" >}}
</a>


### Ref Links

```md
[Shortcodes]({{</* ref "shortcodes.md" */>}})
[Shortcodes - Carousel]({{</* relref "shortcodes/#carousel" */>}})
```

[Shortcodes]({{< ref "shortcodes.md" >}})

[Shortcodes - Carousel]({{< relref "shortcodes/#carousel" >}})


### Footnotes

```md
This is a sentence with a footnote[^1]. Here is some more text.
[^1]: This is the footnote text. It provides additional information or clarification related to the preceding sentence.
```

This is a sentence with a footnote[^1]. Here is some more text.

[^1]: This is the footnote text. It provides additional information or clarification related to the preceding sentence.


## Image

```md
{{</* img src="/img/content/examples/image_1.jpeg" alt="Description of image 1" loading="lazy" width="45%" height="auto" */>}}
{{</* img src="/img/content/examples/image_2.jpeg" alt="Description of image 2" loading="lazy" width="54%" height="auto" */>}}
{{</* img src="/img/content/examples/image_3.jpg" alt="Description of image 2" loading="lazy" width="100%" height="auto" */>}}
```

{{< figure src="/img/content/examples/image_1.jpeg" alt="Description of image 1" loading="lazy" width="45%" height="auto" >}}
{{< figure src="/img/content/examples/image_2.jpeg" alt="Description of image 2" loading="lazy" width="54%" height="auto" >}}
{{< figure src="/img/content/examples/image_3.jpg" alt="Description of image 2" loading="lazy" width="100%" height="auto" >}}


## Audio

```md
{{</* audio name="sample_song" type="mp3" */>}}
```

{{< audio name="sample_song" type="mp3" >}}


## Video

```md
{{</* video "/video/sample_video.mp4" "100%" "auto" */>}}
```

{{< video "/video/sample_video.mp4" "100%" "auto" >}}


## PDF

```md
{{</* pdf "/pdf/sample.pdf" */>}}
```

{{< pdf "/pdf/sample.pdf" >}}


## Download File

```md
[Open PDF](/pdf/sample.pdf)
[Open Video](/video/sample_video.mp4)
```

[Open PDF](/pdf/sample.pdf)

[Open Video](/video/sample_video.mp4)

## Comments

[Markdown Syntax Guide for Comments](/typography/#markdown-syntax-guide-for-comments)