---
title: Shortcodes
date: 2024-01-14T09:02:00Z
summary: This guide is for exploring alternative views that encompass various features within the presented theme, offering a multitude of options to suit your preferences.
draft: false
toc: true
type: post
author: Archieve
categories:
  - Documentation
tags:
  - hugo
  - guide
  - documentation
  - shortcodes
---

## Progress bar

```md
{{</* progress 999.99 theme "Name of progress bar" */>}}
{{</* progress 80 green "Name of progress bar" */>}}
{{</* progress 77.54 red "Name of progress bar" */>}}
{{</* progress 123.45 blue "Name of progress bar" */>}}
{{</* progress 11.11 orange "Name of progress bar" */>}}
```

{{< progress 999.99 theme "Name of progress bar" >}}

{{< progress 80 green "Name of progress bar" >}}

{{< progress 77.54 red "Name of progress bar" >}}

{{< progress 123.45 blue "Name of progress bar" >}}

{{< progress 11.11 orange "Name of progress bar" >}}


## Accordion

```md
{{</* accordion "Foods" open */>}}
* Vegetables
* Fruits
* Fish
{{</* /accordion */>}}

{{</* accordion "Travel Destinations" false */>}}
- Paris
- Tokyo
- New York City
- Sydney
{{</* /accordion */>}}
```

{{< accordion "Foods" open >}}
* Vegetables
* Fruits
* Fish
{{< /accordion >}}

{{< accordion "Travel Destinations" false >}}
- Paris
- Tokyo
- New York City
- Sydney
{{< /accordion >}}

## Divided Line

```md
{{</* line "This is the divided line text" */>}}
```

{{< line "This is the divided line text" >}}

```md
{{</* line */>}}
```

{{< line >}}


## Hidden Text

```md
{{</* blur "This text is blurred" */>}}
This is a {{</* blur "Hide text" */>}}

```

{{< blur "This text is blurred" >}}

This is a {{< blur "Hide text" >}}


## Spoiler

``````md
{{</* spoiler name="Hello World" */>}}
```py
print("Hello World!")
```
{{</* /spoiler */>}}
``````

{{< spoiler name="Hello World" >}}
```py
print("Hello World!")
```
{{< /spoiler >}}


## Links Card

```md
{{</* link-card title="This is an example link card" url="https://picsum.photos" desc="This is an example link card. This is an example link card." img="https://picsum.photos/1920/1080/?random=1" */>}}
{{</* link-card title="This is an example link card" url="https://picsum.photos" desc="This is an example link card." */>}}
```

{{< link-card title="This is an example link card" url="https://picsum.photos" desc="This is an example link card. This is an example link card" img="https://picsum.photos/1920/1080/?random=1" >}}

{{< link-card title="This is an example link card" url="https://picsum.photos" desc="This is an example link card." >}}


## Image Gallery

### Basic Gallery

```md
{{</* gallery columns="3" */>}}
  {{</* gallery-image src="https://images.unsplash.com/photo-1506905925346-21bda4d32df4" caption="Majestic mountain peaks in the Dolomites" */>}}
  {{</* gallery-image src="https://images.unsplash.com/photo-1494500764479-0c8f2919a3d8" caption="Tranquil lake in the Canadian Rockies" */>}}
  {{</* gallery-image src="https://images.unsplash.com/photo-1441974231531-c6227db76b6e" caption="Sunlight streaming through forest canopy" */>}}
  {{</* gallery-image src="https://images.unsplash.com/photo-1518495973542-4542c06a5843" caption="Dramatic ocean waves at sunset" */>}}
  {{</* gallery-image src="https://images.unsplash.com/photo-1509316785289-025f5b846b35" caption="Desert sand dunes at twilight" */>}}
  {{</* gallery-image src="https://images.unsplash.com/photo-1454496522488-7a8e488e8606" caption="Snow-covered peaks in the Alps" */>}}
{{</* /gallery */>}}
```

{{< gallery columns="3" >}}
  {{< gallery-image src="https://images.unsplash.com/photo-1506905925346-21bda4d32df4" caption="Majestic mountain peaks in the Dolomites" >}}
  {{< gallery-image src="https://images.unsplash.com/photo-1494500764479-0c8f2919a3d8" caption="Tranquil lake in the Canadian Rockies" >}}
  {{< gallery-image src="https://images.unsplash.com/photo-1441974231531-c6227db76b6e" caption="Sunlight streaming through forest canopy" >}}
  {{< gallery-image src="https://images.unsplash.com/photo-1518495973542-4542c06a5843" caption="Dramatic ocean waves at sunset" >}}
  {{< gallery-image src="https://images.unsplash.com/photo-1509316785289-025f5b846b35" caption="Desert sand dunes at twilight" >}}
  {{< gallery-image src="https://images.unsplash.com/photo-1454496522488-7a8e488e8606" caption="Snow-covered peaks in the Alps" >}}
{{< /gallery >}}

### Custom Layout

```md
{{</* gallery columns="4" spacing="20" */>}}
  {{</* gallery-image src="https://images.unsplash.com/photo-1449824913935-59a10b8d2000" caption="Urban architecture" */>}}
  {{</* gallery-image src="https://images.unsplash.com/photo-1514565131-fce0801e5785" caption="City lights" */>}}
  {{</* gallery-image src="https://images.unsplash.com/photo-1518391846015-55a9cc003b25" caption="Glass and steel skyscrapers" */>}}
  {{</* gallery-image src="https://images.unsplash.com/photo-1599927438208-ddd6f9764ae6" caption="Vibrant street life" */>}}
{{</* /gallery */>}}
```

{{< gallery columns="4" spacing="20" >}}
  {{< gallery-image src="https://images.unsplash.com/photo-1449824913935-59a10b8d2000" caption="Urban architecture" >}}
  {{< gallery-image src="https://images.unsplash.com/photo-1514565131-fce0801e5785" caption="City lights" >}}
  {{< gallery-image src="https://images.unsplash.com/photo-1518391846015-55a9cc003b25" caption="Glass and steel skyscrapers" >}}
  {{< gallery-image src="https://images.unsplash.com/photo-1599927438208-ddd6f9764ae6" caption="Vibrant street life" >}}
{{< /gallery >}}

### With Thumbnails

```md
{{</* gallery columns="3" spacing="15" */>}}
  {{</* gallery-image 
    src="https://images.unsplash.com/photo-1462275646964-a0e3386b89fa" 
    thumbnail="https://images.unsplash.com/photo-1462275646964-a0e3386b89fa?w=500" 
    caption="Cherry blossoms" */>}}
  {{</* gallery-image 
    src="https://images.unsplash.com/photo-1434725039720-aaad6dd32dfe" 
    thumbnail="https://images.unsplash.com/photo-1434725039720-aaad6dd32dfe?w=500" 
    caption="Tropical waterfall" */>}}
  {{</* gallery-image 
    src="https://images.unsplash.com/photo-1432405972618-c60b0225b8f9" 
    thumbnail="https://images.unsplash.com/photo-1432405972618-c60b0225b8f9?w=500" 
    caption="Crystal clear mountain stream" */>}}
{{</* /gallery */>}}
```

{{< gallery columns="3" spacing="15" >}}
  {{< gallery-image 
    src="https://images.unsplash.com/photo-1462275646964-a0e3386b89fa" 
    thumbnail="https://images.unsplash.com/photo-1462275646964-a0e3386b89fa?w=500" 
    caption="Cherry blossoms" >}}
  {{< gallery-image 
    src="https://images.unsplash.com/photo-1434725039720-aaad6dd32dfe" 
    thumbnail="https://images.unsplash.com/photo-1434725039720-aaad6dd32dfe?w=500" 
    caption="Tropical waterfall" >}}
  {{< gallery-image 
    src="https://images.unsplash.com/photo-1432405972618-c60b0225b8f9" 
    thumbnail="https://images.unsplash.com/photo-1432405972618-c60b0225b8f9?w=500" 
    caption="Crystal clear mountain stream" >}}
{{< /gallery >}}

### Parameters

For the `gallery` shortcode:
- `columns`: Number of columns (2-4, default: 3)
- `spacing`: Gap between images in pixels (default: 10)
- `class`: Additional CSS classes

For `gallery-image` shortcode:
- `src`: Path to full-size image
- `thumbnail`: Path to thumbnail image (optional)
- `caption`: Image caption (optional)
- `alt`: Alternative text (defaults to caption if not specified)

### Carousel Gallery

```md
{{</* carousel height="500" unit="px" items="3" duration="2000" images="https://picsum.photos/1920/1080/?random=1,https://picsum.photos/1920/1080/?random=2,https://picsum.photos/1920/1080/?random=5,https://picsum.photos/1920/1080/?random=4" */>}}
```

{{< carousel height="500" unit="px" items="3" duration="2000" images="https://picsum.photos/1920/1080/?random=1,https://picsum.photos/1920/1080/?random=2,https://picsum.photos/1920/1080/?random=5,https://picsum.photos/1920/1080/?random=4" >}}


## OpenSeadragon

**Checkout The Project:** [OpenSeadragon DZI Converter]({{< ref "OpenSeadragon-DZI-Converter.md" >}})

```md
{{</* dzi id="seadragon-viewer" url="/dzi/mountain_files/" format="jpg" imgWidth="15920" imgHeight="9800" */>}}
```

{{< dzi id="seadragon-viewer" url="/dzi/mountain_files/" format="jpg" imgWidth="15920" imgHeight="9800" >}}


## Syntax HighLighting

``````md
```py
WRITE THE CODE HERE
```
``````

```py
import random


def askForGuess():
    while True:
        guess = input('> ')  # Enter the guess.

        if guess.isdecimal():
            return int(guess)  # Convert string guess to an integer.
        print('Please enter a number between 1 and 100.')


print('Guess the Number, by Al Sweigart al@inventwithpython.com')
print()
secretNumber = random.randint(1, 100)  # Select a random number.
print('I am thinking of a number between 1 and 100.')

for i in range(10):  # Give the player 10 guesses.
    print('You have {} guesses left. Take a guess.'.format(10 - i))

    guess = askForGuess()
    if guess == secretNumber:
        break  # Break out of the for loop if the guess is correct.

    # Offer a hint:
    if guess < secretNumber:
        print('Your guess is too low.')
    if guess > secretNumber:
        print('Your guess is too high.')

# Reveal the results:
if guess == secretNumber:
    print('Yay! You guessed my number!')
else:
    print('Game over. The number I was thinking of was', secretNumber)
```

### Syntax HighLighting With Line Numbers

Highlighting is carried out via the built-in [highlight shortcode](https://gohugo.io/content-management/shortcodes/#highlight). It takes exactly one required parameter for the programming language to be highlighted and requires a closing shortcode.

- `linenos`: Configures line numbers. Valid values are `true`, `false`, `table`, or `inline`. `false` will turn off line numbers if it's configured to be on in the site configuration. `table` will give copy-and-paste-friendly code blocks.

- `hl_lines`: Lists a set of line numbers or line number ranges to be highlighted.

- `linenostart=199`: Starts the line number count from 199.

- `anchorlinenos`: Configures anchors on line numbers. Valid values are `true` or `false`.

- `lineanchors`: Configures a prefix for the anchors on line numbers. It will be suffixed with a dash (-), so linking to line number 1 with the option `lineanchors=prefix` adds the anchor `prefix-1` to the page.

- `hl_inline`: Highlights inside a `<code>` (inline HTML element) tag. Valid values are `true` or `false`. The code tag will get a class with the name `code-inline`.

``````md
{{</* highlight html "linenos=table,hl_lines=11-15,linenostart=1,title='index.html'" */>}}
WRITE THE CODE HERE
{{</* / highlight */>}}

// or

```html {linenos=table,hl_lines=["11-15"],linenostart=1,title="index.html"}
WRITE THE CODE HERE
```
``````

```html {linenos=table,hl_lines=["11-15"],linenostart=1,title="index.html"}
<!DOCTYPE html>
<html>
<body>

    <h2>JavaScript Statements</h2>

    <p>Multiple statements on one line are allowed.</p>

    <p id="demo1"></p>

    <script>
        let a, b, c;
        a = 5; b = 6; c = a + b;
        document.getElementById("demo1").innerHTML = c;
    </script>

</body>
</html>
```


## Tabs

``````md
{{</* tabs "Languages" */>}}

{{</* tab "JS" */>}}
```js
console.log("This is the JS tab.")
```
{{</* /tab */>}}

{{</* tab "Python" */>}}
```py
print("This is the Python tab.")
```
{{</* /tab */>}}

{{</* tab "C" */>}}
```c
#include <stdio.h>

int main()
{
	printf("This is the C tab.");
	return 0;
}
```
{{</* /tab */>}}

{{</* tab "Java" */>}}
```java
public class Main {
    public static void main(String[] args) {
        System.out.println("This is the Java tab.");
    }
}
```
{{</* /tab */>}}

{{</* /tabs */>}}
``````

{{< tabs "Languages" >}}

{{< tab "JS" >}}
```js
console.log("This is the JS tab.")
```
{{< /tab >}}

{{< tab "Python" >}}
```py
print("This is the Python tab.")
```
{{< /tab >}}

{{< tab "C" >}}
```c
#include <stdio.h>

int main()
{
	printf("This is the C tab.");
	return 0;
}
```
{{< /tab >}}

{{< tab "Java" >}}
```java
public class Main {
    public static void main(String[] args) {
        System.out.println("This is the Java tab.");
    }
}
```
{{< /tab >}}

{{< /tabs >}}


## Diagrams

Feel free to explore the Mermaid.js documentation for more diagram types and customization options: https://mermaid-js.github.io/mermaid/#/

There are some complex diagrams that you can use with the Mermaid.js shortcode:

### Flowchart

```md
{{</* diagram */>}}
flowchart TD
    Start --> Input
    Input --> Condition
    Condition -- Yes --> Process1
    Condition -- No --> Process2
    Process1 --> Process3
    Process2 --> Process4
    Process3 --> Process5
    Process4 --> Process5
    Process5 --> Output
    Output --> End
{{</* /diagram */>}}
```

{{< diagram >}}
flowchart TD
    Start --> Input
    Input --> Condition
    Condition -- Yes --> Process1
    Condition -- No --> Process2
    Process1 --> Process3
    Process2 --> Process4
    Process3 --> Process5
    Process4 --> Process5
    Process5 --> Output
    Output --> End
{{< /diagram >}}

### Sequence Diagram

```md
{{</* diagram */>}}
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>Bob: Request
    Bob->>Alice: Response
{{</* /diagram */>}}
```

{{< diagram >}}
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>Bob: Request
    Bob->>Alice: Response
{{< /diagram >}}

### Gantt Chart

```md
{{</* diagram */>}}
gantt
    title Project Timeline
    dateFormat  YYYY-MM-DD
    section Phase 1
    Task 1           :a1, 2023-06-01, 10d
    Task 2           :a2, 2023-06-12, 5d
    section Phase 2
    Task 3           :a3, 2023-06-19, 8d
    Task 4           :a4, 2023-06-28, 3d
{{</* /diagram */>}}
```

{{< diagram >}}
gantt
    title Project Timeline
    dateFormat  YYYY-MM-DD
    section Phase 1
    Task 1           :a1, 2023-06-01, 10d
    Task 2           :a2, 2023-06-12, 5d
    section Phase 2
    Task 3           :a3, 2023-06-19, 8d
    Task 4           :a4, 2023-06-28, 3d
{{< /diagram >}}

### Class Diagram

```md
{{</* diagram */>}}
---
title: Animal example
---
classDiagram
    note "From Duck till Zebra"
    Animal <|-- Duck
    note for Duck "can fly\ncan swim\ncan dive\ncan help in debugging"
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
        +String beakColor
        +swim()
        +quack()
    }
    class Fish{
        -int sizeInFeet
        -canEat()
    }
    class Zebra{
        +bool is_wild
        +run()
    }
{{</* /diagram */>}}
```

{{< diagram >}}
---
title: Animal example
---
classDiagram
    note "From Duck till Zebra"
    Animal <|-- Duck
    note for Duck "can fly\ncan swim\ncan dive\ncan help in debugging"
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
        +String beakColor
        +swim()
        +quack()
    }
    class Fish{
        -int sizeInFeet
        -canEat()
    }
    class Zebra{
        +bool is_wild
        +run()
    }
{{< /diagram >}}


## Charts

```md
{{</* chart "myChart" "line" "{\"labels\": [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\"], \"datasets\": [{ \"label\": \"Sales\", \"data\": [50, 80, 30, 70, 60, 90], \"backgroundColor\": \"rgba(54, 162, 235, 0.2)\", \"borderColor\": \"rgba(54, 162, 235, 1)\", \"borderWidth\": 1, \"pointBackgroundColor\": \"rgba(54, 162, 235, 1)\", \"pointBorderColor\": \"#fff\", \"pointHoverBackgroundColor\": \"#fff\", \"pointHoverBorderColor\": \"rgba(54, 162, 235, 1)\" }, { \"label\": \"Expenses\", \"data\": [40, 60, 45, 55, 70, 50], \"backgroundColor\": \"rgba(255, 99, 132, 0.2)\", \"borderColor\": \"rgba(255, 99, 132, 1)\", \"borderWidth\": 1, \"pointBackgroundColor\": \"rgba(255, 99, 132, 1)\", \"pointBorderColor\": \"#fff\", \"pointHoverBackgroundColor\": \"#fff\", \"pointHoverBorderColor\": \"rgba(255, 99, 132, 1)\" }] }" "{\"responsive\": true, \"scales\": { \"y\": { \"beginAtZero\": true, \"title\": { \"display\": true, \"text\": \"Amount\" } } } }" */>}}
```

{{< chart "myChart" "line" "{\"labels\": [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\"], \"datasets\": [{ \"label\": \"Sales\", \"data\": [50, 80, 30, 70, 60, 90], \"backgroundColor\": \"rgba(54, 162, 235, 0.2)\", \"borderColor\": \"rgba(54, 162, 235, 1)\", \"borderWidth\": 1, \"pointBackgroundColor\": \"rgba(54, 162, 235, 1)\", \"pointBorderColor\": \"#fff\", \"pointHoverBackgroundColor\": \"#fff\", \"pointHoverBorderColor\": \"rgba(54, 162, 235, 1)\" }, { \"label\": \"Expenses\", \"data\": [40, 60, 45, 55, 70, 50], \"backgroundColor\": \"rgba(255, 99, 132, 0.2)\", \"borderColor\": \"rgba(255, 99, 132, 1)\", \"borderWidth\": 1, \"pointBackgroundColor\": \"rgba(255, 99, 132, 1)\", \"pointBorderColor\": \"#fff\", \"pointHoverBackgroundColor\": \"#fff\", \"pointHoverBorderColor\": \"rgba(255, 99, 132, 1)\" }] }" "{\"responsive\": true, \"scales\": { \"y\": { \"beginAtZero\": true, \"title\": { \"display\": true, \"text\": \"Amount\" } } } }" >}}
<br>


## LaTeX | Math

**Here is some inline math:**

```md
{{</* math */>}}\(ax^2 + bx + c = 0\){{</* /math */>}}
```

{{< math >}}\(ax^2 + bx + c = 0\){{< /math >}}

**And here is a displayed equation:**

```md
{{</* math */>}}
$$ E={\sqrt  {p^{2}c^{2}+m^{2}c^{4}}}=\gamma mc^{2} $$

$$ \int_{-\infty}^{\infty} e^{-x^2} \, dx = \sqrt{\pi} $$
{{</* /math */>}}
```

{{< math >}}
$$ E={\sqrt  {p^{2}c^{2}+m^{2}c^{4}}}=\gamma mc^{2} $$

$$ \int_{-\infty}^{\infty} e^{-x^2} \, dx = \sqrt{\pi} $$
{{< /math >}}
