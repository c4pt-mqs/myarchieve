---
title: "RED Team Rules of Engagement (RoE)"
date: 2022-11-03T09:00:00Z
draft: false
toc: true
type: post
author: Anonim
categories:
  - Cyber Security
tags:
  - penetration-testing
  - cybersecurity
  - guide
---

| Section Name | Section Details
|--------------|--------------------
| Executive Summary| An overarching summary of all contents and authorization within the RoE document.    |
| Purpose| Defines the purpose of the RoE document and its intended use.  |
| References  | Any references used throughout the RoE document (e.g., HIPAA, ISO standards).   |
| Scope  | Statement of the agreement to restrictions and guidelines within the engagement.  |
| Definitions  | Definitions of technical terms used throughout the RoE document to ensure clarity.    |
| Rules of Engagement and Support Agreement| Defines obligations of both parties and general technical expectations of engagement conduct.|
| Provisions  | Define exceptions and additional information from the Rules of Engagement.  |
| Requirements, Restrictions, and Authority| Define specific expectations of the red team cell and outline its authority.|
| Ground Rules| Define limitations of the red team cell's interactions and conduct during the engagement.    |
| Resolution of Issues/Points of Contact   | Contains all essential personnel involved in an engagement and points of contact for issue resolution. |
| Authorization    | Statement of authorization for the engagement, indicating approval to proceed.   |
| Approval | Signatures from both parties approving all subsections of the preceding document.|
| Appendix  | Any further information from preceding subsections, such as additional details or supporting documentation.  |

---

## Types of Plans

##### Engagement Plan
An overarching description of technical requirements of the red team.
- CONOPS, Resource and Personnel Requirements, Timelines

##### Operations Plan
An expansion of the Engagement Plan. Goes further into specifics of each detail.
- Operators, Known Information, Responsibilities, etc.

##### Mission Plan
The exact commands to run and execution time of the engagement.
- Commands to run, Time Objectives, Responsible Operator, etc.

##### Remediation Plan
Defines how the engagement will proceed after the campaign is finished.
- Report, Remediation consultation, etc.


### Components and Purposes

**CONOPS (Concept of Operations):**
Non-technically written overview of how the red team meets client objectives and target the client.

**Resource Plan:**
Includes timelines and information required for the red team to be successful—any resource requirements: personnel, hardware, cloud requirements.

**Personnel:**
Information on employee requirements.

**Stopping Conditions:**
How and why should the red team stop during the engagement.

**RoE (optional):**
Technical requirements for the red team's success.

**Command Playbooks (optional):**
Exact commands and tools to run, including when, why, and how. Commonly seen in larger teams with many operators at varying skill levels.

**Execution Times:**
Times to begin stages of engagement. Can optionally include exact times to execute tools and commands.

**Responsibilities/Roles:**
Who does what, when.

**Report:**
Summary of engagement details and report of findings.

**Remediation/Consultation:**
How will the client remediate findings? It can be included in the report or discussed in a meeting between the client and the red team.


## Cyber Kill Chain

**Reconnaissance:**
- No identified TTPs, use internal team methodology

**Weaponization:**
- Command and Scripting Interpreter
  - PowerShell
  - Python
  - VBA
- User executed malicious attachments

**Delivery:**
- Exploit Public-Facing Applications
- Spearphishing

**Exploitation:**
- Registry modification
- Scheduled tasks
- Keylogging
- Credential dumping

**Installation:**
- Ingress tool transfer
- Proxy usage

**Command & Control:**
- Web protocols (HTTP/HTTPS)
- DNS

**Actions on Objectives:**
- Exfiltration over C2


### Best Practices

- **Identify critical information:** Critical information includes, but is not limited to, red team’s intentions, capabilities, activities, and limitations.
- **Analyse threats:** Threat analysis refers to identifying potential adversaries and their intentions and capabilities.
- **Analyse vulnerabilities:** An OPSEC vulnerability exists when an adversary can obtain critical information, analyse the findings, and act in a way that would affect your plans.
- **Assess risks:** Risk assessment requires learning the possibility of an event taking place along with the expected cost of that event.
- **Apply appropriate countermeasures:** Countermeasures are designed to prevent an adversary from detecting critical information, provide an alternative interpretation of critical information or indicators (deception), or deny the adversary’s collection system.


## Steps for Establishing C2 Beaconing

### Stageless Payload
1. The Victim downloads and executes the Dropper.
2. Beaconing to the C2 Server begins.

{{< figure src="/img/content/cyber-security/roe-stageless-payload.png" alt="roe-stageless-payload" loading="lazy" width="75%" height="auto" >}}

### Staged Payload
1. The Victim downloads and executes the Dropper.
2. The Dropper calls back to the C2 Server for Stage 2.
3. The C2 Server sends Stage 2 back to the Victim Workstation.
4. Stage 2 is loaded into memory on the Victim Workstation.
5. C2 Beaconing Initializes, and the Red Teamer/Threat Actors can engage with the Victim on the C2 Server.

{{< figure src="/img/content/cyber-security/roe-staged-payload.png" alt="roe-staged-payload" loading="lazy" width="75%" height="auto" >}}

### C2 Beaconing in Restricted Network Segment

1. The Victims call back to an SMB named pipe on another Victim in a non-restricted network segment.
2. The Victim in the non-restricted network segment calls back to the C2 Server over a standard beacon.
3. The C2 Server then sends commands back to the Victim in the non-restricted network segment.
4. The Victim in the non-restricted network segment then forwards the C2 instructions to the hosts in the restricted segment.

{{< figure src="/img/content/cyber-security/roe-restricted-network-segment.png" alt="roe-restricted-network-segment" loading="lazy" width="75%" height="auto" >}}

### Domain Fronting Process

1. The C2 Operator has a domain that proxies all requests through Cloudflare.
2. The Victim beacons out to the C2 Domain.
3. Cloudflare proxies the request, then looks at the Host header and relays the traffic to the correct server.
4. The C2 Server responds to Cloudflare with the C2 Commands.
5. The Victim then receives the command from Cloudflare.

{{< figure src="/img/content/cyber-security/roe-domain-fronting.png" alt="roe-domain-fronting" loading="lazy" width="75%" height="auto" >}}

### C2 Profiles Process

1. The Victim beacons out to the C2 Server with a custom header in the HTTP request, while a SOC Analyst has a normal HTTP Request.
2. The requests are proxied through Cloudflare.
3. The C2 Server receives the request and looks for the custom header, and then evaluates how to respond based on the C2 Profile.
4. The C2 Server responds to the client and responds to the Analyst/Compromised device.

{{< figure src="/img/content/cyber-security/roe-how-c2-profiles-work.png" alt="roe-how-c2-profiles-work" loading="lazy" width="75%" height="auto" >}}


### CIA and DAD
- **Confidentiality:** Ensures that only the intended persons or recipients can access the data.
- **Integrity:** Aims to ensure that the data cannot be altered; moreover, we can detect any alteration if it occurs.
- **Availability:** Aims to ensure that the system or service is available when needed.

- **Disclosure:** The opposite of confidentiality. In other words, disclosure of confidential data would be an attack on confidentiality.
- **Alteration:** The opposite of Integrity. For example, the integrity of a cheque is indispensable.
- **Destruction/Denial:** The opposite of Availability.

### Additional Concepts
- **Authenticity**
- **Repudiation**
- **Vulnerability**
- **Threat**
- **Risk**
- **Depth**
- **Trust but verify**
- **Zero trust**