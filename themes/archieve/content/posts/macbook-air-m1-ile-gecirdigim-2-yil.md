---
title: "MacBook Air M1 İle Geçirdiğim 2 Yıl"
date: 2023-12-30T18:58:06+03:00
draft: false
toc: false
type: post
author: Kaptan
categories:
  - Teknoloji
tags:
  - donanım
  - teknoloji
  - kullanıcı-deneyimi
---

Bu yazıda, MacBook'un M1 çipine sahip cihazımla olan 2 yıllık deneyimlerimi paylaşmak istiyorum. Eğer bu cihazı ilk defa kullanıyorsanız, endişelenmenize gerek yok çünkü Macbook'un kullanıcı dostu arayüzü size birçok kolaylık sağlayacak.

**Ekran:**

MacBook'un ekranı gerçekten göz yormayan ve yansıma yapmayan bir özellik sunuyor. Ben genel olarak yarı parlaklıkta kullanıyorum ve 10 saat boyunca aktif bir şekilde çalıştığım zamanlarda bile gözlerimde herhangi bir yorgunluk hissetmiyorum. Ekran çözünürlüğünü istediğiniz gibi ayarlayabilir ve herhangi bir bozulma olmadan kullanabilirsiniz. İlk başta boyutlar konusunda endişeleniyorsanız, kısa sürede alıştığınızı göreceksiniz.

**Daha Fazla Güç - Daha Az Enerji:**

Apple'ın sunduğu performans ile ilgili vaatler gerçekten de doğru. M1 çipi, önceki Intel işlemcilere kıyasla çok daha hızlı bir CPU performansı, etkileyici bir GPU performansı ve olağanüstü bir makine öğrenme yeteneği sunuyor. Bu, cihazın gücüne dair etkileyici bir nokta.

**Pil Performansı:**

Pil konusunda MacBook gerçekten sınıfında lider. Wi-Fi bağlantısı aktif, birkaç tarayıcı ve yazılım aracı ile çalıştığım zamanlarda bile %20-%100 aralığında kullanım ile ortalama olarak 11-15 saat pil ömrü elde ediyorum. Bu, mobil çalışma konusunda gerçekten özgürlük sağlıyor. Ayrıca, adaptör kullanımı ve pil ömrü ile ilgili dikkat edilmesi gereken noktalara dair faydalı bir [video](https://www.youtube.com/watch?v=u2e-4oncf5A) gördüm, mutlaka izlemenizi öneririm.

**Fan Sesi Yok:**

Bu bilgisayarın sessiz çalışma özelliği; fan sesinin olmaması bence diğer bilgisayarlara kıysalada bir adım önde gittiğini söyleyebilirim. Performanstan ödün vermeden sessiz bir çalışma ortamı sağlamak, özellikle bilgisayar başında çok fazla zaman geçirenler için önemli bir özellik olduğunu düşünüyorum.

**Dezavantajları:**

- Fiyatı diğer bilgisayarlara göre daha yüksek

- USB-C portları ile uyumlu cihazlar gerekebilir (Harici Hub taşımanızı gerektirdiği durumlar oluyor.)

- Uzun süre boyunca Windows kullanıcısı iseniz beğenmediğiniz bir çok nokta olacak ve sürekli bir Windows cihazı arayışına girmeniz muhtemel olabilir.

Eğer yazılım, grafik tasarım veya siber güvenlik gibi alanlarla ilgileniyorsanız, MacBook M1 çipiyle donatılmış bu cihazın size uygun olabileceğini düşünüyorum. Umarım bu deneyimlerim, MacBook'u daha yakından tanımanıza yardımcı olur.
