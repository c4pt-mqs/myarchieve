---
title: "Ortaokul ve Lisede ki Gençleri için Yazılım Süreci İçin Öneriler"
date: 2023-08-05T18:58:06+03:00
draft: false
toc: false
type: post
author: Kaptan
categories:
  - Fikirler
tags:
  - kişisel-gelişim
  - tavsiye
  - eğitim
---

Erken yaşta olan ortaokul ve lise öğrencileri; üniversiteye atılmadan önce kendinizi belli alanlarda geliştirmeniz, üniversite de daha hızlı yol kat etmenize yardımcı olucaktır.

1. Bir işi yapmaya karar verdiğiniz zaman, o işin alt alanları için de "şunu, şunları" da yapacağım dememelisiniz. Yani her şeyi kendinize yük edinmemelisiniz. Belirlediğiniz alan hakkında çalışma yürütmeye başlamalısınız. Siz zaten kendinizi geliştirmek istediğiniz alanda bir şeyler yapıyorsanız devamı bir şekilde gelicektir.

2. İngilizce bu işin yapı taşı. Üniversite'ye geçmeden önce okuduğunuz bir çok makaleyi anlayacak ve videoları İngilizce altyazı ile rahat bir şekilde izleyebilecek düzeye gelmeniz çok işinize yarayacaktır. Her gün dil öğrenim için zaman ayırırsanız, kısa zamanda iyi bir yol kat edersiniz.

3. Tavsiye niteliğinde olarak; İlk olarak Python programlama dilini küçük projeler üretecek seviyede ilerlettiğinizde, diğer dilleri de rahatlıkla öğrenebilirsiniz. Bir dilde proje üretecek seviyeye gelmeden başka dili öğrenmeye kalkışmayın, elinizde 2 tane yarım kalmış iş sizi rahatsız edicektir.

4. Elinizde kötü bir bilgisayar olabilir veya hiç olmayabilir. Eğer elinizde kötü bir cihaz varsa belli süre daha idare etmeye çalışın. İmkanınız varsa, yazın herhangi bir yerde çalışıp bütçe dahilinde istediğiniz cihazı alabilirsiniz. Bunu kendinize yatırım olarak düşünürseniz, almak için mücadele etmeye çalışırsınız.

5. Bu yapacağınız iş, ciddi disiplin ve azim gerektiriyor. İşin ticari kısmı şu ân kendinizi geliştirmeniz ve ileride yapacağınız projeler ile ilgili. Bir ân da para kazanmanız mümkün değil bu işte. Ancak alanınızda çok iyi işler çıkarıyorsanız, para sizi bulacaktır. Yavaş bile olsa istikrarlı bir şekilde giderseniz, elbette karşılığını alıcaksınız.
