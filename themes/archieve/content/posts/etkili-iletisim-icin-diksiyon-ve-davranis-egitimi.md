---
title: Etkili İletişim İçin Diksiyon ve Davranış Eğitimi
date: 2022-04-13T09:00:00Z
draft: false
type: post
author: Kaptan
categories:
  - Kişisel Gelişim
tags:
  - kişisel-gelişim
  - motivasyon
  - eğitim
---


Diksiyon ve davranış, günlük iletişimimizde önemli bir rol oynar. İnsanlarla etkili bir şekilde iletişim kurabilmek, hem iş hem de kişisel yaşamda başarıyı artırabilir. İşte, diksiyon ve davranış üzerine değerli bilgiler içeren [İsmet Topaloğlu](https://www.youtube.com/@ismettopaloglu9330) videolarından öğrendiklerim:

### İyi Bir Konuşmacı Olmanın Temel İlkeleri

- **Lafınız Kesilmeden Dinlenmek:** İyi bir konuşmacı olmak için, dinleyicinin size kesintisiz bir şekilde odaklanmasını sağlamak önemlidir.

- **Önyargıdan Kaçının:** Öngörüde bulunmak, önyargı yapmamak ve genellemelerden kaçınmak, sağlıklı iletişimin anahtarıdır.

- **Çift Taraflı İletişim:** Konuşmalarınızda, karşılıklı bir iletişim kurun. Bir şey söyledikten sonra dinleyin ve karşılıklı konuşmayı sürdürün.

- **Galat-ı Meşhur Uyarısı:** Yaygın yanlış kullanılan ifadelerden kaçının ve etkili iletişim için doğru kelimeleri seçin.

- **D.N.D (Dudak - Nefes - Duygu):** Etkili konuşmanın temel unsurları arasında dudak, nefes ve duygu önemlidir.

- **Öncelik Tanıma:** İş veya ilişkilerde herkese eşit öncelik tanımanın, etkili iletişim için kritik olduğunu unutmayın.

- **Anlaşılmayı Bekleme, Anlat:** İletişimde beklentilerinizi açıkça ifade edin ve anlamak yerine anlatmaya odaklanın.

- **Niyet ve İstek:** Hedeflerinize ulaşmak için niyet ve isteğinizi koruyun.

**Kaynak:** [Diksiyon ve Davranış Eğitimi 1. Bölüm](https://www.youtube.com/watch?v=6EY8TiUi4cs&list=PL5mvk7gAVgt_ymU8yr-OeyUV3HnwUzBto&index=8&pp=iAQB)

### Duygusal Zeka ve Ses Kullanımı

- **Duygusal Zekaya Önem Verin:** Akıl ile duygular arasındaki dengeyi korumak, etkili bir şekilde iletişim kurmanın anahtarıdır.

- **Güçlü Ses Kullanımı:** Kelimelerinizi dolgun bir şekilde ileterek, bağırmaktan kaçının. Güçlü ses kullanımı etkili iletişimi destekler.

- **Tevazu ve Hödüklük Dengesi:** Hem tevazu göstermek hem de kendinizi ifade etmek arasında dengeyi bulun.

- **Doğru Telaffuz:** Harflerin doğru çıkarılması, kelimelerin doğru telaffuz edilmesini sağlar. Bu, iletişimin anlaşılır olmasını sağlar.

- **Dudaktan Çıkan Harfler:** Harf çıkışlarını doğru yaparak, cümlelerinizi daha etkili hale getirin.

- **Duygusal Zeka Geliştirme:** Duygusal zekayı geliştirmek için farkındalık ve deneyim kazanımına odaklanın.

- **Kolaylıklara Odaklanın:** Kötümserlikten kaçının ve güzelliklere odaklanarak kolaylıklara ulaşmaya çalışın.

**Kaynak:** [Diksiyon ve Davranış Eğitimi 2. Bölüm](https://www.youtube.com/watch?v=vM-E3wCcVms&list=PL5mvk7gAVgt_ymU8yr-OeyUV3HnwUzBto&index=9&pp=iAQB)
