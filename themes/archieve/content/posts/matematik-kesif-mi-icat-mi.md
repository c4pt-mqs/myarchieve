---
title: "Matematik Keşif mi? İcat mı?"
date: 2023-09-11T18:58:06+03:00
draft: false
toc: false
type: post
author: Kaptan
categories:
  - Fikirler
tags:
  - bilim
  - felsefe
  - mantık
---

Bu soruya bir fikir belirtmek için farklı bir yöntem tercih edeceğim. Bu yötemde bilgisayarların 0 ve 1 mantığına dayalı işlem yapabilmesini anlamlandırmak için programlama dillerinin ortaya çıkışı ile ilgili bir örnek kullanacağım:

Bilgisayarlar, temelde elektrik sinyallerinin açık (1) veya kapalı (0) olduğu transistörlerden oluşur. 0 ve 1'ler yapılan işlemlerin dijital formatta anlaşılabilir bir hale gelmesi için sayı sistemlerini kullanılır. Bu sayı sistemleri belli değerler ile eşleştirilerek bizlere okunabilir formatta olan programlama dillerini ortaya çıkarır. Bunun temel amacı insanların bilgisayarlarla daha rahat iletişim kurmasını sağlamak ve yapılan işlemleri insanların anlayabileceği ve yönetilebileceği bir formata dönüştürmektir. Bir nevi ara dil olarak düşünülebilir.

Gelişen bu süreçte insanların karmaşık (matematiksel) işlemleri basit komutlarla ifade etmelerini sağlıyor. İlk programlama dilleri, makine dili olarak adlandırılan düşük seviyeli (low level) dillerdi. Bizler doğrudan bilgisayarın işlemcisini kontrol etmek için kullanıyorduk. Ancak bu diller karmaşıktı ve insanlar için anlaşılması zordu. Buna zamanla bir çözüm getirilerek yüksek seviyeli programlama dilleri geliştirildi. İnsanlar için daha anlaşılır ve okunabilir kodlar yazmak mümkün oldu. Diller, matematiğin mantığını programcılar için daha erişilebilir kılıyorken aynı zamanda yazılım geliştirmeyi hızlandırıyordu. Bununla birlikte daha az hata yapma olasılığı sağladı.

Sonuç olarak; programlama dilleri, matematiğin mantığının icadı ve insanlar için erişilebilir hale getirilmesinin bir sonucudur. Yani matematikte, 0 ve 1'ler gibi evren de mevcuttu ve keşfedildi. Ancak geliştirilip bir matematik bilimi/dili olması programlama dillerinin icadı gibi daha sonradan oldu. Bir sürecin parçası olduğundan aşamalı bir keşif ve dolaylı olarak da bir icat diyebilirim.

(Burada matematiğin aslında 0 ve 1'lerden daha önce var olduğunu biliyoruz. Ancak durumu somut bir dille açıklamak için böyle bir örnek kullanmak istedim.)
