---
title: "Dil Öğrenmenin Zorlukları ve Yanlış Yapılan Uygulamalar"
date: 2023-04-16T18:58:06+03:00
draft: false
toc: false
type: post
author: Anonim
categories:
  - Fikirler
tags:
  - dil-öğrenimi
  - iletişim
  - kişisel-gelişim
---

Dil öğrenme sürecinde zorlanmak tüm insanların yaşadığı bir durum. Dil öğrenmenin altında yatan sebeplerin başında; motivasyon, şevk ve disiplin eksikliği gibi psikolojik faktörler yer alırken, eğitim sisteminde ki yanlış uygulamalar da önemli rol oynamaktadır.

Doğal olarak dili kullanmak yerine, ezbere ve gramer kurallarına dayalı olan yanlış eğitim sistemi, dil öğrenmenin zorluğunu artıran en önemli faktörlerden biridir. Kendi milletimize karşı samimi ve dürüst olamama, emek harcamadan sonuç bekleyen bir yapı, nihai sonuca odaklı olma, sürekli başkalarından yardım bekleyen bir eğitim anlayışımız da dil öğrenme zorlukları arasında yer almaktadır.

Dil öğrenirken gramer kurallarına göre konuşma ve mükemmellik takıntısı gibi yanlış yaklaşımların da öğrenmeyi engellediği bilinmektedir. Dinlemek öğrenmenin doğal yollarından biridir ve mutlaka kullanılması gereken bir yöntemdir.

Bir dilin akademik seviyede öğrenilmesi için, öncelikle ana dilimize hakim olmamız, uzun yazılar yazabilecek ve spesifik konularda konuşabilecek seviyede olmamız gerekmektedir. İletişimsel yaklaşım genellikle en pekiştirici dil öğrenme yöntemidir. Dilin yaşayan bir kavram olduğu için, karşılıklı kullanım, sesli ve mimikli geri bildirim almak temel bir şarttır.

Pratik imkanları olanlar için, pratik yapmak büyük bir avantajdır. Ancak, pratik yapamayanlar da farklı yöntemlerle konuşma akıcılığına ulaşabilirler. Bahane üretmek yerine çözüm odaklı olunmalıdır.

Birçok ülkede gözlemlediğimiz gibi, gereksiz gramere ve test kitaplarına aşırı ağırlık verilerek beyin gereksiz yere iştigal edilmekte, dil kilitlemesi yaşanmakta ve öğrenciler psikolojik problemlerle karşılaşmaktadır. Türkiye'de de benzer sorunlar yaşanmaktadır.