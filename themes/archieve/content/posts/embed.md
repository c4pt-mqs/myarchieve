---
title: Embed
date: 2024-01-14T09:01:00Z
summary: This guide provides instructions on how to view and share embedded code within the presented theme, allowing you to see and share the appearance of the code you wish to showcase.
draft: false
toc: true
type: post
author: Archieve
categories:
  - Documentation
tags:
  - hugo
  - guide
  - documentation
---

## GIPHY


```md
{{</* giphy AcfTF7tyikWyroP0x7 */>}}
```

{{< giphy AcfTF7tyikWyroP0x7 >}}


## Codepen

```md
{{</* codepen razonyang YzWGJNQ */>}}
```

{{< codepen razonyang YzWGJNQ >}}


## JSFiddle

```md
<script async src="//jsfiddle.net/bheng/e8uwgrmt/embed/html,css,js,result/dark/"></script>
```

<script async src="//jsfiddle.net/bheng/e8uwgrmt/embed/html,css,js,result/dark/"></script>

## CodeSandbox

```md
<iframe src="https://codesandbox.io/embed/simple-example-2etqr?fontsize=14&hidenavigation=1&theme=dark"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="Simple example"
     allow="accelerometer; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts">
</iframe>
```

<iframe src="https://codesandbox.io/embed/simple-example-2etqr?fontsize=14&hidenavigation=1&theme=dark"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="Simple example"
     allow="accelerometer; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts">
</iframe>


## asciinema

<!--
Parameters:
     id (#0) : the asciicast ID.
     theme   : asciinema, tango, solarized-dark, solarized-light, monokai.
     autoplay: auto play, default to false.
     speed   : playback speed, default to 1.
     cols    : override width (in characters) of the emulated terminal.
     rows    : override height (in lines) of the emulated terminal.
-->

```md
{{</* asciinema 573031 */>}}
```

{{< asciinema 573031 >}}


## Git

### GitHub

```md
{{</* github "torvalds" "linux" */>}}
```

{{< github "torvalds" "linux" >}}


### GitLab

```md
{{</* gitlab "linux-kernel" "linux" */>}}
```

{{< gitlab "linux-kernel" "linux" >}}


### Gist

```md
{{</* gist jmooring 50a7482715eac222e230d1e64dd9a89b */>}}
```

{{< gist jmooring 50a7482715eac222e230d1e64dd9a89b >}}


## X

```md
{{</* twitter user="GoHugoIO" id="1672203446756188165" */>}}
```

{{< twitter user="GoHugoIO" id="1672203446756188165" >}}


## Instagram

```md
{{</* instagram url="https://www.instagram.com/p/CIO0RUesL8M/" */>}}
```

{{< instagram url="https://www.instagram.com/p/CIO0RUesL8M/" >}}


## Pinterest

```md
{{</* pinterest url="https://pinterest.com/pin/311381761733041625/" */>}}
```

{{< pinterest url="https://pinterest.com/pin/311381761733041625/" >}}


## Spotify

<!--
Parameters:
    type - (Required) album / track / playlist / artist
    id - (Required) Target ID
    width - (Optional) width
    height - (Optional) height
-->

```md
{{</* spotify type="playlist" id="37i9dQZF1DX4sWSpwq3LiO" */>}}
```

{{< spotify type="playlist" id="37i9dQZF1DX4sWSpwq3LiO" >}}


## SoundCloud

```md
<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1242868615&color=%23ff5500&auto_play=false&hide_related=false&show_comments=false&show_user=true&show_reposts=false&show_teaser=true"></iframe>
<div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"></div>
```

<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1242868615&color=%23ff5500&auto_play=false&hide_related=false&show_comments=false&show_user=true&show_reposts=false&show_teaser=true"></iframe>
<div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"></div>


## Youtube

```md
{{</* youtube id="qtIqKaDlqXo?start=120" autoplay="false" title="A New Hugo Site" */>}}
```

{{< youtube id="qtIqKaDlqXo?start=120" autoplay="false" title="A New Hugo Site" >}}


## Vimeo

```md
{{</* vimeo 55073825 */>}}
```

{{< vimeo 55073825 >}}


## Dailymotion

```md
<div style="position:relative;padding-bottom:56.25%;height:0;overflow:hidden;">
  <iframe src="https://geo.dailymotion.com/player.html?video=x82hnx2"
    style="width:100%; height:100%; position:absolute; left:0px; top:0px; overflow:hidden; border:none;"
    allowfullscreen
    title="Dailymotion Video Player"
    allow="web-share">
  </iframe>
</div>
```

<div style="position:relative;padding-bottom:56.25%;height:0;overflow:hidden;">
  <iframe src="https://geo.dailymotion.com/player.html?video=x82hnx2"
    style="width:100%; height:100%; position:absolute; left:0px; top:0px; overflow:hidden; border:none;"
    allowfullscreen
    title="Dailymotion Video Player"
    allow="web-share">
  </iframe>
</div>


## Streamable

```md
{{</* streamable id="qir4uj" autoplay="0" loop="0" */>}}
```

{{< streamable id="qir4uj" autoplay="0" loop="0" >}}


## Google Docs

```md
{{</* google-doc url="https://docs.google.com/document/d/1S3Sjek0ZcR_9M-b_TSmPehDaTAquD3xE72jBKzE9ggw/" */>}}
```

{{< google-doc url="https://docs.google.com/document/d/1S3Sjek0ZcR_9M-b_TSmPehDaTAquD3xE72jBKzE9ggw/" >}}

### Sheets

```md
{{</* google-doc url="https://docs.google.com/spreadsheets/d/1_BHv008JYEqETQjScuWH9tzDVIuhrHLagYk4zJzsH60/" */>}}
```

{{< google-doc url="https://docs.google.com/spreadsheets/d/1_BHv008JYEqETQjScuWH9tzDVIuhrHLagYk4zJzsH60/" >}}

### Slides

```md
{{</* google-doc url="https://docs.google.com/presentation/d/1zmmOtvqf9Xrt4fiR4_Q4rGfgiagPW-6YRTOqXrnZqoE/" */>}}
```

{{< google-doc url="https://docs.google.com/presentation/d/1zmmOtvqf9Xrt4fiR4_Q4rGfgiagPW-6YRTOqXrnZqoE/" >}}

### Forms

```md
{{</* google-doc url="https://docs.google.com/forms/d/e/1FAIpQLSf7XT26ijYpBGbMsUEVCJsDpbnMq6bXSEro1y-rCYExgNkg0Q/" */>}}
```

{{< google-doc url="https://docs.google.com/forms/d/e/1FAIpQLSf7XT26ijYpBGbMsUEVCJsDpbnMq6bXSEro1y-rCYExgNkg0Q/" >}}


## SlideShare

```md
<iframe src="https://www.slideshare.net/slideshow/embed_code/key/2ap6Q0gyN2k4VL?startSlide=1" width="800" height="600" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px;max-width: 100%;" allowfullscreen></iframe>
```

<iframe src="https://www.slideshare.net/slideshow/embed_code/key/2ap6Q0gyN2k4VL?startSlide=1" width="800" height="600" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px;max-width: 100%;" allowfullscreen></iframe>
