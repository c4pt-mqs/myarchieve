---
title: "Docker: Virtualization and Containerization Technology with CLI Commands"
date: 2022-06-13T09:00:00Z
draft: false
toc: true
type: post
author: Kaptan
categories:
  - DevOps
tags:
  - docker
  - devops
  - yazılım-geliştirme
---

## Introduction
Docker is a powerful tool for building, shipping, and running applications within containers. Here are some essential Docker CLI commands to help you navigate Docker efficiently.

**These topics will be add in the future:**

- Docker: What is it?
- Docker Evolution: A Brief History
- Virtualization vs. Containerization
- Understanding Containers and Images
- Docker File Hierarchy
- Docker Volume
- Docker Swarm
- Docker Mount and Bind Mount
- Docker Plugin and Network Driver
- STDIN, STDOUT, STDERR
- Why should I use Docker?


## Getting Started

**Check Docker Version**

```docker
docker version
```

**Docker Information**

```docker
docker info
```

**View Logs**

```docker
docker logs
```

**Docker Management Commands Cleanup**

```docker
docker [Management CMD] prune
```


## Creating and Running Containers

**Searching for a Specific Image**

```docker
docker search [image_name]
```

**Run an Interactive Container**

```docker
docker run -it [image_name]
```

- *("i" means run interactively, and "t" tells Docker to run a shell within the container.)*

**Assign a Custom Name to a Container**

```docker
docker run --name [custom_name] [image_name]
```

**Execute a Command in a Running Container**

```docker
docker exec -it [container_ID] [command]
```

**Start/Stop/Restart Container**

```docker
docker start -ai [container_ID]
docker stop [container_ID]
docker restart [container_ID]
```

**Download Specific Docker Version**

```docker
docker run [redis:5]
```

**Automatically Remove Container After Execution**

```docker
docker run --rm [image_name]
```

**Run Container in Detached Mode**

```docker
docker run -d [image_name]
```

**Copy Docker**

```docker
docker image tag [existing_image_tag] [new_image_name]
```

**List Images**

```docker
docker images
```


## Managing Images and Containers

**Delete Untagged Images**

```docker
docker rmi $(docker images -f "dangling=true" -q)
```

**Delete All Containers**

```docker
docker rm -v -f $(docker ps -qa)
```

**List All Containers**

```docker
docker container ls -a
```

**List Historical Containers**

```docker
docker ps
docker ps --all
```

- The container's ID
- What command is the container running
- When was the container created
- How long has the container been running
- What ports are mapped
- The name of the container

**Create a Volume**

```docker
docker volume create [NAME]
```

**Inspect Volume Details**

```docker
docker volume inspect [NAME]
```

**Use Volume**

```docker
docker container run -it -v [NAME]:/[DIR] [IMAGE] [CMD]
```

**List Volumes**

```docker
docker volume ls
```

**Rename Container**

```docker
docker container rename [container_name/ID] [new_name]
```

**General Container Information**

```docker
docker inspect [container_ID]
```

**View Container Memory/CPU Usage**

```docker
docker stats
```

**Limit Memory/CPU Usage**

```docker
docker container run --memory=[MB]m/g --memory-swap=[MB]m/g --cpus=[usage_cpu_core_num] --cpuset-cpus=[which_cpu_core_nums] [image_name]
```

**Set Environment Variables**

```docker
docker container run --env VAR1=[Name] VAR2=[Name] [image_name]
docker container run --env-file [env.list] [image_name]
```

**Show Running Processes**

```docker
docker top [container_ID]
```

**Copy Files to/from Container**

```docker
docker cp [file_name] [container_ID]:/[destination_path]
docker cp [container]:[src-path] [local-dest-path]
```