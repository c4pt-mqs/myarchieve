---
title: "Yeni Bir Unix'in Doğuşu ve Özgür Yazılım Devrimi"
date: 2023-11-27T18:58:06+03:00
image: "/img/content/ideas/unix-operating-system-development-adventure-and-free-software-philosophy.jpeg"
draft: false
toc: false
type: post
author: Anonim
categories:
  - Özgür Yazılım
tags:
  - unix
  - linux
  - özgür-yazılım
  - teknoloji
---

Bu yazıda, sizlere Unix işletim sisteminin serüveninden bahsetmek istiyorum. Şimdi zamanda biraz geriye gitmeliyiz.

{{< figure src="/img/content/ideas/unix-operating-system-development-adventure-and-free-software-philosophy.jpeg" alt="bug bounty hints" loading="lazy" width="70%" height="auto" >}}

1960'ların sonlarına doğru, büyük bilgisayar şirketleri kendi işletim sistemlerini geliştirmeye çalışıyorlardı. IBM örneğin, o dönemlerde System 360, System 370, OS 360 gibi farklı işletim sistemlerine sahipti. Sadece IBM değil; Uniak, CDC gibi farklı şirketler de kendi işletim sistemlerini geliştiriyorlardı. Tüm bu büyük firmalar, kendi işletim sistemini geliştirme çılgınlığına kapılarak kendi paylarına düşeni almak istiyorlardı.

1960-1970 yılları, büyük bir işletim sistemi geliştirme çılgınlığı dönemiydi. Bilgisayar teknolojisi hızla ilerliyordu ve daha yaygın hale geliyordu. Bu durum, farklı teknoloji şirketlerini ve araştırma kuruluşlarını kendi işletim sistemlerini oluşturmaya yönlendirdi. Pek çok şirket, kendi işletim sistemini geliştirdi, ancak bu sistemler genellikle kapalı kaynaklıydı ve birbirinden farklıydı. Bu durum da sistemler arasında uyumsuzluğa neden oldu.

İşte, bu dönemde kendinizi bir yazılımcı olarak hayal edin, bir program geliştirmeye çalışıyorsunuz ve aynı programı farklı sistemlere defalarca yazmanız gerekiyor. Unutmayın ki bu sistemlerin hepsi kapalı kaynak. Yani, herhangi bir standardı yok., her şeyin erken safhalarında olduğu gibi işletim sistemlerinin de erken safhalarında standart yoktu. Herkes kafasına göre bir standart tutturmuş, birileri çıkıp "Durun kardeşim, bu iş böyle gitmez, bunu bir standart haline getirelim" demesi gerekiyordu.

İşte bu kaotik ortamda, bir grup bilgisayar bilimcisi ve yazılım geliştirici daha genel ve taşınabilir bir işletim sistemi fikrini geliştirmeye karar verdi. Bu grup içindeki iki insan, Ken Thompson ve Dennis, 1969 yılında Bell Laboratuvarları'nda çalışırken Unix işletim sistemini geliştirmeye karar verdiler. Peki, neden? Neden durup dururken Unix işletim sistemini geliştirmeye karar verdiler? Durun, durun, Unix'e biraz erken giriş yaptık galiba.

Öncelikle, bu gidişata bir dur demek isteyen farklı bir projeden bahsetmemiz gerekiyor. Tabii ki hiçbir şey durup dururken yaşanmıyor. Ken Thompson ve Dennis dahil oldukları bir projede Unix fikrinin tohumlarını ekecekler. Bu proje, 1960'ların sonlarında ITT şirketinin bünyesinde bulunan Bell Laboratuvarı'nın MIT ve General Electric ile birlikte geliştirdiği Multi adını taşıyan bir projeydi. MultiX, aynı anda birden fazla kullanıcının bir ana bilgisayara bağlanarak işlem yapmasına olanak sağlayan bir çoklu görev sistemi idi. Örneğin, üç kişi aynı anda bilgisayara bağlanıp farklı işlemleri yapabiliyorlardı. Yani, bugünkü kafayla düşünürsek, aslında bu bir işletim sistemiydi. Şimdilerde işletim sistemlerinin bir özelliği olan bu sistem, o dönemlerde sanki çok bambaşka bir şeymiş gibi lanse ediliyordu.

Evet, 1969 yılına geldiğimizde Multi projesi artık çıkmaza girmeye başlamıştı. Her yazılım projesinin başına gelebilecek olan şey, Multi'nin başına da gelmeye başladı. Multi'nin boyutu ve karmaşıklığı nedeniyle hayal kırıklığına uğrayan Bell Laboratuvarı, projeden çekilme kararı aldı. Çünkü bu projenin yakın zamanda çalışan bir işletim sistemi sunma ihtimalinin düşük olduğunu biliyordu. Bell ekibi, aslında bilmeden de olsa doğmamış olan Unix işletim sistemini arzuluyor ve onun hayalini kuruyordu. Bakıldığında, Multi bu hayalin yakınından bile geçmiyordu.

Bell, projeden ayrıldı. Daha sonra geriye kalan son araştırmacılar da Multi projesinden çekilmeye başladı. İçlerinde Ken Thompson, Dennis, Douglas Mcgree gibi isimler bulunuyordu. Multi'nin geliştirme aşamasında çok fazla ses getirdi ve insanlar merakla bu işletim sistemini beklemeye başladılar.

1969 yılında pdp-7 üzerinde geliştirilen Unix işletim sistemi, 1971 yılında ilk sürümünü duyurdu. Assembly dilinde yazılmış olan bu sürüm, pdp-11 ve pdp-20 makinelerinde kusursuz bir şekilde çalışıyordu. Hatta, Unix patent dosyaları ilk olarak pdp-11 makinesinde yazılmıştı. 1973 yılında ise çok beklenen ikinci sürüm geldi. Unix, C diliyle yeniden yazıldı ve prangalarından kurtuldu. Artık Unix, taşınabilir bir işletim sistemi olmuştu.

1975 yılında, Unix evinden ayrılıyor ve 6. sürüm kullanıcılara sunuluyor. 6. sürüm, bel ekip dışında kullanıma sunulan ve yaygın olarak kullanılan ilk sürümdür. İlk BSD sürümü de bu Unix 6 versiyonundan türetilmiştir.

Tarihler bu şekilde devam ederken, Unix de bir şekilde ilerliyordu. Ancak, biraz geriye gitmek istiyorum. Ekip üyeleri, Unix için uzun saatler harcadılar. Hatta arka planda Multi gibi bir proje kendini feda etti. Unix için kabul edersiniz ki, bu emeğe karşılık iyi bir sonuç ortaya çıktı. O kadar iyi sonuçlar çıktı ki, 1970'lerin başında Unix, duvarların arkasından sızdı. Bu kadar iyi bir proje duvarların arkasında kilitli kalamazdı. Unix, sunduğu özellikleri ve basitliği ile çok ilgi çekti ve beğenildi. Bu da Unix işletim sisteminin birçok farklı sürümünü ortaya çıkardı. Farklı ülkelerden geliştiriciler, farklı sürümleri kendi dillerinde geliştirdiler. Örneğin, Berkeley Üniversitesi'nin BSD projesi, yeni bir Unix benzeri işletim sistemi geliştirmek istiyordu. Unix değil yalnız Unix benzeri; çünkü Unix olması için orijinal kodları kullanmaları gerekiyordu ve bunun için lisans ödemeleri gerekiyordu.

Bu sebepten dolayı da tek satır orijinal Unix kodu kullanmadan Unix'i klonladılar. Unix tasarım prensiplerini anlamak ve taklit etmek için Bell'in yayınladığı dokümanları kullandılar. Evet, klonlamak dediğim için kötü gibi gelebilir, ama kötü değil. Unix gibi bir girişim, tekelleşemezdi. Dünyayı değiştirebilmesi için Unix özgür olmalıydı. Bu klonlama yaklaşımı, tamamen özgür yazılım felsefesine dayanmaktadır.

Berkeley Üniversitesi'ne ait olan BSD projesi, kullanıcılara kullanma, değiştirme ve dağıtma hakkını verir. BSD projesi, diğer özgür yazılım lisanslarından farklı olarak ticari kullanımı da izin verir. Yani, BSD işletim sistemiyle bir proje yapıp bunu satabilirsiniz, bundan para kazanabilirsiniz ve 5 kuruş lisans ödemezsiniz. Berkeley Üniversitesi'nin BSD projesi, özgür yazılımın öncülerinden kabul edilir. Bu yüzden BSD projesi çok önemli bir yere sahiptir.

...