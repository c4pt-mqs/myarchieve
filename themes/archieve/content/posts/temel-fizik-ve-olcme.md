---
title: Temel Fizik ve Ölçme
date: 2023-11-10T09:00:00Z
draft: false
toc: false
type: post
author: Kaptan
categories:
  - Eğitim ve Bilim
tags:
  - fizik
  - bilim
  - eğitim
---

Açıkçası fiziğin ne olduğunu tanımlamak oldukça zordur; çünkü insanlığın ilerlemesi ve yeni keşiflerle birlikte fizik sürekli gelişmekte ve yeni keşiflerle birlikte değişmektedir. Ancak genel bir tanımlama yapmak gerekirse; Fizik, doğanın temel prensiplerini anlamaya ve açıklamaya odaklanan bir bilim dalıdır. Ayrıca evrende meydana gelen olayların nedenlerini inceleyerek bu olguları anlamaya çalışır. Karmaşık bir evreni basit, anlaşılır ve açıklayıcı yasalarla ifade etmeyi amaçlar.

Ölçüm ise nicel bilgiye ulaşmak için kullanılan bir süreçtir. Bu nicelikleri ölçmek için standart birimler kullanılır ve büyüklüklerin doğru ve tutarlı bir şekilde ölçülmesi, fizik biliminin temelini oluşturur.

- https://tr.khanacademy.org/science/physics/one-dimensional-motion/introduction-to-physics-tutorial/a/what-is-physics

{{< figure src="/img/notes/physics/fizik-1/atomun-icindeki-maceralar.png" alt="Atomun içindeki maceralar" loading="lazy" width="80%" height="auto" >}}

## Fiziksel Büyüklükler

Fiziksel büyüklükler, ölçülebilir özellikleri olan her şeydir. Uzunluk, kütle, zaman, sıcaklık, basınç, hız, ivme, kuvvet, enerji, elektrik yükü, manyetik alan gibi pek çok farklı fiziksel büyüklük vardır.

Fiziksel büyüklüklerin ölçümleri için birimler kullanılır. Birimler, fiziksel büyüklüklerin standart ölçümleridir. Örneğin, uzunluğun standart birimi metredir. Kütlenin standart birimi kilogramdır. Zamanın standart birimi saniyedir.

Uluslararası birim sistemi (SI), dünyada en yaygın kullanılan birim sistemidir. SI, temel birimler ve türetilmiş birimlerden oluşur.

**Temel Birimler:**

- Uzunluk: metre (m)
- Kütle: kilogram (kg)
- Zaman: saniye (s)
- Amper: amper (A)
- Kelvin: kelvin (K)
- Mol: mol (mol)
- Kandela: kandela (cd)

**Temel birimlerden türetilen birimler:**

- Hız: metre/saniye (m/s)
- İvme: metre/s² (m/s²)
- Kuvvet: newton (N)
- Enerji: joule (J)
- Basınç: paskal (Pa)
- Sıcaklık: kelvin (K)
- Elektrik yükü: coulomb (C)
- Manyetik alan: tesla (T)

### Madeni Yapı Taşları

Madde, atomlardan ve moleküllerden oluşur. Atomlar, maddenin temel yapı taşlarıdır. Molekül, iki veya daha fazla atomun bir araya gelmesiyle oluşur. 

Atomlar; proton, nötron ve elektronlardan oluşur. Protonlar ve nötronlar atomun çekirdeğinde bulunurken, elektronlar atomun orbitallerinde bulunur. 

### Yoğunluk

Yoğunluk, bir maddenin birim hacmine düşen kütle miktarıdır.

- {{< math >}}
$$ Yoğunluk = \frac{Kütle}{Hacim} $$

$$ D = \frac{m}{V} $$ 
{{< /math >}}

### Boyut Analizi

Boyut analizi, fiziksel büyüklüklerin boyutlarını belirleme sürecidir. Fiziksel büyüklüklerin birbirleriyle nasıl ilişkili olduğunu anlamamıza yardımcı olur. Boyut analizi yapılırken, fiziksel büyüklüklerin temel büyüklüklere indirgenmesi gerekir.

### Birimleri Çevirme

Fiziksel büyüklükleri farklı birimlerde ifade etmek için birim çevirme işlemi yapılır. Birim çevirme işlemi yapılırken, büyüklüklerin boyutları dikkate alınmalıdır.
- {{< math >}}
$$5.8 \text{m} = 5.8 \times 100 = 580 \text{cm}$$

$$2.5 \text{kg} = 2.5 \times 1000 = 2500 \text{g}$$

$$3.6 \text{s} = 3.6 \times 1000 = 3600 \text{ms}$$

$$4.2 \text{L} = 4.2 \times 1000 = 4200 \text{mL}$$
{{< /math >}}

### Büyüklük-Mertebesi Hesaplamaları ve Tahminler

Büyüklük-mertebesi hesaplamaları, büyük sayıların daha kolay anlaşılabilmesi için kullanılır. Hesaplamalarda, sayı 10'un üssü şeklinde ifade edilir. Örneğin, 1000 sayısı 10³'tür. Bu hesaplamalar, fiziksel problemleri çözerken de kullanılabilir. Örneğin, bir cismin kütlesini güneş kütlesinin bir milyonda biri olarak ifade etmek için {{< math >}}$$ m = \frac{M}{10^6} $${{< /math >}} formülünü kullanabiliriz.

### Anlamlı Rakamlar

- Sıfırdan farklı rakamlar ve başka rakamların arasında kalan sıfırlar anlamlıdır. Örneğin, 607901 sayısında toplam altı anlamlı rakam bulunur.
- Baştaki sıfırlar, yani sıfırdan farklı rakamlardan önce gelen sıfırlar anlamsızdır. Örneğin, 0,00817 sayısının başındaki üç sıfır anlamsızdır ve sayıda üç anlamlı rakam vardır.
- Ölçüm sonuçlarında altı çizili veya üstü çizili rakam bulunursa, bu bize altı çizili olan rakama kadar olan sayıların anlamlı olduğunu gösterir.
- Virgül varsa (örneğin 10,00), sondaki sıfırlar anlamlıdır ve bu sayıda dört anlamlı rakam vardır.
- Virgül yoksa (örneğin 45000), sayı 45000'e yuvarlanmış olabilir ve 4,5 × 10^4 şeklinde yazılabilir. Dolayısıyla sondaki sıfırlar anlamlı değildir ve 45000 sayısında iki anlamlı rakam bulunur.

### % Hata Hesabı

%hata, ölçümlerin doğruluğunu ve kesinliğini değerlendirmede kullanılır. %hata değeri ne kadar küçükse, ölçümün doğruluğu ve kesinliği o kadar yüksektir.

- {{< math >}}
$$ \% Hata = \frac{|Teorik \ Değer - Deneysel \ Değer|}{Teorik \ Değer} \times 100 $$ 
{{< /math >}}