---
title: Güzel Bir CV Nasıl Olur?
date: 2023-07-28T09:00:00Z
draft: false
type: post
author: Anonim
categories:
  - Kişisel Gelişim
tags:
  - kişisel-gelişim
  - motivasyon
---

1. **Türkçe Karakter Kullanımı:**

Türkçe bir CV yazarken, Türkçe karakterleri (İ, Ğ, Ü, Ö, vb.) kullanmaya özen göstermelisin. Bu, profesyonel ve düzenli bir görüntü sağlar.

2. **Kişisel Bilgiler:**

Kişisel bilgiler bölümü gereksiz detaylar içermemeli. Doğum tarihi, medeni hal, milliyet, din, cinsiyet gibi bilgilerin CV'ye eklenmesine gerek yok.

3. **İlgi Alanları:**

İlgi alanlarını spesifik hale getir. Genel ifadeler yerine, ilgi alanlarını daha açıklayıcı ve işle ilgili bir şekilde ifade etmelisin.

4. **Yetkinlikler:**

Yetkinliklerini belirtirken düzeyini de belirt. Mümkünse global derecelendirme ölçütlerinde gösterebileceğin kanıtları eklemek, CV'ni güçlendirebilir.

5. **İletişim Bilgisi:**

Sadece mail adresini paylaş, cep telefonunu eklemek kişisel veri güvenliği riski taşıyabilir.

6. **Mail Adresi:**

Mail adresini adın ve soyadınla oluşturabilirsin. Profesyonele daha uygun bir izlenim bırakabilir.

7. **CV Uzunluğu ve Format:**

CV'nin 2. sayfaya taşmasını önle. Eğer 2. sayfaya geçeceksen, formatı bozmadan düzenlemelisin.

8. **Kariyer Hedefi:**

Kariyer hedefin, genel bir hedef içermeli ve şirketlere yardım edebileceğin ifadesi yerine daha genel ve profesyonel bir dil kullanmalısın.

9. **Projeler:**

Projeleri detaylandır ve mümkünse linkler ekleyerek incelenebilirliği artır.

10. **Sertifikalar:**

Sertifikalar kısmında sertifikaların yanı sıra, kişilerin doğrulayabileceği linkleri eklemelisin.

11. **Eğitim:**

Eğitim bilgilerini düzenli bir şekilde sun. Üniversite adı, program türü, program adı, giriş ve tahmini bitiriş tarihi gibi detayları eklemelisin.

12. **Dil ve Noktalama Kuralları:**

CV'nin tamamında Türkçe dil ve noktalama kurallarına uygunluğa dikkat etmelisin.

13. **Renk Kullanımı:**

CV'deki renkleri göz yormayacak şekilde düzenle. Daha sade ve profesyonel bir görünüm elde etmek için renkleri ölçülü kullan.
