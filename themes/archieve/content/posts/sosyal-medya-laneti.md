---
title: "Sosyal Medya Laneti"
date: 2023-07-23T18:58:06+03:00
draft: false
toc: false
type: post
author: Kaptan
categories:
  - Fikirler
tags:
  - sosyal-medya
  - dijital
  - psikoloji
---

Merhaba,

Bugün sizlere, günümüzdeki dijital çağda ruhlarımızın sosyal medya üzerinde nasıl çalındığı, manipüle edildiği ve kandırıldığı hakkında biraz konuşmak istiyorum. Bu konuda yaptığım testlerden elde ettiğim deneyimlerimi paylaşacağım ve bu testlerden çıkardığım sonuçları sizinle paylaşarak sizi düşünmeye ve bilinçlenmeye davet ediyorum.

Bundan üç yıl önce sahip olduğum motivasyon ve kişisel gelişim Instagram sayfası, benzersiz içerik ve tasarımlarla doluydu ve aynı zamanda kendi websitesine sahipti. Ancak, bu sayfa aktif bir şekilde yönetiliyor olmasına rağmen, bir ay içinde 1K takipçiye bile ulaşamadı.

Şimdi size, başka bir Instagram hesabından bahsedeceğim. Bu hesap, doğal görünümlü ve 20-25 yaş aralığında fake bir kadın modelin fotoğraflarını içeriyordu. Amacım, ne kadar etki ve tepki çekebileceğimi görmekti. Günde bir saat harcayarak, 3-4 gün içinde 300'den fazla takipçiye ulaştım. Takip ettiğim kişilerin çoğu erkek olmasına rağmen, profilin sahte olduğunu anlamamaları için kadınlardan da takip ediyor ve belirli bir süre sonra erkekleri takipten çıkıyordum.

Sonuç olarak, kişisel gelişim ve motivasyon sayfam 1 ayda 1K takipçiye ulaşamazken, sahte bir kadın profili bu sayıya kısa sürede ulaşabiliyordu. Bu durumun temel sebebi, erkeklerin ilgi ve merakı, arzularını tatmin etmekten başka bir şey değildi. Gördüğünüz gibi, fake bir hesap olsun veya olmasın, insan kendisini maddi ve manevi olarak sömürmeye izin veriyor ve bu duruma bağımlı hale geliyor.

**Eğer bu durumun farkındaysak, ne yapabiliriz?**

1. Öncelikle sosyal medyayı kaldırmak ya da sadece belirli bir süre kullanmak önemli bir adım olabilir (ancak bu süre genellikle aşılıyor).
   
2. Takip ettiğiniz kişileri gerçekten tanıdığınız insanlar oluştursun ve takipçi sayınız ne kadar az olursa o kadar az sorun yaşayacağınızı unutmayın.

3. Bilgi toplanmasına izin vermemek ve kandırılmak istemiyorsanız, mümkün olduğunca tanımadığınız kişilerle iletişim kurmaktan kaçının.

4. Görüntünün sizi kandırmasına izin vermeyin.

5. Bu durumlara duyarsız kalmayın; bir şeyleri değiştirmek için bilinçli adımlar atmamız gerekiyor. Kullandığımız cihazların nasıl çalıştığını ve asıl amacını anlamak, işlerimizin daha sağlıklı yürümesine yardımcı olacaktır.

Bu konuları paylaşmamın ana sebebi, sistemin bizi nasıl kandırdığını ve köleleştirdiğini görmek ve bu durumlara karşı önlemler almaktır. Unutmayın, bilinçlenmek ve harekete geçmek bu dijital dünyada kendi haklarımızı korumamıza yardımcı olabilir. 

Saygılar.
