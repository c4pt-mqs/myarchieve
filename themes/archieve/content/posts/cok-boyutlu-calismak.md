---
title: "Çok Boyutlu Çalışmak"
date: 2023-05-19T18:58:06+03:00
draft: false
toc: false
type: post
author: Kaptan
categories:
  - Fikirler
tags:
  - kişisel-gelişim
  - motivasyon
  - eğitim
---

İşleri tek boyutta düşünmek yerine, birçok farklı alanda çalışmanın gerçekten çok faydası oluğunu düşünüyorum. Beyin asıl odaklandığı işi bırakıp farklı işlerle ilgilendiğinde bir özyineleme mekanizması şeklinde hareket ediyor. Yani beyin uyku moduna giriyormuşuz gibi algılıyor. Bu da yorulmadan üretkenliğimizi devam etmemizi sağlıyor. Birden fazla görev ve alanda çalışmak, farklı deneyimlere daha az zaman diliminde ulaşmamızı da sağlıyor. Ayrıca işlere olan bakış açımızı daha kapsayıcı hale getiriyor. Bu öğrenme sürecini hem daha eğlenceli bir hâle getiriyor, hem de ileride yapmak istediğimiz şeyler konusunda alternatifleri artırıyor.

Bu konuda araştırma yaptığımda, eski ilim/bilim adamlarının neden birden fazla alanda çalıştığını anladım. Örneğin fizik, matematik, astrofizik, kimya, psikoloji, tıp gibi farklı disiplinlerle ilgilenmelerinin sebebi tam olarak buymuş. Çok yönlü ve geniş bir düşünme perspektifi geliştirmek için bu farklı alanlarda çalışmak çok önemli. Ömer Hayyam, Newton, Einstein, İbn-i Sina, Tesla gibi büyük beyinlerin ortak noktalarından biri de buymuş. Tâbi her şeyde olduğu gibi denge önemli. Zamanı iyi yönetmeli ve enerjimizi dengeli bir şekilde dağıtmalıyız.

Birden fazla alanda çalışmak aynı zamanda odaklanma yeteneğimizi geliştiriyor. Beyin sadece tek bir noktada sabit kalmak yerine farklı konulara yönelip hareket ederse, daha üretken olabiliyor. Bulunduğumuz mekanı zaman zaman değiştirmek de psikolojik olarak yaratıcı düşüncelerin ortaya çıkmasına yardımcı oluyor. Eskilerin de dediği gibi, "Tebdili mekanda ferahlık vardır."
