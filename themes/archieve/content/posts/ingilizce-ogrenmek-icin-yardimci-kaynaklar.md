---
title: İngilizce Öğrenmek İçin Yardımcı Kaynaklar
date: 2021-04-27T09:00:00Z
summary: "İngilizce Öğrenme adına Video, Okuma, Dinleme, Yazma ve Konuşma için kaynaklar gösteriyorum! Günlük pratik, diziler/filmler ve uygulamalar ile İngilizce seviyenizi geliştirin."
draft: false
toc: true
type: post
author: Kaptan
categories:
  - Kişisel Gelişim
tags:
  - dil-öğrenimi
  - iletişim
  - kişisel-gelişim
---


Ben burada İngilizce öğrenme ile ilgili genel ifadeleri ve kaynakları paylaşacağım. Buradaki kaynakları okuyup-değerlendirip kendinize göre uyarlarsanız, 1 yıl içinde kendinizi ileri seviyelerde bulabilirsiniz.

### İngilizce Öğrenmeye Yönelik Kaynaklar
İngilizce öğrenme sürecinde ilk adımı bu yazıyı okumaya başlayarak bir adım attınız. Bu süreçte kendinize bir şeyler katmak istiyorsanız; Okuyun, Öğrenin, Uygulayın.

## Educational Videos

- [Sıfırdan İngilizce Öğreniyorum Eğitim Seti [TR]](https://youtube.com/playlist?list=PLxX2m2Np79VfE8qgLtUZHwCF0VyuFH8QI)
- [İngilizce günlük hayatta nasıl kullanılıyor [TR]](https://youtu.be/XOxlvTGY4mg)
- [İngilizce Adına Neler Yapabilirsiniz [Metin]](https://www.fluentu.com/blog/english-tur/evde-ingilizce-ogren/)
- [Korkusuzca-Konuş Tekniği [Video]](https://www.youtube.com/watch?v=CgXPnpvrGNM)
- [Let's Learn English (Beginning Level) [EN]](https://youtube.com/playlist?list=PLuncffeXVQSTeWowQmHRA31JxNlT5_O0G)
- [English Course (Beginner - Elementary) [EN]](https://youtube.com/playlist?list=PLydqIa50k59lp8r6105sUyCHfaZfwao14)

## Reading

- News in Levels: https://www.newsinlevels.com/
- Engoo Daily News: https://engoo.com.tr/app/daily-news
- Breaking News English: https://breakingnewsenglish.com/
- Mentalfloss: https://www.mentalfloss.com/
- Science Alert: https://www.sciencealert.com/
- The National Geographic: https://www.nationalgeographic.com/
- Mashable: https://mashable.com/
- Voice of America: https://learningenglish.voanews.com
- British Council: https://learnenglish.britishcouncil.org/skills/reading
- Classic Children's Books: https://www.loc.gov/free-to-use/classic-childrens-books/

## Listening

- Voscreen: https://www.voscreen.com/
- English with Sera: https://open.spotify.com/show/2AArQsgJWzNinmGE7l1W8S?si=a5944fdfed0c4b69&nd=1
- Six Minute English: https://open.spotify.com/show/3CF9ANEicXGxEROA3cOryE?si=f633d1dc698c4423&nd=1
- BBC Learning English: https://www.bbc.co.uk/learningenglish/
- CNN News: https://edition.cnn.com/cnn10
- ELT Podcast: https://eltpodcast.com/archive/bc
- British Council: https://learnenglish.britishcouncil.org/general-english/audio-zone
- Luke's English: https://teacherluke.co.uk/

## Writing

- Grammarly: https://grammarly.com
- Ludwig: https://ludwig.guru
- QuillBot: https://quillbot.com
- ProWritingAid: https://prowritingaid.com/
- Ginger: https://www.gingersoftware.com/grammarcheck
- Cliché Finder: https://www.clichefinder.net/

## Speaking

- HelloTalk: https://www.hellotalk.com/
- Cambly: https://www.cambly.com/
- Open English: https://www.openenglish.com
- Speaky: https://www.speaky.com/
- Tandem: https://www.tandem.net/
- LingoGlobe: https://www.lingoglobe.com/
- Forvo: https://forvo.com/
- ELSA: https://elsaspeak.com/

## Kitaplar
Seviyenize Göre Kitap Önerisi:
Kitapları alabilir ya da fotokopi çıkarıp okuyabilirsiniz. Dijital ortamda da okuyabilirsiniz. Ancak elinizde olan bir kitap için notlar almak, altını çizmek daha rahat olucaktır.

### Starter

- [Word List](https://www.cambridgeenglish.org/Images/young-learners-sample-papers-2018-vol1.pdf)
- [More Wordlist](https://www.cambridgeenglish.org/Images/423014-cambridge-english-young-learners-sample-papers-2018-volume-2.pdf)
- [Starter-Vol1](https://www.cambridgeenglish.org/images/starters-word-list-picture-book.pdf)
- [Starter-Vol2](https://www.cambridgeenglish.org/Images/506166-starters-movers-flyers-word-list-2018.pdf)

### A1-A2
- [Beginner](https://learnenglish-new.com/category/english-short-stories-for-beginners/)
- [Elementary](https://learnenglish-new.com/category/stories-for-elementary/)

### B1-B2
- [Pre-Intermediate](https://learnenglish-new.com/category/pre-intermediate/)
- [Intermediate](https://learnenglish-new.com/category/intermediate-level/)

### C1-C2
- [Advanced](https://learnenglish-new.com/category/english-short-stories-for-advanced/)
- [Proficiency](https://paradigmenglish.com/6-books-advanced-c1-c2-english-students-must-read-now/)

## Uygulamalar

- Memrise: https://www.memrise.com/
- Busuu: https://www.busuu.com/
- Beelinguapp: https://beelinguapp.com/
- Lyricstraining: https://lingoclip.com/
- Quizlet: https://quizlet.com/
- Brainscape: https://www.brainscape.com/
- Drops: https://languagedrops.com/
- Chegg: https://www.chegg.com/flashcards
- Cram: https://www.cram.com/

## Youtube Kanalları
**Öğretici İçerikli Kanallar:**
- [English Class 101 [Her Türlü Konu]](https://www.youtube.com/c/EnglishClass101/videos)
- [Real English [Sokak Röportajları]](https://www.youtube.com/c/realenglish1/videos)

**Film Kesitleri:**
- [MOVIECLIPS](https://www.youtube.com/c/MOVIECLIPS)
- [Axecutioner Movieclips](https://www.youtube.com/c/AxecutionerMovieclips)
- [MovieclipsTRAILERS](https://www.youtube.com/c/MovieclipsTRAILERS)

**Diğer kanallar:**
- [A very wide and active channel about movies, TV, games](https://www.youtube.com/@whatculture)
- [She lives in Turkey, travels around the cities](https://www.youtube.com/@TravelComic/videos)
- [The channel that broadcasts reaction videos; people comments on videos](https://www.youtube.com/@React)
- [Making videos about strange events](https://www.youtube.com/@MarvelousVideos)
- [Short biographies](https://www.youtube.com/c/@Biography)
- [Interpreting on movies.](https://www.youtube.com/@JeremyJahns)
- [English instructor makes enjoyable videos](https://www.youtube.com/c/@TheEnglishCoach)
- [BBC english learning channel](https://www.youtube.com/@bbclearningenglish)
- [News](https://www.youtube.com/c/@Vox)
- [History](https://www.youtube.com/c/@FeatureHistory)
- [History-2](https://www.youtube.com/c/@KingsandGenerals)

## Diziler

Genel anlamda IMDB'de ki [ilk 100](https://www.imdb.com/search/title/?groups=top_100&sort=user_rating,desc) filmi izleyebilirsiniz. Ancak Türkçe altyazı kesinlikle kullanmayın. Dizi olarak günlük hayatta sıklıkla kullanılan kelimeleri anlamak adına bunlardan birini izleyebilirsiniz:
- Brooklyn 99
- Arrow
- Friends
- Grace and Frankie
- How I met your mother
- Modern Family
- Vampire Diaries

Yabancı dil öğrenirken sadece izlemekle yetinmeyin. Dizileri izlerken özet yazın ve notlar alın. Günlük konuşmaların bol olduğu diziler seçin ve her gün bir bölüm izleyin. İlk başta anlamasanız da sabredin ve devam edin, beyniniz öğrenmeye adapte olacaktır. Kişisel tercihlerinize göre uygulamalar ve web siteleri bulun. Dinlediklerinizden öğrendiğiniz şeyleri uygulayarak veya konuşarak ilerleyin. Çünkü ne kadar çok pratik yaparsanız o kadar ilerlersiniz.

## Diğer

+ İngilizce-İngilizce / İngilizce-Türkçe bir sözlük alıp, aktif bir şekilde kullanmalısınız.
+ Elbette bir kitaba ihtiyaç duyacaksınız. Bu yüzden bir kitap temin etmelisiniz. Ancak kitapların fiyatlarından şikayetçi iseniz kitabın PDF'ini çıkarmak daha uygun olabilir.
+ Ayrıca ulaşaileceğiniz diğer güzel kaynaklar da **_[buradadır](https://github.com/yvoronoy/awesome-english)_**.

