---
title: Bash Scripting
date: 2023-07-15T09:00:00Z
draft: false
type: note
author: Kaptan
category: Programming
subcategory: Languages
tags:
  - shell-scripting
  - linux
---

**Sihirli Parantezler:**

Dosya uzantısını değiştirmek için iki süslü parantez kullanabilirsiniz:

```zsh
mv ./file.{html,md}
# file.html -->> file.md
```

**Bir sayı kümesi oluşturup bunu iterate edebilirsiniz:**

```zsh
echo {1..3}

# {START..END..INCREMENT} pattern
for i in {0..7..2}
  do;
    echo "for loop n: $i";
done
```

**Semboller:**

'$( )' sembolleri arasına yazdığınız karakterler bash'de değişken olarak yorumlanır.

```zsh
touch foo-$(date -I)
```

Komutunuzun sonuna '&' işareti ekleyerek işlemin arka planda çalışmasını sağlayabilirsiniz.

```zsh
curl --upload-file ./hello.txt https://transfer.sh/hello.txt &
```

'$!' en son yürüttüğünüz arka plan işleminin ID'sini gösterecektir.

```zsh
sleep 60 &
kill $!
```

Son yazdığınız komutu tekrar yazmak istiyorsanız '!!' kullanabilirsiniz.

Kullanıcı eksik veya hatalı bir indirme yaparsa, sadece ana fonksiyon çalışmayacak ve kurulum gerçekleşmeyecektir. Böylece işler daha düzgün ve sorunsuz ilerleyecektir.

```zsh
main() {
  # your codes
}

main "$@"
```

**Klavye Kısayolları**

İşinize yarıyacak bazı popüler terminal kısayolları:

- `Ctrl + R`: Komut geçmişinizde arama yapar
- `Ctrl + E`: İmleci en başa götürür
- `Ctrl + A`: İmleci en sona götürür
- `Ctrl + U`: İmlecin solundaki kısmı siler
- `Ctrl + K`: İmlecin sağındaki kısmı siler
- `Ctrl + P`: Komut geçmişinizde geri gider
- `Ctrl + L`: Terminal ekranınızı temizler
- `Ctrl + D`: Eğer aktif bir process'in içerisinde iseniz onu kapatır, değilseniz ve terminal'de herhangi bir şey yazılıysa imleçten sonraki karakteri siler, eğer hiçbişe yazılı değilse kullanıcıdan veya bulunduğun terminal'den çıkış yapar

**Bazı İpuçlar:**

- $$ is the PID of the current process.
- $? is the return code of the last executed command.
- $# is the number of arguments in $*
- $* is the list of arguments passed to the current process
- $@ holds a space delimited string of all arguments passed to the script

### Resources

**Project Ideas:**

[Bug Bounties With Bash](https://www.youtube.com/watch?v=s9w0KutMorE)
https://www.dunebook.com/the-20-best-shell-scripting-project-ideas-to-improve-your-linux-skills/

**Cheet Sheet:**

https://devhints.io/bash

**Tutorial with examples:**

https://linuxhint.com/3hr_bash_tutorial/
