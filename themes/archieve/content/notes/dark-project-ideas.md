---
title: Dark Ideas
date: 2022-06-22T09:00:00Z
draft: false
type: note
author: Kaptan
category: Programming
subcategory: Projects
tags:
  - proje
  - programlama
  - yazılım
---

### Random Telefon Rehberi Yapma

1. API olarak telefon numaralarının geçerli olup olmadığını kontrol eden bir script oluştur.
2. Bir telefon listesi aralığı oluştur.
3. Yazdığın scripti telefon numaralarına bağla. Herbirini 500 kişi olucak şekilde böl.
4. Valid olanları farklı bir text dosyasına kaydet. Daha sonra .vfc şeklinde sed ile yeniden yazdır.
5. haveibeenpwned, deepweb ve Telegramda ki tüm hacklenmiş databaseleri de bul ve bunlara özel bir panel yap.
