---
title: Geo | Maps
date: 2023-02-10T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: OSINT
tags:
  - osint
  - search
  - reconnaissance
---

**Location from images:**

https://labs.tib.eu/geoestimation/

**Flags and Countries of the World:**

https://www.fotw.info/flags/index.html

**GeoGuessr:**

https://www.geoguessr.com/

**City Guessr:**

https://virtualvacation.us/guess

**Maps:**

- https://earth.google.com/web/
- https://maps.google.com/
- https://yandex.com/maps

**World Plugs:**

https://www.iec.ch/world-plugs

**Wiki Map:**

https://wikimapia.org/

**World Cam:**

https://worldcam.eu/

**Mapping:**

- https://wiki.openstreetmap.org/wiki/Tr:Map_Features
- https://wiki.openstreetmap.org/wiki/Tr:Getting_Involved
- https://wiki.openstreetmap.org/wiki/WikiProject_Turkey
- https://wiki.openstreetmap.org/wiki/Search_engines

**Driving directions home:**

https://www.drivingdirectionsandmaps.com/


**Language:**

- https://theweek.com/articles/617776/how-identify-language-glance
- https://theweek.com/articles/620397/how-identify-asian-african-middle-eastern-alphabets-glance

**Alphabets:**

https://www.key-shortcut.com/en/writing-systems/world-map-of-alphabets-scripts
