---
title: JS Resources
date: 2022-08-29T09:00:00Z
draft: false
type: note
author: Kaptan
category: Programming
subcategory: Languages
tags:
  - javascript
  - learning
  - coding
---

- https://developer.mozilla.org/en-US/docs/Web/JavaScript
- https://www.codecademy.com/courses/introduction-to-javascript/lessons/introduction-to-javascript/exercises/intro
- https://www.w3schools.com/js/
- https://edu.anarcho-copy.org/Programming%20Languages/web/JavaScript/Eloquent_JavaScript.pdf
- https://www.edx.org/course/cs50s-web-programming-with-python-and-javascript
- **JavaScript Style Guide:** https://github.com/airbnb/javascript
