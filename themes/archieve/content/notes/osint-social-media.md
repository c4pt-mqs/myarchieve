---
title: Social Media
date: 2023-02-05T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: OSINT
tags:
  - osint
  - search
  - reconnaissance
---

## Instagram

**Downloader:**

https://instadp.io/

**Analyzer and Viewer:**

- https://gramhir.com/
- https://greatfon.com/
- https://www.picuki.com/
- https://imginn.com/
- https://storiesdown.com/
- https://instastories.watch/

**Instagram ID:**

- https://takipci.shop/instagram-id-bulma
- https://codeofaninja.com/tools/find-instagram-user-id/


## Twitter

- geocode:40.0080878,32.8498498,0.1km ( to:asdas / from:asdas )  #örnek(arama) 
- Twitter Advanced Search -  https://twitter.com/search-advanced
- Social Bearing -  https://socialbearing.com/
- Twitonomy -  https://www.twitonomy.com/
- Sleeping Time -  http://sleepingtime.org/
- Mentionmapp -  https://mentionmapp.com/
- Tweetbeaver -  https://tweetbeaver.com/ (TWeeter ID)
- Spoonbill.io -  http://spoonbill.io/
- Tinfoleak -  https://tinfoleak.com/
- TweetDeck -  https://tweetdeck.com/ (many page in same screen)
- Twitter ID & Mail Finder -  https://twiteridfinder.com/


## Linkedin

**Methods:**

- https://www.osintme.com/index.php/2020/04/26/how-to-conduct-osint-on-linkedin/
- https://github.com/sinwindie/OSINT/blob/master/LinkedIn/Bookmarklet%20Tools
- https://www.secjuice.com/linkedin-osint-part-1/
- https://www.secjuice.com/linkedin-osint-techniques-part-ii/
- https://hatless1der.com/new-linkedin-search-features-mean-new-osint-opportunities/

## Github

**URL Hacks:**

https://justingarrison.com/blog/2021-07-11-github-url-hacks/

**Find e-mail address:**

*github > repo > commit > "you can add '.patch' after the commit ID in URL"*

https://www.nymeria.io/blog/how-to-manually-find-email-addresses-for-github-users

**Applications:**

https://github.com/GONZOsint/gitrecon

## Discord

https://github.com/Dutchosintguy/OSINT-Discord-resources

