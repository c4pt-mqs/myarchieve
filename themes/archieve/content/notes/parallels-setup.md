---
title: Parallels Setup
date: 2023-06-07T09:00:00Z
draft: false
type: note
author: Kaptan
category: Help
subcategory: Support
tags:
  - macos
  - troubleshooting
  - configuration
---

- https://gist.github.com/gdurastanti/e79b1fae40a4eff14f5636b8994a89fc
- https://github.com/alsyundawy/Parallels
