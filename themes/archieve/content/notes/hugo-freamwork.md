---
title: Hugo
date: 2022-03-07T09:00:00Z
draft: false
type: note
author: Kaptan
category: Programming
subcategory: Languages
tags:
  - static-site-generator
  - hugo
---

**my-site/themes/my-theme:**

- **layouts**: This directory will contain the HTML templates for your Hugo theme.
- **static**: This directory will contain static assets like CSS, JavaScript, and images.
- **archetypes**: This directory will contain templates for new content files.

1. Inside the layouts directory, create the following subdirectories:
- **_default**: This directory will contain the default layout templates for your theme.
- **partials**: This directory will contain reusable HTML code snippets that can be included in your templates.


2. Inside the _default directory, create the following files:
- **baseof.html**: This file will contain the basic HTML structure of your theme.
- **list.html**: This file will be used to render lists of content, such as blog post summaries.
- **single.html**: This file will be used to render individual content pages, such as blog posts or project pages.

3. Inside the static directory, create the following subdirectories:
- **css**: This directory will contain your CSS files.
- **js**: This directory will contain your JavaScript files.
- **img**: This directory will contain your image files.

4. Inside the archetypes directory, create the following file:
- **default.md**: This file will contain the default front matter for new content files.

## Commands

Displays the installed Hugo version.


```zsh
hugo version
```

Starts the Hugo server and automatically rebuilds the site whenever changes are detected.

```zsh
hugo serve
```

Starts the Hugo development server and binds it to a specific IP address and port number.

```zsh
hugo serve --bind=<IP_ADDRESS> --port=<PORT_NUMBER>
```

Creates a new Hugo site in the specified directory.

```zsh
hugo new site <directory>
```

Creates a new content file in the specified path.

```zsh
hugo new <path/to/content>
hugo new blog/my-post.md
```

Create the web site best clean mode

```zsh
hugo --minify && hugo mod clean && hugo mod tidy && hugo mod get -u ./... && \
    hugo --cleanDestinationDir && hugo server -D
```

Builds the Hugo site with minification enabled, reducing file size for faster page load times.

```zsh
hugo --minify
```

Cleans the Hugo module cache, removing unused dependencies and modules.

```zsh
hugo mod clean
```

Ensures Hugo module dependencies are up to date and removes unused dependencies from the module configuration file.

```zsh
hugo mod tidy
```

Updates Hugo module dependencies to their latest versions by fetching and updating the module configuration file.

```zsh
hugo mod get -u ./...
```

Generates the Hugo site, cleaning the destination directory before creating new files.

```zsh
hugo --cleanDestinationDir
```

Starts the Hugo development server, disabling fast rendering for accurate testing of all pages during development.

```zsh
hugo server --disableFastRender
```
