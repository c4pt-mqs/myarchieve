---
title: Tools
date: 2022-11-18T09:00:00Z
draft: false
type: note
author: Kaptan
category: Links
subcategory: Useful Links
tags:
  - tools
  - resources
---

**Online çizim:**
- https://excalidraw.com/

**Arkadaşlarınla birlikte video izle:**
- https://w2g.tv/

**Collection of free design tools and resources:**
- https://undesign.learn.uno/

**Create a Firefox profile as you wish:**
- https://ffprofile.com/

**Linux web simulatores online:**
- Distrowatch
- Katacoda
- Distrotest
- Webminal
- Jslinux 
