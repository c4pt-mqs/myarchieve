---
title: 5 Steps for Programmers
date: 2022-08-29T09:00:00Z
draft: false
type: note
author: Kaptan
category: Programming
subcategory: Basics
tags:
  - programming
  - tutorial
  - coding
  - guide
---

**How to improve your coding skills :**

1. `Don't spend more than 1 month watching tutorials.`
2. `Learn as needed.`
3. `Build Projects.`
4. `Do Coding Challenges.`
5. `Take on simple freelancing projects.`

**Yeni Yazılımcı İçin Öneriler:**

- https://www.birlikteihracat.com/yeni-yazilimciya-oneriler/
- https://www.patika.dev/blog/yazilim-kariyerine-baslangic-rehberi
- https://vizyonergenc.com/icerik/yazilim-alaninda-kendini-gelistirmek-i-steyenlere-8-oneri

**Competitive Programming:**

- https://scriptyuvasi.com/leetcode-rekabetci-programlama-nedir

**Yardımcı Kaynak:**

- https://www.google.com/
- https://leetcode.com/
- https://neetcode.io/
- https://www.hackerrank.com/
- https://www.w3schools.com/
- https://coderbyte.com/
- https://app.patika.dev/
- https://stackoverflow.com/
- https://www.freecodecamp.org/
- https://www.quora.com/
- https://www.theodinproject.com
