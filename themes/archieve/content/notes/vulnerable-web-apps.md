---
title: Vulnerable Web Apps
date: 2023-02-13T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Web Security
tags:
  - vulnerabilities
  - pentesting
  - security
---

### BWAPP

```zsh
docker pull raesene/bwapp
docker run -d -p 80:80 raesene/bwapp
```

### web-dvwa

```zsh
docker pull vulnerables/web-dvwa
docker run --rm -it -p 80:80 vulnerables/web-dvwa
```

- Username: admin
- Password: password

### webgoat-8.0

```zsh
docker pull webgoat/webgoat-8.0
docker run -p 8080:8080 -t webgoat/webgoat-8.0
```

### Web for Pentest

```zsh
docker pull justhumanz/web_for_pentest
docker run -itd --name pentest -p 8888:80 justhumanz/web_for_pentest
```

### Juice-Shop

```zsh
docker run --rm -p 3000:3000 bkimminich/juice-shop
```

**Vulnerable Machines:**
- https://hub.docker.com/search?q=vulnerable

**Docker build with Mac M1:**
- https://medium.com/geekculture/docker-build-with-mac-m1-d668c802ab96
