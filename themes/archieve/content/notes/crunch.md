---
title: crunch
date: 2022-12-09T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

```zsh
crunch 8 8 0123456789abcdefABCDEF -o wordlist.txt
```

- `@`: lower case alpha characters
- `,`: upper case alpha characters
- `%`: numeric characters
- `^`: special characters including space

