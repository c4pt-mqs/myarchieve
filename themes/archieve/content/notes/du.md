---
title: du
date: 2022-10-23T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Flags        | Description                               
-------------|-------------------------------------------
`-a` | Will list files as well with the folder.
`-h` | Will list the file sizes in human readable format(B,MB,KB,GB)
`-c` | Using this flag will print the total size at the end. Jic you want to find the size of directory you were enumerating
`-d [number]` | Flag to specify the depth-ness of a directory you want to view the results for (eg. -d 2)
`--time` | To get the results with time stamp of last modified.
