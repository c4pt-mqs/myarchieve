---
title: smbclient
date: 2022-11-18T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Nmap'in smb script'leri ile taramak:

```zsh
nmap -p 445 --script=smb-enum-shares.nse,smb-enum-users.nse [IP]
```

Samba üzerinden paylaşımları taramak:

```zsh
enum4linux -a [IP]
```

SMBClient ile IP adresindeki anonim paylaşıma bağlanmak:

```zsh
smbclient //[IP]/anonymous
```

IP adresindeki paylaşımları listelemek:

```zsh
smbclient -L [IP]
```

IP adresindeki paylaşıma kullanıcı adı ile bağlanmak:

```zsh
smbclient //[IP]/share -U [username]
```

IP adresindeki paylaşıma çalışma grubu ve yol adı ile bağlanmak:

```zsh
smbclient \\\\WORKGROUP\\[path_name] -I [IP] -N
```

SMBGet ile paylaşımdan dosyayı indirmek:

```zsh
smbget -R smb://[IP]/anonymous
```

SMBClient ile içerde kod çalıştırmak:

```zsh
smbclient \\[IP]\"temporary share" -c 'recurse;ls'
```

+ Dosya indirirken otomatik onaylamak: `prompt OFF`
+ Dosyaları bozulmadan düzgün bir şekilde kopyalamak: `binary`
+ Mevcut paylaşımdaki tüm dosyaları indirmek: `mget *`