---
title: English Glassory
date: 2022-06-18T09:00:00Z
draft: false
type: note
author: Kaptan
category: Mine
subcategory: Languages
tags:
  - language
  - english
  - learning
---

**approved:** onaylı

**conduct:** yönetmek, idare etmek

**authorised:** yetkili

**wide:** Geniş

**variety:** çeşitlilik

**approach:** yaklaşma

**involve:** içermek

**gathering:** toplamak

**definitions:** tanımlar

**remediation:** iyileştirme

**overlap:** çakışmak

**determine:** belirlemek

**Intentionally:** bilerek 

**extensive:** yaygın

**acronym:** kısaltma

**interacting:** etkileşim

**consuming:** tüketen

**infrastructure:** altyapı

**attached:** eklenmiş

**confidential:** gizli

**aid:** yardım

**represent:** temsil etmek

**interfering:** müdahale eden

**scalability:** ölçeklenebilirlik

**conceptual:** kavramsal

**acronyms:** kısaltmalar

**pointless:** anlamsız, yararsız

**stack:** yığın

**restrict:** kısıtlama

**clues:** ipuçlar

**intelligence:** istihbarat, zeka

**for instance:** örneğin

**initial:** ilk, baş

**territory:** bölge

**discreetly:** gizlice

**attempt:** girişim

**invasive:** istilacı, saldıran

**obtain:** elde etmek

**query:** sorgu

**trove:** hazine

**consistency:** tutarlılık

**assessment:** değerlendirme

**utilize:** faydalanmak

**adequate:** yeterli

**prevent:** engel olmak

**brief:** kısa bilgi

**capture:** ele geçirmek

**assumption:** varsayım

**demonstrate:** göstermek

**indicate:** belirtmek

**accomplish:** başarmak

**precisely:** kesin olarak

**destination:** hedef

**determine:** belirlemek

**reachable:** ulaşılabilir

**rows:** satırlar

**initial:** ilk,baş

**merged:** birleşmiş, karışmış

**redistribute:** yeniden dağıtmak

**compromises:** tavizler

**adjacencies:** komşuluklar, çevre

**implement:** uygulamak

**client:** müşteri

**policies:** politikalar

**comprehensive:** kapsamlı

**denial:** inkar, reddetmek

**lawn:** çim

**concern:** ilgilendirmek, endişe

**confess:** itiraf etmek

**caveats:** uyarılar

**allegations:** iddialar

**initiate:** başlatmak

**intrusion:** izinsiz giriş

**prevention:** önleme, engelleme

**assumption:** varsayım

**elegant:** hoş, şık, zarif

**scavenger hunt:** search

**necessarily:** ille de, illa ki, her zaman

**current:** mevcut, güncel, akım, akıntı

**repulse:** tiksindirmek

**compensation:** telafi, tazminat (+ for)

**approximately:** yaklaşık olarak (roughly)

**wholesome:** sağlıklı, sağlığa yararlı

**attempt:** teşebbüs etmek

**indication:** gösterge

**defect:** kusur, defo

**exceed:** aşmak (sınır)

**counter:** karşı koymak, karşı

**objective:** hedef, tarafsız

**overtly:** açık bir şekilde, alenen

**assign:** tayin etmek, görevlendirmek

**reach:** ulaşmak

**alluring:** cezbedici, cazip

**previous:** önceki

**respectively:** sırasıyla

**treaty:** anlaşma

**extensive:** kapsamlı

**reversal:** tersine çevirme/dönme

**represent:** temsil etmek

**outside:** dış

**opportunity:** fırsat

**penetrate:** işlemek, nüfuz etmek, girmek

**reiterate:** tekrar tekrar söylemek

**transmit:** iletmek

**enthusiasm:** heves, coşku

**fulfil:** ifa etmek, yerine getirmek

**drought:** kuraklık

**improbable:** muhtemel olmayan

**attribution:** atıf (to)

**persistent:** ısrarcı

**removal:** taşıma, yok etme

**destruction:** yıkım, tahribat

**familiar with:** aşina

**nervously:** gergin bir şekilde

**expect:** beklemek, ummak

**entitlement:** yetkilendirme

**rely on:** güvenmek, bel bağlamak

**pretentious:** gösterişli, abartılı

**impartial:** tarafsız

**cohesion:** kaynaşma

**descendant:** torun

**sincere:** samimi

**naive:** saf

**reputedly:** güya, iddiaya göre (allegedly)

**spoil:** bozmak, şımartmak

**unexpectedly:** beklenmedik bir şekilde

**evade:** elinden kaçmak, kurtulmak

**equivocally:** muğlak bir şekilde

**infrequently:** seyrek bir şekilde

**complete:** tamamlamak

**response:** tepki, cevap

**unfavourably:** olumsuz bir şekilde

**susceptible to:** savunmasız

**insurance:** sigorta

**disclose:** ifşa etmek, açıklamak

**supplement:** desteklemek

**attainable:** ulaşılabilir, başarılabilir

**suspiciously:** şüpheli bir şekilde

**repressive:** baskıcı

indifferent umursamaz
**loosely:** gevşek bir şekilde

**sensible:** mantıklı

**inadequacy:** yetersizlik

**coherent:** tutarlı, anlaşılır

**aware of:** farkında

**candidate:** aday

**desolate:** mutsuz, terkedilmiş

**defectively:** kusurlu bir şekilde

**feasible:** uygulanabilir

**degenerate:** yozlaşmak, bozulmak

**compliment:** iltifat etmek

**contain:** içermek

**partial:** kısmi

**demand:** talep (etmek)

**repudiate:** aksini iddia etmek, reddetmek

**predominant:** baskın

**persuade:** ikna etmek

**dispose of:** kullanıp atmak

**refer to:** atfetmek

**reputably:** ünlü bir şekilde

**reduce:** azaltmak

**useful:** faydalı

**expenditure:** harcama, masraf, gider

**consumption:** tüketim

**conclude:** sonuç çıkarmak, sonlandırmak

**predictable:** tahmin edilebilir

**interaction:** etkileşim

**readily:** hemen

**surely:** elbette

**conditional:** koşullu, durumsal

**outrageously:** abartılı bir şekilde (rüküş)

**convertible:** dönüştürülebilir

**nutrient:** besleyici madde

**expectancy:** beklenti

**pertinently:** uygun bir şekilde

**lift:** kaldırmak, asansör

**dismissal:** kovma

**precisely:** tam olarak

**uniformly:** tek tip bir şekilde

**complacent:** rahat

**account:** hesap

**appearance:** görünüm

**crew:** mürettebat

**indispensably:** vazgeçilmez bir şekilde

**agreement:** anlaşma

**elaborate:** ayrıntılandırmak, açıklamak

**overcome:** üstesinden gelmek

**conform to:** uymak, uyum sağlamak

**commodity:** ticari mal

**launch:** başlatmak, piyasaya sürmek

**informative:** bilgi verici

**compression:** sıkıştırma

**expansive:** kapsamlı, geniş, sohbeti bol

**permanence:** kalıcılık

**vigorously:** gayretli bir şekilde

**bearable:** dayanılabilir

**certainty:** kesinlik

**consultation:** danışma

**appeal:** cazibe, cezbetmek

**deception:** aldatma

**refund:** parasını iade etmek

**permanent:** kalıcı

**range from .. to:** arasında değişmek

**discovery:** keşif

**amplification:** güçlendirme

**thoroughly:** baştan sona, tamamen

**preventive:** önleyici

**adverse:** ters

**abolish:** yürürlükten kaldırmak

**consciously:** bilinçli bir şekilde

**impressively:** etkileyici bir şekilde

**create:** yaratmak

**interfere:** araya girmek, müdahale etmek

**objection to:** itiraz

**legitimacy:** meşruluk

**quantitative:** nicel

**anticipate:** önceden tahmin etmek

**distinctly:** belirgin bir şekilde

**enclose:** iliştirmek, ekte yollamak

**remedy:** çare

**sustenance:** sürdürme

**invest:** yatırım yapmak

**attractive:** çekici

**base:** temel, dayandırmak

**unaccountable:** açıklanamaz

**order:** düzen, sipariş etmek, emretmek

**controversial:** tartışmalı

**investment:** yatırım

**preservation:** koruma

**cooperation:** iş birliği

**graft:** doku nakli yapmak

**comprehensive:** kapsamlı

**fill:** doldurmak

**robust:** sağlam, gürbüz, güçlü, sağlıklı

**permeate:** içine sızmak, nüfuz etmek

**wastefully:** savurgan bir şekilde

**separate:** ayırmak, ayrı

**unstable:** istikrarsız

**rightly:** haklı olarak

**outrageous:** cüretkâr, çirkince, abartılı

**already:** zaten, çoktan

**entire:** tüm

**vulnerable to:** savunmasız, hassas

**assumption:** varsayım

**boastful:** böbürlenen

**reluctantly:** isteksiz bir şekilde

**frequent:** sık

**react to:** tepki göstermek

**principally:** temelde, esasen

**locate:** yerini belirlemek

include içermek
**abuse:** istismar etmek, kötüye kullanmak

**apparent:** açık, belli, bariz

**brutally:** acımasızca

**sustain:** sürdürmek

**inversion:** tersine çevirme

**present:** mevcut, hediye, sunmak

**treat:** davranmak, tedavi etmek

**expression:** ifade

**exhale:** nefes vermek (inhale)

**dismay:** mutsuz etmek

**conditionally:** koşullu olarak

**significantly:** önemli ölçüde

**correlate:** ilişkilendirmek

**corruption:** rüşvet, yolsuzluk

**promptly:** derhal, hemen

**framework:** çerçeve

**abolition:** yürürlükten kaldırma

**untamed:** yabani

**hesitation:** tereddüt, duraksama

**endearingly:** sevimli bir şekilde

**upheaval:** isyan, ayaklanma

**entice:** ayartmak

**daringly:** cesurca

**refusal:** reddetme

**conclusion:** sonuç

**remove:** yok etmek, ortadan kaldırmak

**discredit:** gözden düşmek

**unreliable:** güvenilmez

**issue:** konu, sorun, sayı

**injudicious:** mantıksız

**recommendation:** tavsiye, öneri

**substance:** madde

**exemplary:** örnek niteliğinde

**properly:** düzgün bir şekilde


**Kalıplar:**

**hassle-free:** sorunsuz

**lead to:** sebep olmak, neden olmak

**bring about:** neden olmak

**account for:** oluşturmak

**stem from:** den kaynaklanmak

**end up with:** ile neticelenmek

**figure out:** keşfetmek, çözmek

**rely on:** bel bağlamak, güvenmek

**come up with:** (çözüm) bulmak

**carry out:** yapmak, yürütmek

**break out:** patlak vermek

**make up:** oluşturmak

**put off:** ertelemek

**find out:** öğrenmek, bulmak

**make out:** bir şeyi anlayabilmek

**put up with:** tahammül etmek

**make up for:** telafi etmek

**take over:** ele geçirmek

**break down:** bozulmak, parçalanmak

**pull through:** iyileşmek

**deal with:** ile ilgilenmek

**put out:** söndürmek

**set out:** yola çıkmak

**set up:** kurmak

**take up:** zaman almak,yer kaplamak

**give up:** bırakmak, vazgeçmek

**hold up:** geciktirmek

**turn down:** reddetmek, sesini kısmak

**break into:** zorla soygun amacıyla girmek

**settle down:** yerleşmek

**come across:** karşı karşıya gelmek

**get off:** araçtan inmek

**keep up:** devam etmek

**keep up with:** -e ayak uydurmak

**put down:** isyan vb. bastırmak

**put through:** telefon bağlamak
