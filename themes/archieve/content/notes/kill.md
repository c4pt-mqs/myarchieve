---
title: kill
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

**Proccess killing:**

- kill -9 [ID]
- kill -s SIGKILL [ID]
- kill -s 9 [ID]
- killall -9 [ID]