---
title: BlackArch
date: 2022-09-06T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - linux
  - security
  - penetration-testing
---

AUR packages:

```zsh
sudo pacman -S git base-devel
```

Paketleri ara:

```zsh
pacman -Ss [packet_name]
```

Paket indir:

```zsh
pacman -S [packet_name]
pacman -Syu [packet_name]
```

Paket sil:

```zsh
pacman -R [packet_name]
```

Grupları listele:
```zsh
pacman -Sg | grep blackarch
```

blackman kurulumu:
```zsh
pacman -S blackman
```

tüm paketleri indirmek:
```zsh
blackman -a
```

- https://web.archive.org/web/20231005110029/https://0x00sec.org/t/arch-black-0x0/18031
- https://linuxhint.com/docker_arch_linux/