---
title: Forensics
date: 2023-10-22T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: CTFs
tags:
  - digital-forensics
  - data-recovery
---

Remove the value from Image:

```zsh
bbe -e 's/\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42//g' chall.jpg > new.jpg
# or
bbe -e 's/BBBBBBBBBB//g' chall.jpg > new.jpg
```

