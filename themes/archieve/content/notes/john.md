---
title: john
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Crack the hash:

```zsh
ssh2john id_rsa > hash
```

```zsh
john hash --wordlist=[wordlist_path]
```

John Rules:

```zsh
nano /etc/john/john.conf

[List.Rules:my-rules]
Az"[0-9][0-9]" ^[!@]
```

```zsh
john --wordlist=clinic.lst --rules=my-rules --stdout > dict.lst
```

-2john in MacOS:

- /System/Volumes/Data/opt/homebrew/Cellar/john-jumbo/1.9.0_1/share/john/gpg2john
- /opt/homebrew/Cellar/john-jumbo/1.9.0_1/share/john/gpg2john


**Mask:** https://github.com/openwall/john/blob/bleeding-jumbo/doc/MASK
