---
title: find
date: 2022-10-11T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---


`find . -name flag1.txt`: find the file named "flag1.txt" in the current directory

`find /home -name flag1.txt`: find the file names "flag1.txt" in the /home directory

`find / -type d -name config`: find the directory named config under "/"

`find / -type f -perm 0777`: find files with the 777 permissions (files readable, writable, and 
executable by all users)

`find / -perm a=x`: find executable files

`find /home -user frank`: find all files for user "frank" under "/home"

`find / -mtime 10`: find files that were modified in the last 10 days

`find / -atime 10`: find files that were accessed in the last 10 day

`find / -cmin -60`: find files changed within the last hour (60 minutes)

`find / -amin -60`: find files accesses within the last hour (60 minutes)

`find / -size 50M`: find files with a 50 MB size

`find / -writable -type d 2>/dev/null `: Find world-writeable folders

`find / -perm -222 -type d 2>/dev/null`: Find world-writeable folders

`find / -perm -o w -type d 2>/dev/null`: Find world-writeable folders

`find / -perm -o x -type d 2>/dev/null `: Find world-executable folders

`find / -perm -u=s -type f 2>/dev/null`: Find files with the SUID bit, which allows us to run the file with a higher privilege level than the current user.

**Tools and supported languages:**

`find / -name perl*`

`find / -name python*`

`find / -name gcc*`

find what is where:

`find -name "file-*"`

`find / [filename]`: search all directories

Find all files whose name ends with ".xml"

`find / -type f -name "*.xml"`

Find all files in the /home directory (recursive) whose name is "user.txt" (case insensitive)

`find /home -type d -name "user.txt"`

Find all directories whose name contains the word "exploits"

`find / -type d -name "\*exploits\*"`

Özel olarak içeriğini arama(password)

`grep -l -e "password" -f *`

Search for password fow shadow file

`find / 2>>/dev/null | grep "shadow.*"`
username[1]:x[2]:userid[3]:groupid[4]:useridinfo[5]:/folder/location[6]:/shell/location[7]

İstenileni bul:

`find / -iname "\*flag1\*" 2>/dev/null`

Özel bir text'in hangi klasörde olduğunu bulmak

`find / -type f -exec grep -H 'text-to-find-here' {} \;`

Gizli .txt dosyasına arama yapmak:

`find / -user root -type f 2>&1 | grep -v "Permission" | grep -v "No such"`

find all the files and copy them in a file

`find / -type f \( -name [file] -o -name [file] -o -name [file] \) -exec cp "{}" [copy\_to\_path] \; 2>/dev/null`

### filename, size, user/group, date modified/accessed, keyword contents

What we can do  | Syntax  | Real example of syntax
----------------|---------|-------------------------
Find files based on filename | find [directory path] -type f -name [filename] | find /home/Andy -type f -name sales.txt
Find Directory based on directory name | find [directory path] -type d -name [filename] | find /home/Andy -type d -name pictures
Find files based on size | find [directory path] -type f -size [size] | find /home/Andy -type f -size 10c<br>(c for bytes,<br>k for kilobytes<br>M megabytes<br>G for gigabytes<br>type:'man find' for full information on the  options)
Find files based on username | find [directory path] -type f -user [username] | find /etc/server -type f -user john
Find files based on group name | find [directory path] -type f -group [group name] | find /etc/server -type f -group teamstar
Find files modified after a specific date | find [directory path] -type f -newermt '[date and time]' | find / -type f -newermt '6/30/2020 0:00:00'<br>(all dates/times after 6/30/2020 0:00:00 will be considered a condition to look for)
Find files based on date modified | find [directory path] -type f -newermt [start date range] ! -newermt [end date range] | find / -type f -newermt 2013-09-12 ! -newermt 2013-09-14<br>(all dates before 2013-09-12 will be excluded; all dates after 2013-09-14 will be excluded, therefore this only leaves 2013-09-13 as the date to look for.)
Find files based on date accessed | find [directory path] -type f -newerat [start date range] ! -newerat [end date range] | find / -type f -newerat 2017-09-12 ! -newerat 2017-09-14<br>(all dates before 2017-09-12 will be excluded; all dates after 2017-09-14 will be excluded, therefore this only leaves 2017-09-13 as the date to look for.)
Find files with a specific keyword | grep -iRl [directory path/keyword] | grep -iRl '/folderA/flag'
read the manual for the find command | man find | man find

### copy/move/rename/create files and folders

What we can do  | Syntax  | Real example of syntax
----------------|---------|-------------------------
copy file/folder | cp [filename/folder] [directory] | cp ssh.conf /home/newfolder<br>(remember, if the filename/folder name has spaces then you will need to encase the filename with speech marks such as cp "[filename with spaces]" [directory]. This applis to other commands such as mv. )
move file/folder | mv [filename] [directory] | mv ssh.conf /home/newfolder
move multiple files/folders simultaneously | mv [file1] [file2] [file3] -t [directory to move to] | mv logs.txt keys.conf script.py -t /home/savedWork
Move all files from current directory into another directory | mv * [directory to move files to] | mv * /home/scripts
rename files/folder | mv [current filename] [new filename] | mv ssh.conf NewSSH.conf
create a file | touch [filename] | touch newFile.txt
create a folder | mkdir [foldername] | mkdir newFolder
open file for editing | nano [filename] | nano keys.conf
output contents of file | cat [filename] | cat keys.conf
upload file to a remote machine | scp [filename] [username]@[IP of remote machine ]:/[directory to upload to] | scp example.txt john\@192.168.100.123:/home/john/
run an bash script program | ./[name of script] | ./timer
open a file for reading/editing | nano [filename] | nano readME.txt
