---
title: Academic Research
date: 2022-11-18T09:00:00Z
draft: false
type: note
author: Kaptan
category: Links
subcategory: Useful Links
tags:
  - research
---

[List of Academic Databases and Search Engines](https://en.wikipedia.org/wiki/List_of_academic_databases_and_search_engines)

**İngilizce:**

- https://www.jstor.org/
- https://www.scopus.com/
- https://scholar.google.com/
- https://academia.edu/
- https://researchgate.net/

**Türkçe:**

- https://ulakbim.tubitak.gov.tr/
- https://akademik.yok.gov.tr/
- https://bilimteknik.tubitak.gov.tr/


**Academic Terms:**

- https://techterms.com/