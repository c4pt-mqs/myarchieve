---
title: Other Commands
date: 2022-11-29T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

## amass

```zsh
amass enum -brute -d [URL] -src
amass enum -brute -d [URL] -rf resolvers.txt -w [wordlist]
```

## masscan

```zsh
masscan -p1-65535 -iL [targets] --max-rate 1800 -oG out.log
dnmasscan -p1-65535 [targets] --max-rate 1800 -oG out.log
```

## diff & comm

**Resources:**

- [Comparing files & directories with diff and comm](https://www.networkworld.com/article/3279724/comparing-files-and-directories-with-diff-and-comm.html)
- https://www.geeksforgeeks.org/diff-command-linux-examples/
- https://www.geeksforgeeks.org/comm-command-in-linux-with-examples/

## file & stat

File command reads the file headers and tells you what the file actually is(inspite of what extension is used). Similarly is stat command, which gives you a file's/file system's status.

```zsh
file [filename]
stat [filename]
```

## tee

It will give you a chance to view the output in real-time and save the output in a file at the same time

```zsh
[commands] | tee [output]
ls -l | tee list_of_files.txt
```

## export

This command is used to set the environment variables(The variables that got set whenever a shell/user session is opened). You can read more about this command through [here](https://www.geeksforgeeks.org/export-command-in-linux-with-examples/).

```zsh
export VARIABLE_NAME=value
export PATH=/usr/local/bin:$PATH

echo $VARIABLE_NAME
echo $PATH
```

## reset

Say if your terminal is not working properly, any problem is occurring, but you can't afford to close the shell, you're just one reset command away to get your shell back to normal. 

After running the `reset` command, your terminal should be cleared of any visual artifacts or display issues, and it should return to its default settings.

## systemctl & service

service operates on the files in /etc/init.d and was used in conjunction with the old init system. systemctl operates on the files in /lib/systemd. If there is a file for your service in /lib/systemd it will use that first and if not it will fall back to the file in /etc/init.d.

**Example commands with systemctl:**

- Start a service: `sudo systemctl start [servicename]`
- Stop a service: `sudo systemctl stop [servicename]`
- Enable a service to start on boot: `sudo systemctl enable [servicename]`
- Disable a service from starting on boot: `sudo systemctl disable [servicename]`
- Check the status of a service: `sudo systemctl status [servicename]`

**Example commands with service:**

- Start a service: `sudo service [servicename] start`
- Stop a service: `sudo service [servicename] stop`
- Restart a service: `sudo service [servicename] restart`

**Resoruces:**

- https://gist.github.com/adriacidre/307d2f9f5179fc748f22edac5af3d218

## encode & decode

**Encoding a File to Base64:**

```zsh
base64 [filename]
```

**Decoding a Base64 String:**

```zsh
echo "SGVsbG8gV29ybGQ=" | base64 -d
```

**sha256sum Command:**

```zsh
sha256sum [filename]
```

**md5sum Command:**

```zsh
md5sum [filename]
```

## Brute-force Cracking

Brute-force cracking local user accounts via su:

```zsh
sucrack -u [username] -w 100 [wordlist]
```

- https://github.com/hemp3l/sucrack
- https://labs.portcullis.co.uk/tools/sucrack/

### Zip Cracker

```zsh
fcrackzip -b --method 2 -D -p [wordlist] -v ./[filename]
fcrackzip -vbDp [wordlist] [filename]
```

### CeWL Custom Word List Generator

```zsh
cewl [URL] > [wordlist]
```

## Fix correpted pdf files:
```zsh
pdftocairo -pdf crash_file.pdf repaired.pdf
```

```zsh
gs \
  -o repaired.pdf \
  -sDEVICE=pdfwrite \
  -dPDFSETTINGS=/prepress \
   crash_file.pdf
```