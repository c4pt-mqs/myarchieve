---
title: Linux Temelleri
date: 2023-04-30T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Basics
tags:
  - linux
  - commands
  - system
---

## Dosya Sistemi Hiyerarşisi

| Dizinler   | İşlevi                                 |
|--------------|-------------------------------------------|
| `/bin`     | Sistemin düzgün çalışması için gereken temel komut satırı yardımcı programlarını içerir. /bin dizininde bulunan program örnekleri arasında "ls", "cp" ve "cat" yer alır. |
| `/boot`     | Sistemi başlatmak için gereken dosyaları içerir, örneğin çekirdek ve başlangıç yükleyici  yapılandırma dosyaları. |
| `/dev`     | Donanım cihazlarını temsil eden özel dosyalar olan aygıt dosyalarını içerir, örneğin sabit diskler, USB sürücüler ve yazıcılar. |
| `/etc`     | Sistem ve sistemde yüklü uygulamalar için yapılandırma dosyalarını içerir. |
| `/home`     | Sistemdeki kullanıcıların ev dizinlerini içerir. Her kullanıcının /home dizininde kendi kullanıcı adlarıyla bir alt dizini bulunur ve kişisel dosyaları ile ayarları burada saklanır. |
| `/lib`     | Sistem ve sistemde yüklü uygulamalar için gereken kütüphaneleri içerir. |
| `/media`     | USB sürücüler ve CD'ler gibi çıkarılabilir ortamların bağlama noktalarını içerir. |
| `/mnt`     | Dosya sistemleri için geçici bir bağlama noktası olarak kullanılır. |
| `/opt`     | İsteğe bağlı yazılım paketlerini yüklemek için kullanılır. |
| `/proc`     | Sistem bilgilerini ve çekirdek ayarlarını temsil eden sanal dosyaları içerir. |
| `/root`     | Kök kullanıcısı için ana dizindir ve bu kullanıcı sistem yönetimi görevleri için süper kullanıcıdır. |
| `/sbin`     | Aistem yönetimi görevleri için kullanılan sistem ikili dosyalarını içerir. |
| `/tmp`     | Uygulamalar ve sistem tarafından oluşturulan geçici dosyalar için kullanılır. |
| `/usr`     | Kullanıcıyla ilgili programları, kütüphaneleri ve verileri içerir. |
| `/var`     | Log dosyaları ve spool dizinleri gibi değişken dosyaları içerir. |

## Kısayollar

| Kısayollar   | Açıklaması                                 |
|--------------|-------------------------------------------|
| `Ctrl + A`     | İmleç satır başına gider.                  |
| `Ctrl + E`     | İmleç satır sonuna gider.                 |
| `Ctrl + P`     | Önce çalıştırılmış komut gösterilir.      |
| `Ctrl + N`     | Sonra çalıştırılmış komut gösterilir.     |
| `Alt + B`      | Sola doğru (geri) bir kelime kadar imleç kayar. |
| `Alt + F`      | Sağa doğru (ileri) bir kelime kadar imleç kayar.|
| `Ctrl + F`     | İmleç bir karakter ileri gider.           |
| `Ctrl + B`     | İmleç bir karakter geri gider.            |
| `Ctrl + XX`    | Geçerli imleç konumundan, imleç satır başına geçer. |
| `Ctrl + L`     | Ekran temizlenir ve imleç en üst satıra çıkar yani clear komutu ile aynı işlemi yapar. |
| `Alt + D`     |	İmleçten sonraki kelimeyi siler. |
| `Ctrl + U`    | 	İmlecin solundaki her şeyi siler. |
| `Ctrl + K`    | 	İmlecin sağındaki her şeyi siler. |
| `Ctrl + Y`    | 	Kesilmiş olan son metni ekrana yapıştırır. |
| `Esc + T`     |	İmleçten önceki iki kelime yer değiştir. |
| `Ctrl + H`    | 	Sola doğru tek tek karakterleri siler.(yani Backspace gibi davranır) |
| `Alt + U`     |	İmlecin başladığı yerden sözcüğün sonuna kadar bütün karakterleri büyük harf yapar. |
| `Alt + L`     |	İmlecin başladığı yerden sözcüğün sonuna kadar bütün karakterleri küçük harf yapar. |
| `Alt + C`     |	İmlecin üstünde bulunduğu karakteri büyük harf yapar. |
| `Ctrl + C`    |	Komutu durdurur/keser. |
| `Ctrl + R`    |	Daha önce kullanılmış olan komutlar arasında arama yapma ve o komutu tekrardan kullanma imkanı sağlar. |
| `Ctrl + Z`    |	Çalışan süreci arka plana atar ve duraklatır. |
| `Ctrl + D`    |	Veri girişinin sonunu temsil eden yani mevcut terminali sonlandırır. |
| `TAB`   |  Konsol üzerinden bir kısmını yazmış olduğunuz, dosya, dizin ve komut türevi her türlü ifadeyi otomatik tamamlar. Ayrıca iki kez üst üste basıldığında da yazmış olduğunuz ifade ile başlayan tüm içeriği listeler. |

