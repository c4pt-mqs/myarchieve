---
title: tr
date: 2022-10-23T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Flags        | Description                               
-------------|-------------------------------------------
`-d` | To delete a given set of characters
`-t` | To concat source set with destination set(destination set comes first; t stands for truncate)
`-s` | To replace the source set with the destination set(s stands for squeeze)
`-c` | This is the REVERSE card in this game, for eg. If you specify -c with -d to delete a set of characters then it will delete the rest of the characters leaving the source set which we specified (c stands for complement; as in doing reverse of something)

```zsh
cat file.txt | tr -s '[:lower:]' '[:upper:]'
```