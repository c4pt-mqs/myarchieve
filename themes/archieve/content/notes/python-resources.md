---
title: Python Resources
date: 2022-12-03T09:00:00Z
draft: false
type: note
author: Kaptan
category: Programming
subcategory: Languages
tags:
  - python
  - programming
  - resources
  - coding
---

**Python Programming Reference Sheet:**

https://pythonprinciples.com/reference/

**Learn regex with:**

https://www.w3resource.com/python-exercises/re/
https://www.hackerrank.com/domains/regex

**Python Projects Challenges:**

https://projecteuler.net/

**About Images:**

- https://realpython.com/storing-images-in-python/
- https://erdogant.github.io/undouble/pages/html/index.html
- https://nostarch.com/recursive-book-recursion
- https://inventwithpython.com/bigbookpython/

