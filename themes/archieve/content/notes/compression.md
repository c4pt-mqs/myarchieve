---
title: Compression
date: 2023-04-19T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

```zsh
binwalk -e flag
```

```zsh
lzip -d -k flag
```

```zsh
lz4 -d flag.out flag2.out
```

```zsh
lzma -d -k flag.lzma
```

```zsh
lzop -d -k flag.lzop -o flag2
```

```zsh
xz -d -k flag.xz
```

Tar sıkıştırma:

```zsh
tar -czvf [filename].tar.gz ./[output_filename]  
```

Tar'dan çıkarma:

```zsh
tar xfz [filename].tar.gz
```

**Resources:**

- https://neverendingsecurity.wordpress.com/2015/04/13/linux-tar-commands-cheatsheet/
- https://www.geeksforgeeks.org/tar-command-linux-examples/
