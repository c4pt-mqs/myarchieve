---
title: radare2
date: 2022-12-09T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

**Debugging Mode:**

```zsh
gdb [file] 
```

**Print disassembled function:**

```zsh
pdf
```

**Analyze all (fcns + bbs) same that running r2 with -A:**

```zsh
aa
```

**List all functions (number of functions):**

```zsh
afl
```

**Set breakpoint:**

```zsh
db [addr]
```

**Remove breakpoint:**

```zsh
db -[addr]
```

**Step:**

```zsh
ds [num]
```

**Step over:**

```zsh
dso [num]
```

```zsh
s [address]: Move cursor to address or symbol
s [function_name]
```

**View ascii-art basic block graph of current function:**

```zsh
V or V!
```

**Debug in Visual(V) Mode:**

**p/P:** Rotate print (visualization) modes
    hex, the hexadecimal view
    disasm, the disassembly listing
		Use numbers in [] to follow jump
		Use "u" to go back
		Use "V" to view graph mode

- toggle breakpoints with F2
- single-step with F7 (s)
- step-over with F8 (S)
- continue with F9

- **pf**: Print with format
- **pf.**: list all formats
- **pf [5]z @ [vaddr]**: print the vaddr value of 5 charcters

- **iz**: Strings in data section
- **izz**: Strings in the whole binary

- **axT:** Returns cross references to (xref to)

```zsh
axt @ @ str.*
```

**axF:** Returns cross references from (xref from)

```zsh
axF @ [vaddr]
```

- **iI:** will print basic info
- **ii:** Imports

- **iS:** Sections
  - iS~w returns writable sections
- **is:** Symbols
  - is~FUNC exports

**value of the bytes:**

```zsh
rax2 -s 0x403085
```

**byte decode:**

```zsh
bytes.fromhex("[bytes]").decode("utf-8")
```

**Resources:**

- https://r2wiki.readthedocs.io/en/latest/
- [Radare2 Series 0x2 - r2pm Ghidra Decompiler Usage in R2](https://www.youtube.com/watch?v=98AmXMxTXIE&list=PLCwnLq3tOElpDyvObqsAI5crEO9DdBMNA&index=4)
- [Binary Exploitation / Memory Corruption by LiveOverflow](https://www.youtube.com/playlist?list=PLhixgUqwRTjxglIswKp9mpkfPNfHkzyeN)
- https://dojo.pwn.college/cse466/asm