---
title: Security & Privacy
date: 2023-02-11T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: OSINT
tags:
  - privacy
  - security
  - anonymity
  - tools
---

**Applications:**
- TOR: https://www.torproject.org
- Zeronet: https://zeronet.io/ 
- I2P: https://geti2p.net/en/ 
- Freenet: https://freenetproject.org/index.html 

**Cloud Virtual Machines:**
- Google Cloud:
  - https://aws.amazon.com/workspaces/ 
- Microsoft Azure VDI:
  - https://cloud.google.com/compute
  - https://azure.microsoft.com/en-au/free/virtual-machines
- Paperspace:
  - https://www.paperspace.com/

