---
title: Movies
date: 2022-11-14T09:00:00Z
draft: false
type: note
author: Kaptan
category: Self Improvement
subcategory: Things to Watch
tags:
  - movies
  - self-improvement
---


## Biography/Drama

- [ ] Steve Jobs
- [ ] MoneyBall
- [x] The Imitation Game
- [ ] The Social Network
- [x] Enigma
- [x] Silk Road
- [x] Snowden
- [ ] The Theory of Everything
- [x] Mr. Nobody
- [ ] A Beautiful Mind


## Sci-Fi/Thriller:

- [x] Black Mirror / Shut up and Dance
- [ ] Searching
- [ ] Minority Report
- [ ] Enemy of State
- [ ] Artificial Intelligence
- [ ] Her
- [ ] The Net
- [ ] Antitrust
- [x] I, Robot
- [x] Source Code
- [ ] Transcendence
- [x] Arrival
- [x] Blackhat
- [x] Limitless
- [ ] OtherLife
- [x] IBoy
- [ ] In Time
- [ ] TAU
- [x] Ghost in the Shell
- [x] Blade Runner
- [x] The Matrix
- [x] Terminator
- [x] Upgrade
- [ ] Akira
- [ ] Johnny Mnemonic
- [ ] RoboCop
- [x] Başlat: Ready Player One
- [x] Runner Runner
- [ ] The Truman Show
- [x] Eternal Sunshine of the Spotless Mind
- [ ] Westworld
- [x] Weathering with You
- [ ] Don't Breathe 2


## Action/Adventure

- [x] Star Wars Series
- [x] Alita: Battle Angel
- [x] Ghost Rider Series
- [ ] Top Gun & Maverick
- [x] Red Notice
- [ ] Dune Series


## Crime/Drama

- [x] Out of Sight
- [x] Snowpiercer
- [x] No Country For Old Men


## Mystery/Thriller

- [x] Identity
- [x] The Prestige
- [ ] Memento
- [ ] Stay
- [x] Maze Runner Series
- [x] Saw


## Comedy

- [ ] The Hangover
- [ ] Superbad
- [ ] Bridesmaids
- [ ] Anchorman: The Legend of Ron Burgundy
- [ ] The Grand Budapest Hotel
- [x] Charlie's Angels


## Animation/Family

- [ ] Toy Story
- [ ] Finding Nemo
- [ ] Spirited Away
- [ ] The Lion King
- [x] Shrek


## War/History

- [x] Schindler's List
- [x] Gladiator
- [ ] Braveheart
- [ ] Lincoln
- [ ] Apollo 13
- [x] The Pianist
