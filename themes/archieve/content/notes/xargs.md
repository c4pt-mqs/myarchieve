---
title: xargs
date: 2022-12-13T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Flags                | Description                               
---------------------|-------------------------------------------
`-0` | Will terminate the arguments with null character (helps to handle spaces in the argument)
`-a file` | This option allows xargs to read item from a file
`-d delimiter` | To specify the delimiter to be used when differentiating arguments in stdin
`-L int` | Specifies max number non-blank inputs per command line
`-s int` | Consider this as a buffer size that you allocate while running xargs, it sets the max-chars for the command, which includes it's initial arguments and terminating nulls as well.(You won't be using this most of the times but it's good to know). Default size is around 128kB (if not specified).
`-x` | This flag will exit the command execution if the size specified is exceeded.(For security purposes.)
`-E str` | This is to specify the end-of-file string (You can use this in case you are reading arguments from a file)
`-I str` | (Capital i) Used to replace str occurrence in arguments with the one passed via stdin(More like creating a variable to use later)
`-p` | prompt the user before running any command as a token of confirmation.
`-r` | If the standard input is blank (i.e. no arguments passed) then it won't run the command.
`-n int` | This specifies the limit of max-args to be taken from command input at once. After the max-args limit is reached, it will pass the rest arguments into a new command line with the same flags issued to the previously ran command. (More like a looping) 
`-t` | verbose; (Print the command before running it).Note: This won't ask for a prompt

Running Multiple Commands with xargs in One Line:

```zsh
echo "file1 file2 file3" | xargs -t -I argVar sh -c "touch argVar && ls -l argVar"
```

Using xargs with conjunction to find and delete files:

```zsh
find / -type f -print0 | xargs -0 -t -I {} rm -f {}
```

Using xargs to grep text from files in directories meeting a specific pattern/criteria:

```zsh
find . -type f -print0 2>/dev/null | xargs -0 -I {} grep '[your_pattern]' {}
```

Example of using xargs to count lines in multiple files:

```zsh
find [/path/to/search] -type f -print0 | xargs -0 wc -l
```
