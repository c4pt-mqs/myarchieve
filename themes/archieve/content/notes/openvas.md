---
title: openvas
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

## Installation

```zsh
apt-get install postgresql-14
```

```zsh
apt-get install openvas -y
```

```zsh
apt-get full-upgrade && apt-get update -y
```

If you take version error(that's for 12-13):

{{< figure src="/img/notes/cyber-security/commands/openvas-1.png" alt="openvas commands" loading="lazy" width="70%" height="auto" >}}

<br>

```zsh
gvm-setup
```

```zsh
gvm-check-setup
```

UserName - Pass:

```zsh
sudo runuser -u _gvm -- gvmd --create-user=[user_name] --password=[password]
```

If you want: change pass:

```zsh
sudo gvmd --user=madeofpocket0011 --new-password=[password]
```

```zsh
systemctl restart postgresql
```

```zsh
gvm-check-setup
```

```zsh
sudo gvmd --user=madeofpocket0011 --new-password=[password]
```

**to Browser:** https://localhost:9392

