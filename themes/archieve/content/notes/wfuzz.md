---
title: wfuzz
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Host başlığını değiştirerek URL'ye istekler gönderir ve 977 kelime içeren yanıtları görüntüler:

```zsh
wfuzz -c -w [wordlist_path] -u [URL] -H "Host: FUZZ.[domain]" --hw 914
```