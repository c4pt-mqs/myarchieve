---
title: powershell
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Powershell bypasses the execution

```zsh
powershell -ep bypass
```

Enumerate the domain users

```zsh
Get-NetUser | select cn
```

Enumerate the domain groups

```zsh
Get-NetGroup -GroupName *admin*
```

https://gist.github.com/HarmJ0y/184f9822b195c52dd50c379ed3117993

```zsh
# ls | dir
Get-ChildItem
```

- -Path Specifies a path to one or more locations. Wildcards are accepted.
- -File / -Directory To get a list of files, use the File parameter. To get a list of directories, use the Directory parameter. You can use the Recurse parameter with File and/or Directory parameters.
- -Filter Specifies a filter to qualify the Path parameter.
- -Recurse Gets the items in the specified locations and in all child items of the locations.
- -Hidden To get only hidden items, use the Hidden parameter.
- -ErrorAction SilentlyContinue Specifies what action to take if the command encounters an error.

For example, if you want to view all of the hidden files in the current directory you are in, you can issue the following command:

```zsh
Get-ChildItem -File -Hidden -ErrorAction SilentlyContinue
```

Another useful cmdlet is Get-Content. This will allow you to read the contents of a file.
You can run this command as follows:

```zsh
Get-Content -Path file.txt
```

Measure-Object in PowerShell is used to measure the property of the command. There are various measurement parameters are available.

To change directories

```zsh
Set-Location
```

Search a particular file for a pattern

```zsh
Select-String
```

When you stucked

```zsh
Get-Help
```

Search for the lines

```zsh
(Get-Content -Path .\[file_name])[line_number] 
```

search for the words

```zsh
Select-String -Path '2.txt' -Pattern 'Redryder' -AllMatches
Get-Content -Path .\[file_name] | Select-String -Pattern 'my_word'
```

hash of a file

```zsh
Get-FileHash -Algorithm MD5 file.txt
```

view ADS using

```zsh
Get-Item -Path file.exe -Stream *
```

run to launch the hidden executable hiding within ADS

```zsh
wmic process call create $(Resolve-Path file.exe:streamname)
```

cmd History

```zsh
type %userprofile%\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt
```

PowerShell History

```zsh
type $Env:userprofile\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt
```

Users credentials

```zsh
cmdkey /list
```

Password login

```zsh
runas /savecred /user:admin cmd.exe
```

Password Files
- C:\inetpub\wwwroot\web.config
- C:\Windows\Microsoft.NET\Framework64\v4.0.30319\Config\web.config

```zsh
type C:\Windows\Microsoft.NET\Framework64\v4.0.30319\Config\web.config | findstr connectionString
```

proxy credentials - ProxyPassword

```zsh
reg query HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\Sessions\ /f "Proxy" /s
```
