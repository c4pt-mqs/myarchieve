---
title: nikto
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Temel kullanım:

```zsh
nikto -h [URL]
```

```zsh
nikto -h 10.10.11.11 -p 80,8000,8080
```

Kullanıcı adı ve şifre ile kimlik doğrulama:

```zsh
nikto -h [URL] -id [username]:[password]
```

Nmap ile ip bloğunu tarayıp nikto ile taratmak:

```zsh
nmap -p80 [IP]/24 -oG - | nikto -h -
```

```zsh
# -T: Tuning
# 1: Interesting files
# 2: Misconfiguration
# 3: Information Disclosure
# 4: Injection (XSS/Script/HTML)
nikto -T 1 2 3 -h [IP]
```

```zsh
# -useragent: Custom user agent
nikto -useragent <user-agent> -h [IP]
```

```zsh
# -e: IDS evasion
# 1: Random URI encoding
# 7: Change the case of URL
nikto -e 1 7 -h [IP]
```