---
title: Keep on Mind
date: 2022-09-22T09:00:00Z
draft: false
type: note
author: Kaptan
category: Bug Bounty
subcategory: Basics
tags:
  - bug-bounty
  - cybersecurity
  - hacking
  - learning
---

**Important Ideas:**

I have found that leads to consistent results, is persistence. Being persistent in looking for and exploiting vulnerabilities will almost always lead to success.

**Rule 1:**
`Stick with one program.`

**Rule 2:**
`Observe your actions and ask yourself a question what I am doing wrong?`

- **Time Management:**
{College + Content Creation + Bug bounty + Learn something new in cybersecurity}

- **Advice:**
Take time to select a target but don't jump from one to another. It wastes your time. Stick to a program for a longer period.

- **Patience:**
When I don't find a bug after spending a lot of time, I think it's a waste of time. But I can learn something new in this time. I was wrong, and every failure taught me so much!

- Explain how an attacker can use this against the organisation

- Pick one bug and look for it everywhere

- POC or GTFO

- Write a quality report

- Invest in yourself

- Focus less on $ and more on learning

-  `Persistence and patience is the key`

-  It's all hardwork, there is no shortcuts

-  Use tools; but learn how & why they work.?

- True master never stop learning: be hamble

- READ more & PRACTISE = all the information there is out there

- You don't have write code but read

- Stay curious: My crime is that of curiosity ' _ `

- Rest = No burnout

- Share & show what you've done
