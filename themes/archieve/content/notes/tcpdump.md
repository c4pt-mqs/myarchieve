---
title: tcpdump
date: 2022-10-11T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

**Capture All Network Traffic on a Specific Interface:**

```zsh
sudo tcpdump -i eth0
```

**Capture Traffic to and from a Specific IP Address:**

```zsh
sudo tcpdump host 192.168.1.11
```

**Capture Traffic on a Specific Port:**

```zsh
sudo tcpdump port 80
```

**Save Captured Traffic to a File:**

```zsh
sudo tcpdump -i eth0 -w captured.pcap
```

**Read Captured Traffic from a File:**

```zsh
tcpdump -r captured.pcap
```

**Display Captured Packets in Real-Time:**

```zsh
sudo tcpdump -i eth0 -c 10
```
