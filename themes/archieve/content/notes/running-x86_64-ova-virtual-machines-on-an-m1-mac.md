---
title: Running x86_64 .ova Virtual Machines on an M1 Mac
date: 2023-09-30T09:00:00Z
draft: false
type: note
author: Kaptan
category: Help
subcategory: Support
tags:
  - virtualization
  - macos
  - qemu
---

Here's a refined guide to smoothly run x86_64 .ova virtual machines on your M1 Mac:

1. **Install Homebrew (if not already installed):**

```zsh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

2. **Install QEMU:**

```zsh
brew install qemu
```

3. **Extract the `.ova` File:**

```zsh
tar -xvf /path/to/ova
```

4. **Convert the .ova File to a .qcow2 File:**

```zsh
qemu-img convert -O qcow2 /path/to/vdmk /path/to/output/qcow2
```

- Ensure the output path has the .qcow2 extension.
- The process may take up to 5 minutes with no visible output until completion.

5. **Download UTM from [here](https://mac.getutm.app/)**

6. **Create a New Virtual Machine in UTM:**

- Click the "+" icon on the top menu and select "Start from scratch".
- Navigate to the "Drives" tab and click "Import Drive", then choose the .qcow2 file we generated.
- In some scenarios, you might need to disable UEFI booting:
  - Go to "System", then "Advanced Settings", and unselect "UEFI Booting".
- To optimize performance, allocate at least 6GB of RAM, 6 cores, and enable multi-core mode.
- Finally, click "Save".

7. **Start the Virtual Machine and enjoy x86_64 emulation on your M1 Mac!**

**Resource:** https://gist.github.com/tadhgboyle/a0c859b7d7c0a258593dc00cdc5006cc
