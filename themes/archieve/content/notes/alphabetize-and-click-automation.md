---
title: Alphabetize and Click Automation
date: 2023-10-24T09:00:00Z
draft: false
type: note
author: Kaptan
category: Programming
subcategory: Projects
tags:
  - javascript
  - automation
---

```js
var inputTextarea = document.getElementById("words");
var outputTextarea = document.getElementById("answer");

var wordsArray = inputTextarea.value.split(", ");

wordsArray.sort();

var sortedWords = wordsArray.join(", ");

outputTextarea.value = sortedWords;

var submitButton = document.querySelector('button.button--main.right');
submitButton[1].click();
```

&&

```js
var inputTextarea = document.getElementById("words");
var outputTextarea = document.getElementById("answer");

var wordsArray = inputTextarea.value.split(", ");

wordsArray.sort();

var sortedWords = wordsArray.join(", ");

outputTextarea.value = sortedWords;

var buttons = document.querySelectorAll('.button.button--main.right');

for (var i = 0; i < buttons.length; i++) {
    if (buttons[i].textContent === "Submit") {
        buttons[i].click();
        break;
    }
}
```
