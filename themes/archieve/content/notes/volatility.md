---
title: Volatility
date: 2022-08-22T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

## Volatility 2

**Image Identification:**

- _imageinfo:_ Retrieve information about the memory image.

**Process Analysis:**

- _pslist:_ List running processes.
- _pstree:_ Display process tree.
- _cmdline:_ Extract command-line arguments of a process.
- _dlllist:_ List loaded DLLs for a process.
- _handles:_ List open handles for a process.

**Memory Dumping:**

- _memdump:_ Dump the memory of a specific process.

**Registry Analysis:**

- _hivelist:_ List registry hive offsets.
- _printkey:_ Print registry key values.
- _hashdump:_ Dump password hashes from the SAM database.

**File Analysis:**

- _filescan:_ Scan for file objects in memory.
- _mftparser:_ Parse the Master File Table (MFT) for file metadata.
- _dumpfiles:_ Extract files from memory.

**Network Analysis:**

- _connections:_ List network connections.
- _sockets:_ List open sockets.

**Malware Analysis:**

- _malfind:_ Find hidden or injected code.
- _malthfind:_ Find hidden processes.
- _ldrmodules:_ List loaded modules.

**Timeline Analysis:**

- _timeliner:_ Generate a timeline of system activity.
- _filescan:_ Scan for file objects with timestamps.

**Volatility Configuration:**

- _-\-plugins:_ Specify additional plugin directories.
- _-\-profile:_ Specify the profile of the memory image.


## Volatility 3

**Image Identification:**

- _windows.info.Info:_ Retrieve information about the memory image.

**Process Analysis:**

- _windows.pslist.PsList:_ List running processes.
- _windows.pstree.PsTree:_ Display process tree.
- _windows.cmdline.CmdLine:_ Extract command-line arguments of a process.
- _windows.dlllist.DllList:_ List loaded DLLs for a process.
- _windows.handles.Handles:_ List open handles for a process.

**Memory Dumping:**

- _windows.memdump.MemDump:_ Dump the memory of a specific process.

**Registry Analysis:**

- _windows.registry.hivelist.HiveList_: List registry hive offsets.
- _windows.registry.printkey.PrintKey_: Print registry key values.
- _windows.registry.hive.Hive_: Extract registry hive.

**File Analysis:**

- _windows.filescan.FileScan:_ Scan for file objects in memory.
- _windows.mftparser.MFTParser:_ Parse the Master File Table (MFT) for file metadata.
- _windows.dumpfiles.DumpFiles:_ Extract files from memory.

**Network Analysis:**

- _windows.netstat.Netstat:_ List network connections.
- _windows.sockets.Sockets:_ List open sockets.

**Malware Analysis:**

- _windows.malfind.Malfind:_ Find hidden or injected code.
- _windows.malthfind.Malthfind:_ Find hidden processes.
- _windows.modules.Modules:_ List loaded modules.

**Timeline Analysis:**

- _windows.timeline.Timeline:_ Generate a timeline of system activity.
- _windows.filescan.FileScan:_ Scan for file objects with timestamps.

**Volatility Configuration:**

- _-\-plugin-dirs:_ Specify additional plugin directories.
- _-\-profile:_ Specify the profile of the memory image.


## Resources

- https://github.com/volatilityfoundation/volatility3

- https://github.com/volatilityfoundation/volatility/wiki/Command-Reference#imageinfo

- https://volatility3.readthedocs.io/en/latest/

- https://samsclass.info/121/proj/p5-Vol.htm

- https://downloads.volatilityfoundation.org/releases/2.4/CheatSheet_v2.4.pdf

- https://book.hacktricks.xyz/generic-methodologies-and-resources/basic-forensic-methodology/memory-dump-analysis/volatility-examples

- https://www.varonis.com/blog/how-to-use-volatility

- https://www.sans.org/blog/for408-windows-forensic-analysis-has-been-renumbered-to-for500-windows-forensics-analysis/

- https://github.com/stuxnet999/MemLabs

- [SecIC January 2018: "Memory Forensics with Vol(a|u)tility" - Matt Brenton / @chupath1ngee](https://www.youtube.com/watch?v=dB5852eAgpc)

- https://github.com/kevthehermit/VolUtility

- https://dfir.science/2022/02/Introduction-to-Memory-Forensics-with-Volatility-3
