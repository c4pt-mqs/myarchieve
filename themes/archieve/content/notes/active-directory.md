---
title: Active Directory
date: 2022-10-13T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: CTFs
tags:
  - windows-security
  - cyber-security
  - security
---

In a Windows domain, credentials are stored in a centralised repository called: **Active Directory**

The server in charge of running the Active Directory services is called: **Domain Controller**

Security Group       | Description                               
---------------------|--------------------------
Domain Admins | Users of this group have administrative privileges over the entire domain. By default, they can administer any computer on the domain, including the DCs.
Server Operators | Users in this group can administer Domain Controllers. They cannot change any administrative group memberships.
Backup Operators | Users in this group are allowed to access any file, ignoring their permissions. They are used to perform backups of data on computers.
Account Operators | Users in this group can create or modify other accounts in the domain.
Domain Users | Includes all existing user accounts in the domain.
Domain Computers | Includes all existing computers in the domain.
Domain Controllers | Includes all existing DCs on the domain.
SYSTEM / LocalSystem | An account used by the operating system to perform internal tasks. It has full access to all files and resources available on the host with even higher privileges than administrators.
Local Service | Default account used to run Windows services with "minimum" privileges. It will use anonymous connections over the network.
Network Service | Default account used to run Windows services with "minimum" privileges. It will use the computer credentials to authenticate through the network.

- **Builtin**: Contains default groups available to any Windows host.
- **Computers**: Any machine joining the network will be put here by default. You can move them if needed.
- **Domain Controllers**: Default OU that contains the DCs in your network.
- **Users**: Default users and groups that apply to a domain-wide context.
- **Managed Service Accounts**: Holds accounts used by services in your Windows domain.

<hr>

- **OUs**; are handy for **applying policies** to users and computers, which include specific configurations that pertain to sets of users depending on their particular role in the enterprise. Remember, a user can only be a member of a single OU at a time, as it wouldn't make sense to try to apply two different sets of policies to a single user.
- **Security Groups**; on the other hand, are used to **grant permissions over resources**. For example, you will use groups if you want to allow some users to access a shared folder or network printer. A user can be a part of many groups, which is needed to grant access to multiple resources.

<hr>

- **Domain Admins**: they control the domains and are the only ones with access to the domain controller
- **Service Accounts (can be domain admins)**: these are never used except for service maintenance, they are required by Windows for services such as SQL to pair a service with a service account
- **Local Administrators**: can make changes to local machines as an admin and may control other normal users, but cannot access the domain controller
- **Domain Users**: everyday users, can login to machines they have authorization to access and may have local admin rights to machines depending on the organization
Groups make it easier to give permissions to users and objects by organizing them into groups with specific permissions. There are two overarching types of AD groups:
- **Security Groups**: used to specify permissions for a large number of users
- **Distribution Groups**: used to specify email distribution lists
There are a lot of default security groups:
- **Domain Controllers**: All domain controllers in the domain
- **Domain Guests**: All domain guests
- **Domain Users**: All domain users
- **Domain Computers**: All workstations and servers joined to the domain
- **Domain Admins**: Designated administrators of the domain
- **Enterprise Admins**: Designated administrators of the enterprise
- **Schema Admins**: Designated administrators of the schema
- **DNS Admins**: DNS Administrators Group
- **DNS Update Proxy**: DNS clients who are permitted to perform dynamic updates on behalf of some other clients (such as DHCP servers).
- **Allowed RODC Password Replication Group**: Members in this group can have their passwords replicated to all read-only domain controllers in the domain
- **Group Policy Creator Owners**: Members in this group can modify group policy for the domain
- **Denied RODC Password Replication Group**: Members in this group cannot have their passwords replicated to any read-only domain controllers in the domain
- **Protected Users**: Members of this group are afforded additional protections against authentication security threats. See http://go.microsoft.com/fwlink/?LinkId=298939 for more information.
- **Cert Publishers**: Members of this group are permitted to publish certificates to the directory
- **Read-Only Domain Controllers**: Members of this group are Read-Only Domain Controllers in the domain
- **Enterprise Read-Only Domain Controllers**: Members of this group are Read-Only Domain Controllers in the enterprise
- **Key Admins**: Members of this group can perform administrative actions on key objects within the domain.
- **Enterprise Key Admins**: Members of this group can perform administrative actions on key objects within the forest.
- **Cloneable Domain Controllers**: Members of this group that are domain controllers may be cloned.
- **RAS and IAS Servers**: Servers in this group can access remote access properties of users

### Applications

- Executables
- winPEAS.exe
- Seatbelt.exe (compile)
- Watson.exe (compile)
- SharpUp.exe (compile)

+ PowerShell
+ Sherlock.ps1
+ PowerUp.ps1
+ jaws-enum.ps1
+ Exploit Suggester (Metasploit)
+ windows-exploit-suggester.py (local)

* winPEAS > PowerUp > suggester

* PuTTY\plink.exe

**Resources:**

- https://book.hacktricks.xyz/windows-hardening/checklist-windows-privilege-escalation
- https://github.com/carlospolop/PEASS-ng
- https://sushant747.gitbooks.io/total-oscp-guide/content/privilege_escalation_windows.html
- [PayloadsAllTheThings - Active Directory Attack](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Active%20Directory%20Attack.md)
- https://github.com/Tib3rius/Active-Directory-Exploitation-Cheat-Sheet