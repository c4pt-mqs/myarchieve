---
title: Documentaries
date: 2022-11-14T09:00:00Z
draft: false
type: note
author: Kaptan
category: Self Improvement
subcategory: Things to Watch
tags:
  - science
  - technology
  - documentaries
---

## Science and Nature Documentaries

- [ ] Planet Earth (2006) | IMDb 9.5
- [ ] Cosmos: A Spacetime Odyssey (2014) | IMDb 9.5
- [ ] Cosmos (1980) | IMDb 9.3
- [ ] Human Planet: Behind the Lens (2011) | IMDb 9.3
- [ ] Frozen Planet (2011) | IMDb: 9.3
- [ ] Africa (2013) | IMDb: 9.3
- [ ] Life in the Undergrowth (2005) | IMDb: 9.2
- [ ] Life (2009) | IMDb: 9.1
- [ ] Nature's Great Events (2009) | IMDb: 9.1
- [ ] The Universe (2007) | IMDb: 9.0


## Discovery and Information Documentaries

- [ ] Through the Wormhole (2010) | IMDb: 8.9
- [ ] Wonders of the Universe (2011) | IMDb: 8.8
- [ ] Journey to the Edge of the Universe (2008) | IMDb: 8.7
- [ ] Nuit et brouillard (1956) | IMDb 8.6
- [ ] Home (2009) | IMDb: 8.6
- [ ] Senna (2010) | IMDb: 8.6
- [ ] The Cove (2009) | IMDb: 8.5
- [ ] Zeitgeist: The Movie (2007) | IMDb: 8.3
- [ ] Inside Job (2010) | IMDb: 8.2
- [ ] The Fog of War (2003) | IMDb 8.2


## Computer, Technology and Cyber-Security Documentaries

- [ ] Deep Web (2015)
- [ ] Terms and Conditions May Apply (2013)
- [ ] Ashley Maddison: Sex, Lies and Cyber Attacks (2016)
- [ ] Cybercrimes with Ben Hammersley (2014)
- [ ] Silicon Cowboys (2016)
- [ ] Steve Jobs: The Lost Interview (2012)
- [ ] The Startup Kids (2012)
- [ ] AlphaGo (2017)
