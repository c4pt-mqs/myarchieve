---
title: Mail Parameters
date: 2023-02-01T09:00:00Z
draft: false
type: note
author: Kaptan
category: Bug Bounty
subcategory: Methodology
tags:
  - bug-bounty
  - security
---

```zsh
email=v@g.com&email=a@g.com
email[]=v@g.com&email[]=a@g.com
email=v@g.com%20email=a@g.com
email=v@g.com|email=a@g.com
email=v@g.com,a@g.com
email=v@g.com%20a@g.com
{"email":"v@g.com","email":"a@g.com"}
{"email":["v@g.com","a@g.com"]}
```