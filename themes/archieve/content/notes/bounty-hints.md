---
title: Hints
date: 2022-09-22T09:00:00Z
draft: false
type: note
author: Kaptan
category: Bug Bounty
subcategory: Methodology
tags:
  - bug-bounty
  - security
  - vulnerability-research
---

this will add all the [domain]'s subdomains in burp suite
.*\.domain\.com$


**Tips:**

  1. Look for a company that suites with your interests or industry expertise.
  2. Check thier triager response as well as the reward offered for bugs.
  3. Read about other hackers' experiences and findings for that company if its a large company.

**Paying attention to out of scope / exclusions list:**

XSS, CSRF, Business Logic, Info Disclosure, Subdomain Takeovers

**Automate your works:**

If you write the same command (that is relative long) 2 or more times a day, then make a function in bashrc or make a script and move it to /usr/local/bin to call it from everywhere. This will save you time.

+ Automate subdomain enumeration and discovery.
+ Automate brute-forcing directories.
+ Automate visualization of live subdomains.
+ Automate everything that takes "long" time to do it manually so you can focus on something else while it is running.

{{< figure src="/img/notes/bug-bounty/methodology/bounty-hints-1.png" alt="bug bounty hints" loading="lazy" width="70%" height="auto" >}}

**Scan:**

  - https://remonsec.com/posts/passive-recon-with-spyse-part-I/
  - https://remonsec.com/posts/passive-recon-with-spyse-part-II/


1. technology being used on the website (programing language)
2. subdomains
3. subdomain take over checker
4. DNS records, Reverse DNS
5. screenshots of sub domains
6. Database being used
7. Server type
8. emails that belongs to the web or usernames
9. hidden HTTP parameters


**More:**

+ Tüm portswigger lab'ları ve OWASP TOP güvenlik açıkları
+ Başlangıç olarak HackerOne
+ Az ödül & Az rapor olan şirketler
+ Yeni programlara XSS denemek
+ Hesap devralma ve SQL açıkları
+ Bazı şirketler rapora ip adresini eklemeni de istiyor.
