---
title: nmap
date: 2022-10-23T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Basic methodologies of web penetration tests. A default port is 80. HTTPS uses a port 443.

**Enumeration:**

```zsh
nmap --script http-auth --script-args http-auth.path=/login -p 80,443 <target-ip>
nmap --script http-devframework -p 80,443 <target-ip>
nmap --script http-enum -p 80,443 <target-ip>
nmap --script http-headers -p 80,443 <target-ip>
nmap --script http-methods -p 80,443 <target-ip>
```


## Nmap Scan Types and Parameters

Commands     | Description                               
-------------|-------------------------------------------
Specify source port number | `--source-port PORT_NUM`
Append random data to reach the given length | `--data-length NUM`
Explain how Nmap made its conclusion | `--reason`
Verbose output | `-v`
Very verbose output | `-vv`
Debugging output | `-d`
More detailed debugging output | `-dd`
Save output in normal format | `-oN`
Save output in grepable format | `-oG`
Save output in XML format | `-oX`
Save output in normal, XML, and Grepable formats | `-oA`
TCP Null Scan | `-sN`
TCP FIN Scan | `-sF`
TCP Xmas Scan | `-sX`
TCP Maimon Scan | `-sM`
TCP ACK Scan | `-sA`
TCP Window Scan | `-sW`
Custom TCP Scan | `--scanflags URGACKPSHRSTSYNFIN`
Spoofed Source IP | `-S [IP]`
Spoofed MAC Address | `--spoof-mac [MAC]`
Decoy Scan | `-D [IP]`
Idle (Zombie) Scan | `-sI [IP]`
Fragment IP data into 8 bytes | `-f`
Fragment IP data into 16 bytes | `-ff`

## Nmap Scripting Engine

Script Category     | Description                               
-------------|-------------------------------------------
`auth`     | Authentication-related scripts.
`broadcast`     | Discover hosts by sending broadcast messages.
`brute`     | Perform brute-force password auditing against logins.
`default`     | Default scripts, same as -sC.
`discovery`     | Retrieve accessible information, such as database tables and DNS names.
`dos`     | Detect servers vulnerable to Denial of Service (DoS).
`exploit`     | Attempt to exploit various vulnerable services.
`external`     | Checks using a third-party service, such as Geoplugin and Virustotal.
`fuzzer`     | Launch fuzzing attacks.
`intrusive`     | Intrusive scripts such as brute-force attacks and exploitation.
`malware`     | Scans for backdoors.
`safe`     | Safe scripts that won't crash the target.
`version`     | Retrieve service versions.
`vuln`     | Checks for vulnerabilities or exploits vulnerable services.

## General Port Scanning Commands

Commands     | Description                               
-------------|-------------------------------------------
Basic Port Scan | `nmap -T4 [IP]`
General Port Scan | `nmap -p0-1000 -v -A -T4 [IP]`
OS Detection Scan | `nmap -A -sT -T4 [IP]`
Port and Service Scan | `nmap -p [x,y,z] -v -sT -sV -T4 [IP]`
Comprehensive CTF Scan | `nmap -p [x,y,z] -A -sS -sV -sC -Pn -vv [IP]`
Alternative Port Scanner (Rustscan) | `rustscan -a [IP] -p 21,80 -- -A -sC`
