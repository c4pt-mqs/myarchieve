---
title: curl
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Dosya indirmek:

```zsh
curl -O [URL]
```

Belirli bir porttan GET isteği yapmak:

```zsh
curl -s http://[DOMAIN-IP]:22/
```

Bir sitede belirli bir metni aramak:

```zsh
curl -s [URL] | grep [kelime]
```

HEAD seçeneğini kullanarak response başlıklarını yazdır:

```zsh
curl -I HEAD -i [URL]
```

Çerez (cookie) atamak:

```zsh
curl -v --cookie "USER_TOKEN=Yes" [URL]
```

Çerezleri elle ayarlayarak istek yapmak:

```zsh
curl -b "name1=value1; name2=value2" [URL]
```

POST metodu ile veri göndermek/çerez eklemek:

```zsh
curl -X POST [URL] -d "file=my_image.png"
```

```zsh
curl -X POST [URL] -b "file=../../../../etc/passwd" -o output.txt
```

Başlık (Header) bilgisi vererek istek göndermek:

```zsh
curl -d '{"id": 123456}' -H "Content-Type: application/json" [URL]
```

Butona tıklamak:

```html
<input type="hidden" name="button1" value="button1">
<button name="submit" value="submit">Button 1</button>
```

```zsh
curl -d "button1=button1&value=button1" [URL]
```

Kullanıcı adı ve şifre göndererek istek yapmak:

```zsh
curl -X POST -d "username=asdas123&password=123456&submit=submit" [URL]
```

Siteyi index olarak indirmek:

```zsh
curl [URL] -o output.html
```

```zsh
curl [URL] > output.html
```

URL'leri bir dosyadan indirmek:

```zsh
xargs -n 1 curl -O < URLs_list.txt
```

İndirmeyi devam ettirmek:

```zsh
curl –C - - O [indirilen_dosya_sunucu_adresi]
```

Yönlendirmeleri takip etmek:

```zsh
curl –L [URL]
```

Parametreler | Açıklaması                               
-------------|-------------------------------------------
`-#` | Will display a progress meter for you to know how much the download has progressed.(or use --silent flag for a silent crawl)
`-o` | Saves the file downloaded with the name given following the flag.
`-O` | Saves the file with the name it was saved on the server.
`-C -` | This flag can resume your broken download without specifying an offset.
`--limit-rate` | Limits the download/upload rate to somewhere near the specified range (Units in 100K,100M,100G)
`-u` | Provides user authentication (Format: -u user:password)
`-T` | Helps in uploading the file to some server(In our case php-reverse-shell)
`-x` | If you have to view the page through a PROXY. You can specify the proxy server with this flag. (-x proxy.server.com -u user:password(Authentication for proxy server))
`-I` | (Caps i) Queries the header and not the webpage.
`-A` | You can specify user agent to make request to the server
`-L` | Tells curl to follow redirects
`-b` | This flag allows you to specify cookies while making a curl request(Cookie should be in the format "NAME1=VALUE1;NAME2=VALUE2")
`-d` | This flag can be used to POST data to the server(generally used for posting form data).
`-X` | To specify the HTTP method on the URL. (GET,POST,TRACE,OPTIONS)
