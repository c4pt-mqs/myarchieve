---
title: Creating USB boot for Win10 on macOS
date: 2023-08-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Help
subcategory: Support
tags:
  - windows
  - macos
  - system
---

```zsh
diskutil list
diskutil unmountDisk /dev/diskX
sudo dd if=/path/to/windows10.iso of=/dev/rdiskX bs=1m
```