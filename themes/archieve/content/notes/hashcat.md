---
title: hashcat
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---


Hash kırmak:

```zsh
echo "[hash]" > hash

hashcat -m 1400 -a 0  [hash] [wordlist_path]
```

Kırılan şifreleri göstermek:

```zsh
hashcat -m 1400 -a 0 [hash] [wordlist_path] --show
```

Kural vermek:

```zsh
hashcat -m 1700 -a 0 [hash] [wordlist_path] \
    -r /opt/homebrew/Cellar/hashcat/6.2.6_1/share/doc/hashcat/rules/best64.rule \
    --force -d 1
```