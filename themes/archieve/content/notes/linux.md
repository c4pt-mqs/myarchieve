---
title: Linux
date: 2023-04-30T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
weight: 1
tags:
  - linux
  - commands
  - tools
  - bash
---


| Komutlar     | Açıklaması                                |
|--------------|-------------------------------------------|
| `ls`     | mevcut dizindeki dosya ve dizinleri listeler.|
| `cd`     | mevcut dizini değiştirir.|
| `pwd`     | mevcut çalışma dizinini yazdırır.|
| `mkdir`     | yeni bir dizin oluşturur.|
| `rmdir`     | boş bir dizini kaldırır.|
| `touch`     | yeni bir dosya oluşturur veya var olan bir dosyanın değiştirilme zamanını günceller.|
| `rm`     | bir dosyayı veya dizini kaldırır.|
| `mv`     | bir dosyayı veya dizini taşır veya yeniden adlandırır.|
| `cp`     | bir dosyayı veya dizini kopyalar.|
| `ln`     | dosyalar arasında bir bağlantı (sert bağlantı veya sembolik bağlantı) oluşturur.|
| `find`     | belirtilen kriterleri karşılayan dosyaları bir dizin hiyerarşisinde arar.|
| `grep`     | bir dosyada belirtilen bir deseni eşleştiren satırları arar.|
| `sed`     | bir dosya veya standart giriş üzerinde metin dönüşümleri yapabilen bir akış |düzenleyicisidir.
| `awk`     | yapılandırılmış veriler üzerinde çeşitli işlemler gerçekleştirmek için kullanılabilen bir |metin işleme dilidir.
| `tar`     | tar arşivleri oluşturmak ve işlemek için kullanılır.|
| `gzip`     | dosyaları gzip algoritması kullanarak sıkıştırmak için kullanılır.|
| `gunzip`     | gzip ile sıkıştırılmış dosyaları açmak için kullanılır.|
| `ssh-keygen`     | kimlik doğrulama için SSH anahtar çiftleri oluşturmak için kullanılır.|
| `scp`     | ağ üzerinden hostlar arasında dosya kopyalamak için kullanılır.|
| `chroot`     | bir işlem için kök dizini değiştirerek bir "hapis" ortamı oluşturur.|
| `systemctl`     | systemd sistem ve hizmet yöneticisini kontrol etmek için kullanılır.|
| `journalctl`     | systemd tarafından oluşturulan günlükleri görüntülemek ve yönetmek için kullanılır.|
| `uname`     | sistem hakkında bilgileri görüntüler, örneğin çekirdek sürümü ve donanım mimarisi.|
| `uptime`     | sistemin ne kadar süredir çalıştığını ve zaman içindeki ortalama sistem yükünü |görüntüler.
| `date`     | mevcut tarih ve saati görüntüler.|
| `cal`     | belirtilen ay veya yıl için bir takvim görüntüler.|
| `passwd`     | bir kullanıcının şifresini değiştirmek için kullanılır.|
| `su`     | başka bir kullanıcı hesabına geçmek için kullanılır.|
| `sudo`     | ayrıcalıklı izinlerle bir komutu çalıştırmak için kullanılır.|
| `useradd`     | yeni bir kullanıcı hesabı oluşturmak için kullanılır.|
| `userdel`     | bir kullanıcı hesabını silmek için kullanılır.|
| `groupadd`     | yeni bir grup oluşturmak için kullanılır.|
| `groupdel`     | bir grup silmek için kullanılır.|
| `mount`     | bir dosya sistemini bağlamak için kullanılır.|
| `umount`     | bir dosya sistemini bağlamadan çıkarmak için kullanılır.|
| `fsck`     | bir dosya sisteminin durumunu kontrol eder ve onarır.|
| `hdparm`     | sabit disk sürücüleri için parametreleri ayarlamak için kullanılır.|
| `lspci`     | sisteme bağlı PCI cihazlarını listeler.|
| `lsusb`     | sisteme bağlı USB cihazlarını listeler.|
| `free`     | sistemin bellek kullanımı hakkında bilgi görüntüler.|
| `ifconfig`     | ağ arayüzlerinin yapılandırmasını, IP adreslerini, ağ maskelerini ve MAC adreslerini |görüntüler.
| `ip addr`     | tüm ağ arayüzlerinin IP adreslerini görüntüler.|
| `ip route`     | yönlendirme tablosunu, varsayılan ağı ve her hedef ağ için tercih edilen arayüzü |görüntüler.
| `ping`     | yerel bilgisayar ile hedef anahtar arasındaki bağlantıyı test etmek için belirtilen ağ |adresine veya anahtar adına ICMP yankı istekleri gönderir.
| `traceroute`     | yerel bilgisayardan belirtilen ağ adresine veya anahtar adına ICMP yankı istekleri |göndererek bir IP paketinin yolunu izler.
| `netstat`     | etkin ağ bağlantılarını, dinleme portlarını ve ilgili ağ istatistiklerini görüntüler.|
| `ss`     | netstat gibi çalışır, ancak her bağlantının işlem kimliği (PID) ve her bağlantının durumu |gibi daha ayrıntılı bilgileri görüntüler.
| `nmap`     | bir ağı tarar ve ağdaki anahtarların IP adreslerini, ana bilgisayar adlarını ve açık |portlarını bulur.
| `tcpdump`     | belirtilen ağ arayüzünde ağ trafiğini yakalar ve görüntüler.|
| `iptables`     | Linux güvenlik duvarını yapılandırmak için kullanılır, gelen ve giden trafik için |kurallar içerir.
| `route`     | yerel yönlendirme tablosunu görüntüler veya değiştirir.|
| `arp`     | ARP (Adres Çözünürlük Protokolü) önbelleğini görüntüler, IP adreslerini MAC adreslerine |eşler.
| `dig`     | belirtilen alan adı veya IP adresi için DNS sorguları yapar.|
| `nslookup`     | dig gibi çalışır, ancak DNS sorguları yapmak için daha eski bir araçtır.|
| `hostname`     | yerel bilgisayarın ana bilgisayar adını görüntüler.|
| `whois`     | bir alan adı veya IP adresi hakkında bilgi almak için WHOIS veritabanına sorgu yapar.|
| `ssh`     | Güvenli Kabuk (SSH) protokolünü kullanarak uzak bir bilgisayara bağlanmak için kullanılır.|
| `scp`     | SSH protokolünü kullanarak yerel bilgisayar ile uzak bir bilgisayar arasında dosya |kopyalamak için kullanılır.
| `rsync`     | ağ üzerinden yerel bilgisayar ile uzak bir bilgisayar arasında dosyaları senkronize |etmek için kullanılır.
| `curl`     | HTTP, HTTPS, FTP ve SMTP dahil olmak üzere çeşitli protokoller kullanarak sunucuyla veri |aktarımı yapmak için kullanılır.
| `wget`     | HTTP, HTTPS ve FTP dahil olmak üzere çeşitli protokoller kullanarak internetten dosya |indirmek için kullanılır.
| `who`     | şu anda oturum açmış olan kullanıcılar hakkında bilgi görüntüler.|
| `w`     | oturum açmış olan kullanıcılar hakkında bilgi görüntüler, çalıştırdıkları işlemleri de |içerir.
| `ps`     | şu anda çalışan işlemler hakkında bilgi görüntüler.|
| `top`     | şu anda çalışan işlemler hakkında gerçek zamanlı bilgi görüntüler, CPU ve bellek |kullanımını içerir.
| `kill`     | bir işlemi sonlandırmak için bir sinyal gönderir.|
| `pkill`     | kill ile benzerdir, ancak belirtilen desene uyan bir veya daha fazla işleme bir sinyal |gönderir.
| `chmod`     | bir dosya veya dizinin izinlerini değiştirir.|
| `chown`     | bir dosya veya dizinin sahibini ve grubunu değiştirir.|
| `df`     | her bağlı dosya sisteminin disk kullanımı ve kullanılabilir alan hakkında bilgi görüntüler.|
| `du`     | bir dosya veya dizinin disk kullanımını görüntüler.|
| `pbcopy`     | bir dosyanın içeriğini panoya kopyalar.|
| `scutil --dns`     | DNS yapılandırmasını gösterir.|
| `scutil --nwi`     | ağ bilgilerini gösterir.|
| `set`     | kabuğa ait değişkenlerin adlarını ve değerlerini döndürür.|
| `printenv`     | sistemdeki tüm çevresel değişkenleri görüntüler.|
| `env`     | sistemdeki tüm "exported" (ihracat edilen) değişkenleri gösterir, yani değiştirilmiş |değişkenleri içerir.
| `apropos`     | başlıkta belirtilen anahtar kelimeleri içeren el kitabı bölümlerini gösterir.|
| `whatis`     | herhangi bir komut satırı hakkında kısa bir açıklama almanızı sağlar.|


