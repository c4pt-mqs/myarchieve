---
title: rustscan
date: 2022-11-12T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Commands     | Description                               
-------------|-------------------------------------------
Scanning a single IP address with default ports | `rustscan -a [IP]`
Scanning multiple IP addresses | `rustscan -a 127.0.0.1,0.0.0.0`
Scanning a host (domain) using DNS resolution | `rustscan -a www.google.com`
Scanning a range of IP addresses using CIDR notation | `rustscan -a 192.168.0.0/30`
Scanning multiple targets listed in a hosts file (hosts.txt) | `rustscan -a 'hosts.txt'`
Scanning a specific port on an IP address | `rustscan -a 127.0.0.1 -p 5353`
Scanning multiple specified ports on an IP address | `rustscan -a 127.0.0.1 -p 53,80,121,65535`
Scanning a range of ports on an IP address | `rustscan -a 127.0.0.1 --range 1-1000`
Combining RustScan with custom Nmap arguments | `rustscan -a 127.0.0.1 -- -A -sC`<br>`nmap -Pn -vvv -p [PORTS] -A -sC 127.0.0.1`
Scanning with a random port order in a specified range | `rustscan -a 127.0.0.1 --range 1-1000 --scan-order "Random"`
