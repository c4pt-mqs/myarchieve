---
title: uniq
date: 2022-12-13T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

uniq command, filters the output (from either a file or stdin) to remove any duplicates.

**Important Flags for uniq:**

Flags                | Description                               
---------------------|-------------------------------------------
`-c` | To count the occurrences of every line in file or stdin
`-d` | Will only print the lines that are repeated, not the one which are unique
`-u` | Will only print lines that are already uniq
`-i` | Ignores case(Default is case-sensitive)
