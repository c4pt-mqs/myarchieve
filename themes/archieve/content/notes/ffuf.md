---
title: ffuf
date: 2022-11-10T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

**Not:** `[URL]` = `[URL]`

Genel Kullanım:

```zsh
ffuf -u [URL] -w [wordlist_path]
```

Renklendirme ile çıktıyı görüntüle (default: false):

```zsh
ffuf -u [URL] -c -w [wordlist_path]
```

Özel anahtar kullanımı:

```zsh
ffuf -u [URL]/FUZZ -w [wordlist_path]:FUZZ
```

Uzantıları ara:

```zsh
ffuf -u [URL]/FUZZ -w [wordlist_path]
```

Uzantıları belirt ve FUZZ anahtarını genişlet:

```zsh
ffuf -u [URL]/FUZZ -w [wordlist_path] -e .php,.txt
```

Belirli bir HTTP durum kodunu filtrele | 403'leri gizle:

```zsh
ffuf -u [URL]/FUZZ -w [wordlist_path] -fc 403
```

Belirli bir HTTP durum kodunu eşle | 200'leri göster:

```zsh
ffuf -u [URL]/FUZZ -w [wordlist_path] -mc 200
```

Belirli bir HTTP yanıt boyutunu filtrele:

```zsh
ffuf -u [URL]/FUZZ -w [wordlist_path] -fs 0
```

Nokta ile başlayan tüm dosyaları eşleştirme:

```zsh
ffuf -u [URL]/FUZZ -w [wordlist_path] -fr '/\..*'
```

0 ile 255 arasındaki tüm sayıları içeren bir liste oluşturur ve bu sayıları FUZZ et:

```zsh
ruby -e '(0..255).each{|i| puts i}' | ffuf -u '[URL]?id=FUZZ' -c -w - -fw 33
```

```zsh
for i in {0..255}; do echo $i; done | ffuf -u '[URL]?id=FUZZ' -c -w - -fw 33
```

```zsh
seq 0 255 | ffuf -u '[URL]?id=FUZZ' -c -w - -fw 33
```

Kullanıcı adı ve şifre doğrulama:

```zsh
ffuf -w [wordlist_path] -X POST -d "username=FUZZ&password=x" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -u [URL]/signup -mr "username already exists"
```

Wordlist tabanlı brute-force saldırıları:

```zsh
ffuf -u [URL] -c -w [wordlist_path] -X POST \
    -d 'uname=[username]&passwd=FUZZ&submit=Submit' \
    -H 'Content-Type: application/x-www-form-urlencoded'
```

Basit subdomain FUZZing:

```zsh
ffuf -u FUZZ.[domain] -c -w [wordlist_path]
```

Gelişmiş subdomain FUZZing:

```zsh
ffuf -u [domain] -c -w [wordlist_path] -H 'Host: FUZZ.mydomain.com' -fs 0
```

Tüm ffuf trafiğini bir web proxy üzerinden yönlendirme (HTTP veya SOCKS5):

```zsh
ffuf -u [URL] -c -w [wordlist_path] -x 127.0.0.1:8080
```

Sadece eşleşenleri proxy'e yönlendirme:

```zsh
ffuf -u [URL] -c -w [wordlist_path] -replay-proxy 127.0.0.1:8080
```

Wordlist'teki yorumları yoksay:

```zsh
ffuf -u [URL]/FUZZ -c -w [wordlist_path] -ic -fs 0
```

Eşleştirme seçenekleri:

Parametreler | Açıklaması                               
-------------|-------------------------------------------
`-mc` | Match HTTP status codes (default: 200,204,301,302,307,401,403,405)
`-ml` | Match amount of lines in response
`-mr` | Match with regex
`-ms` | Match HTTP response size
`-mw` | Match amount of words in response

Filtreleme seçenekleri:

Parametreler | Açıklaması                               
-------------|-------------------------------------------
`-fc` | Filter HTTP status codes from response.
`-fl` | Filter by amount of lines in response.
`-fr` | Filter with regex
`-fs` | Filter HTTP response size.
`-fw` | Filter by amount of words in response.
