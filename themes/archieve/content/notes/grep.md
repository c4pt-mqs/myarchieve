---
title: grep
date: 2022-10-23T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Flags        | Description                               
-------------|-------------------------------------------
`-R` | Does a recursive grep search for the files inside the folders(if found in the specified path for pattern search; else grep won't traverse diretory for searching the pattern you specify)
`-h` | If you're grepping recursively in a directory, this flag disables the prefixing of filenames in the results.
`-c` | This flag won't list you the pattern only list an integer value, that how many times the pattern was found in the file/folder.
`-i` | I prefer to use this flag most of the time, this is what specifies grep to search for the PATTERN while IGNORING the case 
`-l ` | will only list the filename instead of pattern found in it.
`-n` | It will list the lines with their line number in the file containing the pattern.
`-v` | This flag prints all the lines that are NOT containing the pattern
`-E` | This flag we already read above... will consider the PATTERN as a regular expression to find the matching strings. 
`-e` | The official documentation says, it can be used to specify multiple patterns and if any string matches with the pattern(s) it will list it.
