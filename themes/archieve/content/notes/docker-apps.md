---
title: Docker Apps
date: 2023-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - docker
  - security
  - penetration-testing
---

## WpScan

```zsh
docker run -it --rm wpscanteam/wpscan --url [URL] --enumerate u
wpscan --url [URL] -U c0ldd -P [wordlist] -t 20
wpscan –-url [URL] –-passwords [wordlist] –-usernames cmnatic
```

## hakrawler

```zsh
docker run --rm -i hakluke/hakrawler --help
echo [URL] | docker run --rm -i hakluke/hakrawler -subs
```


