---
title: OpenDns
date: 2022-09-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Help
subcategory: Support
tags:
  - dns
  - networking
  - configuration
  - internet
  - security
---

### Mac OS

1. Go to System Preferences.
2. Click on Network.
3. Select the first connection in your list and click Advanced.
4. Select the DNS tab and add 208.67.222.222 and 208.67.220.220 to the list of DNS servers.
5. Click OK

### Windows 10

1. Open the Control Panel.
2. Click Network and Internet.
3. Click Network and Sharing Center.
4. On the left pane, click Change adapter settings.
5. Right-click the network interface connected to the internet, then click Properties.
6. Choose Internet Protocol Version 4 (TCP/IPv4).
7. Click Properties.
8. Click Use the following DNS server addresses.
9. Click Advanced.
10. Enter 208.67.222.222 and 208.67.220.220 in the DNS server fields.
11. Click OK, then click Close.
