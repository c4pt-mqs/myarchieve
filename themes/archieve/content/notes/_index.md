---
title: "Notes"
description: "Collection of all notes and documentation"
type: page
layout: notes
---

Welcome to the notes section. Here you'll find various documentation, guides, and personal notes organized by category.