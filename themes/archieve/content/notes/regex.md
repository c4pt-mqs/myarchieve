---
title: Regex
date: 2022-12-18T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
weight: 3
tags:
  - regex
  - programming
  - search
---

## Meta Karakterler

Meta karakter     | Açıklaması                               
-------------|-------------------------------------------
`.` | Satır başlangıcı hariç herhangi bir karakterle eşleşir.
`[ ]` | Köşeli parantezler arasında bulunan herhangi bir karakterle eşleşir.
`[^ ]` | Köşeli parantez içerisinde yer alan `^` işaretinden sonra girilen karakterler haricindeki karakterlerle eşleşir.
`*` | Kendisinden önce yazılan karakterin sıfır veya daha fazla tekrarı ile eşleşir.
`+` | Kendisinden önce yazılan karakterin bir veya daha fazla olan tekrarı ile eşleşir.
`?` | Kendisinden önce yazılan karakterin varlık durumunu opsiyonel kılar.
`{n,m}` | Kendisinden önce yazılan karakterin en az n en fazla m değeri kadar olmasını ifade eder.
`(xyz)` | Verilen sırayla xyz karakterleriyle eşleşir.
`|` | Karakterden önce veya sonra verilen ifadelerin herhangi biriyle eşleşebilir. İfadeye "ya da" anlamı katar.
`\` | `[ ] ( ) { } . * + ? ^ $ \ |` özel karakterin aranmasını sağlar.
`^` | Girilen verinin başlangıcını ifade eder. (haricinde her şey)
`$` | Girilen verinin sonunu ifade eder.

## Karakter Sınıfları

Meta karakter     | Açıklaması                               
-------------|-------------------------------------------
`\w` | Alfanumerik karakterlerle eşleşir: `[a-zA-Z0-9_]`
`\W` | Alfanumerik olmayan karakterlerle eşleşir: `[^\w]`
`\d` | Rakamlarla eşleşir: `[0-9]`
`\D` | Rakam olmayan karakterlerle eşleşir: `[^\d]`
`\s` | Boşluk karakteri ile eşleşir: `[\t\n\f\r\p{Z}]`
`\S` | Boşluk karakteri olmayan karakterlerle eşleşir: `[^\s]`

## Gruplama ve Özel İfadeler

Meta karakter     | Açıklaması                               
-------------|-------------------------------------------
`?=` | Verdiğimiz ifade sonrası arar ve eşleşme varsa sonuç döndürür.
`?!` | Verdiğimiz ifade sonrası arar ve eşleşme yoksa sonuç döndürür.
`?<=` | Verdiğimiz ifade öncesini arar ve eşleşme varsa sonuç döndürür.
`?<-!-` | Verdiğimiz ifade öncesini arar ve eşleşme yoksa sonuç döndürür.

## Diğer Meta Karakterler

Meta karakter     | Açıklaması                               
-------------|-------------------------------------------
`i` | Eşleştirmeleri küçük/büyük harfe karşı duyarsız yapar.
`g` | Genel Arama: Girilen harf öbeği boyunca bir desen arar.
`m` | Çok satırlı: Sabitleyici meta karakteri her satırda çalışır.

## Ek Notlar

- `()` içinde kullanılan `|` operatörü, birden fazla alternatif ifadeyi gruplamak için kullanılabilir.
- `{n}` ifadesi, kendisinden önceki karakterin tam olarak n kez tekrar etmesini sağlar.
- `{n,}` ifadesi, kendisinden önceki karakterin n veya daha fazla kez tekrar etmesini sağlar.
- `{,m}` ifadesi, kendisinden önceki karakterin en fazla m kez tekrar etmesini sağlar.
- `{n,m}` ifadesi, kendisinden önceki karakterin en az n en fazla m kez tekrar etmesini sağlar.
- `\b` kelimenin başlangıcını veya sonunu belirtir.

Bu notlar daha geniş bir regex kullanımına yardımcı olacaktır. İhtiyaçlarınıza göre bunları özelleştirebilir ve yeni notlar ekleyebilirsiniz.

{{< figure src="/img/notes/cyber-security/commands/regex-1.png" alt="regex commands" loading="lazy" width="70%" height="auto" >}}

{{< figure src="/img/notes/cyber-security/commands/regex-2.png" alt="regex commands" loading="lazy" width="70%" height="auto" >}}


**Kaynaklar:**

- https://regexlearn.com/cheatsheet