---
title: Images
date: 2023-02-05T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: OSINT
tags:
  - osint
  - search
  - reconnaissance
---

**Resimler de OSINT yaparken kendinize sormanız gereken bazı sorular:**

1. Resimde, cadde adı veya vitrin işaretleri gibi konumu ortaya çıkaran bariz veriler var mı?
2. Resimde bir ülkeyi, kıtayı veya bölgeyi, yolun hangi tarafını kullandıklarını, dil veya mimari özelliklerini ortaya çıkarabilecek bilgiler var mı?
3. Yol işareti stillerini, doğayı ve çevresel özellikleri, popüler motorlu araç markalarını veya araç türlerini tanıyor musunuz?
4. Herhangi bir görünür altyapının kalitesi nasıldır? Yol asfalt mı yoksa çakıllı mı?
5. Görüntünün coğrafi konumunu belirlemenize yardımcı olabilecek benzersiz simge, yapılar, binalar, köprüler, heykeller veya dağlar görüyor musunuz?

**Finding Images by geo-location:**

- https://bearhunt38.medium.com/geo-locating-photos-with-osm-overpass-api-10ed35b95f11


**Find Badges, Logos, Symbols, Fonts:**

- https://allbadges.net/en
- https://logodix.com/
- https://www.whatfontis.com/
- https://www.myfonts.com/pages/whatthefont
- https://www.adl.org/resources/hate-symbols/search

**Image Reverse:**

- https://www.bing.com/images/trending?form=Z9LH
- https://yandex.com/images/
- https://images.google.com/
- https://image.baidu.com/
- https://lens.google/
- https://m.baidu.com/
- https://www.tineye.com
- https://pimeyes.com/en
- https://tineye.com/
- https://www.osintcombine.com/reverse-image-analyzer

**Image Analyze:**

- https://www.aperisolve.com/
- https://fotoforensics.com/
- https://www.faceplusplus.com/face-comparing/
- https://facecheck.id/
- https://github.com/GuidoBartoli/sherloq

**Image Edit:**

- https://cleanup.pictures/
- https://www.gimp.org/
- https://inkscape.org/
- https://www.myheritage.com.tr/photo-enhancer
- https://www.photopea.com/

**Hints:**

*Reading Text in Image:*
Flip Horizantly >> Contrast Up >> Brightness Down

**Recover Images:**

https://github.com/beurtschipper/Depix

