---
title: Ekrana Görüntü Gelmiyor Sorunu
date: 2021-12-16T09:00:00Z
draft: false
type: note
author: Kaptan
category: Help
subcategory: Support
tags:
  - donanım
  - teknik
  - sorun-giderme
---

1. **Tüm Kabloların Bağlantısının Doğru Bir Şekilde Takılıp-Takılmadığını Kontrol Edin**

2. **Ekran Kartının Doğru Yerleştirildiğinden Emin Olun**

3. **Monitörün Güç Düğmesinin Açık Olduğunu Kontrol Edin**

4. **Monitörü Anakartta Bulunan Görüntü Girişine Bağlayın (Genelde VGA girişi)**

5. **RAM'lerin Uç Kısımlarını Silgi İle Hafifçe Sil ve Yerlerini Değiştirin**

6. **Kullanığınız Kabloları Farklı Bir Kablo İle Değiştirmeyi Deneyin**

7. **Başka Bir Monitör veya TV'ye Bağlayın**

Eğer yukarıdaki adımlar hiçbir sonuç vermezse anakart veya ekran kartında bir arıza olabilir. Bu durumda, cihazın teknik bir servise gönderin. 
