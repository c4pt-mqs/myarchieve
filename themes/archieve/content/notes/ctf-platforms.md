---
title: CTF Platforms
date: 2022-12-11T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: CTFs
tags:
  - ctf
  - security
  - tutorial
---

I have compiled a list of Red Team/Blue Team CTF Platforms to test your skills on. I have to say that they're not in order but if you wanna start with any of them then I would Highly Recommend OverTheWire followed by THM and then HTB.

- [Attack-Defense](https://attackdefense.com/)
- [steike's CTFs](https://alf.nu/alert1)
- [CTF Komodo](https://ctf.komodosec.com/)
- [CryptoHack](https://cryptohack.org/)
- [CMD Challenge](https://cmdchallenge.com/)
- [Explotation Education](https://exploit.education/)
- [Google CTF](https://capturetheflag.withgoogle.com)
- [HackTheBox](https://www.hackthebox.com)
- [Hackthis](https://www.hackthis.co.uk/)
- [Hacksplaining](https://www.hacksplaining.com/exercises)
- [Hacker101](https://ctf.hacker101.com/)
- [HackerSec](https://hackersec.com/ctf/)
- [Hacking-Lab](https://hacking-lab.com/)
- [HSTRIKE](https://hstrike.com/)
- [ImmersiveLabs](https://immersivelabs.com/)
- [NewbieContest](https://www.newbiecontest.org)
- [OverTheWire](http://overthewire.org/)
- [Practical Pentest Labs](https://practicalpentestlabs.com)
- [Pentestlab](https://pentesterlab.com/)
- [Hackaflag BR](https://hackaflag.com.br/)
- [Penetration Testing Practice Lab](http://www.amanhardikar.com/mindmaps/Practice.html)
- [PentestIT LAB](https://lab.pentestit.ru/)
- [PicoCTF](https://picoctf.com/)
- [PWNABLE](https://pwnable.kr/play.php)
- [Root-Me](https://www.root-me.org/)
- [Root in Jail](http://rootinjail.com/)
- [SANS Challenger](https://www.holidayhackchallenge.com)
- [SmashTheStack](http://smashthestack.org/wargames.html)
- [The Cryptopals](https://cryptopals.com/)
- [TryHackMe](https://tryhackme.com/)
- [Vulnhub](https://www.vulnhub.com/)
- [W3Challs](https://w3challs.com/)
- [WeChall](http://www.wechall.net/)
- [Zenk-Security](https://www.zenk-security.com/)
- [Cyber Defenders](https://cyberdefenders.org/)
- [LetsDefend](https://letsdefend.io/)
- [Vulnmachines](https://vulnmachines.com)
- [Menzil Kuvveti](https://www.rangeforce.com)
- [Ctftime](https://ctftime.org)
- [Pwn koleji](https://dojo.pwn.college)
- [Attack-Defense](https://attackdefense.com/)
- [Alert to win](https://alf.nu/alert1)
- [CTF Komodo Security](https://ctf.komodosec.com/)
- [CMD Challenge](https://cmdchallenge.com/)
- [Hacker Security](https://lnkd.in/ex7R-C-e)
- [Defend the Web](https://defendtheweb.net/)
- [PwnLab](https://ctf.pwnlab.me/)
