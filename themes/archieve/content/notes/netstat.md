---
title: netstat
date: 2022-10-11T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Flags                | Description                               
---------------------|-------------------------------------------
`-i` | Shows interface statistics.
`-a` | Display all sockets
`-at` or `-au` | can also be used to list TCP or UDP protocols respectively.
`-n` | Do not resolve names
`-o` | Display timers
`-l` | listening ports
`-tp` | list connections with the service name and PID information. 
`-s` | list network usage statistics by protocol

**Resources:**

- https://www.rekha.com/netstat-cheat-sheet-for-newbies.html