---
title: Project Ideas
date: 2022-12-03T09:00:00Z
draft: false
type: note
author: Kaptan
category: Programming
subcategory: Projects
tags:
  - programming
  - development
  - ideas
  - learning
---

## Simple Ideas

1. Python ile Bir Note Programı Yaz
2. Harf tahmin etme oyunu
3. Beni ne kadar tanıyor programı
4. Chat-Bot (Karşılıklı server ile ya da discord)
5. **Discord da oto-mesaj '/bump' gibi**
6. Discord Bottan kişisel veya genel sorular istenilecek ve bot otomatik olarak random soru vericek En/Tr
7. Web sitelerinin varlığını kontrol eden uygulama
8. **Foto tanımlayıcı ve yandex browser gibi eşleştirici bir program**
9. **Cambly için otomatik siteye girip kayıt olan program**
10. Alarm kurma uygulaması
11. Password generator
12. Her türlü hash/kriptoloji şifresi Encode/Decode algoritması
13. **Yazı fontu değiştirip, worde kaydetme**
14. Hava durumu programı (api)
15. Discord bot / NewWeb - kapsamlı bot (api, ide)
16. Space Invaders Game Python Project
17. **Kısayol tuşuna basınca şu eylemi yap komutu**
18. Bir çok uygulamayı docker da birleştir
19. Otomatik Oto-İzin Silici
20. Herhangi bir numaranın olup olmadığını sorgulamak
21. **Single/Multiple images downloader**
22. Sed benzeri program.txt dosyalarını önce listeye sonra diğer text'e ekleme yapabilmek
23. Fake mail ile cambly'e giren site
24. **Instagram/Facebook/twitter video resim indirme**
25. **Tüm hesapların şifresini otomatik olarak değiştiren program**
26. Önce random şifre oluşturucu yaz
27. İstediğimiz portu yazdiğimizda bize nerede çalıştığını gösteren program - tüm portları listeler özelliği
28. **Pdf, word, png, jpg converter programı**
29. **Sürekli olarak içerde bulunan web sitelerini bulan crawler/link extracter**
30. **CLI ve GUI da çalışan youtube video downloader**
31. **Bir Resmi Google ya da Yandex Görsellerde tarıyacak ve içindeki yazıyı bir metin dosyasına kopyalayacak.**

## Detailed Ideas

1. Sürekli olarak içerde bulunan web sitelerini bulan crawler/link extracter
2. Google Maps'te her hangi bir ülke veya şehir için fotoğraflar bölümünü en alta kadar inicek ve o fotoğrafları kopyalayacak/indirecek.
3. Bir dünya haritası oluştur ve her bir ülkeyi kendi alanlarına böl. Tıklanan her bir ülke için alt satıra o ülke ile ilgili coğrafi ve genel bilgiler ver. Bunu 10 ülke için yap ve istenildiğinde ülke sayısı arttırılabilsin ve o ülkeler ile ilgili daha fazla bilgi girilebilsin.
4. Bir web sitesinde tüm ülkelerin bulunduğu bir API sistemi geliştirilecek. O ülkeler ile ilgili genel ve detaylı bilgiler şeklinde iki bölüm oluşturulacak. Hem coğrafya hem de osint bilgi/becerilerini geliştirmek isteyenler için güzel fırsatlar sunucak.
5. Bir RoadMaper oluşturulacak ve bu içerik olarak alanını belirlemek ve kaynaklarıa ulaşmak isteyenler için hızlı bir yol haritası bulma metodu geliştiricek.
6. Bir Blog oluşturulacak ve blog hem mobile hem de desktop için güzel bir şekilde çalışacak. Açılır kapanır hamburger menu ile birlikte işlevsel hâle gelicek.
7. İnternettte kişi bazlı muazzam bir OSINT otomasyon tool'u yazmak
8. ChatGPT'ye bağlı otomatize edilmiş bir güvenlik tool'u nasıl olur? Yapay zeka ile entegreli olacak diğerlerinden farklı ne yapabiliriz?
9. Honeypot sistemi; siber saldırya uğrayan firmaların sistemlerine honeypot kurup, saldırganın tekrar saldırmasına karşı yakalama şansı arttırılabilir:
- Bug Bounty platformu (private)
- Exploit Service
- Honeypot
- Yapay zeka destekli pentest uygulaması
10. Phising mailleri tespit eden bir yazılım
11. Ekran görüntüsü alarak anlık olarak çeviri yapan program
12. Kullanıcı bir kısayol tuşu belirler ve tuşa bastığında fare bir seç imlecine dönüşür. Seçilen bölge bir ekran görüntüsü olarak ânlık alınır ve google çeviri gibi bir api sistemi ile istenilen dile çevrilir. 
13. Futuristik bir görüntüye sahip bir roadmap şeması oluştur.
14. C2 Adversary Simulation Project: Terminal Tabanlı bir FUD C2 Framework Sistemi

## Other Ideas

1. **Todo List API:**

Create a RESTful API that allows users to create, read, update, and delete tasks from a todo list. You can use MongoDB as the database to store the tasks.

2. **Weather App API:**

Create a RESTful API that allows users to get the current weather for a specific location. You can use a weather API like OpenWeatherMap to retrieve the weather data.

3. **Image Upload API:**

Create a RESTful API that allows users to upload images to your server. You can use the multer middleware to handle file uploads.

4. **Authentication API:**

Create a RESTful API that allows users to register, login, and logout. You can use JSON Web Tokens (JWT) to handle authentication.

5. **Movie Database API:**

Create a RESTful API that allows users to search for movies and get movie details. You can use a movie API like OMDB to retrieve movie data. You can also store movie data in a local database to reduce API calls.


### Ideas for Resources

- https://www.upgrad.com/blog/python-projects-ideas-topics-beginners