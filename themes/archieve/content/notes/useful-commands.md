---
title: Useful Commands
date: 2023-04-13T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - linux
  - commands
  - system
  - tools
---

**Çalışan servisleri görüntüle:**

```zsh
sudo lsof -i -n -P | grep TCP
```

**Dosyada kaç satır olduğunu göster:**

```zsh
wc -l [filename]
```

**Text dosyasını tersten yazdır:**

```zsh
tac test.txt > reversed.txt
```

**Homebrew ile macOS'taki yazılım paketlerini güncelle, hataları tespit et, gereksiz dosyaları temizle ve kullanılmayan paketleri kaldır:**

```zsh
brew update && brew upgrade && brew doctor && brew cleanup && brew autoremove
```

**Komut durduruluncaya kadar Mac'in uyku moduna geçmesini engele:**

```zsh
caffeinate -u -t [seconds]
```

**Bir dizinin içindeki tüm dosyaların sonuna ".txt" eklemek**

```zsh
find [/dir] -type f -exec mv {} {}.txt \;
```

**Bir dizinin içindeki tüm dosyaların uzantısını ".txt" ile değiştirmek**

```zsh
find [/dir] -type f -exec bash -c 'mv "$1" "${1%.*}.txt"' _ {} \;
```

**Bir dizinin içindeki tüm dosyalardan ".txt" uzantısını silmek**

```zsh
find [/dir] -type f -name "*.txt" -exec bash -c 'mv "$1" "${1%.txt}"' _ {} \;
```
