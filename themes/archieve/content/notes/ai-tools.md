---
title: AI Tools
date: 2023-08-26T09:00:00Z
draft: false
type: note
author: Kaptan
category: Links
subcategory: Shared Lists
tags:
  - artificial-intelligence
  - automation
  - technology
---

1. **Ideas**

- ChatGPT
- Perplexity
- HuggingChat
- Bing Chat
- Claude

2. **Website**

- 10Web
- Framer
- Durable
- Dora
- Unicorn Platform

3. **Writing**

- Rytr
- Jasper
- Writesonic
- Copymate
- Longshot

4. **Chatbot**

- Chatbase
- SiteGPT
- ChatSonic
- CustomGPT
- Chatsimple

5. **Automation**

- Make
- Zapier ​​
- Bardeen
- Levity ​​
- Xembly

6. **UI/UX**

- Uizard
- Galileo AI
- UiMagic
- InstantAI
- Photoshop

7. **Image**

- Midjourney
- Bing create
- Stable Diffusion
- Get IMG
- Stock IMG

8. **Video**

- HeyGen
- InVideo
- Fliki
- Synthesia
- Runway

9. **Design**

- Flair AI
- Booth AI
- Canva
- Autodraw
- Clipdrop

10. **Music**

- Boomy
- Amper
- Jukedeck
- Melodrive
- Brain FM

11. **Marketing**

- AdCreative
- Simplified
- Pencil
- AdCopy
- Ai-Ads

12. **Twitter**

- Typefully
- TweetHunter
- Tribescaler
- Tweetlify
- Postwise


**Other Tools:**

- https://hotpot.ai/art-generator/
- https://cleanvoice.ai/
- https://copymonkey.ai/
- https://www.descript.com/
- https://www.copy.ai/
- https://podcastle.ai/
- https://www.ocoya.com/
