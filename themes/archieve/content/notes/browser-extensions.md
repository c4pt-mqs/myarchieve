---
title: Browser Extensions
date: 2023-07-25T09:00:00Z
draft: false
type: note
author: Kaptan
category: Links
subcategory: Useful Links
tags:
  - browser
  - privacy
---

**Bitwarden**
- https://bitwarden.com/download/

**ClearURLs**
- https://github.com/ClearURLs/Addon

**Context Search**
- https://addons.mozilla.org/en-US/firefox/addon/contextual-search/

**Cookie-AutoDelete**
- https://github.com/Cookie-AutoDelete/Cookie-AutoDelete

**Cookie Quick Manager**
- https://github.com/ysard/cookie-quick-manager

**Get cookies.txt LOCALLY**
- https://github.com/kairi003/Get-cookies.txt-LOCALLY

**FoxyTab**
- https://addons.mozilla.org/tr/firefox/addon/foxytab/

**Tree Style Tab**
- https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/

**Dark Reader**
- https://github.com/darkreader/darkreader

**enhanced-h264ify**
- https://github.com/alextrv/enhanced-h264ify

**Enhancer for YouTube**
- https://www.mrfdev.com/enhancer-for-youtube

**Multi-Account Containers**
- https://github.com/mozilla/multi-account-containers

**GitHub gloc**
- https://github.com/kas-elvirov/gloc

**Joplin Web Clipper**
- https://github.com/laurent22/joplin/blob/dev/readme/clipper.md

**OneNote Web Clipper**
- https://www.onenote.com/clipper

**Unhook: Remove YouTube Recommended Videos Comments**
- https://unhook.app/

**Session Sync**
- https://github.com/ReDEnergy/SessionSync

**Temporary Containers**
- https://github.com/stoically/temporary-containers

**uBlock Origin**
- https://github.com/gorhill/uBlock

**UserAgent-Switcher**
- https://github.com/ray-lothian/UserAgent-Switcher

**Video Download Helper**
- https://github.com/DoctorLai/VideoDownloadHelper

**HTTPS by Default in Your Browser**
- https://www.eff.org/https-everywhere/set-https-default-your-browser

**Privacy Badger**
- https://github.com/EFForg/privacybadger
