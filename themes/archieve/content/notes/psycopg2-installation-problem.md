---
title: psycopg2 Installation Problem
date: 2023-11-24T09:00:00Z
draft: false
type: note
author: Kaptan
category: Help
subcategory: Solutions
tags:
  - python
  - troubleshooting
  - installation
---

**Error:**

```zsh
$ pip3 install psycopg2

ld: warning: search path '/usr/local/opt/openssl/lib' not found
ld: library 'ssl' not found
clang: error: linker command failed with exit code 1 (use -v to see invocation)
error: command '/usr/bin/clang' failed with exit code 1
[end of output]

note: This error originates from a subprocess, and is likely not a problem with pip.
ERROR: Failed building wheel for psycopg2
Failed to build psycopg2
ERROR: Could not build wheels for psycopg2, which is required to install pyproject.toml-based projects
```

**Solution:**

```zsh
# Unix
export CFLAGS="-I$(brew --prefix openssl)/include"
export LDFLAGS="-L$(brew --prefix openssl)/lib"

# Ubuntu/Debian
export CFLAGS="-I/usr/include/openssl"
export LDFLAGS="-L/usr/lib/openssl"

# Red Hat/Fedora
export CFLAGS="-I/usr/include/openssl"
export LDFLAGS="-L/usr/lib64/openssl"
```