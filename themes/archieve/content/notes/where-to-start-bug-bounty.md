---
title: Where to Start
date: 2023-03-03T09:00:00Z
draft: false
type: note
author: Kaptan
category: Bug Bounty
subcategory: Resources
tags:
  - bug-bounty
  - security
  - hacking
  - resources
---

- [Guide Getting Started In Bug Bounty Hunting](https://whoami.securitybreached.org/2019/06/03/guide-getting-started-in-bug-bounty-hunting/)
- https://bugbountyforum.com/resources/
- https://application.security/free/owasp-top-10
- [Best Tips To Avoid Failures in Bug Bounty](https://www.youtube.com/watch?v=R94NfBeLeiI)
- https://shubs.io/so-you-want-to-get-into-bug-bounties/
- [How to Stop Learning and Start Hacking!](https://www.youtube.com/watch?v=etP1hgJXijw)
- [Burp for Beginners: A practical intro to help you find your first bug](https://www.youtube.com/watch?v=Ezs19sj04DU&list=PLbyncTkpno5FwsKpcaiXBvmG2r75RLGo3&index=4)
- [Finding Your First Bug: Finding Bugs Using APIs](https://www.youtube.com/watch?v=yCUQBc2rY9Y&list=PLbyncTkpno5HqX1h2MnV6Qt4wvTb8Mpol&index=2)
- https://umr4n6.github.io/post/Comms

- OWASP 
  - [OWASP top 10](https://owasp.org/www-project-top-ten/)

- CTFs 
  - [Hacker101](https://ctf.hacker101.com/)
  - [Damn Vulnerable Web Application](http://www.dvwa.co.uk/)
  - [Juice Shop](https://owasp.org/www-project-juice-shop/)

- Courses 
  - [Network Pentesting](https://www.youtube.com/watch?v=WnN6dbos5u8)
  - [PentesterLab](https://pentesterlab.com/)
  - [Web Security Academy](https://portswigger.net/web-security)

- Videos 
  - [STOK](https://www.youtube.com/@STOKfredrik)
  - [The Cyber Mentor](https://www.youtube.com/@TCMSecurityAcademy)

- Tools 
  - [Portswigger Knowledgebase](https://portswigger.net/burp/documentation/desktop/getting-started)
  - [Recon notes](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Methodology%20and%20Resources)

- Conferences 
  - [DEFCON](https://www.youtube.com/@DEFCONConference)
  - [BSides (I picked Manchester)](https://www.youtube.com/@bsidesmanchester5371)
  - [BlackHat]()

- Podcasts 
  - [The Bug Bounty Podcast](https://open.spotify.com/show/3yTTlfXH1avrI3FsXZyCpv)
  - [Darknet Diaries](https://darknetdiaries.com/ )
  - [Security Now](https://www.grc.com/securitynow.htm)
  - [Risky Business](https://risky.biz/)
  - [Planet Money - The Price Of A Hack](https://www.youtube.com/@BlackHatOfficialYT)

- Books 
  - [Web Hacking 101 (free from HackerOne)](https://www.hackerone.com/ethical-hacker/hack-learn-earn-free-e-book)
  - [OWASP Testing Guide](https://owasp.org/www-project-web-security-testing-guide/)
  - [Bug Bounty Cheat sheet Books](https://github.com/EdOverflow/bugbounty-cheatsheet/blob/master/cheatsheets/books.md)

- Write Ups & Disclosure 
  - [@disclosedh1](https://twitter.com/disclosedh1)
  - [HackerOne Hacktivity](https://hackerone.com/hacktivity)

- Aggregators/Newsletters 
  - [Pentester Land](https://pentester.land/)


**Reporting a Vulnerability?**

- [Writing Successful Bug Submissions](https://www.bugcrowd.com/blog/writing-successful-bug-submissions-bug-bounty-hunter-methodology/)
- [Writing a good and detailed vulnerability report](https://medium.com/@tolo7010/writing-a-good-and-detailed-vulnerability-report-bdb86cedcff)
- [What does a good report look like?](https://bugbountyguide.com/hunters/writing-reports.html)
