---
title: cewl
date: 2022-12-09T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

```zsh
cewl -w list.txt -d 5 -m 5 [URL]
```

**Explanation:**

- -w will write the contents to a file. In this case, list.txt.
- -m 5 gathers strings (words) that are 5 characters or more
- -d 5 is the depth level of web crawling/spidering (default 2)