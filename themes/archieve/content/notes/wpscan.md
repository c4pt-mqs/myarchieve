---
title: wpscan
date: 2022-10-29T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Temel kullanım:

```zsh
wpscan --url [URL]
```

Eklentiler ile ilgili bilgi toplanması:

```zsh
wpscan --url [URL] --enumerate p
```

Temanın zafiyet konrolünün yapılması:

```zsh
wpscan --url [URL] --enumerate vt
```

Kullanıcılar ile ilgili bilgi toplanması:

```zsh
wpscan --url [URL] --enumerate u
```

Verilen dosya ile bruteforce atağı gerçekleştirme:

```zsh
wpscan --url [URL] --wordlist wordlist.txt
```
