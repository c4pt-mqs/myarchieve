---
title: openssl
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Encrypt a file using a symmetric key:

```zsh
openssl enc -aes-256-cbc -salt -in message.txt -out encrypted_message
```

Decrypt a symmetrically encrypted file:

```zsh
openssl enc -aes-256-cbc -d -in encrypted_message -out original_message.txt
```

Encrypt a file using a password-derived key:

```zsh
openssl enc -aes-256-cbc -pbkdf2 -iter 10000 -in message.txt -out encrypted_message
```

Generate a random key for symmetric encryption:

```zsh
openssl rand -base64 32
```

Generate a private key:

```zsh
openssl genpkey -algorithm RSA -out private_key.pem
```

Generate a certificate signing request (CSR):

```zsh
openssl req -new -key private_key.pem -out csr.pem
```

Generate a self-signed certificate:

```zsh
openssl req -new -x509 -key private_key.pem -out certificate.pem
```

Convert a certificate from PEM to DER format:

```zsh
openssl x509 -outform der -in certificate.pem -out certificate.der
```

To generate a 2048-bit RSA key pair:

```zsh
openssl genrsa -out private_key.pem 2048
```

To extract the public key from an RSA private key:

```zsh
openssl rsa -in private_key.pem -pubout -out public_key.pem
```

**SSL Certificates:**

It may contain the sensitive information about the target company.
We can find it on the key icon in the URL bar in the most web browsers.

Detect TLS Version: We can also enumerate TLS version as below:

```zsh
openssl s_client -connect [URL]:443 -tls1
openssl s_client -connect [URL]:443 -tls1_1
openssl s_client -connect [URL]:443 -tls1_2
openssl s_client -connect [URL]:443 -tls1_3
```