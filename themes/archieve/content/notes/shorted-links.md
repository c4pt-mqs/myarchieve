---
title: Shorted Links
date: 2023-02-11T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: OSINT
tags:
  - osint
  - reconnaissance
---

**Add "+" to Short Links then see more information:**

Service    | Verification method  | Example
-----------|----------------------|---------
3.ly | add ~p at the end for link<br>add ~s at the end for statistics | https://3.ly/aeQVV~p<br>https://3.ly/aeQVV~s
amzn.to | add + at the end | https://amzn.to/theweeknd+
bbc.in | add + at the end | https://bbc.in/devicehelp+
bhpho.to | add + at the end | https://bhpho.to/quickrelease+
binged.it | add + at the end | https://binged.it/2heDXUE+
bit.do | add - at the end | https://bit.do/yeetyeet-
bit.ly | add + at the end | https://bitly.com/3qEgtMT+
bloom.bg | add + at the end | https://bloom.bg/instagram+
buff.ly | add + at the end | https://buff.ly/1irhfHu+
chilp.it | add p. before chilp.it for link<br>add s. before chilp.it for statistics | http://p.chilp.it/3a1fde<br>http://s.chilp.it/3a1fde
cli.re | add + at the end | https://cli.re/support+
cnet.co | add + at the end | https://cnet.co/2IorgTi+
cutt.ly | add @ at the end | https://cutt.ly/gsuite@
da.gd | add /coshorten/ between da.gd and short code | https://da.gd/coshorten/g
free-url-shortener.rb.gy | add + at the end | https://rb.gy/i8kxzd+
goo.gl (deprecated) | add ?d=1 at the end | https://goo.gl/l6MS?d=1
han.gl | add + at the end | https://han.gl/MpMYe+
huff.to | add + at the end | https://huff.to/1sNMnn4+
is.gd | add - at the end | https://is.gd/dKGELA-
j.mp | add + at the end | https://j.mp/3d-help+
kurzelinks.de | add + at the end | https://kurzelinks.de/y9t9+
lat.ms | add + at the end | https://lat.ms/ds86+
linkd.pl | add nothing | https://linkd.pl/pduzr
lmy.de | add + at the end | https://lmy.de/icBPw+
lnk.to | add ?autoredirect=false at the end | https://lnk.to/infernovideo?autoredirect=false
lnnk.in | add + at the end | https://lnnk.in/@LnnkinExplainerVideo+
nyr.kr | add + at the end | https://nyr.kr/1DaF5MM+
nyti.ms | add + at the end | https://nyti.ms/3e0fvnL+
oe.cd | add + at the end | https://oe.cd/4iZ+
on.fb.me | add + at the end | https://on.fb.me/iPQ2FQ+
on.mtv.com | add + at the end | https://on.mtv.com/3iNznyz+
politi.co | add + at the end | https://politi.co/3GB7fI7+
rotf.lol | add + at the end | https://rotf.lol/silabus-sd+
s.id | add + at the end | https://s.id/1u5Xl+
shorturl.gg | add nothing | https://shorturl.gg/mfGmhopm
soo.gd | add ! at the end | https://soo.gd/r76DV!
split.to | add /preview at the end | https://split.to/nhHBzwv/preview
tcrn.ch | add + at the end | https://tcrn.ch/3wJ8IpO+
t1p.de | add + at the end | https://t1p.de/9xn8+
t.ly | add + at the end | https://t.ly/iXVY+
tiny.cc | add = at the end to view the link<br>add ~ for statistics | https://tiny.cc/t1a1tz=<br>https://tiny.cc/t1a1tz~
tiny.one | add + at the end | https://tiny.one/kitcovid+
tiny.pl | add ! at the end | https://tiny.pl/9jkhm!<br>(or  https://tiny.pl/co.php?d=9jkhm)
tinyurl.com | add "preview." before "tinyurl.com" | https://preview.tinyurl.com/y53y8oq6
usat.ly | add + at the end | https://usat.ly/2p00LPb+
utn.pl | add + at the end | https://utn.pl/80wUQ+
ux.nu | add nothing | https://ux.nu/HJo7C
vo.la | add + at the end | https://vo.la/zdHf0+
wapo.st | add + at the end | https://wapo.st/3u6ZWCr+
yhoo.it | add + at the end | https://yhoo.it/2nLr2NW+







