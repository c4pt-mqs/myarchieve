---
title: Russian Forums
date: 2022-08-08T09:00:00Z
draft: false
type: note
author: Kaptan
category: Links
subcategory: Useful Links
tags:
  - forums
  - resources
  - communities
---

- https://gsmforum.ru/
- https://ucrack.com/
- https://codeby.net/
- https://forum.ucoz.ru/
- https://sirus.su/#/
- https://regforum.ru/
- http://www.xf-russia.ru/
- https://lolz.guru/
- https://forum.namalsk-rp.ru/
- https://xss.is/
- https://xss.as/
- https://breached.vc/