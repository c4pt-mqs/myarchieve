---
title: Temeller
date: 2023-02-12T09:00:00Z
draft: false
type: note
author: Kaptan
category: Bug Bounty
subcategory: Basics
tags:
  - bug-bounty
  - security
  - hacking
---

BugBounty programı; yazılım geliştiricileri ve kuruluşlar tarafından paylaşılan web sitelerin, Bughunter'lar tarafından güvenlik açıklarıyla ilgili hataları bildirdikleri ve bunun için ödenek alabilecekleri bir anlaşmadır.

**1.a** Tüm bugların; alanında Profesyonel BugHunter'lar tarafından bulunduğu düşünebilirsin ancak 1 yıl boyunca sende devam edersen ve pes etmezsen bir BugHunter olabilirsin. Hangi seviyede olduğun önemli değil sadece bir yerden başlayıp devam edebilmen önemli.

**1.b** Konfor Alanından çıkmalı ve zamanını ciddi bir iş üzerinde olduğunu kavramalısın.

**Başarı için bir örnek:**

1. Yıl: $0/ay
2. Yıl: $100/ay
3. Yıl: $200/ay
4. Yıl: $2,500/ay
5. Yıl: $6,000/ay

{{< quote author="" >}}
"Sürecinizde yavaş ilerlediğinizi düşünüyorsanız endişelenmeyin ve ilerlemenizin doğrusal olmasını beklemeyin. Bazı günler bıkıcak ve çalışmak istemeyeceksiniz. Ancak devam ettikçe yeni şeylerin farkına varacaksınız."
{{< /quote >}}

**2.a** Bir şeyi öğrenirken ya da incelerken kitaplardan yararlanın.

**2.b** VulnHub/PentesterLab'lar çözersen Pratik kazanırsınız.

**3.a** Bazı BugHunter'ları ve Hacker'ları Sosyal platformlarda takip etmeniz, sizi motive edicek ve kafanızda belirli bir yol haritası çizmenize yardımcı olucaktır.

**3.b** Güncel olayları ve yeni açıkları takip edin.

{{< quote author="" >}}
"Tüm yapmak istediklerini 1 günde yapmaya çalışmayın, uzun sürede bile olsa istikrarı sürdürmek daha faydalı olucaktır. Sabırlı olun ve gerçekçi hedeflerle gününüzü sürdürün."
{{< /quote >}}

**4.a** Her neyi öğreniyorsan; Önce mantığını anlayıp-kavrayın. Daha sonra pratik ile uygulamaya koyun.

**Example:**

1. Sign up for Hackerone to get Petes book Webhacking 101 https://www.hackerone.com
2. Watch anything you can from Jason Haddix just google it.
3. Watch all the tutorials and do the CTF on Hacker101 https://www.hacker101.com/
4. Sign up for Pentersterlab and try their stuff out! https://pentesterlab.com/pro
5. Watch everything on https://www.bugcrowd.com/university
https://github.com/bugcrowd/bugcrowd_university
6. Sign up for Hackerone (bit.ly/hackerone-stok) Bugcrowd or any other BB platform.
7. Get a Burp pro license, its way better than getting a "ethical hacker course" https://portswigger.net/
https://owasp.org/www-project-web-security-testing-guide/
8. Find a program that you like and vibe with, its more fun to hack on a program or brand you like. 
9. Don't waste time on VDP's
10. Don't be discouraged that everyone else has automated everything, its just not true.
11. Always approach a target like you're the first one there. Your view is unique.
12. Remember, Zero days can be new bugs in old code. Tavis has shown that over and over again.
13. Be proud of your work, you did this!