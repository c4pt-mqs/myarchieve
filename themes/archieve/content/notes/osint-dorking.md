---
title: Dorking
date: 2023-05-03T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: OSINT
tags:
  - osint
  - search
  - reconnaissance
---

**Diagram - Roadmap:**

- https://github.com/Proviesec/google-dorks
- https://github.com/humblelad/Shodan-Dorks
- https://dorks.faisalahmed.me/

**Shortcuts:**

View source: (CMD + OPT + U)
Search: (CMD + F)

**Search Term	Explanation	More information:**

Search Term | Explanation | More information
------------|-------------|--------------------
\<!-- | Comments | See above
@ | email addresses | Pivoting from an Email address
ca-pub | Google Publisher ID | Google's Description
ua- | Google AdSense ID | Bellingcat Tutorial
.jpg | Also try other image file extensions | Likely to reveal more directory structure


The `*` mentor<br>
`"`asdas`"` `OR/AND` deneme `site:`example.com `filetype:`pdf<br>
`intext:`password `inurl:`deneme `intitle:`haber `-`www `-`mentor

'www' ve 'mentor' sonuçlarını göstermeden metinlerin içinde 'password', linklerin içinde 'deneme', başlıklarda 'haber' ara.

**İstenilen tarihten öncesi ve sonrası için:**

`before:`10-27-1996 `after:`10-25-1996 

**Web Sites:**

- site:s3.amazonaws.com "example.com"
- site:blob.core.windows.net "example.com"
- site:googleapis.com "example.com"
- site:drive.google.com "example.com"
- site:*/joomla/login
- site:http://jsfiddle.net "target[.]com"
- site:http://codebeautify.org "target[.]com"
- site:http://codepen.io "target[.]com"
- site:http://pastebin.com "target[.]com"
- site:target.com filetype:php
- site:target.com inurl:.php?id=
- site:target.com inurl:.php?user= 
- site:target.com inurl:.php?book=
- site:target.com inurl:login.php
- site:target.com intext:"login"
- site:target.com inurl:portal.php
- site:target.com inurl:register.php
- site:target.com intext:"index of /"

**Find Parameters:**

- inurl:q= | inurl:s= | inurl:search= | inurl:query= inurl:& site:example.com
- inurl:url= | inurl:return= | inurl:next= | inurl:redir= inurl:http site:example.com
- inurl:/wp-admin/admin-ajax.php
- inurl:example.com intitle:"index of"
- inurl:example.com intitle:"index of /" "*key.pem"
- inurl:example.com ext:log
- inurl:example.com intitle:"index of" ext:sql|xls|xml|json|csv
- inurl:example.com "MYSQL_ROOT_PASSWORD:" ext:env OR ext:yml -git
- intext:"Powered by" & intext:Drupal & inurl:user
- "submit vulnerability report" | "powered by bugcrowd" | "powered by hackerone"
- site:.eu responsible disclosure
- inurl:index.php?id=
- site:.nl bug bounty
- "index of" inurl:wp-content/ (Identify Wordpress Website)
- inurl:"q=user/password" (for finding drupal cms )

**Google Docs:**

{{< figure src="/img/notes/cyber-security/osint/dorking-1.png" alt="google dorking" loading="lazy" width="70%" height="auto" >}}

{{< figure src="/img/notes/cyber-security/osint/dorking-2.png" alt="google dorking" loading="lazy" width="70%" height="auto" >}}

- site:docs.google.com inurl:/spreadsheets/d/
- site:drive.google.com inurl:/file/d/
- site:docs.google.com inurl:/document/d/
- site:docs.google.com inurl:/document/d/ inurl:edit
- site:docs.google.com inurl:/spreadsheets/d/ inurl:edit

