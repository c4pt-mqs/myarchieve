---
title: gpg
date: 2023-05-12T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

**Create a key:**

```zsh
gpg --gen-key
```

**To export a public key:**

```zsh
gpg --armor --export name@example.com > public_key.asc
```

**Export a public key into file public.key:**

```zsh
gpg --export -a "User Name" > public.key
```

**Create a file called 'public.key' with the public key for User Name:**

```zsh
gpg --export -a "User Name"
```

**Export a private key:**

```zsh
gpg --export-secret-key -a "User Name" > private.key
```

**Import a public key:**

```zsh
gpg --import public.key/.asc
```

**Decrypt the file:**

```zsh
gpg --decrypt file.pgp
```

**Decrypt a GPG-encrypted file:**

```zsh
gpg --output original_message.txt --decrypt message.txt.gpg
```

**Import a private key (not work everywhere):**

```zsh
gpg --allow-secret-key-import --import private.key
```

**Delete a public key (from your public key ring):**

```zsh
gpg --delete-key "User Name"
```

`NOTE!` If there is a private key on your private key ring associated with this public key, you will get an error! You must delete your private key for this key pair from your private key ring first.

**Delete an private key (a key on your private key ring):**

```zsh
gpg --delete-secret-key "User Name"
```

**List the keys in your public key ring:**

```zsh
gpg --list-keys
```

**List the keys in your secret key ring:**

```zsh
gpg --list-secret-keys
```

**To generate a new key pair:**

```zsh
gpg --full-generate-key
```

**Sign a file using your private key:**

```zsh
gpg --sign message.txt
```

**Verify a digital signature of a GPG-signed file:**

```zsh
gpg --verify message.txt.gpg
```

**Generate a short list of numbers that you can use via an alternative method to verify a public key:**<br>
(This creates the file fingerprint with your fingerprint info.)

```zsh
gpg --fingerprint > fingerprint
```

**Encrypt data:**

```zsh
gpg -e -u "Sender User Name" -r "Receiver User Name" somefile
```

**Encrypt a file using a recipient's public key:**

```zsh
gpg --encrypt --recipient recipient@example.com message.txt
```

**Decrypt data:**

```zsh
gpg -d mydata.tar.gpg
gpg --edit-key
gpg --gen-revoke
```

- **--gen-revoke** creates a revocation certificate, which when distributed to people and keyservers tells them that your key is no longer valid, see http://www.gnupg.org/gph/en/manual/r721.html

- **--edit-key** allows you do do an assortment of key tasks, see http://www.gnupg.org/gph/en/manual/r899.html

+ **Use Case \*.1:** Mentioned above were the commands for exporting and importing secret keys, and I want to explain one reason of why maybe you'd want to do this. Basically if you want one key-pair for all of your computers (assuming you have multiple computers), then this allows you export that key-pair from the original computer and import it to your other computers.

+ **Use Case \*.2:** Mentioned above were the commands for exporting and importing secret keys, and I want to explain one reason of why maybe you'd want to do this. Basically, if you belonged to a group, and wanted to create a single key-pair for that group, one person would create the key-pair, then export the public and private keys, give them to the other members of the group, and they would all import that key-pair. Then a member of the group or someone outside could use the group public key, encrypt the message and/or data, and send it to members of the group, and all of them would be able to access the message and/or data. Basically you could create a simplified system where only one public key was needed to send encrypted stuffs to muliple recipients.

**Resources:**

- https://www.tutorialspoint.com/unix_commands/gpg.htm
- http://irtfweb.ifa.hawaii.edu/~lockhart/gpg/