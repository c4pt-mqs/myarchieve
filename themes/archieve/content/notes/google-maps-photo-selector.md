---
title: Google Maps Photo Selector
date: 2022-12-20T09:00:00Z
draft: false
type: note
author: Kaptan
category: Programming
subcategory: Projects
tags:
  - javascript
  - automation
---

1. İlk arama işlemi en aşağıya inene kadar devam eder.

```js
var loop = window.setInterval(() => {
    document.querySelector('div.m6QErb.DxyBCb.kA9KIf.dS8AEf').scrollBy(0, 500)
},90);

var dakika = 10;
window.setTimeout(() => {
    clearInterval(loop);

    console.log('Döngü durdu!');

    var imgLinks = [];
    var elements = document.querySelectorAll('div.Uf0tqf.loaded');

    elements.forEach((s) => {
        if(s.style.backgroundImage !== 'none' && s.style.backgroundImage) imgLinks.push(s.style.backgroundImage.replace('url("', '').replace('")', ''))
    });

    console.log(imgLinks);
    copy(imgLinks);
}, dakika * 1000 * 60);
```

2. Kopyalamak için ekrana yazdırır.

```js
var loop = window.setInterval(() => {
  document.querySelector('div.m6QErb.DxyBCb.kA9KIf.dS8AEf').scrollBy(0, 500)
},90);

var dakika = 0.01;
window.setTimeout(() => {
  clearInterval(loop);

  console.log('Döngü durdu!');

  var imgLinks = [];
  var elements = document.querySelectorAll('div.Uf0tqf.loaded');

  elements.forEach((s) => {
      if(s.style.backgroundImage !== 'none' && s.style.backgroundImage) imgLinks.push(s.style.backgroundImage.replace('url("', '').replace('")', ''))
  });

  console.log(imgLinks);
}, dakika);
```

3. Console ekranında çıkan listeye sağ tıkla => copy object

**Dip Not:**

```js
$$('div.Uf0tqf.loaded').map(function(img) { ;
return img.style["backgroundImage"].split("\"") [1]; })

// or

$$('div.Uf0tqf.loaded').map(function(img) { ;
return img.style["background-image"].split("\"") [1]; })
```

Info: Class'a ait bilgileri öğrenme:

```js
$$('.class_name')
```

**Resources:**

- https://pyimagesearch.com/2014/12/01/complete-guide-building-image-search-engine-python-opencv/
- https://stackoverflow.com/questions/52969381/how-can-i-capture-all-network-requests-and-full-response-data-when-loading-a-pag
- https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Intercept_HTTP_requests
