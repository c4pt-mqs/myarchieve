---
title: Cyber News
date: 2022-12-11T09:00:00Z
draft: false
type: note
author: Kaptan
category: Links
subcategory: Shared Lists
tags:
  - cyber
  - news
---

- IT Security Guru
- Security Weekly
- The Hacker News
- Infosecurity Magazine
- CSO Online
- The State of Security - Tripwire
- The Last Watchdog
- Naked Security
- Graham Cluley
- Cyber Magazine
- WeLiveSecurity
- Dark Reading
- Threatpost
- Krebs on Security
- Help Net Security
- HackRead
- SearchSecurity
- TechWorm
- GBHackers On Security
- The CyberWire
- Cyber Defense Magazine
- Hacker Combat
- Cybers Guards
- Cybersecurity Insiders
- IT Security Guru
- Information Security Buzz
- The Security Ledger
- Security Gladiators
- Infosec Land
- Cyber Security Review
- Comodo News
- Internet Storm Center | SANS
- Daniel Miessler
- TaoSecurity
- Reddit
- All InfoSec News
- CVE Trends
- Securibee
- Twitter

**Here are my top-5 choices:**

1. Reddit — Information is posted quickly.
2. All InfoSec News — Aggregation of websites.
3. CVE Trends — Trending CVE's on social media.
4. Securibee — Latest bug-bounty hunting news.
5. The Hacker News — Quality content with articles.
