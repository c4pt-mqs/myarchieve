---
title: Discussing Forums
date: 2023-02-05T09:00:00Z
draft: false
type: note
author: Kaptan
category: Links
subcategory: Useful Links
tags:
  - forums
  - resources
  - technology
---

- https://stackexchange.com/sites
- https://bbs.archlinux.org/
- https://www.cozumpark.com/community/