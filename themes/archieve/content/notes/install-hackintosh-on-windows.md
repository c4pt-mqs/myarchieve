---
title: How to Install Hackintosh on Windows?
date: 2023-06-09T09:00:00Z
draft: false
type: note
author: Kaptan
category: Help
subcategory: Support
tags:
  - macos
  - system
---

### Important!

1. Check if the computer is compatible with both macOS Ventura and Windows 11
2. Backup important files and data


**BIOS Settings:**

- VT-D: Disabled
- VT-X: Disabled
- SATA: AHCI
- EHCI Hand-off / xHCI Hand-off: Enabled
- Legacy USB: Disabled
- CSM: Disabled
- CFG-Lock: Disabled
- Secure Boot: Disabled !important
- UEFI: Enabled


**Resources:**
- https://www.youtube.com/watch?v=9GJ4V69YS0o
- https://support.apple.com/en-us/HT201372

