---
title: OSINT Resources
date: 2023-02-10T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: OSINT
tags:
  - osint
  - search
  - reconnaissance
---

**Web Siteleri:**

- https://www.osintcombine.com/blog
- https://www.secjuice.com/tag/osint/
- https://sector035.nl/articles/category:week-in-osint
- https://osintcurio.us
- https://inteltechniques.com/blog/
- https://inteltechniques.com/tools/
- https://www.bellingcat.com/category/resources/
- https://www.opensecrets.org/
- https://gb.coursera.org/learn/advanced-writing
- https://virtualvacation.us/
- https://pentestbook.six2dez.com/others/web-checklist
- https://osint.link/
- https://start.me/p/ZME8nR/osint
- https://start.me/p/L1rEYQ/osint4all (AIO OSINT)
- https://www.gifgit.com/image/swirl-image
- https://dfircheatsheet.github.io/
- https://pimeyes.com/en (AI Face Search Engine)
- https://www.osinttechniques.com/osint-tools.html (web based AIO OSINT)
- https://www.spiderfoot.net/ (AIO OSINT)
- https://github.com/Datalux/Osintgram (INSTAGRAM OSINT)
- https://github.com/sundowndev/phoneinfoga (PHONE NUMBER OSINT)
- https://www.maltego.com/ (WE ALL KNOW THIS ONE)
- https://www.osintcombine.com/osint-bookmarks (OSINT Bookmark)
- https://epieos.com/ (email osint)
- https://seintpl.github.io/NAMINT/ ---> Advance Dorks Automated
- https://www.verif.tools/ ---> Create Fake Documents
- https://www.recruitin.net  ---> Linkedin Dorks Automated
- https://lyzem.com/ ---> Telegram Search Engine
- https://gmail.inputekno.com/ ----> Generate UNLIMITED Gmail  Accounts
- https://lookup-id.com/ ---->  Lookup-id Facebook
- https://www.sowsearch.info/ --->  Facebook Search (Allows you to search on facebook for posts,people,photos,etc using some filters)
- https://www.hashatit.com/ ---> Hashtags Search Engine (with sources)
- https://threatcops.com ---> New Gen OSINT Framework
- https://atlas.mindmup.com/digintel/digit...index.html ---> OSINT Framework
- https://barometer.agorapulse.com/home --->  Facebook Page Analytics tool
- https://irumble.com/firsttweet/ ---> First Tweet
- https://www.thexifer.net/ ---> Modify META Data 
- https://dorksearch.com ---> Advance Dorking Automated
- https://github.com/CScorza/CORPINT-Corpo...ntelligence ---> Corporate Intelligence
- https://github.com/keraattin/EmailAnalyzer ---> Email Analyzer
- https://base-search.net/ --->  Bielefeld Academic Search Engine ( Search across 311 million 481 thousands documents (most of them with free access). Search by email, domain, first/last) name, part of address or keywords.
- https://github.com/CScorza/GEOINTInvestigation ---> GEOINT 
- https://futurepedia.io/ --->  Future Pedia
- https://annas-archive.org/search ---> Document Search Engine
- https://isearchfrom.com/ ---> Google Search from a different location & device
- https://historicaerials.com/viewer ---> Aerial Photographs for Investigations GEOINT
- https://github.com/matiash26/Steam-OSINT-TOOL ---> STEAM OSINT
- https://wikinearby.toolforge.org ----> Wiki Nearby (Set Coordinates and Find Wikipedia articles)
- https://www.melissa.com/v2/lookups/

**Where to Research:**

- https://www.google.com/advanced_search
- https://support.google.com/websearch/answer/2466433
- https://help.duckduckgo.com/duckduckgo-help-pages/results/syntax/
- https://support.microsoft.com/tr-tr/topic/geli%C5%9Fmi%C5%9F-arama-se%C3%A7enekleri-b92e25f1-0085-4271-bdf9-14aaea720930 
- Google Hacking-DB (GHDB)
- https://tineye.com
- https://pimeyes.com/
- https://viewdns.info/
- https://threatintelligenceplatform.com/
- https://search.censys.io/
- https://archive.org/web/
- https://www.shodan.io/
- https://osintframework.com/
- https://biznar.com/biznar/desktop/en/search.html
- https://yippy.com/
- https://base-search.net/
- https://builtwith.com/
- https://urlscan.io/
- https://whatismyipaddress.com/
- https://assetnote.io/
- https://securityheaders.com/
- https://whois.domaintools.com/
- https://mxtoolbox.com/DNSLookup.aspx
- EXIF Viewer: https://exif-viewer.com
- GPS Coordinates: https://gps-coordinates.net
- WeakestLink:  https://github.com/shellfarmer/WeakestLink
- Oh365 User Finder:  https://github.com/dievus/Oh365UserFinder
- SpyOnWeb: https://spyonweb.com
- msDNSScan:  https://github.com/dievus/msdnsscan
- msDorkDump:  https://github.com/dievus/msdorkdump
- geeMailUserFinder:  https://github.com/dievus/geeMailUserFinder
- BuiltWith: https://builtwith.com
- CentralOps: https://centralops.net
- DNSlytics: https://dnslytics.com
- VirusTotal: https://virustotal.com
- VisualPing: https://visualping.io
- ViewDNS: https://viewdns.info
- crt.sh: https://crt.sh
- PentestTools: https://pentest-tools.com
- Shodan: https://shodan.io
- Wayback Machine: https://web.archive.org
- OpenCorporates: https://opencorporates.com
- PimEyes: https://pimeyes.com
- GeoGuessr: https://geoguessr.com
- IntelligenceX:  https://intelx.io/tools

**Kitaplar:**

- https://verificationhandbook.com/book_tr/
- https://verificationhandbook.com/book2_tr/
- https://teyit.org/files/dezenformasyon-el-kitabi.pdf

**Archived or Deleted Content:**

- https://archive.org/web/
- https://archive.is/
- http://www.cachedpages.com/
- http://cachedview.com/
- http://timetravel.mementoweb.org/

**Leaked data:**

- https://defastra.com/
- https://dehashed.com/
- https://epieos.com/
- https://haveibeenpwned.com/

**Online Virus/Malwares Scans:**

- Nodistribute:  https://nodistribute.com/
- Virustotal:  https://www.virustotal.com/gui/home
- Cuckoo: https://cuckoo.cert.ee/
- Hybrid-Analysis:  https://www.hybrid-analysis.com/
- Any-Run: https://app.any.run
- Reverse.it: https://reverse.it
- Anti-Virus Evasion Tool:  https://github.com/govolution/avet
- DefenderCheck:  https://github.com/matterpreter/DefenderCheck
- ThreatCheck:  https://github.com/rasta-mouse/ThreatCheck
- MetaDefender: https://metadefender.opswat.com/
- ScamAdviser: https://www.scamadviser.com/
- SHAttered: https://shattered.io/
- Triage: https://tria.ge/
- Analyze Intezer: https://analyze.intezer.com/

**IP Address:**

- https://www.infobyip.com/
- https://www.ipfingerprints.com/
- https://www.ipvoid.com/
- https://www.liveipmap.com/
- https://github.com/ninoseki/mitaka
- https://www.shodan.io/
- https://thatsthem.com/reverse-ip-lookup
- https://metrics.torproject.org/exonerator.html

**Web Siteleri:**

- PeopleFinder.com
- who.is
- builtwith.com
- fofa.so
- zoomeye.org
- viz.greynoise.io
- leakix.net
- censys.io
- onyphe.io
- app.binaryedge.io
- hunter.io
- wigle.net
- ghostproject.fr
- ivre.rocks
- natlas.io
- truecaller.com
- mSpy app

**Personel:**
 
- https://haveibeenpwned.com/  # username-password is pwned?
- https://whatsmyname.app/  # find the person - search everywhere
- https://webmii.com/  # find user
- https://peekyou.com/  # search user
- https://www.infobel.com/fr/world  # company research
- https://www.radarbox. com/  # uçan uçakların listesi
- https://labs.tib.eu/geoestimation/  # geolocation ile resmi tarar
- https://academo.org/demos/spectrum-analyzer/ # spektrum analyzer
- https://demo.phoneinfoga.crvx.fr/#/  # phone number
- https://hashes.com/en/tools/hash_identifier  # hash-identifier
- https://en.wikipedia.org/wiki/List_of_file_signatures  # file signatures
- https://ui.ctsearch.entrust.com/ui/ctsearchui  # sub-domain
- https://crt.sh/  # sub-domain
- https://neatnik.net/steganographr/  # steg private message
- https://www.diffchecker.com/  #  compare texts
- https://mobsf.live/  #  file analyzer
- https://www.fontspace.com/unicode/analyzer  #  Text Analyzer
- https://epieos.com/  #  user info
- https://www.osintessentials.com/
- https://dnsdumpster.com/
- https://www.shodan.io/
- https://leakix.net/

**Tor Browser Search Engines:**

	1. Ahmia
http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion [ahmia.fi]
	
	1. Torch
http://xmh57jrknzkhv6y3ls3ubitzfqnkrwxhopf5aygthi7d6rplyvk3noyd.onion/cgi-bin/omega/omega
	
	1. DuckDuckGo [ https://duckduckgo.com ]
http://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion 
	
	1. Tor Search
http://torchdeedp3i2jigzjdmfpn5ttjhthh5wbmda2rr3jvqjg5p77c54dqd.onion
	
	1. Trust Wiki
http://wiki6dtqpuvwtc5hopuj33eeavwa6sik7sy57cor35chkx5nrbmmolqd.onion
	
	1. Onion Index
http://oniondxjxs2mzjkbz7ldlflenh6huksestjsisc3usxht3wqgk6a62yd.onion/
	
	1. Kilos | Darknet Market Search Engine
http://mlyusr6htlxsyc7t2f4z53wdxh3win7q3qpxcrbam6jf3dmua7tnzuyd.onion
	
	1. Big Library
http://kx5thpx2olielkihfyo4jgjqfb7zx7wxr3sd4xzt26ochei4m6f7tayd.onion/

	1.  Leaked pass
http://leakfindrg5s2zcwwdmxlvz6oefz6hdwlkckh4eir4huqcpjsefxkead.onion/LeakedPass

	1.  Dark Fail
https://dark.fail/

	1.  Darksearch.io
http://darkschn4iw2hxvpv2vy2uoxwkvs2padb56t3h4wqztre6upoc5qwgid.onion

	1.  Haystack
http://haystak5njsmn2hqkewecpaxetahtwhsbsa64jom2k22z5afxhnpxfid.onion/
	
	1.  TOR66
http://tor66sewebgixwhcqfnp5inzp5x5uohhdy3kvtnyfxc2e5mxiuh34iid.onion/
	
	1.  OnionLand 
http://3bbad7fauom4d6sgppalyqddsqbf5u5p56b5k5uk2zxsy3d6ey2jobad.onion/
	
	1.  Phobos
http://phobosxilamwcg75xt22id7aywkzol6q6rfl2flipcqoc4e4ahima5id.onion/