---
title: gobuster
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Dosya uzantılarını ara:

```zsh
gobuster dir -u [URL] -x php,txt,old,bak,tar,zip -w [wordlist_path] -t 100
```

Hata kodlarını gösterme:

```zsh
gobuster dir -u [URL] -w [wordlist_path] -t 100 -b 403,401,404
```

Subdomain'leri arama:

```zsh
gobuster vhost -u [URL] -w [wordlist_path]
```