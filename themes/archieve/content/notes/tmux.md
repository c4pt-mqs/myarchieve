---
title: tmux
date: 2022-06-18T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

Komutlar     | Açıklaması                               
-------------|-------------------------------------------
Yeni bir tmux oturumu başlat | `tmux [isim]`
Belirli bir oturuma bağlanma | `tmux attach -t [numara]`
Prefix (tmux komutlarını başlatmak için) gönderme | `CMD + B + CMD + B`
Panolar arasında dolaşma | `CMD + B + C+o`
Geçerli istemciyi askıya alma | `CMD + B + C+z`
Sonraki düzeni seçme | `CMD + B + Space`
Penceleri yeni bir pencereye taşıma | `CMD + B + !`
Pencereyi dikey olarak böleme | `CMD + B + "`
Tüm panoları listeleme | `CMD + B + #`
Geçerli oturumu yeniden adlandırma | `CMD + B + $`
Pencereyi yatay olarak böleme | `CMD + B + %`
Geçerli pencereyi kapatma | `CMD + B + &`
Pencere indeksini seçme | `CMD + B + '`
Önceki istemciye geçme | `CMD + B + (`
Sonraki istemciye geçme | `CMD + B + )`
Geçerli pencereyi yeniden adlandırma | `CMD + B + ,`
En son kullanılan panoyu silme | `CMD + B + -`
Geçerli pencereyi taşıma | `CMD + B + .`
Tuş bağlamasını açıklama | `CMD + B + /`
Pencereyi değiştirme | `CMD + B + [numara]`
Komut girmek için açıklama verme | `CMD + B +`
Önceki aktif panele gitme | `CMD + B + ;`
Panolar arasından yapıştırma işlemi | `CMD + B + =`
Tuş bağlamalarını listeleme | `CMD + B + ?`
Tmux seçeneklerini özelleştirme | `CMD + B + C`
Listeden istemci seçme | `CMD + B + D`
Panoları eşit aralıklarla dağıtma | `CMD + B + E`
Son istemciye geçme | `CMD + B + L`
İşaretlenen paneli temizleme | `CMD + B + M`
Kopya moduna giriş | `CMD + B + [`
En son panoyu yapıştırma | `CMD + B + ]`
Yeni bir pencere oluşturma | `CMD + B + c`
Geçerli istemciyi ayırma | `CMD + B + d`
Pano içinde arama yapma | `CMD + B + f`
Pencere bilgilerini görüntüleme | `CMD + B + i`
Önceki pencereyi seçme | `CMD + B + l`
İşaretlenen paneli açma/kapatma | `CMD + B + m`
Sonraki pencereyi seçme | `CMD + B + n`
Sonraki panele geçme | `CMD + B + o`
Önceki pencereyi seçme | `CMD + B + p`
Panoların numaralarını görüntüleme | `CMD + B + q`
Geçerli istemciyi yeniden çizme | `CMD + B + r`
Oturumu seçme | `CMD + B + s`
Saat gösterme | `CMD + B + t`
Pencere seçme | `CMD + B + w`
Aktif paneli kapatma | `CMD + B + x`
Aktif paneli büyütme (zoom) | `CMD + B + z`
Aktif paneli üstteki pane ile değiştirme | `CMD + B + {`
Aktif paneli alttaki pane ile değiştirme | `CMD + B + }`
Mesajları görüntüleme | `CMD + B + ~`
Pencere içeriğini yeniden başlatma | `CMD + B + DC`
Kopya moduna girip yukarı kaydırma | `CMD + B + PPage`
Aktif panelin üstündeki panele geçme | `CMD + B + Up`
Aktif panelin altındaki panele geçme | `CMD + B + Down`
Aktif panelin solundaki panele geçme | `CMD + B + Left`
Aktif panelin sağındaki panele geçme | `CMD + B + Right`
Eşit yatay düzeni ayarlama | `CMD + B + M+1`
Eşit dikey düzeni ayarlama | `CMD + B + M+2`
Ana yatay düzeni ayarlama | `CMD + B + M+3`
Ana dikey düzeni ayarlama | `CMD + B + M+4`
Karoları seçme | `CMD + B + M+5`
Bildirimli sonraki pencereyi seçme | `CMD + B + M+n`
Panoları ters yönde dolaşma | `CMD + B + M+o`
Bildirimli önceki pencereyi seçme | `CMD + B + M+p`
Panele yukarı yönde boyut verme | `CMD + B + M+Up`
Panele aşağı yönde boyut verme | `CMD + B + M+Down`
Panele sola doğru boyut verme | `CMD + B + M+Left`
Panele sağa doğru boyut verme | `CMD + B + M+Right`
Panele yukarı yönde 5 birim boyut verme | `CMD + B + S+Up`
Panele aşağı yönde 5 birim boyut verme | `CMD + B + S+Down`
Panele sola doğru 5 birim boyut verme | `CMD + B + S+Left`
Panele sağa doğru 5 birim boyut verme | `CMD + B + S+Right`