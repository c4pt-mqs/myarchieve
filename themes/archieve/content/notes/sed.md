---
title: sed
date: 2022-11-27T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

### Regex


{{< tag "#74FB4C" "'" >}}\[condition(s)(optional)]{{< tag "#74FB4C" "[command/mode(optional)]" >}}/{{< tag "#EA3324" "[source/to-be-searched pattern(mandatory)]" >}}/{{< tag "#75FBFE" "[to-be-replaced pattern(depends on command/mode you use)]" >}}/{{< tag "#E933F6" "[args/flags to operate on the pattern searched(optional)]" >}}{{< tag "#74FB4C" "'" >}}

{{< figure src="/img/notes/cyber-security/commands/sed-1.png" alt="sed commands" loading="lazy" width="90%" height="auto" >}}

<br>

```zsh
sed 's/\(^\b[[:alpha:] ]*\)\([[:digit:]]*\)/\=\> \1\[\2\]/g' file.txt
```

**Examples:**

```zsh
sed -e '1,3 s/john/JOHN/g' file.txt
```

Viewing a range of Lines:

```zsh
sed -n '3,5p' file.txt
```

Viewing the entire file except a given range:

```zsh
sed '3,5d' file.txt
```

Viewing multiple ranges of lines inside a file:

```zsh
sed -n -e '1,2p' -e '4,5p' file.txt
```

To start searching from nth pattern occurrence in a line you can use combination of /g with /1,/2,/3.

```zsh
sed 's/youtube/YOUTUBE/1' file.txt
```

İstenilen satırı aramak ve görüntülemek için:

```zsh
sed -n '[line_number]p' [filename]
```

Kelimenin başına yazı eklemek veya değiştirmek için:

```zsh
sed 's/^/[TEXT]:/g' [filename] > [output_filename]
```

**Satırları silmek:**

Belirli bir aralığı silmek için:

```zsh
sed '[start_line_number],[end_line_number]d' [filename]
```

Belirli bir deseni içeren satırları silmek için:

```zsh
sed '/[pattern]/d' [filename]
```

***Aşağıda bulunan ilk komut belirtilen satırı ve sonrasındaki tüm satırları silerken, ikinci komut belirtilen satırı korur ve sonrasındaki tüm satırları siler.***

Belirli bir satırdan sonraki tüm satırları silmek için:

```zsh
sed '[start_line_number],$d' [filename]
```

Belirli bir satırdan başlayarak istediğiniz tüm satırları silmek için:

```zsh
sed '[start_line_number],$!d' [filename]
```

Flags        | Description                               
-------------|-------------------------------------------
`-e` | To add a script/command that needs to be executed with the pattern/script(on searching for pattern)
`-f` | Specify the file containing string pattern
`-E` | Use extended regular expressions 
`-n` | Suppress the automatic printing or pattern spacing

Command      | Description                               
-------------|-------------------------------------------
`s` | (Most used)Substitute mode (find and replace mode)
`y` | Works same as substitution; the only difference is, it works on individual bytes in the string provided(this mode takes no arguments/conditions)

Arguments      | Description                               
-------------|-------------------------------------------
`/g` | globally(any pattern change will be affected globally, i.e. throughout the text; generally works with s mode) 
`/i` | To make the pattern search case-insensitive(can be combined with other flags)
`/d` | To delete the pattern found(Deletes the whole line; takes no parameter like conditions/modes/to-be-replaced string)
`/p` | prints the matching pattern(a duplicate will occur in output if not suppressed with -n flag.)
`/1,/2,/3../n` | To perform an operation on an nth occurrence in a line(works with s mode)


### Resources

- https://www.baeldung.com/linux/remove-last-n-lines-of-file
- https://unix.stackexchange.com/questions/323948/sed-explanation-sed-d-file
- [Split 15 GB Text File in Windows. Split Large Text File](https://www.youtube.com/watch?v=GAzo-Zk_nSo)
- https://www.geeksforgeeks.org/sed-command-in-linux-unix-with-examples/
- https://www.cyberciti.biz/faq/how-to-use-sed-to-find-and-replace-text-in-files-in-linux-unix-shell/
- https://www.tecmint.com/linux-sed-command-tips-tricks/
- [Other](https://www.oakton.edu/user/2/rjtaylor/cis218/Handouts/sed.txt#:~:text=The%20sed%20command%20can%20add,after%20a%20match%20is%20found.&text=The%20sed%20command%20can%20add%20a%20new%20line%20before%20a,before%20a%20match%20is%20found.)