---
title: Stegnography
date: 2022-10-17T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: CTFs
tags:
  - steganography
  - forensics
  - security
  - tools
---

### File Extraxt

```zsh
stegcracker [filename] [wordlist]
steghide extract -sf [filename]
stegseek [filename] [wordlist] # daha hızlı
docker run --rm -it -v "$(pwd):/steg" rickdejager/stegseek [filename] [wordlist]
```

```zsh
binwalk --dd='.*' [filename]
binwalk -e [filename]
```

```zsh
exiftool [filename]
stegoveritas [filename]
stegoveritas -steghide [filename]
zsteg [filename]
```

Tool Name   | Description
------------|-------------------------
[Convert](http://www.imagemagick.org/script/convert.php) | Convert images b/w formats and apply filters
[Exif](http://manpages.ubuntu.com/manpages/trusty/man1/exif.1.html) | Shows EXIF information in JPEG files
[Exiftool](https://linux.die.net/man/1/exiftool) | Read and write meta information in files
[Exiv2](http://www.exiv2.org/manpage.html) | Image metadata manipulation tool
[ImageMagick](http://www.imagemagick.org/script/index.php) | Tool for manipulating images
[Outguess](https://www.freebsd.org/cgi/man.cgi?query=outguess+&apropos=0&sektion=0&manpath=FreeBSD+Ports+5.1-RELEASE&format=html) | Universal steganographic tool
[Pngtools](http://www.stillhq.com/pngtools/) | For various analysis related to PNGs
[SmartDeblur](https://github.com/Y-Vladimir/SmartDeblur) | Used to deblur and fix defocused images
[Steganabara](https://www.openhub.net/p/steganabara) | Tool for stegano analysis written in Java
[Stegbreak](https://linux.die.net/man/1/stegbreak) | Launches brute-force dictionary attacks on JPG image
[StegCracker](https://github.com/Paradoxis/StegCracker) | Steganography brute-force utility to uncover hidden data inside files
[Stegextract](https://github.com/evyatarmeged/stegextract) | Detect hidden files and text in images
[Steghide](http://steghide.sourceforge.net/) | Hide data in various kind of images
[Stegsolve](http://www.caesum.com/handbook/Stegsolve.jar) | Apply various steganography techniques to images
[Zsteg](https://github.com/zed-0xff/zsteg/) | PNG/BMP analysis
[LSB-Steganography](https://github.com/RobinDavid/LSB-Steganography) | Python program to steganography files into images using the Least Significant Bit.
[StegSpy](http://www.spy-hunter.com/stegspydownload.htm) | Checks classical steganographical schemes
[StegSnow](http://manpages.ubuntu.com/manpages/bionic/man1/stegsnow.1.html) | A program for concealing messages in text files by appending tabs and spaces on the end of lines
[Binwalk](https://github.com/ReFirmLabs/binwalk) | Firmware Analysis Tool
[Stego-Toolkit](https://github.com/DominicBreuker/stego-toolkit) | Steganography Toolkit
[StegDetect](http://old-releases.ubuntu.com/ubuntu/pool/universe/s/stegdetect/) | Performs statistical tests to find if a stego tool was used (jsteg, outguess, jphide, …).
[StegoVeritas](https://github.com/bannsec/stegoVeritas) | Yet another Stego Tool


**Steganography image analyzer:**

- https://stylesuxx.github.io/steganography/
- https://www.aperisolve.com/

**Checklist:**

- https://www.aperisolve.com/cheatsheet
- https://stegonline.georgeom.net/checklist
- https://stegonline.georgeom.net/
- https://github.com/eugenekolo/sec-tools/tree/master/stego/stegsolve