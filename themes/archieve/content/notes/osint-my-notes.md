---
title: My Notes
date: 2023-02-04T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: OSINT
tags:
  - osint
  - search
  - reconnaissance
---

**All the Staff:**

- https://start.me/p/DPYPMz/the-ultimate-osint-collection
- https://start.me/p/rx6Qj8/nixintel-s-osint-resource-list
- https://start.me/p/BnrMKd/01-ncso
- https://github.com/jivoi/awesome-osint
- https://www.osintdojo.com/resources/
	
- **Identify what questions need to be answered:**
(add, remove, or modify these questions)
  - Who is behind this account?
  - What is their name?
  - What country are they in?
  - What is their approximate age?
  - Are they on any other platforms?

- **Determine end goals:**
  - What is the expected outcome of the investigation?
  - Will it result in a written report, notifying the authorities, or something else?
  - Identify what platforms may need to be accessed
  - Assess the technical capabilities of the targets

- **Locating and collecting information for investigations:**
  - You don't know what you don't know…
  - Do not let your sock puppet accounts and your real accounts interact in any way.
  - Collect First, Analyze Later: Save it and then move forward.
  - Broaden your search and try again.
  - Setting up filters or alerts on platforms analyst to focus being notified.
  - Why do you think this other name or username is the same as the target you are briefing on, they don't seem related at all?

- Search, read, or understand the collected information.
- Reduce the time amount of raw information by filtering.
- Organize the collected data by one by.
- Starts looking like a bad guy. Think like the person who made it.
- Provide the best and most complete information possible.
- Short summary should contain all the important details of the product.
- When releasing to a audience, redacte or remove the sensitive details.
- Analysts may duplicate mistakes or fail to measure up to the standards. Return back and look what's wrong.


  - Collect employee full names, job roles, as well as the software they use.
  - Review and monitor search engine information from Google, Bing, Yahoo, and others.
  - Monitoring personal and corporate blogs, as well as review user activity on digital forums.
  - Identify all social networks used by the target user or company.
  - Review content available on social networks like Facebook, Twitter, Google Plus, or Linkedin.
  - Access old cached data from Google – often reveals interesting information.
  - Identify mobile phone numbers, as well as mail addresses from social networks, or Google results.
  - Search for photographs and videos on common social photo sharing sites, such as Flickr, Google Photos, etc.
  - Use Google Maps and other open satellite imagery sources to retrieve images of users' geographic location.
  - Never forget about the all-powerful OSINT Framework which can be a great source of inspiration for any OSINT investigation.
