---
title: SSH
date: 2023-08-23T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---


| Flag | Description | Example
|------|-------------|-----------
| -i   | Use this flag when you want to access a remote server using a private key for authentication. | `ssh -i private_key.pem user@hostname`
| -L   | Specifies local port forwarding. The format is 'local_port:remote_address:remote_port'. This allows traffic on a local port to be forwarded to a remote server. | `ssh -L 8080:localhost:80 user@hostname`
| -R   | Specifies remote port forwarding. The format is 'port:local_address:local_port'. This forwards traffic from a remote port to a local address and port. | `ssh -R 8080:localhost:80 user@hostname`
| -D   | Enables dynamic port forwarding, creating a SOCKS proxy on localhost. The format is 'local_PORT'. This allows for secure proxying of traffic through SSH. | `ssh -D 1080 user@hostname`
| -N   | Use this flag to prevent execution of a remote command. It is useful when only forwarding ports without executing additional commands on the remote server. | `ssh -N -L 8080:localhost:80 user@hostname`

### File Transfer with SSH

**Transfer to Remote Server:**

```zsh
scp -i id_rsa [file_name] [username]@[IP]:/.
```

**Transfer from Remote Server to Local Machine:**

```zsh
scp -r [username]@[IP]:{'file_1, file_2'} .
```