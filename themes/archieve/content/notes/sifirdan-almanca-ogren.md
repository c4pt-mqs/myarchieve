---
title: Sıfırdan Almanca Öğren
date: 2022-03-12T09:00:00Z
draft: false
type: note
author: Anonim
category: Mine
subcategory: Languages
tags:
  - german
  - language
  - education
  - resources
---

Bir sene önce sıfır Almancam var iken, şuânda da B1 sertifika sınavını geçmiş biri olarak, bu başlıkta Almanca öğrenme konusunda faydalandığım kaynakları paylaşacağım.

Şunu da belirteyim geçen sene haziran gibi sadece ja ve nein'ı biliyordum. Belki bu seviyemi ifade edebilmek için daha net olmuştur. 


## Gramer

İlk olarak şunu belirteyim hâlen pandemiden dolayı neredeyse çoğu eğitim online verildiğinden, ben de çoğu insan gibi 'Goethe Institut' kurumundan online A1 ve B1 kurları aldım. Ne öğrendin diye sorarsanız; söyleyeyim hiçbir şey. Konu anlatılmaz; sadece soru sorulur, metin çalışılır ve bir sürü ödev verilir. 

**Sonuç:** hiçbir şey anlamaz daha yolun başında Almanca'dan koşarak uzaklaşmak istersiniz. Dolayısıyla ben bu kurslardan hiçbir fayda görmedim.

Gramerimi Youtube'da 'Almanca Kolay' hesabındaki A1-B1 videoları ile ve 'Lingoni German' videoları ile hallettim.

- [Easy German](https://www.youtube.com/@AlmancaKolay)
- [lingoni GERMAN](https://www.youtube.com/@lingoniGERMAN)

Ayrıca 'Begi Begi' adlı bir kanal var ve onun da çok faydasını gördüm: 

- [German with Begi Begi](https://www.youtube.com/@BeGiBeGi)

Bir defter alıp, videolarda gördüğüm her şeyi ayrıntısına kadar -sanki dersteymişim gibi- yazdım. Sonrasında alıştırmalara geçtim.

## Alıştırma (A1'den B1'e kadar)

1) '[Deutsche Welle'nın - Nicos Weg A1](https://learngerman.dw.com/tr/nicos-weg/c-54846011)' bölümü bitirilmelisiniz. Alıştırmalarda geçen kalıpları ayrı bir defterinize geçirmek sizin için çok iyi olacaktır.

2) '[Deutsche Welle - Warum Nicht?](https://learngerman.dw.com/en/deutsch-warum-nicht-1-episoden-1-17/a-16375396)' podcastleri'ni tekrar tekrar dinleyin. Üç bölümden oluşuyor ve B1'e kadar gidiyor. 

### Alıştırma Kitapları:

- Grammatik Aktiv (A1-B1)
- Hueber - Übungsgrammatik Für Die Grundstufe (A1-B1)
- Hueber - Übungsbuch Grammatik (A2-B2) 

**Präpositionen:**

Präpositionen'ler Almanca'da çok önemli ve Almanca öğrenmeye yeniden baştan başlamış olsam çoğu kelimeyi Präpositionen'i ile öğrenirdim. Bunun için de önerebileceğim kitap şu: [Hueber - Deutsch Üben-Präpositionen.](https://www.google.com/search?q=hueber+prapositionen&tbm=isch)

### Kelime

Kelimeleri artikeliyle kesinlikle ve ayrıca üstte de belirttiğim gibi Präpositionen'i ile ezberlemeniz gerekiyor. Kelimeleri cümle içinde ezberlerseniz sizin için daha akılda kalıcı olur. 

Goethe İnstitut'un A1 ve B1 kelime listesini ezberlemeye çalışın, çok faydasını görürsünüz:
- [goethe a1-b1 wortliste](https://www.google.com/search?q=goethe+a1-b1+wortliste&tbm=isch&ved=2ahUKEwj9z6fFz_2DAxWuk_0HHQptAecQ2-cCegQIABAA&oq=goethe+a1-b1+wortliste&gs_lcp=CgNpbWcQAzoHCAAQgAQQGFCCBViCBWDEBmgAcAB4AIABpQGIAaYCkgEDMC4ymAEAoAEBqgELZ3dzLXdpei1pbWfAAQE&sclient=img&ei=_v20Zf3YM66n9u8PitqFuA4&bih=619&biw=955)

Bunların dışında 'Nicos Weg' alıştırmalarında geçen kelimeleri de deftere yazmanızı, ara ara tekrar etmenizi tavsiye ederim. 

### Dinleme

Bütün yeni dil öğrenenlere önerilen o klişeyi size önermeyeceğim yani çizgi film izleyin demeyeceğim oldum olası çizgi film sevmedim ve açıkçası oradaki sıkıcı diyalogların da çok faydası olacağını sanmıyorum. 
Benim önerebileceklerim: Easy German A1-B2 videoları, Hallo Deutschschule, Nicos Weg A1-A2-B1 videoları ve bunları sayısız kez izleyebilirsiniz. 

Yani sıkıcı değiller, sadece bir kere izleyip bırakmayın. Ama sertifika sınavına gireceksiniz muhakkak Hören egzersizi yapmanız gerekiyor dolayısıyla onun için; [hören übungb1 hörverstehen](https://www.youtube.com/@horendeutsch)

### Yazma

A2'nin sonlarına doğru günlük tutmak bence en faydalısı ancak ben bunu düzenli gerçekleştiremedim. Lakin çok faydalı olduğunu söyleyebilirim.

### Konuşma

Bu kısım için önerebileceğim tek şey: [Tandem](https://www.tandem.net/). Gerçekten ben aşırı faydasını gördüm. Mesela Türkçe öğrenmek isteyen birine bir gün siz türkçe öğretirsiniz diğer gün Alman partneriniz size Almanca öğretir. Konuşmanın çok önemli olduğunu düşünüyorum. 

### Okuma

Deutsche Welle'daki A1-B1 okuma alıştırmalarını tavsiye ederim hatta çoğunda hören kısmı da olduğundan iki kısım için de çok faydalı oluyor. sonrasında oradaki bölüm alıştırmalarını çözebilirsiniz.

Hikaye kitabı bol bol okuyun mesela, bu koleksiyon daha çok a2'den itibaren ama kendinizi zorlayın inanın her okuduğunuzda daha iyi kavrayacaksınız. [daf-lernkrimi](https://www.google.com/search?q=daf+lernkrimi+a1/a2&sca_esv=601995954&tbm=isch&source=lnms&sa=X&ved=2ahUKEwiNjIy90P2DAxUP8bsIHSHlArAQ_AUoAnoECAIQBA&biw=1475&bih=963&dpr=2)

Çeşitli seviyelere uygun diyalog videoları içeren faydalı bulduğum youtube sitesi: [deutsch with armin](https://www.youtube.com/@deutschmitarmin7294)

- [Geolino Redewendungen](https://www.geo.de/geolino/redewendungen/)
- [Nachrichtenleicht](https://www.nachrichtenleicht.de/)

## Diğer Kaynaklar

### 1. Almanca Sözlükler:

- [dict.cc](https://www.dict.cc/) (kelimeler ve yaygın ifadeler için, telaffuz dahil)
- [Langenscheidt](https://de.langenscheidt.com/deutsch-englisch/) (bireysel kelimeler için, yaygın olanlar örneklerle birlikte)
- [PONS](https://de.pons.com/) (kelimeler ve yaygın ifadeler için, yaygın olanlar örneklerle birlikte)
- [Linguee](https://www.linguee.de/) (kelimeler ve yaygın ifadeler için, yaygın olanlar örneklerle birlikte)
- [Wiktionary](https://en.wiktionary.org/) (kelimenin kökeni, sıfat çekimleri, fiil çekimleri, telaffuz ve türetilmiş terimler için)
- [Reverso Context](https://context.reverso.net/translation/) (bağlam içinde kelimeler için)
- [DWDS](https://www.dwds.de/) (bağlam içinde kelimeler için)
- [Duden](https://www.duden.de/) (tüm Almanca sözlük)
- [Forvo](https://forvo.com/) (telaffuz sözlüğü)

### 2. Çevirmenler:

- [DeepL](https://www.deepl.com/translator) (Almanca'dan İngilizce ve tersi için en iyi çevirmen)
- [Google Translate](https://translate.google.com/)
- [Beluka](https://beluka.de/)

### 3. Almanca Kursları:

- [DW Learn German](https://learngerman.dw.com/en/overview) (tam başlangıç ​​seviyesi için, A0-B1 seviyeleri için)
- [Language Transfer - German](https://www.languagetransfer.org/german) (tam başlangıç ​​seviyesi için, A0 seviyesi için)
- [Duolingo](https://www.duolingo.com/) (başlangıç ​​seviyesi kelime bilgisi için, özellikle hikaye özelliği önerilir, A0-A2 seviyeleri için)
- [Busuu](https://www.busuu.com/) (gramer ve kelime bilgisi içerir, ancak birçok içerik ücretli, A0-B2 seviyeleri için)
- [COERLL - Deutsch im Blick](https://coerll.utexas.edu/dib/) (ücretsiz Almanca çevrimiçi ders kitabı, A0-B2 seviyeleri için)
- [Coffee Break German](https://open.spotify.com/show/3QiX0ZkjILyGWmpemackWB?si=_KSHRu8gRwCkx44hDd90_w) (sadece sesli kurs, A0-B2 seviyeleri için)
- [Pimsleur](https://www.pimsleur.com/) (profesyonel ücretli dil kursları, A0-B1 seviyeleri için)
- [Michel Thomas](https://www.michelthomas.com/) (profesyonel ücretli dil kursları, A0-B1 seviyeleri için)
- [YouTube - 3 Saatlik Başlangıç ​​Videoları](https://www.youtube.com/watch?v=6Ka_3Rq8JZ4) (tam başlangıç ​​seviyesi için A1 konularını kapsayan 3 saatlik video)
- [Lingoli GERMAN](https://www.youtube.com/watch?v=gz9JbZcfnrk&list=PL5QyCnFPRx0GxaFjdAVkx7K9TfEklY4sg) (A1'den B2'ye kadar videoların bulunduğu çalma listeleri, kelime bilgisi, gramer ve telaffuz içerir)

### 4. Almanca Gramer:

- [German.net](https://german.net/) (çevrimiçi gramer alıştırmaları ve metinler, A1-B1 seviyeleri için)
- [Schubert Verlag](https://www.schubert-verlag.de/aufgaben/index.htm) (çevrimiçi gramer alıştırmaları, A1-C1 seviyeleri için)
- [Dartmouth Deutsch Grammatik](https://www.dartmouth.edu/~deutsch/Grammatik/Grammatik.html) (çevrimiçi gramer açıklamaları, A1-B1 seviyeleri için)
- [Lingolia](https://deutsch.lingolia.com/en/) (çevrimiçi gramer açıklamaları ve kelime listeleri, A1-B1 seviyeleri için)
- [Deutsch.info](https://deutsch.info/de/grammar) (çevrimiçi gramer açıklamaları, A0-A1 seviyeleri için)
- [Nthuleen](http://www.nthuleen.com/teach/grammar.html) (çevrimiçi gramer ve kelime çalışma sayfaları, A1-B2 seviyeleri için)
- [Deutschkurse Passau](http://www.deutschkurse-passau.de/JM/index.php?option=com_content&view=article&id=34&Itemid=165) (cevap anahtarı olmadan ücretsiz gramer PDF'leri, cevap anahtarları için kitapların fiziksel kopyasını satın almanız gerekiyor, A1-C1 seviyeleri için)
- [Grammatik Deutsch](https://www.grammatikdeutsch.de/html/grundschule-deutschunterricht.html) (çevrimiçi gramer açıklamaları ve alıştırmalar, sadece Almanca, A1-B2 seviyeleri için)

### 5. Almanca Giriş:

- [LingQ](https://www.lingq.com/) (çevrimiçi çeviri ve flaş kart özelliği ile metinler ve videolar, A1-C1 seviyeleri için)
- [Slow German](https://slowgerman.com/) (yavaş konuşulan Almanca podcast'leri, B1-C1 seviyeleri için)
- [German.net - Reading](https://german.net/reading/) (Almanca metinler, A1-B2 seviyeleri için)
- [Lingua - German Reading](https://lingua.com/german/reading/) (Almanca metinler, İngilizce çevirisi ile, A1-B2 seviyeleri için)
- [Tagesschau](https://www.tagesschau.de/) (Almanca haberler, hem okuma hem dinleme için, B1-C2 seviyeleri için)
- [Language Learning with Netflix](https://languagelearningwithnetflix.com/) (Netflix'te hangi Alman dizileri/filmlerinin ülkenizde mevcut olduğunu gösterir)
- [Netflix Dil Öğrenme Eklentisi](https://chrome.google.com/webstore/detail/language-learning-with-ne/hoombieeljmmljlkjmnheibnpciblicm?hl=en) (Netflix ile dil öğrenme için Chrome eklentisi)
- [german-content](https://www.youtube.com/playlist?list=PLGQBKSq2HITf3Px13eDGBgmJEeXfWa3wC) (Alman YouTuber'ların günlük yüklemeleri, A2-C2 seviyeleri için)

**Almanca Başlangıç Düzeyi İçin Diziler:**

- [Extr@ auf Deutsch (A1 – B1)](https://www.youtube.com/playlist?list=PLGQBKSq2HITf3Px13eDGBgmJEeXfWa3wC)
- [Nicos Weg (A1 – B1)](https://www.youtube.com/playlist?list=PLs7zUO7VPyJ5DV1iBRgSw2uDl832n0bLg)
- [bs.to](https://bs.to/)

**Hazırlayan:** *Ananoim*
