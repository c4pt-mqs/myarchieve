---
title: sort
date: 2022-12-13T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: Commands
tags:
  - commands
  - tools
---

sort command, as the name suggests sorts the lines alphabetically and numerically, automatically. All you got to do is pipe the stdin into sort command.

**Important Flags for sort:**

Flags                | Description                               
---------------------|-------------------------------------------
`-r` | Sorts in reverse order
`-c` | This flag is used to check whether the file is already sorted or not(If not, it will list, where the disorder started)
`-u` | To sort and removes duplicate lines(does work same as stdin redirected into uniq)
`-o save.txt` | To save into a output file
