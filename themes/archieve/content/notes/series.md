---
title: Series
date: 2022-11-14T09:00:00Z
draft: false
type: note
author: Kaptan
category: Self Improvement
subcategory: Things to Watch
tags:
  - series
  - self-improvement
---

## Biography/Drama

- [x] Mr. Robot (2015-2019)
- [ ] The Crown
- [ ] Narcos
- [ ] The Queen's Gambit
- [ ] Spartacus
- [ ] Band Of Brothers
- [ ] American Crime Story
- [ ] Genius

## Sci-Fi/Thriller/Action/Adventure:

- [ ] Archive 81
- [x] Black Mirror
- [ ] Nova: Cyberwar Threat
- [ ] Stranger Things
- [ ] Person of Interest
- [ ] Westworld
- [ ] The Expanse
- [x] Altered Carbon
- [x] Dark
- [ ] Lost
- [ ] The Mandalorian
- [ ] Fringe
- [ ] Orphan Black
- [ ] Battlestar Galactica
- [ ] Game of Thrones
- [ ] Peaky Blinders
- [x] The Witcher
- [x] Arrow
- [ ] The 100
- [ ] The Umbrella Academy
- [x] Daredevil
- [ ] The Boys
- [ ] Into the Badlands

## Crime/Drama

- [ ] Breaking Bad
- [ ] The Sopranos
- [ ] Peaky Blinders
- [ ] Ozark
- [x] True Detective
- [ ] Sherlock
- [ ] Mindhunter
- [ ] Fargo
- [ ] Boardwalk Empire
- [ ] Better Call Saul
- [ ] The Wire

## Mystery/Thriller

- [ ] Mindhunter
- [ ] Luther
- [ ] The Haunting of Hill House
- [ ] Broadchurch
- [ ] Alice in Borderland
- [ ] Twin Peaks
- [x] Merlin
- [ ] The Night Of
- [ ] The Killing
- [ ] Big Little Lies

## Comedy

- [ ] The Office (US)
- [x] Friends
- [x] Brooklyn Nine-Nine
- [x] How I Met Your Mother
- [ ] Parks and Recreation
- [ ] The Marvelous Mrs. Maisel
- [ ] The Big Bang Theory
- [ ] Schitt's Creek
- [ ] Modern Family
- [ ] It's Always Sunny in Philadelphia
- [ ] Veep

## Animation

- [x] Avatar: The Last Airbender
- [x] Ben 10
- [ ] Rick and Morty
- [ ] The Amazing World of Gumball
- [ ] SpongeBob SquarePants
- [ ] Archer
- [ ] The Simpsons
- [x] Adventure Time
- [ ] Family Guy
- [ ] Futurama
- [ ] South Park

## History

- [ ] Rome
- [ ] The Crown
- [x] Vikings
- [ ] Band of Brothers
- [ ] Spartacus
- [ ] The Last Kingdom
- [ ] Boardwalk Empire
- [x] Chernobyl
- [ ] Versailles
- [ ] Marco Polo