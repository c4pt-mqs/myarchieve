---
title: Personel Info
date: 2023-02-10T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: OSINT
tags:
  - osint
  - search
  - reconnaissance
---

## Email

**Diagram (Roadmap):**

https://www.osintdojo.com/diagrams/email

**Email Permutator:**

http://metricsparrow.com/toolkit/email-permutator/

**Email Dossier:**

https://centralops.net/co/emaildossier.aspx

**Blacklists:**

https://mxtoolbox.com/blacklists.aspx

**Reverse mail:**

https://thatsthem.com/reverse-email-lookup

- https://hunter.io  # mail
- https://phonebook.cz  # mail
- https://clearbit connect  # mail-eklenti
- https://tools.verifyemailaddress.io  # mail-verify(is it)
- https://email-checker.net/validate  # email-checker
- https://google.account.com  # more information for email
- https://leakcheck.io  # mail-checker
- https://tools.emailhippo.com/
- https://namechk.com/
- https://whatsmyname.app/
- https://epieos.com/

## Number

**Phone Number OSINT:**

https://medium.com/@SundownDEV/phone-number-scanning-osint-recon-tool-6ad8f0cac27b

