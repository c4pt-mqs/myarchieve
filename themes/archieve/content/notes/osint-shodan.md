---
title: Shodan
date: 2022-10-03T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: OSINT
tags:
  - osint
  - search
  - reconnaissance
---

Searching is shodan is not as easy as we search in Google, It's quite tricky if you need to get sepecific results as it is designed to be used by Tech persons. So here are few shodan dorks and example on how to use them

**city:**

Find devices in a particular city:
`city:"Kathmandu"`

**country:**

Find devices in a particular country:
`country:"NP"`

**geo:**

Find devices by giving geographical coordinates:
`geo:"56.913055,118.250862"`

**hostname:**

Find devices matching the hostname:
`server: "gws" hostname:"google"`

**net:**

Find devices based on CIDR or IP:
`net:144.157.80.0/29`

**os:**

Find devices based on operating system:
`os:"windows XP"`

**port:**

Find devices based on open ports:
`proftpd port:21`

**before/after:**

Find devices before or after between a date/time:
`apache after:22/02/2012 before:14/3/2021`

**Citrix:**

Find Citrix Gateway:
`title:"citrix gateway"`

**Wifi Passwords:**

Find cleartext wifi password in Shodan:
`html:"def_wirelesspassword"`

Searching using these dorks will only get ipto 2 page search results which is only 20 results. So the free membership is not useful.
