---
title: Cyber Movies
date: 2022-11-14T09:00:00Z
draft: false
type: note
author: Kaptan
category: Links
subcategory: Shared Lists
tags:
  - cyber
  - movies
---

- [ ] [We Are Legion: The Story of the Hacktivists](https://www.youtube.com/watch?v=ZHl0WI32XkY)
- [ ] [21st Century Hackers | Full Hacking Documentary 2021](https://www.youtube.com/watch?v=nsKIADw7TEM)
- [ ] [Hackers Wanted - 2009 [Unreleased Director's Cut]](https://www.youtube.com/watch?v=Mn3ooBnShtY)
- [ ] [Hackers in Wonderland](https://www.youtube.com/watch?v=fe8GsPCpE7E)
- [ ] [The Internet's Own Boy: The Story of Aaron Swartz](https://www.youtube.com/watch?v=M85UvH0TRPc)
- [ ] [DEFCON: The Documentary](https://www.youtube.com/watch?v=3ctQOmjQyYg)
- [ ] [Hackers Are People Too](https://www.youtube.com/watch?v=7jciIsuEZWM)
- [ ] [The Secret History of Hacking](https://www.youtube.com/watch?v=PUf1d-GuK0Q)
- [ ] [Risk (2016)](https://www.youtube.com/watch?v=eWeRhpkEP1c)
- [ ] [Zero Days (2016)](https://www.imdb.com/title/tt5446858/)
- [ ] [Guardians of the New World (Hacking Documentary) | Real Stories](https://www.youtube.com/watch?v=jUFEeuWqFPE)
- [ ] [A Origem dos Hackers](https://www.youtube.com/watch?v=LPqXNGcwlxo&t=2s)
- [ ] [The Dilemma of Networks](https://www.netflix.com/tr-en/title/81254224)
- [x] [The Great Hack](https://www.netflix.com/tr-en/title/80117542)
- [ ] [Web Warriors](https://www.youtube.com/watch?v=0IY7DL0ihYI)
- [ ] [The Future of Cyber War](https://www.youtube.com/watch?v=L78r7YD-kNw)
- [ ] [Dark Web Fighting Cybercrime](https://www.youtube.com/watch?v=PjfX4CjSVGE)
- [ ] [Cyber Defense: Military Training for Cyber Warfare](https://www.youtube.com/watch?v=rcDizlmjNQY)
- [ ] [Hacker:HUNTER - Wannacry: The Marcus Hutchins Story](https://www.youtube.com/watch?v=vveLaA-z3-o)
- [ ] [The Life Hacker Documentary](https://www.youtube.com/watch?v=TVgJmAJsOeQ)
- [ ] [Hacker Documentary 2002 - Hacking group The Realm and Electron](https://www.youtube.com/watch?v=GcnkEPTy3QI)
- [ ] [Chasing Edward Snowden](https://www.youtube.com/watch?v=8YkLS95qDjI)
- [ ] [The Hacker Wars](https://www.youtube.com/watch?v=ku9edEKvGuY)
- [ ] [Hackers World: Anonymous investigation](https://www.youtube.com/watch?v=1A3sQO_bQ_E)
- [ ] [In the Realm of the Hackers](https://www.youtube.com/watch?v=0UghlW1TsMA)
- [ ] [TPB AFK: The Pirate Bay Away From Keyboard](https://www.youtube.com/watch?v=eTOKXCEwo_8)
- [ ] [The Inside Life of a Hacker](https://www.youtube.com/watch?v=CuESlhKLhCY)
- [ ] [High Tech Hackers Documentary](https://www.youtube.com/watch?v=l2fB_O5Q6ck)
- [ ] [Drones, Hackers and Mercenaries - The Future of Warfare](https://www.youtube.com/watch?v=MZ60UDys_ZE)