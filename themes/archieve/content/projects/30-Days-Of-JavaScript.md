---
title: "30 Days of JavaScript Exercises"
date: 2023-03-22T00:00:00Z
description: 'I completed all of the exercises in the "30-Days-Of-JavaScript" repository by Asabeneh.rcises in the "30-Days-Of-JavaScript" repository by Asabeneh.'
skills: "HTML, CSS, JavaScript"
image: "/img/project/30DaysOfJavaScript.png"
type: "project"
author: Kaptan
tags:
  - javascript
  - programming
  - learning
  - frontend
---

{{< link-card title="30 Days of JavaScript Exercises" url="https://gitlab.com/c4pt-mqs/30-Days-of-JavaScript-Exercises" desc="These exercises are designed to learn and improve our JavaScript skills by Asabeneh." img="/img/project/30DaysOfJavaScript.png" >}}

Each day, we will be presented with a new challenge that will test our knowledge of JavaScript syntax, programming concepts, and problem-solving skills.

I completed all of the exercises in the [30-Days-Of-JavaScript](https://github.com/Asabeneh/30-Days-Of-JavaScript) repository. I had fun and learn a lot.

## Used Technologies


* Google
* Stack Overflow
* Discord Communities
* ChatGPT

Other online communities and forums helped me to improve my JavaScript coding skills.

I encourage you to use these resources to help you learn and solve problems, but please make sure that you understand the solutions you find and do not copy-paste code without understanding what it does.

## Credits

These exercises were created by Asabeneh Yetayeh, a full-stack web developer, instructor, and founder of Asabeneh. You can find more of his work and resources for learning JavaScript and other web development skills on his website and social media channels. I give my thanks to him.
