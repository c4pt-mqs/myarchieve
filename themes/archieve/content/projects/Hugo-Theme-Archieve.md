---
title: "Hugo Theme Archieve"
date: 2024-01-25T00:00:00Z
toc: true
description: 'Archieve is a minimal, responsive, and feature-rich Hugo theme designed for various types of writing.'
skills: "Python, Docker"
image: "/img/project/archieve.png"
type: "project"
author: Kaptan
tags:
  - hugo
  - static-site-generator
  - web-development
---

{{< link-card title="Hugo Theme Archieve" url="https://gitlab.com/c4pt-mqs/myarchieve" desc="Archieve is a minimal, responsive, and feature-rich Hugo theme designed for various types of writing." img="/img/project/archieve.png" >}}

<div align="center">
  <h3>
    <i>
      Archieve is a minimal, responsive, and feature-rich Hugo theme designed for various types of writing.
    </i>
  </h3>
</div>

<div align="center">
  <a href="https://github.com/gohugoio/hugo/releases/" target="_blank">
    <img src="https://img.shields.io/badge/Hugo-≥0.121.2-red?style=for-the-badge&logo=hugo&logoColor=white&color=5cc82c"/>
  </a>
  <a href="https://gitlab.com/c4pt-mqs/myarchieve/-/pipelines" target="_blank">
    <img src="https://img.shields.io/gitlab/pipeline-status/c4pt-mqs/myarchieve?style=for-the-badge&branch=main&logo=gitlab&logoColor=fca326"/>
  </a>
  <a href="https://gitlab.com/c4pt-mqs/myarchieve/-/commits/main" target="_blank">
    <img src="https://img.shields.io/gitlab/last-commit/c4pt-mqs/myarchieve?style=for-the-badge&branch=main"/>
  </a>
  <a href="https://myarchieve.net" target="_blank">
    <img src="https://img.shields.io/website?url=https://myarchieve.net&style=for-the-badge&branch=main"/>
  </a>
</div>
<div align="center">
  <a href="https://gitlab.com/c4pt-mqs/myarchieve/-/merge_requests" target="_blank">
  <img src="https://img.shields.io/gitlab/merge-requests/all/c4pt-mqs/myarchieve?style=for-the-badge&branch=main" alt="GitLab Merge Requests">
  </a>
  <a href="https://gitlab.com/c4pt-mqs/myarchieve/-/issues" target="_blank">
  <img src="https://img.shields.io/gitlab/issues/all/c4pt-mqs/myarchieve?style=for-the-badge&branch=main&color=blue" alt="GitLab Issues">
  </a>
  <a href="https://gitlab.com/c4pt-mqs/myarchieve/-/forks" target="_blank">
  <img src="https://img.shields.io/gitlab/forks/c4pt-mqs/myarchieve?style=for-the-badge&branch=main" alt="GitLab Forks">
  </a>
  <a href="https://gitlab.com/c4pt-mqs/myarchieve/-/starrers" target="_blank">
  <img src="https://img.shields.io/gitlab/stars/c4pt-mqs/myarchieve?style=for-the-badge&branch=main" alt="GitLab Stars">
  </a>
</div>
<div align="center">
  <a href="https://gitlab.com/c4pt-mqs/myarchieve/-/blob/main/LICENSE" target="_blank">
    <img src="https://img.shields.io/gitlab/license/c4pt-mqs/myarchieve?style=for-the-badge&color=white"/>
  </a>
</div>
<div align="center">
  <a href="https://myarchieve.net" target="_blank">
    <img src="https://img.shields.io/badge/Live%20Demo-7900f2?style=for-the-badge"/>
  </a>
</div>

{{< figure src="/img/content/examples/archieve_desktop.png" alt="Archieve Desktop" loading="lazy" width="90%" height="auto" >}}
{{< figure src="/img/content/examples/archieve_tablet.png" alt="Archieve Tablet" loading="lazy" width="55%" height="50%" >}}
{{< figure src="/img/content/examples/archieve_phone.png" alt="Archieve Phone" loading="lazy" width="35%" height="50%" >}}

## 🚩 Introduction

Archieve is a minimal, responsive, and feature-rich Hugo theme designed for various types of writing. This documentation provides a detailed overview of the theme's features, pages, and configuration options.

## 🔄 Installation

1. [Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

2. [Install Go](https://go.dev/)

3. [Install Hugo](https://github.com/gohugoio/hugo/releases/) (≥0.140.0)

### 🎯 Quick Start

1. **Clone the Archieve theme**

```zsh
git clone https://gitlab.com/c4pt-mqs/myarchieve.git
```

2. **Open config.yaml and add your relevant information based in comments.**

3. **Start the local development server:**

```zsh
rm -rf public/ resources/ && hugo mod clean && hugo mod tidy && hugo mod get -u ./... && hugo --gc --minify --cleanDestinationDir && hugo server -D
```

The development server will be available at `http://localhost:1313/`

### 🦊 Deploy to GitLab Pages

**Follow these steps to deploy your Hugo website on GitLab Pages using the provided configuration:**

1. Visit GitLab and create a new public repository named YOUR_USERNAME.gitlab.io, replacing YOUR_USERNAME with your GitHub username or organization name.

2. Ensure that your Hugo project is correctly configured and adheres to the expected structure.

3. Confirm that the `.gitlab-ci.yml` file is located in the root directory of your Hugo project.

4. In your GitLab repository, navigate to "Settings" > "Repository" > "Protected branches."

5. Locate the main branch and either uncheck the "Protect" option or adjust the settings to permit force pushing.

6. Push your changes, and GitLab CI/CD will automatically trigger the pipeline when modifications are made to the `main` branch.

```zsh
# initialize new git repository
git init

# commit and push code to master branch
git remote add origin https://github.com/YOUR_USERNAME/YOUR_PROJECT_NAME
git add .
git commit -m "Initial commit"
git branch -M main
git push -uf origin main
```

### 🚀 Deploy to GitHub Pages

**Follow these steps to deploy your Hugo website on GitHub Pages using the provided configuration:**

1. Go to [GitHub](https://github.com/) and [create a new public repository](https://github.com/new) named YOUR_USERNAME.github.io, replacing YOUR_USERNAME with your GitHub username.

2. Ensure that your Hugo project is correctly configured and adheres to the expected structure.

3. Confirm that the `.github/workflows/main.yml` file is in the root directory of your Hugo project.

4. In your GitHub repository, navigate to "Settings" > "Branches."

5. Ensure that the `main` branch is set as the default branch.

6. Push your changes, and GitHub Actions will automatically run the workflow when modifications are made to the main branch.

```zsh
# Initialize a new git repository
git init

# Commit and push code to the main branch
git remote add origin https://github.com/YOUR_USERNAME/YOUR_PROJECT_NAME
git add .
git commit -m "Initial commit"
git branch -M main
git push -uf origin main
```

### 📂 Deploy to Hosting with FTP

1. Use an FTP client to connect to your server.

2. Navigate to the local "public" folder (generated by Hugo) on your computer using the FTP client. Upload all files and directories from the "public" folder to the root directory of your FTP server.

3. Ensure that the permissions on your server are set correctly. Directories usually need to have read and execute permissions (755), and files need to have read permissions (644).

4. If you have a custom domain, update the DNS settings to point to your server's IP address.

### 🌩️ Deploy to Other Platforms

- https://gohugo.io/hosting-and-deployment/


## 📝 Configuration & Notes

**hugo.yaml**

The **hugo.yaml** file is a configuration file for Hugo, the static site generator. It allows you to specify various settings for your Hugo site, including parameters related to the site's metadata, language settings, and other global configurations. The exact contents and structure of this file can vary, but it typically contains settings that affect the overall behavior of your Hugo site.

**Priority of posts (weight:)**

The weight parameter in front matter allows you to set the priority or weight of a specific content piece, such as a post. Lower values indicate higher priority, affecting the order in which content is displayed on your site. For example, setting weight: 1 would prioritize the content over a piece with weight: 2.

**draft: false**

The draft parameter in front matter is used to mark content as a draft. Setting **draft: false** indicates that the content is ready for publication, while **draft: true** keeps it in draft mode and prevents it from being included in the published output.

**toc: true (table of contents)**

The toc parameter in front matter is used to control the visibility of a table of contents (TOC) for a particular piece of content. Setting **toc: true** enables the automatic generation and display of a table of contents for the content, providing readers with an organized way to navigate long articles or pages.

**author: (name of author)**

The author parameter in front matter allows you to specify the author of a particular piece of content. This information can be used to display authorship details on the published page, providing attribution.

**lastmod: 2024-01-10T09:00:00Z**

The lastmod parameter in front matter is used to indicate the last modification timestamp of a piece of content. It helps in tracking when the content was last updated, providing useful information to both content creators and readers. (If lastmod information is entered, it will appear at the bottom of the post.)

**data -> links.yml**

The data directory in Hugo is used to store external data files. In this case, links.yml suggests the presence of a YAML file named links within the data directory for the **Links** page. This file contain structured data within a list of links, that can be accessed and utilized in your Hugo templates.

**Notes**

This appears to be a reference to a specific note within the "notes" section of your Hugo site. The URL https://YOUR_WEBSITE_ADDRESS/notes/#example-note contains a specific anchor (#example-note) pointing to a particular note.

```yaml
# You can add/remove Categories and subcategories in hugo.yaml
categories:
  - weight: 1
    icon: fa-solid fa-note-sticky
    title: Quick Notes
    sublinks:
      - icon: fa-regular fa-clipboard
        color: "#9FF339"
        title: Notes
        subcategories:
          - title: Learning Goals
          - title: Tasks
          - title: Ideas
  - weight: 2
    icon: fa-brands fa-gg fa-lg
    title: Special
    sublinks:
      - icon: fa-brands fa-connectdevelop
        color: "#f7f"
        title: Self Improvement
        subcategories:
          - title: Books
          - title: Quotes
      - icon: fa-solid fa-fingerprint
        color: "#f96"
        title: Mine
        subcategories:
          - title: Career
          - title: Articles
```

### Searching

The theme includes a powerful search functionality that indexes different content types:

- **Posts**: Blog posts with title, summary, content, date, and author
- **Projects**: Project pages with similar metadata
- **Notes**: Note entries with additional category and subcategory support

The search index is automatically generated and includes:
```json
{
    "posts": [
        {
            "title": "Post Title",
            "permalink": "/post-url/",
            "summary": "Post summary",
            "content": "Full post content",
            "date": "January 2, 2024",
            "author": "Author Name",
            "type": "post"
        }
    ]
}
```

The search functionality supports all content types (posts, projects, and notes) with type-specific metadata and filtering.

### GitLab and GitHub Feedback button

These buttons likely refer to features that allow users to provide feedback on your content through GitLab and GitHub. The buttons may link to respective issue trackers or feedback forms, enabling users to report issues, suggest improvements, or engage in discussions related to the content.


## 📈 Performance tests

### ⚡️ PageSpeed Insights

### 🔥 GTmetrix

### 📶 Pingdom Tools


## 🔒 Privacy and Rights

- **No Rights Reserved**: This website operates under a "[No rights reserved](https://creativecommons.org/share-your-work/public-domain/cc0/)" policy, indicating that the content is released into the public domain for maximum freedom of use.

- **Powered by Hugo**: This website is generated using [Hugo](https://gohugo.io/), a static site generator written in Go.

- **Copyleft**: The philosophy of this website follows [copyleft](https://tr.wikipedia.org/wiki/Copyleft) principles, emphasizing the freedom to use, modify, and distribute the content.

- **Privacy Assurance**: This website does not track you. No cookies are used, and there is no data collection. There is no collection of statistics about your visit. Your privacy is respected.

- **No Vulnerability Issues**: This website is designed with static site generator, so there are no known vulnerability issues.


## 📜 License

The Archieve is distributed under the CC0 1.0 Universal (CC0 1.0) Public Domain Dedication, which allows you to copy, modify, distribute, and perform the work, even for commercial purposes, without asking for permission.

This license is designed to provide maximum freedom for your use of the theme while ensuring a dedication to the public domain. You can find the full text of the license in the LICENSE file included with the theme.

Feel free to customize, adapt, and use the Archieve according to your needs, and contribute back to the community if you make improvements or modifications. The theme is provided as-is, without any warranties or guarantees, and the authors are not liable for any issues or damages arising from its use.

If you have any questions or suggestions regarding the license or the theme itself, please reach out to us through the main repository or other provided contact channels. Your feedback is appreciated!

***

Feel free to reach out to us through the [Archieve repository](https://gitlab.com/c4pt-mqs/myarchieve) for any questions or feedback.

Happy writing!
