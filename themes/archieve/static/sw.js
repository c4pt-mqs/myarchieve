// sw.js

self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open('my-cache').then(function (cache) {
            return cache.addAll([
                '/img/icon.png',
                '/img/icon.webp',
                '/img/icon.avif'
            ]);
        })
    );
});

self.addEventListener("fetch", (event) => {
    const url = new URL(event.request.url);

    // Skip handling requests to the GitLab API
    if (url.origin === "https://gitlab.com") {
        return;
    }

    // Handle image requests with format negotiation
    if (event.request.destination === 'image') {
        event.respondWith(
            caches.match(event.request).then((response) => {
                return response || fetch(event.request).then(response => {
                    return caches.open('my-cache').then(cache => {
                        cache.put(event.request, response.clone());
                        return response;
                    });
                });
            })
        );
        return;
    }

    event.respondWith(
        caches.match(event.request).then((response) => {
            return response || fetch(event.request);
        })
    );
});
